#!/bin/bash
# The protocol file generated by protogen.py ver. 1.0 on 2013-10-09 10:17
# AUTHOR: Magda S aka Magic Magg magg dot gatech at gmail dot com
# DATE: 2013-10-09
# PURPOSE: 
# REQUIREMENTS:
VERSION="1.0"

# bad arguments
EBADARGS=65

if [ $# -eq 0 ]; then
	echo "Usage: . `basename $0` 1"
	echo "Description: Source the file to set variables for lynx build."
	echo "E.g.: . `basename $0` 1"
	exit $EBADARGS
fi

# set a bunch of environment variables
# be sure you have boost variables set on
export BOOST_INC_PATH=$BOOST_INCLUDE

# be sure that you have the correct cuda paths
export CUDA_BIN_PATH=$CUDA_HOME/bin
export CUDA_LIB_PATH=$CUDA_HOME/lib64
export CUDA_INC_PATH=$CUDA_HOME/include

# first this next that
export LYNX_INSTALL_DIR=/nics/d/home/smagg/opt/lynx-cw

# create env variables for cjit
export CJIT_LIB_PATH=$LYNX_INSTALL_DIR/lib
export CJIT_INC_PATH=$LYNX_INSTALL_DIR/include

# EOF

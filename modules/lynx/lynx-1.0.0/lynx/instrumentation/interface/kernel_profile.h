/**
 * @file kernel_profile.h
 * @author Naila Farooqui, naila@cc.gatech.edu
 * @date 2011-11-15
 * @brief API for kernel profile
 * @author Magdalena Slawinska magg dot gatech at gmail dot com (modifications)
 */
 
#ifndef _KERNEL_PROFILE_H
#define _KERNEL_PROFILE_H

#define MSG_QUEUE "/lynx"
#define MQ_MAX_MESSAGES 10 // EVPath is hardcoded to this value. - originally 8
#define MQ_MAX_MSG_SIZE (sizeof(kernel_profile))
#define MQ_CREATE_OWNER_FLAGS		(O_RDWR | O_CREAT | O_EXCL | O_NONBLOCK)
#define MQ_OPEN_OWNER_FLAGS		(O_RDWR | O_EXCL | O_NONBLOCK)

//! The name of the ini file storing the name of the message queue
//! the Lynx will use for outputing the monitoring events
//#define MQ_CW_INI "cw.ini"


#define MAX_KERNEL_NAME_SIZE    256 // EVPath is hardcoded to this value.


/*-------------------------------------- DEFINITIONS -------------------------*/

typedef enum { NONE, KERNEL_RUNTIME, MEM_EFFICIENCY, INST_COUNT, EXECUTION_COUNT, BRANCH_DIVERGENCE,
ACTIVITY_FACTOR } profile_type;

/**
 * This will group elements that are usefule to compute the activity
 * factor
 */
struct activity_factor{
	unsigned long activeThreads;
	unsigned long maxThreads;
	unsigned long warps;
};

/**
 * This will group raw and predefined metrics data related
 * to memory efficiency
 */
struct memory_efficiency {
	double me_metric;  // @see WarpInstrumentor.cpp:extractResults()
	unsigned long dynamicHalfWarps;
	unsigned long memTransactions;
};

/**
 * group of data related to the branch divergence
 */
struct branch_divergence {
	double bd_metric; // @see WarpInstrumentor.cpp:extractResults()
	unsigned long totalBranches;
	unsigned long divergentBranches;
};



typedef struct _kernel_profile {
    
	// Modifying the layout of this data structure (names, types, ordering)
	// requires modification of EVPath descriptors in kidron/dsimons code, file
	// ffs_common.h, structure gpu_monitoring_format_list

    int pid;
    int device;

    profile_type type;
    union {
        double kernel_runtime;
        struct memory_efficiency mem_efficiency;
        double instruction_count;
        struct branch_divergence branch_divergence;
        // warp data (counters in lynx/trace/implementation/Profiler.cpp
        // see lynx/instrumentation/implementation/WarpInstrumentor.cpp
        struct activity_factor activity_f;
        unsigned long barriers;
    } data;
    
    char name[MAX_KERNEL_NAME_SIZE]; // don't change macro unless you also want to change EVPath
    
} kernel_profile;

#endif /* _KERNEL_PROFILE_H */



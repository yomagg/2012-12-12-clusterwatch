/**
 * @file Message_Queue.h
 *
 * @date Oct 28, 2013
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef MESSAGE_QUEUE_H_
#define MESSAGE_QUEUE_H_

#include <mqueue.h>

#include <string>




namespace clusterwatch {
	/**
	 * The class representing the message queue that is supposed to be used
	 * by ClusterWatch
	 */
	class Message_Queue{
	private:
		//! indicates that the message queue is uninitialized
		const int MQ_UNINITIALIZED;


		//! the descriptor for the message queue
		//! -2 indicates that this is uninitialized
		mqd_t q_descr;
		//! The name of the queue
		std::string q_name;

	public:
		//! the default name for the clusterwatch ini file to store
		//! the name of the created queue
		const static std::string CW_INI_FILE;
		//! the name of the queue name field; it should be checked
		//! with the name in CW_INI_FILE
		const static std::string CW_Q_NAME;

		Message_Queue();
		~Message_Queue();
		int open(std::string a_q_name);
		mqd_t get_mqd();
		bool is_mq_open();

		static std::string get_mq_name(std::string ini_file = CW_INI_FILE);

	};

}
#endif /* MESSAGE_QUEUE_H_ */

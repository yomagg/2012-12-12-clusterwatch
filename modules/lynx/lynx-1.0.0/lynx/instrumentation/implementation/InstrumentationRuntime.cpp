/*! \file   InstrumentationRuntime.cpp
	\author Naila Farooqui <naila@cc.gatech.edu>
	\date   Tuesday November 22, 2011
	\brief  The source file for the InstrumentationRuntime class.
*/

#ifndef INSTRUMENTATION_RUNTIME_CPP_INCLUDED
#define INSTRUMENTATION_RUNTIME_CPP_INCLUDED

// Instrumentation includes
#include <lynx/instrumentation/interface/InstrumentationRuntime.h>
#include <lynx/api/interface/lynx.h>
#include <lynx/instrumentation/interface/kernel_profile.h>

#include <lynx/instrumentation/interface/BasicBlockInstrumentor.h>
#include <lynx/instrumentation/interface/ClockCycleCountInstrumentor.h>
#include <lynx/instrumentation/interface/WarpInstrumentor.h>

#include <lynx/instrumentation/interface/Message_Queue.h>


// Hydrazine includes
#include <hydrazine/interface/Exception.h>
#include <hydrazine/interface/debug.h>
#include <hydrazine/interface/json.h>

#include <errno.h>

#include <sstream>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>

#ifdef REPORT_BASE
#undef REPORT_BASE
#endif

//#define REPORT_BASE 0
#define REPORT_BASE 1

namespace instrumentation
{
    InstrumentationRuntime InstrumentationRuntime::Singleton;



    InstrumentationRuntime::InstrumentationRuntime() : toggledActiveInstrumentor(false)
    {
        report("InstrumentationRuntime Constructor");

        try{
        	// open the message queue
        	std::string lynx_q = clusterwatch::Message_Queue::get_mq_name(clusterwatch::Message_Queue::CW_INI_FILE);
        	mq.clusterwatch::Message_Queue::open(lynx_q);
        } catch (std::string s){
        	report(s.c_str());
        }
/*        struct mq_attr messageQueueAttr;
        messageQueueAttr.mq_maxmsg = MQ_MAX_MESSAGES;
        messageQueueAttr.mq_msgsize = MQ_MAX_MSG_SIZE;


        // Open a message queue for writing (sending data)
        messageQueue = mq_open (MSG_QUEUE, MQ_CREATE_OWNER_FLAGS, 0666, &messageQueueAttr);
        if ( -1 == messageQueue){
        	if( EEXIST  == errno ){
        		report("Message queue exists. Reopening it with other flags ... ");
        		// open again but without creation since we want to reuse it
            	messageQueue = mq_open (MSG_QUEUE, MQ_OPEN_OWNER_FLAGS);
            	if (messageQueue < 0){
            		perror("mq_open()");
            		report("Can't open message queue");
            	}
        	} else {
        		perror("mq_open()");
        	}
        }

        if ( -1 != messageQueue){
        	std::ostringstream stream;
        	stream << "Queue created: " << MSG_QUEUE << "; mq_maxmsg= "
        			<< messageQueueAttr.mq_maxmsg << "; mq_max_msg_size= "
        			<< messageQueueAttr.mq_msgsize << "\n";
        	report(stream.str());
        }
*/

/*        if(messageQueue < 0)
        {
            report( "Failed to open message queue for writing" );       
            if(errno == EACCES)
                report("invalid permissions");
            if(errno == ENOENT)
                report("no queue with this name exists");
            if(errno == EEXIST)
                report("queue with this name already exists");
            if(errno == EINVAL)
                report("invalid attribute properties");
        }
 */
        if(configuration.clockCycleCount)
	    {
		    report( "Creating clock cycle count instrumentor" );
		    lynx::addInstrumentor(new ClockCycleCountInstrumentor());			
	    }
	
	    if(configuration.memoryEfficiency)
	    {
	        report( "Creating memory efficiency instrumentor" );
	        lynx::addInstrumentor(new WarpInstrumentor(
	        instrumentation::WarpInstrumentor::memoryEfficiency));
	    }
	
	    if(configuration.branchDivergence)
	    {
	        report( "Creating branch divergence instrumentor" );
	        lynx::addInstrumentor(new WarpInstrumentor(
	        instrumentation::WarpInstrumentor::branchDivergence));
	    }
	    
	    if(configuration.activityFactor)
	    {
	        report( "Creating activity factor instrumentor" );
	        lynx::addInstrumentor(new WarpInstrumentor(
	        instrumentation::WarpInstrumentor::activityFactor));
	    }
	
	    if(configuration.warpInstructionCount)
	    {
	        report( "Creating warp instruction count instrumentor" );
	        lynx::addInstrumentor(new WarpInstrumentor(
	        instrumentation::WarpInstrumentor::instructionCount));
	    }
	    if(configuration.barrierCount)
	    {
	        report( "Creating barrier count instrumentor" );
	        lynx::addInstrumentor(new WarpInstrumentor(
	        instrumentation::WarpInstrumentor::barrierCount));
	    }
	
	    if(configuration.threadInstructionCount)
	    {
	        report( "Creating thread instruction count instrumentor" );
	        lynx::addInstrumentor(new BasicBlockInstrumentor(
	        instrumentation::BasicBlockInstrumentor::instructionCount));
	    }
	
	    if(configuration.basicBlockExecutionCount)
	    {
	        report( "Creating basic block execution count instrumentor" );
	        lynx::addInstrumentor(new BasicBlockInstrumentor(
	        instrumentation::BasicBlockInstrumentor::executionCount));
	    }
	    
	    for(PTXInstrumentorVector::iterator instrumentor = instrumentors.begin();
	        instrumentor != instrumentors.end(); ++instrumentor)
	    {
	        (*instrumentor)->iterations = configuration.iterations;
	        
	        for(PTXInstrumentor::KernelVector::const_iterator kernel = 
	            configuration.kernelsToInstrument.begin(); 
	            kernel != configuration.kernelsToInstrument.end();
	            ++kernel)
	        {
	            (*instrumentor)->kernelsToInstrument.push_back(*kernel);
	        }     
	    }    
    }

    InstrumentationRuntime::InstrumentationConfiguration::InstrumentationConfiguration()
    : 
    clockCycleCount(false),
    memoryEfficiency(false),
    branchDivergence(false),
    activityFactor(false),
    threadInstructionCount(false),
    warpInstructionCount(false),
    barrierCount(false),
    basicBlockExecutionCount(false)
    {
    
        std::ifstream stream("configure.lynx");
        hydrazine::json::Parser parser;
	    hydrazine::json::Object* object = 0;
   
		try {
		        
	        object = parser.parse_object(stream);

	        hydrazine::json::Visitor mainConfig(object);
	        hydrazine::json::Visitor instrumentConfig = mainConfig["instrument"];
	        

	        if(!instrumentConfig.is_null())
	        {
	        
	            log = instrumentConfig.parse<bool>("log", false);   
	            logfile = instrumentConfig.parse<std::string>("logfile", "");   
	            logKernelInfo = instrumentConfig.parse<bool>("logKernelInfo", false);   

                hydrazine::json::Visitor kernels = instrumentConfig["kernels"];

                if (!kernels.is_null()) {
	                hydrazine::json::Array *array = 
		                static_cast<hydrazine::json::Array *>(kernels.value);
	
	                for (hydrazine::json::Array::ValueVector::iterator it = array->begin();
		                it != array->end(); ++it) {
		                hydrazine::json::Visitor k(*it);
		                kernelsToInstrument.push_back((std::string)k);
	                }
                }
        
                iterations = instrumentConfig.parse<int>("iterations", -1);   

                clockCycleCount = instrumentConfig.parse<bool>("clockCycleCount", false);
                memoryEfficiency = instrumentConfig.parse<bool>("memoryEfficiency", false);
                branchDivergence = instrumentConfig.parse<bool>("branchDivergence", false);
		        activityFactor = instrumentConfig.parse<bool>("activityFactor", false);
		        warpInstructionCount = instrumentConfig.parse<bool>("warpInstructionCount", false);
		        barrierCount = instrumentConfig.parse<bool>("barrierCount", false);
		        threadInstructionCount = instrumentConfig.parse<bool>("threadInstructionCount", false);
		        basicBlockExecutionCount = instrumentConfig.parse<bool>("basicBlockExecutionCount", false);
		
	        }
	    } catch (std::exception exp) {
	        std::cerr << "==LYNX== WARNING: Could not parse config file 'configure.lynx'.\n" << std::endl;
        }          

	    delete object;
    }

    InstrumentationRuntime::~InstrumentationRuntime()
    {
    	// this should be done when messages are emptied
    	// otherwise your data will be lost
    	// it might be that the queue should be unlinked by
    	// the monitor, so basically only
    	/*int err = mq_unlink(MSG_QUEUE);
    	if (err == -1){
    		perror("mq_unlink");
    	} else {
    		report( "Queue unlinked successfully: " MSG_QUEUE);
    	}*/

    	// I think you don't need to close the queue after unlinking
   /* 	int err = mq_close(mq.get_mqd());
    	if(err < 0){
    	   perror("mq_close");
    	   std::ostringstream stream;
    	   stream << "Failed to close message queue: " << MSG_QUEUE
    			   << " " << errno << "\n";
    	   report( stream.str() );
    	} */
            
        lynx::clearInstrumentors();
        instrumentationContexts.clear();       
    }

}

#endif


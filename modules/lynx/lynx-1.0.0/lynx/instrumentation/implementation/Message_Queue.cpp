/**
 * @file Message_Queue.cpp
 *
 * @date Oct 28, 2013
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#include <lynx/instrumentation/interface/Message_Queue.h>
#include <lynx/instrumentation/interface/kernel_profile.h>

#include <hydrazine/interface/debug.h>

#include <sstream>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>

#include <mqueue.h>

#ifdef REPORT_BASE
#undef REPORT_BASE
#endif

//#define REPORT_BASE 0
#define REPORT_BASE 1


namespace clusterwatch {

	const std::string Message_Queue::CW_INI_FILE = "cw.ini";
	const std::string Message_Queue::CW_Q_NAME = "q_name";

	/**
     * the name of the queue
     * @param a_q_name The name of the queue
     */
    Message_Queue::Message_Queue(): MQ_UNINITIALIZED(-2){
    	q_descr = MQ_UNINITIALIZED;
    }

    Message_Queue::~Message_Queue() {
    	int my_q = MQ_UNINITIALIZED;

    	if( MQ_UNINITIALIZED != q_descr){
    		my_q = mq_close(q_descr);
    		if( -1 == my_q ){
    			report("ERROR: mq_close() returned Error\n");
    		} else {
    			report("mq_close(): SUCCESS\n");
    		}
    	}
    }

    /**
     * @param a_q_name
     * @return 0 if everything went fine
     *           throws an exception std::string if the string was empty
     */
    int Message_Queue::open(std::string a_q_name){

    	if( a_q_name.size() == 0){
    		throw std::string("MQ: Empty string for opening the message queue!\n");
    	}
    	q_name = a_q_name;
    	std::stringstream str;
    	str << "Queue name: " << q_name << "\n";
    	report(str.str().c_str());
    	q_descr = mq_open (q_name.c_str(), O_RDWR | O_EXCL | O_NONBLOCK);
    	if (-1 == q_descr){
    		throw std::string("MQ: Can't open the queue for writing\n");
    	}

    	report("MQ open: SUCCESS.\n");

    	return 0;
    }

    /**
     * returns the message queue descriptor
     * @return The message queue descriptor
     */
    mqd_t Message_Queue::get_mqd(){
    	return q_descr;
    }
    /**
     * checks if the message queue has been open
     * @return true Yes it is open
     *         false No, it isn't
     */
    bool Message_Queue::is_mq_open(){
    	return q_descr >= 0;
    }
    /**
     * get the name of the message queue; currently it reads from the
     * file
     *
     * TODO this is a very simplistic parser; this parser could be smarter
     *
     * @param ini_file The location of the ini file to be opened
     * @return The name of the message queue on success
     *         An empty string on error
     */
    std::string Message_Queue::get_mq_name(std::string ini_file){
    	std::string qname;

    	// open the ini file
    	std::ifstream read_f;
    	read_f.open(ini_file.c_str());
    	std::string line;

    	while (!read_f.eof()) {
    		getline(read_f, line);
    		std::size_t found = line.find(CW_Q_NAME.c_str());
    		if (found != std::string::npos){
    			found = line.find('=');
    			qname = line.substr(found+1);
    			break;
    		}
    	}

    	read_f.close();

    	return qname;
    }
}

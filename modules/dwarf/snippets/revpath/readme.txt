# readme.txt
# Created on: Dec 10, 2013
# Author: Magda S. aka Magic Magg magg dot gatech at gmail.com
#

DESCRIPTION
============
The idea is to test how to expose data from the the splitter via revpath.
There should be two parts: local and remote. The local part just do monitoring.
The remote part when appears it adds the output to the splitter stone 
remotely via remote EVPath interface
and starts to receive events from the local that are copied by the splitter.

The example is organized as follows:

The sender sends periodic messages to the receiver. The sender has
a splitter stone that output to a bridge stone that connects to the 
receiver terminal stone. The new_sink remotely creates the output stone
on a sender and bridges the newly created output stone to the new_sink
terminal stone.

sender: splitter -> output --------> receiver : terminal

new_sink: causes the following via remote EVPath interface:
sender: splitter ->output ---------> receiver : terminal
                 ->output ---------> new_sink : terminal

BUILD
=======
# modify the location of EVPath installation dirs
#   env.AppendUnique(LIBPATH=['/rock/opt/evpath/lib'])
#   env.AppendUnique(CPPPATH=['/rock/opt/evpath/include', '.'])

$ vi SConstruct
...

# build
$ scons

# clean the build
$ scons -c
                 
RUNNING
========
# first run the receiver
$ ./receiver 600          # how long the receiver will execute

Forked a comm thread
Contact list (stone-id:my-contact-info) "0:AAIAAJTJ8o2hZQAAATkCmEoBqMA="
....

# run the server with the receiver's contact info
$ ./sender 0:AAIAAJTJ8o2hZQAAATkCmEoBqMA=
My contact split_stone_id:split_action_id:contact: 0:0:AAIAAJTJ8o2/ZQAAATkCmEoBqMA=

.....

# run the sink with the sender's contact info
$ ./new_sink 0:0:AAIAAJTJ8o2/ZQAAATkCmEoBqMA=
....

TROUBLESHOOTING
================
2013-12-11
After executing ./new_sink the ./sender may output

No action found for event b24cb0 submitted to 
local stone number 2
Dump stone ID 2, local addr b25970, default action -1
  proto_action_count 0:
  proto_action_count 0:
  response_cache_count 2:
Response cache item 0, reference format 0xb249a0 (complex_rec:02000018d3325123972100b9)
stage 0, action_type Action_NoAction, proto_action_id -1879043792, requires_decoded 0
Response cache item 1, reference format 0xb249a0 (complex_rec:02000018d3325123972100b9)
stage 1, action_type Action_NoAction, proto_action_id -1879035824, requires_decoded 0
    Unhandled incoming event format was "complex_rec:02000018d3325123972100b9"

	** use "format_info <format_name>" to get full format information

I think you might ignore it because it is probably because the remote
creation is separated. So first the stone is created and action is assigned
later, not in one step. Maybe it should be programmed in a different order.

# EOF

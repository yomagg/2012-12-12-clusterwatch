/**
 * local.cpp
 *
 *  Created on: Dec 10, 2013
 *  Author: Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>
#include <unistd.h>

#include "evpath.h"

#include "data.h"

using namespace std;

int main(int argc, char * argv[]){

	if (2 != argc){
		cout << "Usage: " << argv[0] << " remote-stone-id:contact-string\n";
		cout << "Eg. " << argv[0] << " 0:AAIAAJTJ8o2yZQAAATkCmEoBqMA=\n";
		return 0;
	}

	CManager cm;

	cm = CManager_create();
	CMfork_comm_thread(cm);
	CMlisten(cm);

	// create a split stone - allocate a stone and associate a split
	// action with the stone
	EVstone split_stone = EValloc_stone(cm);
	EVaction split_action = EVassoc_split_action(cm, split_stone, nullptr);

	// get a contact list
	attr_list contact_list = nullptr;
	contact_list =  CMget_contact_list(cm);

	char *contact_list_str=nullptr;
	if (contact_list) {
		contact_list_str = attr_list_to_string(contact_list);
		free_attr_list(contact_list);
		cout << "My contact split_stone_id:split_action_id:contact: "
			 << split_stone << ":" << split_action << ":"
			 << contact_list_str << "\n";
	} else {
		cerr << "ERROR: Issues with getting the contact list. Quitting ...\n";
		return 1;
	}

	// connect to the receiver
	char string_list[2048];
	EVstone remote_stone;
	if (sscanf(argv[1], "%d:%s", &remote_stone, string_list) != 2){
		cout << "ERROR: Bad argument: " << argv[1] << "\n";
		return 1;
	}

	// create a local output stone and connect this stone with the
	// remote stone
	EVstone output_stone = EValloc_stone(cm);
	contact_list = attr_list_from_string(string_list);

	EVassoc_bridge_action(cm, output_stone, contact_list, remote_stone);
	// the below call adds the output stone to the list of stones
	// to which the split stone will replicate data. The call requires
	// the stone to which the split action was registered and the
	// split action that was returned
	EVaction_add_split_target(cm, split_stone, split_action, output_stone);

	EVsource source = EVcreate_submit_handle(cm, split_stone, complex_format_list);

	struct complex_rec data;
	data.r = 13.3;
	data.i = 12.3;


	cout << "Sending ...\n";
	for(int i = 1; i < 600; ++i){
		cout << "* ";
		EVsubmit(source, &data, nullptr);
		CMsleep(cm, 1);
		data.r++;
		data.i++;
	}

	CManager_close(cm);

	return 0;
}


/**
 * @file receiver.cpp
 *
 * @date Created on: Dec 10, 2013
 * @author: Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#include <iostream>
#include "evpath.h"

#include "data.h"

using namespace std;

static int
complex_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
	complex_rec * event = static_cast<complex_rec*> (vevent);
	cout << "Receiver got: (" << event->r << ", " << event->i << ")\n";
    return 1;
}

int main(int argc, char *argv[]){

	if (2 != argc){
		cout << "Usage: " << argv[0] << " how-long-to-run\n";
		cout << "how-long-to-run specify how long the network will be serviced in seconds\n";
		cout << "Eg. " << argv[0] << " 30\n";
		return 0;
	}
	CManager cm;

	cm = CManager_create();
	CMfork_comm_thread(cm);

	cout << "Forked a comm thread\n";
	CMlisten(cm);

	EVstone stone = EValloc_stone(cm);
	EVassoc_terminal_action(cm, stone, complex_format_list, complex_handler, nullptr);

	char *string_list = attr_list_to_string(CMget_contact_list(cm));
	// print the stone id and the stringified contact information
	cout << "Contact list (stone-id:my-contact-info) \"" << stone << ":"<< string_list << "\"\n";

	// how long the program should run
	string period = argv[1];
	CMsleep(cm, atoi(period.c_str()));
	CManager_close(cm);
	//CMrun_network(cm);


	return 0;
}



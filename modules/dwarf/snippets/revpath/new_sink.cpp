/**
 * @file new_sink.cpp
 *
 * @date Created on: Dec 10, 2013
 * @author: Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>
#include <unistd.h>
#include <vector>

#include "evpath.h"
#include "revpath.h"

#include "data.h"

using namespace std;

static int complex_handler(CManager cm,  void *vevent, void *client_data, attr_list attrs){
//static void complex_handler(CManager cm,  CMConnection  conn, void *vevent, void *client_data, attr_list attrs){
	complex_rec * event = static_cast<complex_rec*> (vevent);
	cout << "New sink got: (" << event->r << ", " << event->i << ")\n";

	return 1;
}


int split(const char* in_str, const char* delimiter, vector<string> & output){
	string s = in_str;
	string del = delimiter;

	size_t pos = 0;
	string token;
	while ((pos = s.find(del)) != string::npos) {
		token = s.substr(0, pos);
		output.push_back(token);
		s.erase(0, pos + del.length());
	}
	output.push_back(s);

	return 0;
}

int main(int argc, char * argv[]){

	if (2 != argc){
		cout << "Usage: " << argv[0] << " splitter-stone-id:splitter-action:contact-string\n";
		cout << "Eg. " << argv[0] << " 0:0:AAIAAJTJ8o2yZQAAATkCmEoBqMA=\n";
		return 0;
	}

	CManager cm;

	cm = CManager_create();
	//CMfork_comm_thread(cm);
	CMlisten(cm);

	// assuming that the format is splitter-stone-id:contact-string


	vector<string> sender_contact;
	split(argv[1], ":", sender_contact);
	attr_list sender_contact_list = attr_list_from_string(sender_contact[2].c_str());

	// initiate the connection with the sender
	CMConnection conn = CMinitiate_conn(cm, sender_contact_list);

	if (nullptr == conn){
		cout << "ERROR: initiate connection returned nullptr. Quitting ...\n";
		CMsleep(cm, 1);
		CManager_close(cm);
		return 1;
	}

	// now do the magic
	// * register your handler for incoming messages from the sender
	// * create another remote output stone remotely (on sender)
	// * add the this stone as a splitter target

	// I will start from registering the format of data I am
	// interested in
	// I think I don't need this because this is from the CM point of view
	//CMFormat complex_format = CMregister_format(cm, complex_format_list);
	//CMregister_handler(complex_format, complex_handler, nullptr);

	// create an output stone remotely
	EVstone routput_stone = REValloc_stone(conn);
	EVaction rsplit_action_id = atoi(sender_contact[1].c_str());
	EVstone rsplit_stone_id = atoi(sender_contact[0].c_str());

	// connect the new output stone to the splitter stone
	REVaction_add_split_target(conn, rsplit_stone_id, rsplit_action_id, routput_stone);

	// create local stone that will be connected with the remote stone
	EVstone local_stone = EValloc_stone(cm);
	EVassoc_terminal_action(cm, local_stone, complex_format_list, complex_handler, nullptr);

	// connect routput_stone with the local_stone
	attr_list tmp_list = CMget_contact_list(cm);
	REVassoc_bridge_action(conn, routput_stone, tmp_list, local_stone);
	free_attr_list(tmp_list);


	CMsleep(cm, 600);
	CManager_close(cm);

	return 0;
}

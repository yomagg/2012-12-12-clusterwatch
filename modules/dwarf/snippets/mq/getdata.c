/**
 * @file getdata.c
 *
 * @date Oct 8, 2013
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#include <stdio.h>

#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <string.h>   // for strerror
#include <errno.h>
#include <stdlib.h>


// see transport/evpath/include/kernel_profile.h
#define MSG_QUEUE "/lynxtest"

int main(int argc, char ** argv){

	mqd_t my_q = -1;
	unsigned int msg_prio = 0;
	int ret;

	struct mq_attr obuf;    /* output of mq_getattr(): mq_msgsize */

	my_q = mq_open(MSG_QUEUE, O_RDONLY | O_NONBLOCK);

	if ( -1 == my_q ){
		perror("mq_open ()");
		return -1;
	}

	// get max message size
	if (! (mq_getattr(my_q,&obuf)) ){
		char * msgptr = calloc(1,obuf.mq_msgsize);

		ret = mq_receive(my_q,msgptr,obuf.mq_msgsize,&msg_prio);
	    if (ret >= 0){
	    	// got a message
	    	printf("priority %ld  len %d number  %d\n",
                    msg_prio,  ret, *((int*)msgptr) );
	    } else {
	    	perror("mq_receive()");
	    	return -1;
	    }
	} else {
		perror("mq_getattr()");
		return -1;
	}

	mq_close(my_q);

	return 0;
}

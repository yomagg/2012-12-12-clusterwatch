/**
 * @file putdata.c
 *
 * @date Oct 9, 2013
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */


#include <stdio.h>

#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <string.h>   // for strerror
#include <errno.h>

#define MAX_KERNEL_NAME_SIZE    256 // EVPath is hardcoded to this value.

typedef enum { NONE, KERNEL_RUNTIME, MEM_EFFICIENCY, INST_COUNT, EXECUTION_COUNT, BRANCH_DIVERGENCE } profile_type;

typedef struct _kernel_profile {

	// Modifying the layout of this data structure (names, types, ordering)
	// requires modification of EVPath descriptors in clusterspy (see profiles.h)

    int pid;
    int device;

    profile_type type;
    union {
        double kernel_runtime; //in ms
        double memory_efficiency;
        double instruction_count;
        double branch_divergence;
    } data;

    char name[MAX_KERNEL_NAME_SIZE]; // don't change macro unless you also want to change EVPath

} kernel_profile;


// see transport/evpath/include/kernel_profile.h
#define MSG_QUEUE "/lynxtest"

#define MQ_MAX_MESSAGES 8 // EVPath is hardcoded to this value.
#define MQ_MAX_MSG_SIZE (sizeof(kernel_profile))
#define MQ_OPEN_OWNER_FLAGS		(O_RDONLY | O_NONBLOCK)





int main(int argc, char ** argv){
	int my_q = -1;


	if ( 2 != argc ){
		printf("Usage: %s queue_name\n", argv[0]);
		printf("e.g.: %s /lynxtest\n",argv[0]);
		return 0;
	}

	my_q = mq_unlink(argv[1]);
	if (-1 != my_q){
		fprintf(stderr, "mq_unlink of %s succeeded\n", argv[1]);


//		char stime[26];
//		const time_t tm = time(NULL); /* current time value */
//		(void)ctime_r (&tm,stime);    /* formatted time string */
//		stime[24] = '\0' ;            /* drop annoying \n */
//		for( c=1; c<=count; ++c){
//	         sprintf(msgptr,"%05d %s",c,stime);
//	         if ( mq_send(mqd,msgptr,msglen,msg_prio) ){
//	            perror("mq_send()");
//	            break;
//	         }
//		}
	} else{
	      perror("mq_unlink()" );
	}

/*	mqd_t lynx_q = -1;

	int err = 0;
	struct mq_attr qattr;
	qattr.mq_maxmsg = MQ_MAX_MESSAGES;
	qattr.mq_msgsize = MQ_MAX_MSG_SIZE;

	do {
		lynx_q = mq_open(MSG_QUEUE, MQ_OPEN_OWNER_FLAGS, 0666, &qattr);
		fprintf(stderr, "mq_open failed: errno=%d, %s. Will retry ...\n", errno, strerror(errno));
	} while (-1 == lynx_q);
*/

	mq_close(my_q);
/*	retry_open:
			lynxMsgQueue = mq_open(MSG_QUEUE,
			if(lynxMsgQueue < 0) {
				if (errno == EEXIST) {
					err = mq_unlink(MSG_QUEUE);
					if (err < 0) {
						fprintf(stderr, "mq_unlink failed\n");
					}
					goto retry_open;
				} else {
					fprintf(stderr, "mq_open failed\n");
				}
			}
*/
	return 0;
}

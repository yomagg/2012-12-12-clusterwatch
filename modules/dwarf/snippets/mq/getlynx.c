/**
 * @file getlynx.c
 *
 * @date Oct 11, 2013
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#include <stdio.h>

#include <fcntl.h>           // For O_* constants
#include <sys/stat.h>        // For mode constants
#include <mqueue.h>
#include <string.h>   // for strerror
#include <errno.h>
#include <stdlib.h>


#include <kernel_profile.h>

int p_kernel_profile(kernel_profile *p_profile){

	printf("PID %d DEVICE %d NAME %s TYPE %d", p_profile->pid,
			p_profile->device, p_profile->name,
			p_profile->type);

	switch(p_profile->type){
	case NONE: printf(" NONE 0");
		break;
	case KERNEL_RUNTIME: printf( " KERNEL_RUNTIME %f", p_profile->data.kernel_runtime);
		break;
	case MEM_EFFICIENCY: printf( " MEMORY_EFFICIENCY %f dynamicHalfWarps %lu memTransactions %lu",
			p_profile->data.mem_efficiency.me_metric,
			p_profile->data.mem_efficiency.dynamicHalfWarps,
			p_profile->data.mem_efficiency.memTransactions);
		break;
	case INST_COUNT: printf( " INST_COUNT %f BARRIERS %lu", p_profile->data.instruction_count,
			p_profile->data.barriers);
			break;
	case BRANCH_DIVERGENCE: printf( " BRANCH_DIVERGENCE %f totalBranches %lu divergentBranches %lu",
			p_profile->data.branch_divergence.bd_metric,
			p_profile->data.branch_divergence.totalBranches,
			p_profile->data.branch_divergence.divergentBranches);
		break;
	case ACTIVITY_FACTOR: printf( " activeThreads %lu maxThreads %lu warps %lu", p_profile->data.activity_f.activeThreads,
			p_profile->data.activity_f.maxThreads, p_profile->data.activity_f.warps);
		break;
	case EXECUTION_COUNT: printf( " EXECUTION_COUNT %f", p_profile->data.instruction_count);
		break;


	default: printf( " UNKNOWN 0");
			break;
	}

	printf("\n");

	return 0;
}

int main(int argc, char** argv){

	unsigned int msg_prio = 0;
	int ret;
	struct mq_attr obuf;    // output of mq_getattr(): mq_msgsize
	mqd_t lynx_q = -1;

	lynx_q = mq_open(MSG_QUEUE, O_RDONLY | O_NONBLOCK);

	if ( -1 == lynx_q ){
		perror("mq_open()");
		return -1;
	} else {
		fprintf(stderr, "open of the queue succeeded.\n");
	}

	// get max message size
	if (! (mq_getattr(lynx_q,&obuf)) ){
		kernel_profile * msgptr = (kernel_profile *) calloc(1,obuf.mq_msgsize);

		while(1){
		ret = mq_receive(lynx_q,(char*)msgptr,obuf.mq_msgsize,&msg_prio);
		if (ret >= 0){
			// got a message
			p_kernel_profile(msgptr);

//			printf("priority %ld  len %d pid  %d, profile_type=%d, kernelname = %s\n",
//					msg_prio,  ret, msgptr->pid, msgptr->type, msgptr->name );
		} else {
			perror("mq_receive()");
			return -1;
		}
		}
	} else {
		perror("mq_getattr()");
		return -1;
	}

	mq_close(lynx_q);

	return 0;
}

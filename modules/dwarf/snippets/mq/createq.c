/**
 * @file putdata.c
 *
 * @date Oct 9, 2013
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */


#include <stdio.h>

#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <string.h>   // for strerror
#include <errno.h>



#define MQ_MAX_MESSAGES 1000 // EVPath is hardcoded to this value.
//#define MQ_OPEN_OWNER_FLAGS		(O_RDONLY | O_NONBLOCK)
#define MQ_OPEN_OWNER_FLAGS		( O_RDWR | O_CREAT | O_EXCL | O_NONBLOCK)


int main(int argc, char ** argv){
	mqd_t my_q = -1;
	struct mq_attr qattr;
	qattr.mq_maxmsg = 8;
	qattr.mq_msgsize = 1000;

	if ( 2 != argc ){
		printf("Usage: %s queue_name\n", argv[0]);
		printf("e.g.: %s /lynxtest\n",argv[0]);
		return -1;
	}

	my_q = mq_open(argv[1], MQ_OPEN_OWNER_FLAGS, 0666, &qattr);
	if (-1 != my_q){
		fprintf(stderr, "mq_open succeeded\n");
	} else{
	     perror("mq_open()" );
	}

	return 0;
}

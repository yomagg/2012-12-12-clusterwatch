/**
 * @file getdata.c
 *
 * @date Oct 8, 2013
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#include <stdio.h>

#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <string.h>   // for strerror
#include <errno.h>
#include <stdlib.h>


// see transport/evpath/include/kernel_profile.h
//#define MSG_QUEUE "/lynxtest"


int main(int argc, char ** argv){

	mqd_t my_q = -1;
	unsigned int msg_prio = 0;
	struct mq_attr obuf;    /* output of mq_getattr(): mq_msgsize */

	if ( 2 != argc ){
		printf("Usage: %s queue_name\n", argv[0]);
		printf("e.g.: %s /lynxtest\n",argv[0]);
		return -1;
	}
	my_q = mq_open(argv[1], O_RDONLY | O_NONBLOCK);

	if ( -1 == my_q ){
		perror("mq_open (O_RDONLY)");
		printf("queue name: %s\n", argv[1]);
		return -1;
	}

	if ( !mq_getattr(my_q,&obuf) ){
		printf("MQ: %s flags: 0x%x  maxmsg: %d  msgsize: %d  curmsgs: %d\n",
				argv[1],
				obuf.mq_flags, obuf.mq_maxmsg, obuf.mq_msgsize, obuf.mq_curmsgs);
	} else {
		perror("mq_getattr()");
	}

	mq_close(my_q);
	return 0;
}

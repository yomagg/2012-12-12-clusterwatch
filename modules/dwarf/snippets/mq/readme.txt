# @file readme.txt
# @date: Oct 10, 2013
# @author: Magda S., aka Magic Magg, magg dot gatech at gmail dot com
#
#
DESCRIPTION
===========
Demonstrates usage of message queues. If you create a message queue, please
be sure that you use the 4-argument API instead of 2, otherwise you might
run into the "permission denied" issues later on when you trying to get something
out of the queue, see the status, etc.

$ ./createq   #creates a queue named /lynxtest
$ ./unlinkq   /lynxtest # you need to unlink the queue, otherwise it will be destroyed by 
              # the system shutdown
$ ./putdata   # adds a number to the queue
$ ./getdata   # gets the message from the queue
$ ./getstatus /lynxtest # gets the status of the queue (you need to specify a parameter)
$ ./getlynx   # gets the data out of lynx
   
   #run lynx
   node1$ LD_PRELOAD="/nics/d/home/smagg/opt/lynx-cw/lib/liblynx_runtime.so" ~/NV_SDK/4.1/C/bin/linux/release/BlackScholes
   # run getlynx on the same node
   node1$ ./getlynx
   
   
BUILD
======
# build
$ scons

# clean the build
$ scons -c
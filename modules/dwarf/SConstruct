"""
	@file SConstruct
	@date Sep 12, 2012
	@author Magdalena Slawinska a.k.a. Magic Magg magg dot gatech @ gmail dot com
"""
import os
import socket
import sys
import getpass
import subprocess

Help("""
   $ scons -Q   # builds quietly the program
   $ scons -c   # cleans the program

   # run with the debug compilation options enabled [default is disabled]
   $ scons --fdbg
   
   # to generate the extra documentation; it will be installed in the docs dir
   # if not present the documentation will be not installed
   $ scons --doc

   # no NVML
   $ scons --no-nvml
      
   # to specify the installation directory (other than a default one)
   scons --prefix=/tmp 
 
   # to build and install in a default directory (distro)
   # installation within a default directory and with debug enabled
   scons --fdbg install
 
   # installation within a specified directory and with debug enabled
   scons --fdbg --prefix=/tmp install
  
   NOTE1: if you forget to add 'install', there will be no installation

   NOTE2: to uninstall files, you need to provide the directory and install
   command to tell scons that it should use paths used during installation,
   e.g. if
   scons --fdbg --prefix=/tmp install
      then
   scons -c --prefix=/tmp install

   For defined configurations see function
          
   set_build_variables()
    """)

# here are variables we use in our build system
# they are set in set_build_variables

# this should work for if evpath was compiled with gcc complier
EVPATH = { 
           'ROOT' : None,
           'lib' : ['evpath', 'cercs_env', 'atl', 'gen_thread', 'ffs', 'dill' ] # the name of the library (-l)           
         }


# glib20 headers are installed in a few directories,
# if all the headers are in the same directory just set this
# to the same directory, scons should remove the redundant directories
GLIB20 = { 
          'ROOT' : None,
          'lib'         : 'glib-2.0' # the name of the library (-l)           
          }

# this is for GCC
GCC = { 
        'CC' : 'gcc',
        'CCFLAGS' :  ['-fPIC', '-Wall', '-Wextra', '-Wno-unused-parameter', '-std=gnu99'],
#        'CCDEBUGFLAGS' : ['-ggdb', '-g', '-DDEBUG'],
        'CCDEBUGFLAGS' : ['-ggdb', '-g'],
        'CCOPTFLAGS' : ['-O3']    
      }


CLUSTERSPY = {
	  'ROOT' : None,
	  'lib' : 'clusterspy',
	  'LIB' : None
	}

NVML = {
	'ROOT' : None,
	'lib'  : ['nvidia-ml'],
	'LIB'  : None,
	'INC'  : None
	}
# where is the installation directory
INSTALL_DIR = os.getcwd() + '/build'

#################################################
# helper functions
#################################################
def set_build_context():
    """
    tries to get a configuration characteristic string
    currently: three first letters of the machine hostname
    concatenated with '_' and concatenated with the user name
    e.g. kidlogin.nics.utk.edu and a user john
    kid_john
    """
    
    nodename = socket.gethostname()
    username = getpass.getuser()

    return nodename[:3] + '_' + username

def set_build_variables(build_ctx):
    """
      sets the build variables automatically depending on the system you
      are working on
      @param build_ctx: the detected build context (string) 
    """
    global EVPATH
    global GLIB20
    global GCC
    global CLUSTERSPY
    
    # configuration for kid_smagg keeneland
    if build_ctx == 'kid_smagg':
        print(build_ctx + ' build context detected ...')
        EVPATH['ROOT'] = '/nics/d/home/smagg/opt/evpath/current'
        GLIB20['ROOT'] = '/nics/d/home/smagg/opt/glib-2.33.2'
        CLUSTERSPY['ROOT'] = '/nics/d/home/smagg/proj/wksp-ecl-cpp/2012-12-12-clusterwatch/transport/evpath'
        NVML['INC'] = '/sw/kfs/cuda/4.0/linux_binary/CUDAToolsSDK/NVML'
        NVML['LIB'] = '/usr/lib64'  
        
        # addressing the intel PE (since evpath was build with icc) 
        EVPATH['lib'] = EVPATH['lib'] + ['svml', 'imf', 'intlc']
        GCC['CC'] = '/opt/intel/composer_xe_2011_sp1.11.339/bin/intel64/icc'

    elif build_ctx == 'kfs_smagg':
        print(build_ctx + ' build context detected ...')
        EVPATH['ROOT'] = '/nics/d/home/smagg/opt/evpath/current'
        GLIB20['ROOT'] = '/nics/d/home/smagg/opt/glib-2.33.2'
        CLUSTERSPY['ROOT'] = '/nics/d/home/smagg/proj/wksp-ecl-cpp/2012-12-12-clusterwatch/transport/evpath'
        NVML['INC'] = '/sw/kfs/cuda/4.0/linux_binary/CUDAToolsSDK/NVML'
        NVML['LIB'] = '/usr/lib64'
        
        # addressing the intel PE (since evpath was build with icc)
        EVPATH['lib'] = EVPATH['lib'] + ['svml', 'imf', 'intlc']
        GCC['CC'] = '/opt/intel/composer_xe_2011_sp1.11.339/bin/intel64/icc'

    
    elif build_ctx == 'pro_magg':
    	print(build_ctx + ' build context detected ...')
    	EVPATH['ROOT'] = '/opt/evpath'
        CLUSTERSPY['ROOT'] = '/lvm/lv_earth/magg/src/2012-12-12-clusterwatch/transport/evpath'
        NVML['INC'] = '/home/magg/CUDAToolsSDK4/NVML'
        NVML['LIB'] = '/usr/lib64'
        GLIB20['ROOT'] = '/opt/glib-2.28.7'
    
    elif build_ctx == 'cud_magg':
    	print(build_ctx + ' build context detected ...')
    	EVPATH['ROOT'] = '/opt/evpath'
    	CLUSTERSPY['ROOT'] = '/home/magg/src/2012-12-12-clusterwatch/transport/evpath'
    	GLIB20['ROOT']= '/opt/glib-2.34.2'
    	NVML['INC'] = '/home/magg/CUDAToolsSDK4/NVML'
    	NVML['LIB'] = '/usr/lib64'
    
    elif build_ctx == 'hef_magg':
		print(build_ctx + ' build context detected ...')
		EVPATH['ROOT'] = '/rock/opt/evpath'
		CLUSTERSPY['ROOT'] = '/rock/synchrobox/proj/w-ecl/2012-12-12-clusterwatch/transport/evpath'
		GLIB20['ROOT'] = '/rock/opt/glib-2.36.0'

    elif build_ctx == 'pas_magg' or build_ctx == 'com_magg':
        print(build_ctx + ' build context detected ...')
        EVPATH['ROOT'] = '/home/magg/opt/adbf'
        CLUSTERSPY['ROOT'] = '/home/magg/opt/adbf/src/clusterwatch/transport/evpath'
        GLIB20['ROOT'] = '/home/magg/opt/adbf/glib-2.36.4'
        NVML['INC'] = '/export1/local/0/cuda/CUDAToolsSDK/NVML'
        NVML['LIB'] = '/usr/lib64'
        #GCC['CC'] = 'gcc -include /home/magg/opt/adbf/cluster-watch/modules/dwarf/gcc-preinclude.h'
	#GCC['CCFLAGS'] = GCC['CCFLAGS'] + ['-Wl,--wrap=memcpy']

    else:
		print('''ERROR: no build configuration detected. Define
		a build configuration for this machine SConstruct/set_build_variables()''')
		sys.exit(-1)

def determine_extra_compiler_options(env):
    """
    determines the additional compiler options such as 
    debug or optimization and appends it to the base environment
    @param env: Environment - the environment to which the new things will be appended 
    """

    debug = GetOption('DEBUG')
    
    if None == debug:
        debug = False
    if debug:
        env.Append(CCFLAGS = GCC['CCDEBUGFLAGS'])
        print('Debug compilation option enabled')

    if GetOption('NO_NVML'):
		env.Append(CCFLAGS = ['-DNO_NVML'])

def print_vars():
    """
       print variables that will be used for the build
    """
    global GLIB20
    global EVPATH
    global INSTALL_DIR
    global GCC
    
    print('IMPORTANT BUILD VARIABLES')
    print('=========================')
    
    print('GLIB20[ROOT]= ' + GLIB20['ROOT'])
    print('EVPATH[ROOT]= ' + EVPATH['ROOT'])
    print('CLUSTERSPY[ROOT]' + CLUSTERSPY['ROOT'])
    print('INSTALL_DIR=' + INSTALL_DIR)
    sys.stdout.write('NVML library will be compiled: ')
    print( (not GetOption('NO_NVML')) )
    if GetOption('NO_NVML') == False:
    	print('NVML[INC]=' + NVML['INC'] + ', NVML[LIB]=' + NVML['LIB'] )
        
    print('GCC base settings:')
    for key in GCC.keys():
        sys.stdout.write('\t' + key + '=')
        print(GCC[key])
    sys.stdout.write('Debug options appended: ') 
    print( GetOption('DEBUG'))
    sys.stdout.write('Documentation will be generated: ')    
    print( GetOption('opt_doc'))
    print('=========================')
    
###################################################33
# end of helper functions
###################################################3

# the actual build

AddOption('--fdbg', action='store_true', dest='DEBUG',  
          help='if present the debug compilation options will be enabled', default=False)
AddOption('--prefix', action='store', dest='install_dir', type='string', 
          help='if present the it is the directory where the distro will be installed', default=INSTALL_DIR)
AddOption('--doc', action='store_true', dest='opt_doc',
		  help='if present the extra documentation will be generated', default=False)
AddOption('--no-nvml', action='store_true', dest='NO_NVML',
	  help='if present the nvml will not be compiled', default=False)

# determine the build configuration    
build_ctx = set_build_context()
print('Detected build context: ' + build_ctx)

set_build_variables(build_ctx)

# the base environment for compiling with gcc
BASE_ENV = Environment(
    CPPPATH = ['../include', '../../../lynx/lynx-1.0.0/lynx/instrumentation/interface'],
    CCFLAGS = GCC['CCFLAGS'],
    CC = GCC['CC']
    )          

determine_extra_compiler_options(BASE_ENV)
INSTALL_DIR = GetOption('install_dir') 

# to enable installing outside of the top-level directory; installing
# is still considered a type of file 'build.' The default behavior of
# Scons is to build files in or below the current directory
BASE_ENV.Alias('install', INSTALL_DIR)

print_vars()


# export variables to other scripts
Export(
       'EVPATH',
       'GLIB20', 
       'GCC',
       'INSTALL_DIR',
       'CLUSTERSPY',
       'NVML',       
       'BASE_ENV')

# build the libclusterspy lib
#subprocess.call('make')
# call all scripts
SConscript([    '../../transport/evpath/SConstruct',
                'src/common/SConstruct',
		'src/ranger/SConstruct',
		'src/readers/SConstruct',
		'tools/mq/SConstruct',
		'tests/SConstruct'
	#	'snippets/mq/SConstruct'
		])

if GetOption('opt_doc'):
	DOCS_PATH='docs/prog_guide'
	PDF(target=DOCS_PATH+'/pg.pdf', source = DOCS_PATH + '/pg.tex')
	BASE_ENV.Install(INSTALL_DIR + '/docs', [DOCS_PATH + '/pg.pdf'])

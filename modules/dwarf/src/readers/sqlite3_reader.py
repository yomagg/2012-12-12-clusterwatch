"""
    @file sqlite3_reader.py
    @date Aug 3, 2012
    @author Magdalena Slawinska a.k.a. Magic Magg magg dot gatech @ gmail dot com
    
    This module allows to read data written by a few spies. Executing this
    file requires python2.7 since for some reason on KIDS when executing
    with python3 there is an error about missing argparse package.
    Also ConfigParser was renamed to configparser in python3.
    
    This module needs to know the current hostname of the ranger to automatically
    construct the name of the db. That info is written in the configuration ini file.
    
    IMPORTANT
    Depends: ini file for the Dwarf system, if you change the ini file structure
    chances are that you need to update this module as well.
"""
import mmap
import sqlite3
import datetime
import sys
import os
import time
import logging
import argparse
import common.misc_utils
import ConfigParser
import glob

# the name of the section that contains info about the ranger hostname
# in the ini file; you need update it if you change the section name
# in the ini file
CFG_RANGER_SEC='ranger'
# the host name field in the trooper section
CFG_RANGER_SEC_HOSTNAME='hostname'
# the output dir in the trooper section
CFG_RANGER_SEC_OUTPUT_DIR='output_dir'

CFG_MEM_SPY='mem_spy'
CFG_CPU_SPY='cpu_spy'
CFG_NET_SPY='net_spy'
CFG_NVID_SPY='nvidia_smi_spy'
CFG_NVML_SPY='nvml_spy'
CFG_LYNX_SPY='lynx_spy'

def get_log_file(output_dir, hostname_prefix, spy_name):
    """
        Get the name of the last log name
        with the provided hostname and the spy name
       
        @param output_dir: where to look for the pattern 
        @param hostname_prefix: The hostname
        @param spy_name: the name of the spy
        @rtype: string
        @return: the name of the last file or None if no file has been found          
    """
    pattern = output_dir + hostname_prefix + '-mmap-' + spy_name + '-????-??-??-????.log'
    logging.debug(pattern)
    l = sorted(glob.glob(pattern))
    if l == []:
        logging.error('No log file for %s found!', spy_name )    
        return None
    else:
        logging.debug('Last file: ' + l[-1])
        return l[-1]

def open_mmap_file(filename):
    """
        Open the mmap file corresponding of the file
       
        @param filename: the name of the file to be mmapped 
        @return: mmap for the opened file or None if no file has been found          
    """
    f = open(filename, 'r')
    m = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)
    f.close()    
    return m

def read_mmap_layout(mmap):
    
    # the first line describes the layout of the mmapped file
    first_line = mmap.readline()
    first_line = first_line.strip('\n')    
    arr = first_line.split(' ')
    arr = arr[len(arr)/2:len(arr)-1]
        
    # we expect to have something 6 numeric values after this
    if len(arr) != 6:
        logging.error('Cannot read layout from the mmapped file. Got len(arr)= ' + str(len(arr)) + ' Quitting ...')
        loggin.error('Layout array: ')
        logging.error(arr)
        return None
       
    # the layout = this is how the mmap is organized
    layout = {'block_count' : int(arr[0]),
            'hdr_hdr' : int(arr[1]),
            'hdr_entry' : int(arr[2]),
            'bdy_hdr' : int(arr[3]),
            'bdy_block_entry' : int(arr[4]),
            'bdy_block_entry_count' : int(arr[5]),
            'hdr_total' : 0,
            'bdy_total' : 0,
            'total' : 0                                            
            }
    layout['hdr_total'] = layout['hdr_hdr'] + layout['block_count'] * layout['hdr_entry']
    layout['bdy_total'] = layout['bdy_hdr'] + \
        layout['bdy_block_entry'] * layout['bdy_block_entry_count'] *layout['block_count']
    layout['total'] = layout['hdr_total'] + layout['bdy_total']
    
    return layout

def init_db_entry(output_dir, hostname, spy_name):
    """
        initializes the database entry
        
        @param output_dir: where to look for the pattern 
        @param hostname_prefix: The hostname
        @param spy_name: the name of the spy
        @return: None if some errors
    """
    d = {}
    d['file'] = get_log_file(output_dir, hostname, spy_name)
    if not d['file']:
        logging.error('Cant get the log file. Quitting ...')
        return None
                        
    d['mmap'] = open_mmap_file(d['file'])
    if not d['mmap']:
        logging.error('Cannot open mmap file. Quitting ...')
        return None
    d['mmap_layout'] = read_mmap_layout(d['mmap'])
    d['db_name'] = d['file'] + '.db'
    logging.info('Database name: ' + d['db_name'])
    d['db_conn'] = sqlite3.connect(d['db_name'])
    d['db_cur'] = d['db_conn'].cursor()
    
    return d

# ------------------------------------------------
# mem spy reader
# ------------------------------------------------
def init_db_mem_entry(main_dict, output_dir, hostname_prefix, spy_name):
    """
        Inits the mem entry. creates the tables, etc.
        
        @param output_dir: where to look for the pattern 
        @param hostname_prefix: The hostname
        @param spy_name: The name of the spy
        @rtype: boolean
        @return: True everything ok
                 False some errors          
    """
    main_dict['mem'] = init_db_entry(output_dir, hostname, spy_name)
    if not main_dict['mem']:            
        logging.error('Problems with initialization of mem db.')
        return False
    print(main_dict['mem'])
    
    # create a table
    main_dict['mem']['db_cur'].executescript('''
    CREATE TABLE IF NOT EXISTS mem_cap_tab (
       spy_id INTEGER, 
       ts INTEGER, 
       hostname TEXT, 
       pid INTEGER, 
       total_mem_kb INTEGER, 
       total_swap_kb INTEGER);
    
    CREATE TABLE IF NOT EXISTS mem_mon_tab (
       spy_id INTEGER, 
       ts INTEGER,
       mem_util REAL, 
       mem_buffered REAL, 
       mem_cached REAL, 
       mem_active REAL, 
       mem_inactive REAL,
       slab REAL, 
       mapped REAL, 
       swap_util REAL, 
       swap_cached REAL);
    ''')
    main_dict['mem']['db_conn'].commit()
            
    return True

def wrt_mem_hdr_entries(entry_dict, mmap_file_content):
    """
        @param entry_dict: entry from the dictionary mem or something
        @param mmap_file_content: the content of the mmapped file  
          @rtype: bool
          @return: True if everything went fine    
       
      Checks if new values are added to mmapped file 
      and if yes they are added to the respective db
    """
    layout = entry_dict['mmap_layout']
    hdr_entries = mmap_file_content[layout['hdr_hdr']:layout['hdr_total']]
    cur = entry_dict['db_cur']
    conn = entry_dict['db_conn']
    entry_count = layout['block_count']
    entry_size = layout['hdr_entry']
    tab_name = 'mem_cap_tab'
    # our index in the header area
    curr = 0
    
    for i in range(entry_count):
        # we should get an entry in the hdr part
        # 1343766614194775 6953989 kidlogin2.nics.utk.edu 17616 24676588 2031608
       
        str = hdr_entries[curr:curr + entry_size]
        
        # check if we checked all values from the table
        if str[0] == '\0':
            break
        
        # remove zeros and the rest
        str = str.rstrip(' \t\r\n\0')
        
        ts, id, hostname, pid, total_mem_kb, total_swap_kb = str.split(' ')
        
        # check if this id is in our database             
        cur.execute( 'SELECT * FROM ' + tab_name + ' WHERE spy_id=?' , (id,))
        entry = cur.fetchone()
        if not entry:
            cur.execute('INSERT INTO ' + tab_name + ' VALUES(?, ?, ?, ?, ?, ?)',
                       (id, ts, hostname, pid, total_mem_kb, total_swap_kb))
            conn.commit()
            logging.debug('New data inserted to the cap tab')
        
        # get the next value if any
        curr = curr + entry_size
    
    return True

def wrt_mem_bdy_entries(entry_dict, mmap_file_content):
    """
        @param entry_dict: entry from the dictionary mem or something
        @param mmap_file_content: the content of the mmapped file  
          @rtype: bool
          @return: True if everything went fine    
       
      Checks if new values are added to mmapped file 
      and if yes they are added to the respective db
    """

    layout = entry_dict['mmap_layout']
    entries = mmap_file_content[layout['hdr_total']+layout['bdy_hdr']:layout['total']]
    cur = entry_dict['db_cur']
    conn = entry_dict['db_conn']
    entry_count = layout['block_count']
    entry_size = layout['bdy_block_entry']
    tab_name = 'mem_mon_tab'
    
    curr = 0
    
    for i in range(entry_count):
        # we should get an entry in the hdr part
        # TIMESTAMP SPY_ID M_PERC MBUF_PERC MCACHED_PERC MACT_PERC MINACT_PERC SLAB_PERC MAPPED_PERC S_PERC SCACHED_PERC
        # 1343767391195375 6953989 97.52 0.30 43.92 72.18 17.98 5.71 0.56 0.02 0.00
        
        str = entries[curr:curr + entry_size]
        
        # check if we checked all values from the table
        if str[0] == '\0':
            break
        
        # remove zeros and the rest
        str = str.rstrip(' \t\r\n\0')
                
        ts, id, m_util, m_buff, m_cached, m_active, m_inactive, slab, mapped, s_util, s_cached = str.split(' ')
        # check if this id is in our database
        cur.execute('SELECT * FROM ' + tab_name + ' WHERE spy_id=? AND ts=?', (id,ts))
        entry = cur.fetchone()
        if not entry:
            cur.execute('INSERT INTO ' + tab_name + ' VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                       (id, ts, m_util, m_buff, m_cached, m_active, m_inactive, slab, mapped, s_util, s_cached))
            conn.commit()
            logging.debug(str.split(' '))
        else:
            logging.debug('Data NOT inserted to the mon tab. Already exists.')
        
        # get the next value if any
        curr = curr + entry_size    

    return True

# --------------------------------------------
# nvid spy reader
# -------------------------------------------

def init_db_nvid_entry(main_dict, output_dir, hostname_prefix, spy_name):
    """
        Inits the mem entry. creates the tables, etc.
        
        @param output_dir: where to look for the pattern 
        @param hostname_prefix: The hostname
        @param spy_name: the name of the spy
        @rtype: boolean
        @return: True everything ok
                 False some errors          
    """
    main_dict['nvid'] = init_db_entry(output_dir, hostname, spy_name)
    if not main_dict['nvid']:            
        logging.error('Problems with initialization of nvid db.')
        return False
    print(main_dict['nvid'])
    
    # create a table
    main_dict['nvid']['db_cur'].executescript('''
    CREATE TABLE IF NOT EXISTS nvid_cap_tab (
       spy_id INTEGER, 
       ts INTEGER, 
       hostname TEXT, 
       pid INTEGER,
       driver_ver TEXT,
       gpu_count INTEGER
       );
       
    CREATE TABLE IF NOT EXISTS nvid_gpu_cap_tab (
       spy_id INTEGER,
       gpu_id INTEGER,
       compute_mode TEXT,
       serial_number TEXT,
       prod_name TEXT,
       mem_total_MB INTEGER,
       power_limit_W INTEGER,
       max_SM_clock_MHz INTEGER,
       max_graphics_clock_MHz INTEGER,
       max_mem_clock_MHz INTEGER    
    );
    
    CREATE TABLE IF NOT EXISTS nvid_mon_tab (
       spy_id INTEGER, 
       ts INTEGER,
       gpu_id INTEGER,
       gpu_util REAL, 
       mem_util REAL, 
       mem_used_MB REAL,
       power_draw REAL,
       perf_state TEXT,
       graphics_clock REAL,
       SM_clock REAL,
       mem_clock REAL);       
    ''')
    main_dict['nvid']['db_conn'].commit()
                
    return True

def wrt_nvid_hdr_entries(entry_dict, mmap_file_content):
    """
        @param entry_dict: entry from the dictionary mem or something
        @param mmap_file_content: the content of the mmapped file  
          @rtype: bool
          @return: True if everything went fine    
       
      Checks if new values are added to mmapped file 
      and if yes it parses mmapped file and adds them to the respective db
    """
    layout = entry_dict['mmap_layout']
    hdr_entries = mmap_file_content[layout['hdr_hdr']:layout['hdr_total']]
    cur = entry_dict['db_cur']
    conn = entry_dict['db_conn']
    entry_count = layout['block_count']
    entry_size = layout['hdr_entry']
    node_tab_name = 'nvid_cap_tab'
    node_gpu_tab_name = 'nvid_gpu_cap_tab'
    
    # our index in the header area
    curr = 0
    
    for i in range(entry_count):
        # we should get an entry in the hdr part
        # 1344277840330422 464086970 kid007.nics.utk.edu 285.05.33 3
        # 0 Default 0320112014061 TeslaM2090 5375 200 1301 650 1848
        # 1 Default 0325211010289 TeslaM2090 5375 200 1301 650 1848
        # 2 Default 0325211010108 TeslaM2090 5375 200 1301 650 1848

        str = hdr_entries[curr:curr + entry_size]
        
        # check if we checked all values from the table; the not written entries
        # are written as \0
        if str[0] == '\0':
            break
        
        str = str.rstrip(' \t\r\n\0')
        # split the entry into lines
        lines = str.split('\n')
        
        # the number of lines should the number of gpus plus one
        # the number of gpus is stored in the first line (the last position)
        gpu_count = int(lines[0].split(' ')[5])
        if len(lines) != gpu_count + 1:
            logging.error('The incorrect number of lines in a mmapped file')
            return False

        # get the first line, get rid of the ending '\n'
        str = lines[0].strip('\n')
        
        ts, id, hostname, pid, driver_ver, gpu_count = str.split(' ')
        
        # check if this id is in our database             
        cur.execute( 'SELECT * FROM ' + node_tab_name + ' WHERE spy_id=?' , (id,))
        entry = cur.fetchone()
        if not entry:
            # enter the node capabilities
            cur.execute('INSERT INTO ' + node_tab_name + ' VALUES(?, ?, ?, ?, ?, ?)',
                       (id, ts, hostname, pid, driver_ver, gpu_count))
            conn.commit()
            logging.debug('New data inserted to the cap tab')
            # enter capabilities of the all gpu attached to the node
            for j in range( int(gpu_count)):
                # get rid of the end of the line
                str = lines[j+1].strip('\n')
                # 1 Default 0325211010289 TeslaM2090 5375 200 1301 650 1848
                gpu_id, compute_mode, serial_no, prod_name, mem_total, power_limit, max_SM_cl, max_graph_cl, max_mem_cl = str.split(' ')
                
                logging.debug(str)
                cur.execute('INSERT INTO ' + node_gpu_tab_name + ' VALUES(?,?,?,?,?,?,?,?,?,?)',
                        (id, gpu_id, compute_mode, serial_no, prod_name, mem_total, power_limit, max_SM_cl, max_graph_cl, max_mem_cl))
        
        # get the next value if any
        curr = curr + entry_size
    
    return True

def wrt_nvid_bdy_entries(entry_dict, mmap_file_content):
    """
        @param entry_dict: entry from the dictionary mem or something
        @param mmap_file_content: the content of the mmapped file  
          @rtype: bool
          @return: True if everything went fine    
       
      Checks if new values are added to mmapped file 
      and if yes they are added to the respective db
    """

    layout = entry_dict['mmap_layout']
    entries = mmap_file_content[layout['hdr_total']+layout['bdy_hdr']:layout['total']]
    cur = entry_dict['db_cur']
    conn = entry_dict['db_conn']
    entry_count = layout['block_count']
    entry_size = layout['bdy_block_entry']
    tab_name = 'nvid_mon_tab'
    
    curr = 0
    
    for i in range(entry_count):
        # we should get an entry in the hdr part
        # TIMESTAMP SPY_ID UTIL_GPU_PERC UTIL_MEM_PERC MEM_USED_MB
        # POWER_DRAW_PERC PERF_STATE
        # CL_GR_PERC CL_SM_PERC CL_MEM_PERC
        # GPU_ID PID_COUNT NAME_PID1 NAME_PID2 ....
        # USED_GPU_MEM_PERC
        # 1344283797934058 954420995 0.00 0.00 0.00 0.00 0.00 0.00 10 10 10
        # 14.74 14.51 14.40 P12 P12 P12
        # 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00

        str = entries[curr:curr + entry_size]
        
        # check if we checked all values from the table
        if str[0] == '\0':
            break
        
        # remove zeros
        str = str.rstrip(' \t\r\n\0')
        
        lines = str.split('\n')
        
        first_line = lines[0].strip('\n').split(' ')
        second_line = lines[1].strip(' \n').split(' ')
        third_line = lines[2].strip(' \n').split(' ')
        # count the number of gpus - remove the ts and spy_id
        # the you have 3 fields, each split per the number of gpus        
        gpu_count = (len(first_line) - 2) / 3
        
        for j in range(gpu_count):
            # check if this id is in our database
            cur.execute('SELECT * FROM ' + tab_name + ' WHERE spy_id=? AND ts=? AND gpu_id=?', (first_line[1],first_line[0], j))
            entry = cur.fetchone()
            if not entry:
                # we need to make it generic for different number of gpus attached
                # to the node
                
                logging.debug(j)
                logging.debug(first_line)
                logging.debug(second_line)
                logging.debug(third_line)
                
                # for two gpus you will have
                # 1344283797934058 954420995 0.00 0.00 0.00 0.00 10 10
                # 14.74 14.51 P12 P12
                # 0.00 0.00 0.00 0.00 0.00 0.00
                
                delta = int(gpu_count) # the distance between indices

                cur.execute('INSERT INTO ' + tab_name + ' VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                       (first_line[1], first_line[0], j, first_line[2+j], first_line[2+j+delta], first_line[2+j+2*delta],
                        second_line[j], second_line[j+delta],
                        third_line[j], third_line[j+delta], third_line[j+2*delta]))
                
                conn.commit()
                
            else:
                logging.debug('Data NOT inserted to the mon tab. Already exists.')
                
        # get the next value if any
        curr = curr + entry_size    

    return True

# --------------------------------------------
# nvml spy reader
# -------------------------------------------

def init_db_nvml_entry(main_dict, output_dir, hostname_prefix, spy_name):
    """
        Inits the mem entry. creates the tables, etc.
        
        @param output_dir: where to look for the pattern 
        @param hostname_prefix: The hostname
        @param spy_name: the name of the spy
        @rtype: boolean
        @return: True everything ok
                 False some errors          
    """
    main_dict['nvml'] = init_db_entry(output_dir, hostname, spy_name)
    if not main_dict['nvml']:            
        logging.error('Problems with initialization of nvml db.')
        return False
    print(main_dict['nvml'])
    
    # create a table
    main_dict['nvml']['db_cur'].executescript('''
    CREATE TABLE IF NOT EXISTS nvml_cap_tab (
       spy_id INTEGER, 
       ts INTEGER, 
       hostname TEXT, 
       pid INTEGER,
       driver_ver TEXT,
       gpu_count INTEGER
       );
       
    CREATE TABLE IF NOT EXISTS nvml_gpu_cap_tab (
       spy_id INTEGER,
       gpu_id INTEGER,
       compute_mode TEXT,
       serial_number TEXT,
       prod_name TEXT,
       mem_total_MB INTEGER,
       power_limit_W INTEGER,
       max_SM_clock_MHz INTEGER,
       max_graphics_clock_MHz INTEGER,
       max_mem_clock_MHz INTEGER    
    );
    
    CREATE TABLE IF NOT EXISTS nvml_mon_tab (
       spy_id INTEGER, 
       ts INTEGER,
       gpu_id INTEGER,
       gpu_util REAL, 
       mem_util REAL, 
       mem_used_MB REAL,
       power_draw REAL,
       perf_state TEXT,
       graphics_clock REAL,
       SM_clock REAL,
       mem_clock REAL);       
    ''')
    main_dict['nvml']['db_conn'].commit()
                
    return True

def wrt_nvml_hdr_entries(entry_dict, mmap_file_content):
    """
        @param entry_dict: entry from the dictionary mem or something
        @param mmap_file_content: the content of the mmapped file  
          @rtype: bool
          @return: True if everything went fine    
       
      Checks if new values are added to mmapped file 
      and if yes it parses mmapped file and adds them to the respective db
    """
    layout = entry_dict['mmap_layout']
    hdr_entries = mmap_file_content[layout['hdr_hdr']:layout['hdr_total']]
    cur = entry_dict['db_cur']
    conn = entry_dict['db_conn']
    entry_count = layout['block_count']
    entry_size = layout['hdr_entry']
    node_tab_name = 'nvml_cap_tab'
    node_gpu_tab_name = 'nvml_gpu_cap_tab'
    
    # our index in the header area
    curr = 0
    
    for i in range(entry_count):
        # we should get an entry in the hdr part
        # 1344277840330422 464086970 kid007.nics.utk.edu 285.05.33 3
        # 0 Default 0320112014061 TeslaM2090 5375 200 1301 650 1848
        # 1 Default 0325211010289 TeslaM2090 5375 200 1301 650 1848
        # 2 Default 0325211010108 TeslaM2090 5375 200 1301 650 1848

        str = hdr_entries[curr:curr + entry_size]
        
        # check if we checked all values from the table; the not written entries
        # are written as \0
        if str[0] == '\0':
            break
        
        str = str.rstrip(' \t\r\n\0')
        # split the entry into lines
        lines = str.split('\n')
        
        # the number of lines should the number of gpus plus one
        # the number of gpus is stored in the first line (the last position)
        gpu_count = int(lines[0].split(' ')[5])
        if len(lines) != gpu_count + 1:
            logging.error('The incorrect number of lines in a mmapped file')
            return False

        # get the first line, get rid of the ending '\n'
        str = lines[0].strip('\n')
        
        ts, id, hostname, pid, driver_ver, gpu_count = str.split(' ')
        
        # check if this id is in our database             
        cur.execute( 'SELECT * FROM ' + node_tab_name + ' WHERE spy_id=?' , (id,))
        entry = cur.fetchone()
        if not entry:
            # enter the node capabilities
            cur.execute('INSERT INTO ' + node_tab_name + ' VALUES(?, ?, ?, ?, ?, ?)',
                       (id, ts, hostname, pid, driver_ver, gpu_count))
            conn.commit()
            logging.debug('New data inserted to the cap tab')
            # enter capabilities of the all gpu attached to the node
            for j in range( int(gpu_count)):
                # get rid of the end of the line
                str = lines[j+1].strip('\n')
                logging.debug('str: ')
                logging.debug(str)
                # 1 Default 0325211010289 TeslaM2090 5375 200 1301 650 1848
                gpu_id, compute_mode, serial_no, prod_name, mem_total, power_limit, max_SM_cl, max_graph_cl, max_mem_cl = str.split(' ')
                
                logging.debug(str)
                cur.execute('INSERT INTO ' + node_gpu_tab_name + ' VALUES(?,?,?,?,?,?,?,?,?,?)',
                        (id, gpu_id, compute_mode, serial_no, prod_name, mem_total, power_limit, max_SM_cl, max_graph_cl, max_mem_cl))
        
        # get the next value if any
        curr = curr + entry_size
    
    return True

def wrt_nvml_bdy_entries(entry_dict, mmap_file_content):
    """
        @param entry_dict: entry from the dictionary mem or something
        @param mmap_file_content: the content of the mmapped file  
          @rtype: bool
          @return: True if everything went fine    
       
      Checks if new values are added to mmapped file 
      and if yes they are added to the respective db
    """

    layout = entry_dict['mmap_layout']
    entries = mmap_file_content[layout['hdr_total']+layout['bdy_hdr']:layout['total']]
    cur = entry_dict['db_cur']
    conn = entry_dict['db_conn']
    entry_count = layout['block_count']
    entry_size = layout['bdy_block_entry']
    tab_name = 'nvml_mon_tab'
    
    curr = 0
    
    for i in range(entry_count):
        # we should get an entry in the hdr part
        # TIMESTAMP SPY_ID UTIL_GPU_PERC UTIL_MEM_PERC MEM_USED_MB
        # POWER_DRAW_PERC PERF_STATE
        # CL_GR_PERC CL_SM_PERC CL_MEM_PERC
        # GPU_ID PID_COUNT NAME_PID1 NAME_PID2 ....
        # USED_GPU_MEM_PERC
        # 1344283797934058 954420995 0.00 0.00 0.00 0.00 0.00 0.00 10 10 10
        # 14.74 14.51 14.40 P12 P12 P12
        # 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00

        str = entries[curr:curr + entry_size]
        
        # check if we checked all values from the table
        if str[0] == '\0':
            break
        
        # remove zeros
        str = str.rstrip(' \t\r\n\0')
        
        lines = str.split('\n')
        
        first_line = lines[0].strip('\n').split(' ')
        second_line = lines[1].strip(' \n').split(' ')
        third_line = lines[2].strip(' \n').split(' ')
        # count the number of gpus - remove the ts and spy_id
        # the you have 3 fields, each split per the number of gpus        
        gpu_count = (len(first_line) - 2) / 3
        
        for j in range(gpu_count):
            # check if this id is in our database
            cur.execute('SELECT * FROM ' + tab_name + ' WHERE spy_id=? AND ts=? AND gpu_id=?', (first_line[1],first_line[0], j))
            entry = cur.fetchone()
            if not entry:
                # we need to make it generic for different number of gpus attached
                # to the node
                
                logging.debug(j)
                logging.debug(first_line)
                logging.debug(second_line)
                logging.debug(third_line)
                
                # for two gpus you will have
                # 1344283797934058 954420995 0.00 0.00 0.00 0.00 10 10
                # 14.74 14.51 P12 P12
                # 0.00 0.00 0.00 0.00 0.00 0.00
                
                delta = int(gpu_count) # the distance between indices

                cur.execute('INSERT INTO ' + tab_name + ' VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                       (first_line[1], first_line[0], j, first_line[2+j], first_line[2+j+delta], first_line[2+j+2*delta],
                        second_line[j], second_line[j+delta],
                        third_line[j], third_line[j+delta], third_line[j+2*delta]))
                
                conn.commit()
            else:
                logging.debug('Data NOT inserted to the mon tab. Already exists.')
                
        # get the next value if any
        curr = curr + entry_size    

    return True


# ------------------------------------------
# net reader
# ------------------------------------------
def init_db_net_entry(main_dict, output_dir, hostname_prefix, spy_name):
    """
        Inits the net entry. creates the tables, etc.
        
        @param output_dir: where to look for the pattern 
        @param hostname_prefix: The hostname
        @param spy_name: the name of the spy
        @rtype: boolean
        @return: True everything ok
                 False some errors          
    """
    main_dict['net'] = init_db_entry(output_dir, hostname, spy_name)
    if not main_dict['net']:            
        logging.error('Problems with initialization of net db.')
        return False
    print(main_dict['net'])

    # create a table
    main_dict['net']['db_cur'].executescript('''
    CREATE TABLE IF NOT EXISTS net_cap_tab (
       spy_id INTEGER, 
       ts INTEGER, 
       hostname TEXT, 
       pid INTEGER,
       ib_count INTEGER,
       eth_count INTEGER
       );
       
    CREATE TABLE IF NOT EXISTS net_nic_cap_tab (
       spy_id INTEGER,
       nic_id INTEGER,
       nic_type TEXT,
       max_bandwidth REAL
    );
    
    CREATE TABLE IF NOT EXISTS net_mon_tab (
       spy_id INTEGER, 
       ts INTEGER,
       nic_id INTEGER,
       nic_type TEXT,
       nic_total REAL,
       nic_trans REAL,
       nic_recv  REAL
    );       
    ''')
    main_dict['net']['db_conn'].commit()
                
    return True

def wrt_net_hdr_entries(entry_dict, mmap_file_content):
    """
        @param entry_dict: entry from the dictionary net
        @param mmap_file_content: the content of the mmapped file  
          @rtype: bool
          @return: True
       
      Checks if new values are added to mmapped file 
      and if yes it parses mmapped file and adds them to the respective db
    """
    layout = entry_dict['mmap_layout']
    hdr_entries = mmap_file_content[layout['hdr_hdr']:layout['hdr_total']]
    cur = entry_dict['db_cur']
    conn = entry_dict['db_conn']
    entry_count = layout['block_count']
    entry_size = layout['hdr_entry']
    node_tab_name = 'net_cap_tab'
    node_nic_tab_name = 'net_nic_cap_tab'
    
    # our index in the header area
    curr = 0
    
    for i in range(entry_count):
        # we should get an entry in the hdr part
        # EL_COUNT H_H H_EL B_H B_BL_EL B_BL_EL_COUNT 4 200 200 200 200 1 
        # TIMESTAMP SPY_ID HOSTNAME PID IB_COUNT ETH_COUNT IB0_MBPS IB1_MPBS ... | ETH0_MBPS ETH1_MBPS
        # 1344358147470480 396681282 kidlogin2.nics.utk.edu 14250 2 2 40000.00 40000.00 | 1000.00 1000.00
        
        # remove the \n and the following NULL characters
        str = hdr_entries[curr:curr + entry_size].strip('\n')
        
        # check if we checked all values from the table; the not written entries
        # are written as \0
        if str[0] == '\0':
            break
        
        # get rid of zeros
        str = str.rstrip(' \t\r\n\0')

        # TIMESTAMP SPY_ID HOSTNAME PID IB_COUNT ETH_COUNT IB0_MBPS IB1_MPBS ... | ETH0_MBPS ETH1_MBPS
        # 1344358147470480 396681282 kidlogin2.nics.utk.edu 14250 2 2 40000.00 40000.00 | 1000.00 1000.00
                
        caps = str.split(' ')
               
        # check if this id is in our database             
        cur.execute( 'SELECT * FROM ' + node_tab_name + ' WHERE spy_id=?' , (caps[1],))
        entry = cur.fetchone()
        if not entry:
            # enter the node capabilities
            ib_count = int(caps[4])
            eth_count = int(caps[5])
            
            cur.execute('INSERT INTO ' + node_tab_name + ' VALUES(?, ?, ?, ?, ?, ?)',
                       (caps[1], caps[0], caps[2], caps[3], ib_count, eth_count))
            conn.commit()
            logging.debug('New data inserted to the cap tab')
            # enter capabilities of the all NICs attached to the node
            # 1344358147470480 396681282 kidlogin2.nics.utk.edu 14250 2 2 40000.00 40000.00 | 1000.00 1000.00                

            # first ib
            for j in range(ib_count):
                cur.execute('INSERT INTO ' + node_nic_tab_name + ' VALUES(?,?,?,?)',
                        (caps[1], j, 'ib', caps[6+j]))
                conn.commit()
            # second eth
            for j in range(eth_count):
                cur.execute('INSERT INTO ' + node_nic_tab_name + ' VALUES(?,?,?,?)',
                        (caps[1], j, 'eth', caps[7 + ib_count +  j]))
                conn.commit()
        
        # get the next value if any
        curr = curr + entry_size
    
    return True

def wrt_net_bdy_entries(entry_dict, mmap_file_content):
    """
        @param entry_dict: entry from the dictionary mem or something
        @param mmap_file_content: the content of the mmapped file
        @param nic_counts: A tuple of two integers (ib_count, eth_count)   
          @rtype: bool
          @return: True if everything went fine    
       
      Checks if new values are added to mmapped file 
      and if yes they are added to the respective db
    """

    layout = entry_dict['mmap_layout']
    entries = mmap_file_content[layout['hdr_total']+layout['bdy_hdr']:layout['total']]
    cur = entry_dict['db_cur']
    conn = entry_dict['db_conn']
    entry_count = layout['block_count']
    entry_size = layout['bdy_block_entry']
    tab_name = 'net_mon_tab'
    
    curr = 0
    
    for i in range(entry_count):
        # we should get an entry in the hdr part

        # TIMESTAMP SPY_ID IB0 IB1 ... | ETH0 ETH1 ...
        # 3728433528 856034387 0.00 0.00 0.00 | 0.40 0.28 0.12 0.00 0.00 0.00 0.00 0.00 0.00
        str = entries[curr:curr + entry_size]
        
        # check if we checked all values from the table
        if str[0] == '\0':
            break
        
        # remove zeros
        str = str.rstrip(' \t\r\n\0')
        
        elems = str.split(' ')
        
        cur.execute('SELECT * FROM ' + tab_name + ' WHERE spy_id=? AND ts=?', (int(elems[1]), int(elems[0])))
        entry = cur.fetchone()
        if not entry:
            # add first ib readings
            j = 0
            k = 2
            while True:                
                if '|' == elems[k]:
                    break;
                
                cur.execute('INSERT INTO ' + tab_name + ' VALUES(?, ?, ?, ?, ?, ?, ?)',
                       (elems[1], elems[0], j, 'ib', elems[k], elems[k+1], elems[k+2]))
                j = j+1
                k = k + 3
                
            # now add the eth readings; get the next one after '|' abd
            # add offset for timestamp and spy_id
            eth_idx_start = k + 1
            k = eth_idx_start
            eth_count = (len(elems) - eth_idx_start)/3
            for j in range(0, eth_count):
                cur.execute('INSERT INTO ' + tab_name + ' VALUES(?, ?, ?, ?, ?, ?, ?)',
                        (elems[1], elems[0], j, 'eth', elems[k], elems[k+1], elems[k+2]))
                k = k + 3
                
            conn.commit()        
            logging.debug(str)
        else:
            logging.debug('Data NOT inserted to the mon tab. Already exists.')
                             
        # get the next value if any
        curr = curr + entry_size    

    return True

# --------------------------------
# cpu reader
# --------------------------------

def init_db_cpu_entry(main_dict, output_dir, hostname_prefix, spy_name):
    """
        Inits the mem entry. creates the tables, etc.
        
        @param output_dir: where to look for the pattern 
        @param hostname_prefix: The hostname
        @param spy_name: the name of the spy
        @rtype: boolean
        @return: True everything ok
                 False some errors          
    """
    main_dict['cpu'] = init_db_entry(output_dir, hostname, spy_name)
    if not main_dict['cpu']:            
        logging.error('Problems with initialization of cpu db.')
        return False
    print(main_dict['cpu'])

    # create a table
    main_dict['cpu']['db_cur'].executescript('''
    CREATE TABLE IF NOT EXISTS cpu_cap_tab (
       spy_id INTEGER, 
       ts INTEGER, 
       hostname TEXT, 
       pid INTEGER, 
       core_count INTEGER
       );
    
    CREATE TABLE IF NOT EXISTS cpu_mon_tab (
       spy_id INTEGER, 
       ts INTEGER,
       cpu_total_util REAL
    );
    
    CREATE TABLE IF NOT EXISTS cpu_core_mon_tab (
       spy_id INTEGER, 
       ts INTEGER,
       core_id INTEGER,
       core_util REAL 
    );      
    ''')
    main_dict['cpu']['db_conn'].commit()
            
    return True

def wrt_cpu_hdr_entries(entry_dict, mmap_file_content):
    """
        @param entry_dict: entry from the dictionary mem or something
        @param mmap_file_content: the content of the mmapped file  
          @rtype: bool
          @return: True if everything went fine    
       
      Checks if new values are added to mmapped file 
      and if yes they are added to the respective db
    """
    layout = entry_dict['mmap_layout']
    hdr_entries = mmap_file_content[layout['hdr_hdr']:layout['hdr_total']]
    cur = entry_dict['db_cur']
    conn = entry_dict['db_conn']
    entry_count = layout['block_count']
    entry_size = layout['hdr_entry']
    tab_name = 'cpu_cap_tab'
    # our index in the header area
    curr = 0
    
    for i in range(entry_count):
        # we should get an entry in the hdr part
        #TIMESTAMP SPY_ID HOSTNAME PID CORE_COUNT
        #1344368765359907 74061469 kidlogin2.nics.utk.edu 29694 8
        
        str = hdr_entries[curr:curr + entry_size]
        
        # check if we checked all values from the table
        if str[0] == '\0':
            break
        
        # remove zeros and the rest
        str = str.rstrip(' \t\r\n\0')
        
        elems = str.split(' ')
                        
        # check if this id is in our database             
        cur.execute( 'SELECT * FROM ' + tab_name + ' WHERE spy_id=?' , (elems[1],))
        entry = cur.fetchone()
        if not entry:
            cur.execute('INSERT INTO ' + tab_name + ' VALUES(?, ?, ?, ?, ?)',
                        (elems[1], elems[0], elems[2], elems[3], elems[4]))
            conn.commit()
            logging.debug('New data inserted to the cap tab')
        
        # get the next value if any
        curr = curr + entry_size
    
    return True

def wrt_cpu_bdy_entries(entry_dict, mmap_file_content):
    """
        @param entry_dict: entry from the dictionary mem or something
        @param mmap_file_content: the content of the mmapped file  
          @rtype: bool
          @return: True if everything went fine    
       
      Checks if new values are added to mmapped file 
      and if yes they are added to the respective db
    """

    layout = entry_dict['mmap_layout']
    entries = mmap_file_content[layout['hdr_total']+layout['bdy_hdr']:layout['total']]
    cur = entry_dict['db_cur']
    conn = entry_dict['db_conn']
    entry_count = layout['block_count']
    entry_size = layout['bdy_block_entry']
    tab_name = 'cpu_mon_tab'
    core_tab_name = 'cpu_core_mon_tab'
    
    curr = 0
    
    for i in range(entry_count):
        # we should get an entry in the hdr part
        # TIMESTAMP SPY_ID CPU0 CPU1 CPU2 CPU3 CPU4 CPU5 CPU6 CPU7 CPU8 CPU9 CPU10 CPU11 ...
        # 1344370934985249 1275941363 0.50 2.02 1.00 0.00 0.00 0.00 0.00 0.00 0.00

        # remove the \n and the following NULL characters
        str = entries[curr:curr + entry_size].strip('\n')
        
        # check if we checked all values from the table
        if str[0] == '\0':
            break
        
        # remove zeros
        str = str.rstrip(' \t\r\n\0')
        
        elems = str.split(' ')
        
        # check if this id is in our database
        cur.execute('SELECT * FROM ' + tab_name + ' WHERE spy_id=? AND ts=?', (elems[1],elems[0]))
        entry = cur.fetchone()
        if not entry:
            # first update the general global cpu utilization
            cur.execute('INSERT INTO ' + tab_name + ' VALUES(?, ?, ?)',
                       (elems[1], elems[0], elems[2]))
            
            # now update the particular core utilization
            # this is purely the specific core utilization
            core_util = elems[3:]
            
            for j in range(len(core_util)):
                cur.execute('INSERT INTO ' + core_tab_name + ' VALUES(?, ?, ?, ?)',
                            (elems[1], elems[0], j, core_util[j]))
            # save the data to the db
            conn.commit()
            logging.debug(elems)
        else:
            logging.debug('Data NOT inserted to the mon tab. Already exists.')
        
        # get the next value if any
        curr = curr + entry_size    

    return True

# --------------------------------
# lynx reader
# --------------------------------

def init_db_lynx_entry(main_dict, output_dir, hostname_prefix, spy_name):
    """
        Inits the lynx entry. creates the tables, etc.
        
        @param output_dir: where to look for the pattern 
        @param hostname_prefix: The hostname
        @param spy_name: the name of the spy
        @rtype: boolean
        @return: True everything ok
                 False some errors          
    """
    main_dict['lynx'] = init_db_entry(output_dir, hostname, spy_name)
    if not main_dict['lynx']:            
        logging.error('Problems with initialization of lynx db.')
        return False
    print(main_dict['lynx'])

    # create a table
    main_dict['lynx']['db_cur'].executescript('''
    CREATE TABLE IF NOT EXISTS lynx_cap_tab (
       spy_id INTEGER, 
       ts INTEGER, 
       hostname TEXT, 
       pid INTEGER, 
       qname TEXT
       );
    
    CREATE TABLE IF NOT EXISTS lynx_mon_tab (
       spy_id INTEGER, 
       ts INTEGER,
       pid INTEGER,
       device INTEGER,
       name TEXT,
       mask INTEGER,
       kernel_rt REAL,
       me_metric REAL,
       dynamicHalfWarps INTEGER,
       memTransactions INTEGER,
       instruction_count REAL,
       bd_metric REAL,
       totalBranches BIGINT,
       divergentBranches BIGINT,
       activeThreads INTEGER,
       maxThreads INTEGER,
       warps INTEGER,
       barriers INTEGER
    );
    
    ''')
    main_dict['lynx']['db_conn'].commit()
            
    return True

def wrt_lynx_hdr_entries(entry_dict, mmap_file_content):
    """
        @param entry_dict: entry from the dictionary mem or something
        @param mmap_file_content: the content of the mmapped file  
          @rtype: bool
          @return: True if everything went fine    
       
      Checks if new values are added to mmapped file 
      and if yes they are added to the respective db
    """
    layout = entry_dict['mmap_layout']
    hdr_entries = mmap_file_content[layout['hdr_hdr']:layout['hdr_total']]
    cur = entry_dict['db_cur']
    conn = entry_dict['db_conn']
    entry_count = layout['block_count']
    entry_size = layout['hdr_entry']
    tab_name = 'lynx_cap_tab'
    # our index in the header area
    curr = 0
    
    for i in range(entry_count):
        # we should get an entry in the hdr part
        # TIMESTAMP SPY_ID HOSTNAME PID MQ_NAME
        # 1385439460037776 1226257180 kid003 1659 /lynx

        str = hdr_entries[curr:curr + entry_size]
        
        # check if we checked all values from the table
        if str[0] == '\0':
            break
        
        # remove zeros and the rest
        str = str.rstrip(' \t\r\n\0')
        
        elems = str.split(' ')
                        
        # check if this id is in our database             
        cur.execute( 'SELECT * FROM ' + tab_name + ' WHERE spy_id=?' , (elems[1],))
        entry = cur.fetchone()
        if not entry:
            cur.execute('INSERT INTO ' + tab_name + ' VALUES(?, ?, ?, ?, ?)',
                        (elems[1], elems[0], elems[2], elems[3], elems[4]))
            conn.commit()
            logging.debug('New data inserted to the cap tab')
        
        # get the next value if any
        curr = curr + entry_size
    
    return True

def wrt_lynx_bdy_entries(entry_dict, mmap_file_content):
    """
        @param entry_dict: entry from the dictionary mem or something
        @param mmap_file_content: the content of the mmapped file  
          @rtype: bool
          @return: True if everything went fine    
       
      Checks if new values are added to mmapped file 
      and if yes they are added to the respective db
    """

    layout = entry_dict['mmap_layout']
    entries = mmap_file_content[layout['hdr_total']+layout['bdy_hdr']:layout['total']]
    cur = entry_dict['db_cur']
    conn = entry_dict['db_conn']
    entry_count = layout['block_count']
    entry_size = layout['bdy_block_entry']
    tab_name = 'lynx_mon_tab'
    
    curr = 0
    
    for i in range(entry_count):
        # we should get an entry in the hdr part
        # TIMESTAMP SPY_ID PID DEV_ID NAME TYPE KERNEL_RT MEM_EFF_METRIC ME_H_WARPS ME_MEM_TRANS INST_COUNT BRA_DIV_METRIC BD_TOTAL_BRA BD_DIV_BRA ACT_THREADS AF_MAX_THR AF_WARPS BARRIERS
        # 1385439646037786 1226257180 0 0  0 0.00000 0.00000 0 0 0.00 0.00000 0 0 0 0 0 0

        # remove the \n and the following NULL characters
        str = entries[curr:curr + entry_size].strip('\n')
        
        # check if we checked all values from the table
        if str[0] == '\0':
            break
        
        # remove zeros
        str = str.rstrip(' \t\r\n\0')
        
        elems = str.split(' ')
        
        # check if this id is in our database
        cur.execute('SELECT * FROM ' + tab_name + ' WHERE spy_id=? AND ts=? AND pid=? AND '
                    'device=? AND name=? AND mask=?', (elems[1],elems[0], elems[2], elems[3], elems[4], elems[5]))
        entry = cur.fetchone()
        if not entry:
            # first update the general global cpu utilization
            cur.execute('INSERT INTO ' + tab_name + ' VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                       (elems[1], elems[0], elems[2], elems[3], elems[4],
                        elems[5], elems[6], elems[7], elems[8], elems[9],
                        elems[10], elems[11], elems[12], elems[13], elems[14],
                        elems[15], elems[16], elems[17]))
            
            # save the data to the db
            conn.commit()
            logging.debug(elems)
        else:
            logging.debug('Data NOT inserted to the mon tab. Already exists.')
        
        # get the next value if any
        curr = curr + entry_size    

    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='A common reader that reads ' 
            'data from memory mapped files for'
            ' cpu, gpu, net, and mem spies and inserts the data to a database. '
            'The policy is to read the latest log files in a specified path',
            epilog='E.g. python3 %(prog)s --cpu --mem --net --nvid --nvml')
    parser.add_argument('--db-dir-path',  
            help='The path (no file name) to the location where '
            'db file will be written. If unspecified it is written to the mon log dir',
             default=None)
    parser.add_argument('--cpu', action='store_true', help='enable obtaining data'
                    ' from cpu' )
    parser.add_argument('--mem', action='store_true', help='enable obtaining data'
                    ' from mem' )
    parser.add_argument('--net', action='store_true', help='enable obtaining data'
                    ' from net' )
    parser.add_argument('--nvid', action='store_true', help='enable obtaining data'
                    ' from nvid' )
    parser.add_argument('--nvml', action='store_true', help='enable obtaining data'
                    ' from nvml' )
    parser.add_argument('--lynx', action='store_true', help='enable obtaining data'
                    ' from lynx')
    parser.add_argument('--ini', default='../../config/cw_ex.ini',
                    help='The configuration ini file for the current monitor. This'
                    ' file is required since it contains the hostname where the '
                    ' ranger is executed. This name is necessary to determine '
                    ' which log to open')
    parser.add_argument('--logging-level', default='debug', help='The level of logging.'
                    ' Default is the lowest possible. Acceptable values: debug, '
                    'info, warning, error, critical. If the wrong value will be '
                    'provided the default value is set.')
    
    args = parser.parse_args()
    
    # get the debug level, set logging.DEBUG if the incorrect value is read
    ll = common.misc_utils.str2log_level(args.logging_level)
    if not ll:
        ll=logging.DEBUG
    logging.basicConfig(level=ll)
    
    logging.info('CALL PARAMETERS')
    logging.info(args)
    
    # check if ini file exists
    if not os.path.exists(args.ini):
        logging.error('Cannot access the ini file. Quitting ...')
        sys.exit(1)
    
    # check if hostname and output_dir options exist in the ini file    
    cfg = ConfigParser.ConfigParser()
    cfg.read(args.ini)
    if not CFG_RANGER_SEC in cfg.sections() or \
       not CFG_RANGER_SEC_HOSTNAME in cfg.options(CFG_RANGER_SEC) or \
       not CFG_RANGER_SEC_OUTPUT_DIR in cfg.options(CFG_RANGER_SEC):
        logging.error('No section defined in the ini file. Quitting ...')
        sys.exit(1)
    
    hostname = cfg.get(CFG_RANGER_SEC, CFG_RANGER_SEC_HOSTNAME)
    output_dir = cfg.get(CFG_RANGER_SEC, CFG_RANGER_SEC_OUTPUT_DIR)

    # check if output_dir file exists
    if not os.path.exists(output_dir):
        logging.error('Cannot access output dir. Quitting ...')
        sys.exit(1)

    logging.info(os.path.basename(args.ini) + ': hostname: ' + hostname)
    logging.info(os.path.basename(args.ini) + ': output_dir: ' + output_dir)

    # check the db output dir
    if args.db_dir_path:
        if not os.path.exists(args.db_dir_path):
            logging.error('Database output dir does not exist. Quitting ...')
            sys.exit(1)
    else:
        # by default it is the same directory as the directory for outputting
        # mon log files
        args.db_dir_path = output_dir
    
    logging.info('Database output dir: ' + args.db_dir_path)
    
    db_dict = {'cpu' : {}, 'mem' : {}, 'net' : {}, 'nvid' : {}, 'nvml' : {},
               'lynx' : {} }
    
    # init databases
    
    # this should be implemented as threads and the following
    # loop so I have threads dealing with particular mmapped files
    
    # now just try to re-initialize the file; dirty hack
    init_tab = {'cpu' : False, 'mem' : False, 'net' : False, 'nvid': False, 'nvml' : False,
                'lynx' : False}
    
    # repeat as long as all_db are not initialized
    while(True):
        # this is my indicator to see if all entries have been initialized
        all_init = True;
    
        if args.mem and not init_tab['mem']:
            if not init_db_mem_entry(db_dict, output_dir, hostname, CFG_MEM_SPY):
                all_init = False
                logging.error('Problems with initialization of mem db. Will retry ...')
            else:
                init_tab['mem'] = True
        if args.nvid and not init_tab['nvid']:
            if not init_db_nvid_entry(db_dict, output_dir, hostname, CFG_NVID_SPY):
                logging.error('Problems with initialization of nvid db. Will retry ...')
                all_init = False
            else:
                init_tab['nvid'] = True
        if args.nvml and not init_tab['nvml']:
            if not init_db_nvml_entry(db_dict, output_dir, hostname, CFG_NVML_SPY):
                logging.error('Problems with initialization of nvml db. Will retry ...')
                all_init = False
            else:
                init_tab['nvml'] = True
        if args.net and not init_tab['net']:
            if not init_db_net_entry(db_dict, output_dir, hostname, CFG_NET_SPY):
                logging.error('Problems with initialization of net db. Will retry ...')
                all_init = False
            else:
                init_tab['net'] = True
        if args.cpu and not init_tab['cpu']:
            if not init_db_cpu_entry(db_dict, output_dir, hostname, CFG_CPU_SPY):
                logging.error('Problems with initialization of net db. Will retry ...')
                all_init = False
            else:
                init_tab['cpu'] = True
        if args.lynx and not init_tab['lynx']:
            if not init_db_lynx_entry(db_dict, output_dir, hostname, CFG_LYNX_SPY):
                logging.error('Problems with initialization of lynx db. Will retry ...')
                all_init = False
            else:
                init_tab['lynx'] = True
        if all_init:
            # yes everything is initialized
            break
            
    
    # the main loop
    while True:
        if args.mem:
            curr_dict = db_dict['mem']
            mmap_handler = curr_dict['mmap']
            mmap_handler.seek(0)
            mem_file_content = mmap_handler.read(curr_dict['mmap_layout']['total'])
            # now write it to the db
            wrt_mem_hdr_entries(curr_dict, mem_file_content)
            wrt_mem_bdy_entries(curr_dict, mem_file_content)
        if args.nvid:            
            curr_dict = db_dict['nvid']
            mmap_handler = curr_dict['mmap']
            mmap_handler.seek(0)
            mem_file_content = mmap_handler.read(curr_dict['mmap_layout']['total'])
            wrt_nvid_hdr_entries(curr_dict, mem_file_content)
            wrt_nvid_bdy_entries(curr_dict, mem_file_content)
        if args.nvml:            
            curr_dict = db_dict['nvml']
            mmap_handler = curr_dict['mmap']
            mmap_handler.seek(0)
            mem_file_content = mmap_handler.read(curr_dict['mmap_layout']['total'])
            wrt_nvml_hdr_entries(curr_dict, mem_file_content)
            wrt_nvml_bdy_entries(curr_dict, mem_file_content)
        if args.net:
            curr_dict = db_dict['net']
            mmap_handler = curr_dict['mmap']
            mmap_handler.seek(0)
            mem_file_content = mmap_handler.read(curr_dict['mmap_layout']['total'])
            wrt_net_hdr_entries(curr_dict, mem_file_content)            
            wrt_net_bdy_entries(curr_dict, mem_file_content)
        if args.cpu:
            curr_dict = db_dict['cpu']
            mmap_handler = curr_dict['mmap']
            mmap_handler.seek(0)
            mem_file_content = mmap_handler.read(curr_dict['mmap_layout']['total'])
            wrt_cpu_hdr_entries(curr_dict, mem_file_content)            
            wrt_cpu_bdy_entries(curr_dict, mem_file_content)
        if args.lynx:
            curr_dict = db_dict['lynx']
            mmap_handler = curr_dict['mmap']
            mmap_handler.seek(0)
            lynx_file_content = mmap_handler.read(curr_dict['mmap_layout']['total'])
            wrt_lynx_hdr_entries(curr_dict, lynx_file_content)
            wrt_lynx_bdy_entries(curr_dict, lynx_file_content)
        
        time.sleep(1)
    # never should be executed
    m.close()
    cursor.close()    
    

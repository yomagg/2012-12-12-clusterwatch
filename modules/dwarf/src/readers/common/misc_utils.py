"""
	@file misc_utils.py
	@date Aug 3, 2012
	@author Magdalena Slawinska a.k.a. Magic Magg magg dot gatech @ gmail dot com
	
	Utilities for python modules
"""
import logging
import mmap

def str2log_level(str):
	"""
	Converts a specified string to an appropriate logging level
	@param str: The string to be converted
	@return: a logging LEVEL corresponding to the str or None if the string
	         doesn't correspond to any debug level
	"""
	ll = None
	if str == 'debug':
		ll=logging.DEBUG
	elif str == 'info':
		ll=logging.INFO
	elif str == 'warning':
		ll=logging.WARNING
	elif str == 'error':
		ll=logging.ERROR
	
	return ll


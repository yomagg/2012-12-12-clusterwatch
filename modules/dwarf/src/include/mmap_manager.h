/**
 * @file mmap_manager.h
 *
 * @date Jul 8, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef MMAP_MANAGER_H_
#define MMAP_MANAGER_H_

#include <glib.h>
#include "mmap_manager_types.h"

// functions (public interface)

extern diag_t bhb_mmap_init_first(struct bhb_mmap_ctx *p_ctx, struct bhb_mmap_param *p_params);
extern diag_t bhb_mmap_init_pointers(struct bhb_mmap_ctx *p_ctx, struct spy_ctx *p_spy_ctx);
extern diag_t bhb_mmap_wrt_hdr_header(struct bhb_mmap_ctx *p_ctx, char *str);
extern diag_t bhb_mmap_wrt_hdr_entry(struct bhb_mmap_ctx *p_ctx, char *str, int id);
extern diag_t bhb_mmap_wrt_bdy_header(struct bhb_mmap_ctx *p_ctx, char *str);
extern diag_t bhb_mmap_wrt_bdy_entry(struct bhb_mmap_ctx *p_ctx, char *str, int id);
extern diag_t bhb_mmap_dismiss(struct bhb_mmap_ctx *p_ctx);
extern diag_t bhb_mmap_get_total_size(struct bhb_mmap_ctx *p_ctx, size_t *size);
extern diag_t bhb_mmap_layout_2_str(struct bhb_mmap_ctx *p_ctx, GString *str);


#endif /* MMAP_MANAGER_H_ */

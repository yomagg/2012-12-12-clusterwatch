/**
 * @file nvml_spy_torso_types.h
 *
 * @date Jan 30, 2013
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef NVML_SPY_TORSO_TYPES_H_
#define NVML_SPY_TORSO_TYPES_H_

#include <sys/time.h>
#include "mmap_manager.h"
#include <nvml.h>

// the nvml mmap parameters

//! the number of entries in the index of mmap file for cpu
//! one node has one entry; on Keeneland we have max 120 that's why
//! I put this number here; on KFS we have 256 but operational is typically 239
#define NVML_MMAP_BLOCK_COUNT 256
//! the size of the header for the header
#define NVML_MMAP_HDR_HDR_SIZE_BYTES 250
//! the size of the entry in the header
#define NVML_MMAP_HDR_ENTRY_SIZE_BYTES 300
//! the size of the body header
#define NVML_MMAP_BODY_HDR_SIZE_BYTES 200
//! the size of the entry for the particular block
#define NVML_MMAP_BLOCK_ENTRY_SIZE_BYTES 200
//! the number of elements in the block
#define NVML_MMAP_BLOCK_ENTRY_COUNT 1


//! the size in bytes for the character array that holds the
//! performance state
#define PERF_STATE_STR_LEN 5


// a few constants for maintaining the processes, right now not used

//! the maximum number of processors
#define NVML_MAX_PIDS_COUNT 10
//! the maximum len for the string (including \0)
#define NVML_MAX_PROC_NAME_LEN 20


// constants defined in nvmlConstants::
//#define NVML_DEVICE_INFOROM_VERSION_BUFFER_SIZE 16
//#define NVML_DEVICE_UUID_BUFFER_SIZE 80
//! should correspond to #define NVML_SYSTEM_DRIVER_VERSION_BUFFER_SIZE 80
#define DWARF_NVML_SYSTEM_DRIVER_VERSION_BUFFER_SIZE 80
//#define NVML_SYSTEM_NVML_VERSION_BUFFER_SIZE 80
//! should correspond to #define NVML_DEVICE_NAME_BUFFER_SIZE 64
#define DWARF_NVML_DEVICE_NAME_BUFFER_SIZE 64
//! should correspond to #define NVML_DEVICE_SERIAL_BUFFER_SIZE 30
#define DWARF_NVML_DEVICE_SERIAL_BUFFER_SIZE 30
//#define NVML_DEVICE_VBIOS_VERSION_BUFFER_SIZE 32

// enum nvmlComputeMode_t mapping to string 
// TODO if you are done then change it to the Number
#define DWARF_CMPT_MODE_DEFAULT "default"
#define DWARF_CMPT_MODE_EXCLUSIVE_THREAD "exclusive_thread"
#define DWARF_CMPT_MODE_PROHIBITED "prohibited"
#define DWARF_CMPT_MODE_EXCLUSIVE_PROCESS "exclusive_process"
#define DWARF_CMPT_MODE_UNKNOWN "unknown"

// default values if the feature is not supported on the gpu
#define DWARF_UNSPPRTED_POWER_LIMIT 1.0
#define DWARF_UNSPPRTED_MAX_CLOCK 1
#define DWARF_UNSPPRTED_SERIAL_NUMBER "000000"


// -----------------------------------------------
// structures that will be sent from a spy to an aggregator
// ------------------------------------------------
/**
 * describes the capabilities of the device
 */
struct nvml_dev_cap {
	// nvmlConstants::NVML_DEVICE_NAME_BUFFER_SIZE=64
	char product_name[DWARF_NVML_DEVICE_NAME_BUFFER_SIZE];
	//! This number matches the serial number physically printed on each board.
	//! It  is  a  globally unique immutable alphanumeric value.
	char serial_number[DWARF_NVML_DEVICE_SERIAL_BUFFER_SIZE];
	//! the compute mode
	char compute_mode[20];
	//! an id assigned to this device by us
	int id;
	//! memory total in MB
	int memory_total;
	//! power limit in W
	int power_limit;
	//! max clocks in MHz
	int max_graphics_clock;
	int max_SM_clock;
	int max_memory_clock;
};
/**
 * These are capabilities of the NVIDIA device based on nvidia-smi command
 * by parsing the output
 */
struct nvml_cap {
	//! the timestamp when we took the data
	struct timeval ts;
	//! the identity of the node where the gpu devices are attached
	struct identity id;
	//! the driver version  80-characters
	char driver_ver[DWARF_NVML_SYSTEM_DRIVER_VERSION_BUFFER_SIZE];
	//! how many gpus are attached to the node
	int	 gpu_count;
	//! the array of devices
	struct nvml_dev_cap *devs;
};

/**
 * helpful structure for monitoring
 */
struct nvml_dev_process {
	//! how many processes share gpu do we have
	int pid_count;
	//! the pids array
	long *pids;
	//! names of processes
	char ** processes_names;
	//! used_gpu_mem w.r.t. to the total mem in percentage
	float *used_gpu_mem;
};

/**
 * The structure that will be sent from a collector to an aggregator with monitoring
 * data
 *
 * the monitored data per gpu device; this corresponds to what I can get from
 * nvml
 *
 */
struct nvml_mon {
	//! the timestamp of the measurement
	struct timeval ts;
	//! the id of the node where the measurement is going to be
	int id;
	//! the number of gpu devices attached to a node
	int gpu_count;

	//! stats arrays

	//! the performance state; the other forms of declaration like char *a[]
	//! cause the compilation error; it should work if you define a typedef
	//! with typedef char perf_state_t[5]; and here perf_state_t *performance_state;
	//! and then in the description of the field
	//! {"performance_state", "integer[gpu_count][5]", sizeof(char),
	//! FMOffset(struct nvid_mon *, performance_state)},
	// TODO this will be sent as an int
	char** performance_state;

	//! as reported by Memory Usage->Used; it is strange but the
	//! Used+Free < Total in Memory Usage
	int *mem_used_MB;
	//! Utilization->Gpu in percentage
	float *util_gpu;
	//! Utilization->Memory in percentage
	float *util_mem;
	//! Power Draw / Power Limit (in percentage)
	float *power_draw;
	//! Clock Graphics / Max Clock Graphics
	float *graphics_clock;
	float *sm_clock;
	float *mem_clock;

	//! the process statistics, currently ignored
	struct nvml_dev_process *processes;
};

// -----------------------------------
// utility structures
// -----------------------------------
/**
 * The global structure that should be used in the spy module to simplify
 * organization of the data.
 */
struct nvml_glob_data {
	//! data that will be passed to the secret service
	struct nvml_mon mon;
	//! the capabilities of the device; they are currently not passed to the
	//! secret service, since we pass only mon data
	struct nvml_cap caps;

	//! this will control the mmap_ctx; connection to the memory map
	struct bhb_mmap_ctx *p_bhb_ctx;
};

#endif /* NVML_SPY_TORSO_TYPES_H_ */

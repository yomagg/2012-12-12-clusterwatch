/**
 * @file nvml_spy_torso.h
 *
 * @date Jan 30, 2013
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef NVML_SPY_TORSO_H_
#define NVML_SPY_TORSO_H_


// -----------------------------------------
// the variable for FFS usage
// ----------------------------------------
// the variables for FFS usage defined in spy.c
extern FMField timestamp_field_list[];
extern FMField identity_field_list[];
// they are defined in nvid_spy_torso.c
extern FMField nvml_cap_field_list[];
extern FMField nvml_dev_cap_field_list[];
extern FMStructDescRec nvml_cap_format_list[];

extern FMField nvml_dev_processes_field_list[];
extern FMField nvml_mon_field_list[];
extern FMStructDescRec nvml_mon_format_list[];

//! data handlers
extern void setup_nvml(void);
extern int read_nvml(void *data);
extern int process_nvml(void *data);
#endif /* NVML_SPY_TORSO_H_ */

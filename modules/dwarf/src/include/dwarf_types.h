/**
 * @file dwarf_types.h
 *
 * @date Dec 3, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef DWARF_TYPES_H_
#define DWARF_TYPES_H_

#include "evpath.h"
#include "clusterspy.h"


#include "ini_utils_types.h"
#include "misc_utils_types.h"  // diag_t



// -------------------------------
// dwarf constants
// -------------------------------
//! the name of the system
#define DWARF_NAME "Dwarf"

//! how many times the file to be mmapped should be created
#define FILE_CREATION_TRIES 1000
//! what is the extension of the mmapped file
#define LOG_FILE_EXT "log"

// -----------------------
// dwarf types
// ----------------------
struct dwarf_rt_ctx;
struct spy_ctx;
/**
 * the typedef for pointer to the initialization function
 * @param the context of the runtime
 * @param the spy context
 * @return the diagnostic output
 */
typedef diag_t (* dwarf_init_spy_func_t )(struct dwarf_rt_ctx *, struct spy_ctx *) ;


/**
 * indicates the side we are dealing with
 */
enum dwarf_side {
	COLLECTOR_SIDE,//!< the monitoring agent side
	AGGREGATOR_SIDE   //!< the aggregator side
};

/**
 * the structure that helps describe identity of the patrol
 * such as a unique id, etc
 */
struct identity {
	int 		id;			      //! for storing unique id
	char 		hostname[256];    //! for storing the name of the host
	pid_t		pid;			  //! the pid of the process
};

/**
 * The context structure is for determining if we are dealing with the
 * the monitoring agent (patrol) or the aggregator (ranger)
 *
 * This structure is used during the initialization of the system by
 * dwarf_init, and is cleared by the dwarf_dismiss
 */
struct dwarf_rt_ctx {
	enum dwarf_side side; 	 //! which side we represent
	gboolean is_initialized; //! indicates if the context has been initialized
	ini_t   ini_file;		 //! this is the ini file structure

	struct identity identity;   //! the identity of the entity (process, thread) using dwarf
	//! the table of scouts; the key is a patrol name as in ini file
	//! the value is the struct patrol_ctx that relates to this specific patrol
	GHashTable *patrol_htab;
};

/**
 * The structure to help with managing the mmap context
 */
struct mmap_ctx {
	char   *mmap_start;         //! where the region starts
	size_t  total_size;         //! what total size of the the mmap region in bytes
	size_t  elem_size;          //! the size of a single element
	size_t  elem_count;         //! how many elements is in the block
	size_t  hdr_size;			//! the size of the header in bytes
	long    idx;		        //! current index
	char	*curr_entry;		//! current entry
	gchar *filename_mapped;     //! the path to the mapped file name
};



/**
 * The structure holds pointers to the functions you need to implement
 * for the collector and for the aggregator side to be able to use
 * the evpath overlay
 */
struct evpath_overlay_ctx {
	//! pointer to the format list
	FMStructDescRec *format_list;
	//! a resource type
	ResourceType res_type;
	//! the size of the structure that will be sent over the evpath overlay
	int data_size;
	//! the frequence in sec
	int period_sec;

	// -----------------------
	// collector-side functions
	// -----------------------

	//! initialization of the collector
	void (*clctor_setup_func)(void);
	//! reading
	//! @param data that will be sent over EVPATH, I guess memory for the data
    //!        have been allocated accordingly
    //! @return 0 - you were not able to read anything
    //!         > 0 you have data to be sent; successful read
	int (*clctor_read_func)(void *data);
	//! processing
	//! @param data Data sent over EVPath
	//! @return 0  success
	//!         anything else some issues (should not happen)
	int (*clctor_process_func)(void *data);

	// -----------------------
	// aggregator-side functions;
	// TODO not sure if all functions make sense for the aggregator
	// maybe reading doesn't make sense, need to look into transport/evpath
	// ----------------------

	//! initialization
	void (*agrtor_setup_func)(void);
	//! reading
	//! @param data that will be sent over EVPATH, I guess memory for the data
    //!        have been allocated accordingly
    //! @return 0 - you were not able to read anything
    //!         > 0 you have data to be sent; successful read
	int (*agrtor_read_func)(void *data);
	//! processing
	//! @param data Data sent over EVPath
	//! @return 0  success
	//!         anything else some issues (should not happen)
	int (*agrtor_process_func)(void *data);
};

/**
 * The structure that is maintained by the runtime context to have information
 * about available spies in the system. The structure is used runtime context's
 * patrol htab by both the collector and aggregator side
 */
struct spy_ctx {
	// ----------------------------------
	// this is common for the collector and aggregator side
	// TODO not sure that this is common for both
	// ----------------------------------

	gchar    *name;			//! the name of the spy
	gboolean is_enabled;   	//! indicates if the scout is enabled or not

	//! the context for the evpath transport for sending the
	struct evpath_overlay_ctx evpath_ctx_cap;
	//! the context for the evpath transport for the monitoring data
	struct evpath_overlay_ctx evpath_ctx_mon;


	//! it is like a constructor of this structure
	dwarf_init_spy_func_t init_spy_ctx_func;

	// -----------------------------------
	// this is relevant for the collector
	// -----------------------------------

	//! the pointer to the function that should be called when you want
	//! to init a spy; set to NULL if you are not interested in any initialization
	dwarf_init_spy_func_t init_spy_func;

	// ----------------------------------
	// this is relevant only for the aggregator
	// ----------------------------------

	//! the pointer to the function that should be called when you want
	//! to init a spy; set to NULL if you are not interested in any initialization
	dwarf_init_spy_func_t init_ranger_func;

	//! the context to the memory mapping
	struct mmap_ctx mem_map;
};

/**
 * The structure that can be used to organize mmap file that consists of
 * a header and a body for a more complex patrol
 */
struct mmap_complex_ctx{
	//! the header of the context
	struct mmap_ctx header;
	//! the body of the context
	struct mmap_ctx body;
};

/**
 * the convenient structure mainly for deployment
 * from the ini file for dwarf_patrol.c and dwarf_ranger.c
 */
struct dwarf_spy_ident{
	char *spy_name;   //! the name of the spy
	dwarf_init_spy_func_t init_spy_ctx_func; //! the pointer to the spy initialization function
};

#endif /* DWARF_TYPES_H_ */

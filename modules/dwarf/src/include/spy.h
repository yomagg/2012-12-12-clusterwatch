/**
 * @file spy.h
 *
 * @date Sep 14, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef SPY_H_
#define SPY_H_

// the variables for FFS usage;
extern FMField timestamp_field_list[];
extern FMField identity_field_list[];

#endif /* SPY_H_ */

/**
 * @file cpu_spy_torso_types.h
 *
 * @date Sep 25, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef CPU_SPY_TORSO_TYPES_H_
#define CPU_SPY_TORSO_TYPES_H_

#include <sys/time.h>
#include "mmap_manager.h"

// ----------------------------
// dwarf runtime related functions
// ----------------------------
//! the number of entries in the index of mmap file for cpu
//! one node has one entry; on Keeneland we have max 120 that's why
//! I put this number here; on KFS we have 256 but operational is typically 239
#define CPU_MMAP_BLOCK_COUNT 256
//! the size of the header for the header
#define CPU_MMAP_HDR_HDR_SIZE_BYTES 200
//! the size of the entry from the cpu
#define CPU_MMAP_HDR_ENTRY_SIZE_BYTES 200
//! the size of the body header
#define CPU_MMAP_BODY_HDR_SIZE_BYTES 200
//! the size of the entry for the particular block
#define CPU_MMAP_BLOCK_ENTRY_SIZE_BYTES 200
//! the number of elements in the block
#define CPU_MMAP_BLOCK_ENTRY_COUNT 1


struct dwarf_cpu_ctrl {
	//! this will control the mmap_ctx
	struct bhb_mmap_ctx *p_bhb_ctx;
};

// ---------------------------
// EVPATH transport related structures
// -----------------------------
//! the value of that will be inserted if NAN value is detected; should be a float
#define IN_CASE_NAN 0.0

// --------------------------------
// structures that are used by evpath
// --------------------------------
/**
 * The structure that describes the cpu capabilities
 * This structure will be sent only once
 */
struct cpu_cap {
	//! the timestamp when we started measurement
	struct timeval	ts;
	//! the identity of the cpu_scout
	struct identity id;
	//! the cpu capabilities
	int 	core_count;
};

/**
 * the record that contains monitoring data sent from scouts to troopers
 */
struct cpu_mon {
	//! the id of the sending entity
	int	id;
	//! the timestamp when the measuremnt has occurred
	struct timeval ts;

	//! the number of cores plus 1 (for the total cpu usage)
	int core_plus_one_count;
	//! an array of size core_plus_one_count, that contains cpu usage 0..1,
	//! the size includes the total cpu usage as reported by /proc/stat
	//! the first element will contain the total usage as reported by
	//! /proc/stat; this is a kind of a weird thing since I often got
	//! 100%, whereas the entire system is almost all idle, this requires
	//! more in-depth investigation
	float * cpu_and_core_usage;
};

// ----------------- internal structure

/**
 * these are the fields we will read from /proc/stat; read man proc
 * for more information
 * This is the excerpt from 'man proc'
 *
 * /proc/stat
 *             kernel/system  statistics.   Varies  with   architecture.
 *             Common entries include:
 *
 *             cpu  3357 0 4313 1362393
 *                    The  amount  of time, measured in units of USER_HZ
 *                    (1/100ths of a second on most architectures), that
 *                    the  system spent in user mode, user mode with low
 *                    priority (nice), system mode, and the  idle  task,
 *                    respectively.   The  last  value should be USER_HZ
 *                   times the second entry in the uptime  pseudo-file.
 *
 *                    In  Linux  2.6 this line includes three additional
 *                    columns: iowait - time waiting for I/O to complete
 *                    (since  2.5.41);  irq  - time servicing interrupts
 *                    (since  2.6.0-test4);  softirq  -  time  servicing
 *                    softirqs (since 2.6.0-test4).
 *
 * the example /proc/stat from keeneland compute node
 * cpu [user] [nice] [system] [idle] [user-hz] [iowait] [irq] [softirq] [steal] [guest]
 *
 * user: how much time the system spent on normal processes executing in user mode
 * nice: how much time the system spent on niced processes executing in user mode
 * system: processes executing in kernel mode
 * idle: time twiddling thumbs; doing nothing
 * iowait: waiting for I/O to complete
 * irq: servicing interrupts
 * softirq: servicing softirqs
 * steal and guest - related to virtualization
 *
 * We will use: user, nice, system, idle, iowait, irq, softirq, steal;
 * We will ignore: user-hz
 * We will not take into account: guest
 *
 * cpu  180978792 93 17522592 817920438 959981 6090 16774 0
 * cpu0 17828779 8 2090399 64663121 198259 2306 1344 0
 * cpu1 17870460 4 1704097 65127976 81240 167 89 0
 * cpu2 17477701 20 1547904 65614394 138429 1024 4438 0
 * cpu3 12131185 3 783253 71839946 29289 4 79 0
 * cpu4 12125164 18 736464 71606424 309094 1315 5265 0
 * cpu5 12830291 0 1141017 70784368 27761 21 110 0
 * cpu6 22211763 3 4080023 58386967 101459 650 2793 0
 * cpu7 13974231 0 1189737 69600593 18876 3 71 0
 * cpu8 13485953 12 781184 70501333 13706 321 1150 0
 * cpu9 14251182 0 928914 69595009 8245 11 104 0
 * cpu10 13327686 20 903528 70529850 21185 246 1190 0
 * cpu11 13464392 1 1636065 69670452 12432 16 135 0
 * intr 880881258 847917465 3 0 1 25 2 0 0 1 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 24155804 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1457648 0 0 0 0 0 0 0 976807 0 0 0 0 0 0 0 722326 0 0 0 0 0 0 0 874677 0 0 0 0 0 0 0 687565 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1574107 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 30 0 0 0 0 0 0 0 0 0 390837 0 0 0 0 0 1349243 0 391600 0 0 0 0 0 0 0 383114 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 * ctxt 4151757936
 * btime 1304728728
 * processes 513523
 * procs_running 1
 * procs_blocked 0
 */
struct procfs_cpu{
	long user;
	long nice;
	long system;
	long idle;
	long iowait;
	long irq;
	long softirq;
	long steal;
};

/**
 * For the monitoring poll function
 */
struct cpu_mon_cnt_data {

	//! stores the capabilities (including the id)
	struct cpu_cap caps;

	//! the handler to the open file /proc/stat
	FILE*		p_proc_fs;

	//! data that will be passed via evpath; the cpu_mon.id and cpu_mon.timestamp
	//! is ignored
	struct cpu_mon	data;

	//! the auxilary array for storing data about the cpu_and_core_usage
	//! between subsequent readings from the /proc/stat
	//! the size is the the same as data.cores_plus_one_count;
	//! since the count of cores varies from system to system we need to
	//! do it in a dynamic way and we don't want to allocate
	//! the memory at the monitoring frequency interval as a local variable
	//! so we prepare it upfront; actually we need two such arrays for
	//! current reading and to store the past reading
	struct procfs_cpu *cpu_and_core_usage_curr_arr;
	struct procfs_cpu *cpu_and_core_usage_prev_arr;
};


#endif /* CPU_SPY_TORSO_TYPES_H_ */

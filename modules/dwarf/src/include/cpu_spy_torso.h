/**
 * @file cpu_spy_torso.h
 *
 * @date Sep 25, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef CPU_SPY_TORSO_H_
#define CPU_SPY_TORSO_H_

#include "dwarf_types.h"

// the variables for FFS usage defined in spy.c
extern FMField timestamp_field_list[];
extern FMField identity_field_list[];
// they are defined in cpu_spy_torso.c
extern FMField cpu_cap_field_list[];
extern FMStructDescRec cpu_cap_format_list[];
extern FMField cpu_mon_field_list[];
extern FMStructDescRec cpu_mon_format_list[];

// data handlers
extern int read_cap_cpu(void *data);
extern int process_cap_cpu(void *data);
extern int read_mon_cpu(void *data);
extern int process_mon_cpu(void *data);

// dwarf-rt-related functions
extern diag_t dwarf_init_cpu_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
extern diag_t dwarf_init_cpu_ranger(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
extern diag_t dwarf_init_cpu_spy(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);

#endif /* CPU_SPY_TORSO_H_ */

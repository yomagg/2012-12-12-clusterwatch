/**
 * @file misc_utils_types.h
 *
 * @date Sep 24, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef MISC_UTILS_TYPES_H_
#define MISC_UTILS_TYPES_H_

/**
 * These are the diagnostic codes
 */
typedef enum diag_ {
	DIAG_RT_UNKNOWN_SIDE_ERR = -14,
	DIAG_RT_CTX_ALREADY_INITIALIZED = -13,  //! the context has been initialized
	DIAG_RT_CTX_SPY_ENABLED = -12,
	DIAG_DWARF_ENTRY_TRUNCATED_WARN = -11,
	DIAG_DWARF_ENTRY_NOT_FOUND_WARN = -10,
	DIAG_DWARF_COUNT_OVERFLOW_ERR=-9,
	DIAG_DWARF_ENTRY_EXISTS_WARN=-8, //! the entry is in the hash table
	DIAG_RT_CTX_SPY_NOT_FOUND = -7, //! the spy name can't be found in the patrol_htab
	DIAG_RT_CTX_SPY_REGISTERED = -6, //! indicates that spy is already in htab
	DIAG_NOT_NULL_PTR_ERR = -5, //! the pointer is not null
	DIAG_NAN_ERR = -4, //! NAN reading occurred
	DIAG_MEM_ALLOC_ERR = -3, //! memory allocation errors
	DIAG_NULL_PTR_ERR = -2,
	DIAG_ERR = -1,//!< DIAG_ERR
	DIAG_OK = 0   //!< DIAG_OK
} diag_t;

#endif /* MISC_UTILS_TYPES_H_ */

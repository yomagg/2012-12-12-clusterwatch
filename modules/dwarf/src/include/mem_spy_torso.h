/**
 * @file mem_spy_torso.h
 *
 * @date Sep 14, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef MEM_SPY_TORSO_H_
#define MEM_SPY_TORSO_H_

// the variables for FFS usage defined in spy.c
extern FMField timestamp_field_list[];
extern FMField identity_field_list[];
// they are defined in mem_spy_torso.c I guess
extern FMField mem_cap_field_list[];
extern FMStructDescRec mem_cap_format_list[];
extern FMField mem_mon_field_list[];
extern FMStructDescRec mem_mon_format_list[];


// data handlers
extern int read_cap_mem(void *data);
extern int process_cap_mem(void *data);
extern int read_mon_mem(void *data);
extern int process_mon_mem(void *data);

// dwarf-rt-related functions
extern diag_t dwarf_init_mem_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
extern diag_t dwarf_init_mem_ranger(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
extern diag_t dwarf_init_mem_spy(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);


#endif /* MEM_SPY_TORSO_H_ */

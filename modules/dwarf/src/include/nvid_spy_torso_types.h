/**
 * @file nvid_spy_torso_types.h
 *
 * @date Oct 29, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef NVID_SPY_TORSO_TYPES_H_
#define NVID_SPY_TORSO_TYPES_H_

#include <sys/time.h>
#include "mmap_manager.h"

// the nvid mmap parameters

//! the number of entries in the index of mmap file for cpu
//! one node has one entry; on Keeneland we have max 120 that's why
//! I put this number here; on KFS we have 256 but operational is typically 239
#define NVID_MMAP_BLOCK_COUNT 256
//! the size of the header for the header
#define NVID_MMAP_HDR_HDR_SIZE_BYTES 250
//! the size of the entry in the header
#define NVID_MMAP_HDR_ENTRY_SIZE_BYTES 300
//! the size of the body header
#define NVID_MMAP_BODY_HDR_SIZE_BYTES 200
//! the size of the entry for the particular block
#define NVID_MMAP_BLOCK_ENTRY_SIZE_BYTES 200
//! the number of elements in the block
#define NVID_MMAP_BLOCK_ENTRY_COUNT 1


//! the command for running nvidia-smi
#define NVID_SMI_CMD "nvidia-smi -q -d MEMORY,UTILIZATION,TEMPERATURE,POWER,CLOCK,COMPUTE,PIDS,PERFORMANCE"

//! the size in bytes for the character array that holds the
//! performance state
#define PERF_STATE_STR_LEN 5

// a few constants for maintaining the processes

//! the maximum number of processors
#define NVID_MAX_PIDS_COUNT 10
//! the maximum len for the string (including \0)
#define NVID_MAX_PROC_NAME_LEN 20

// -----------------------------------------------
// structures that will be sent from a spy to an aggregator
// ------------------------------------------------
/**
 * describes the capabilities of the device
 */
struct nvid_dev_cap {
	char product_name[20];
	//! This number matches the serial number physically printed on each board.
	//! It  is  a  globally unique immutable alphanumeric value.
	char serial_number[20];
	//! the compute mode
	char compute_mode[20];
	//! an id assigned to this device by us
	int id;
	//! memory total in MB
	int memory_total;
	//! power limit in W
	int power_limit;
	//! max clocks in MHz
	int max_graphics_clock;
	int max_SM_clock;
	int max_memory_clock;
};
/**
 * These are capabilities of the NVIDIA device based on nvidia-smi command
 * by parsing the output
 */
struct nvid_cap {
	//! the timestamp when we took the data
	struct timeval ts;
	//! the identity of the node where the gpu devices are attached
	struct identity id;
	//! the driver version
	char driver_ver[20];
	//! how many gpus are attached to the node
	int	 gpu_count;
	//! the array of devices
	struct nvid_dev_cap *devs;
};

/**
 * helpful structure for monitoring
 */
struct nvid_dev_process {
	//! how many processes share gpu do we have
	int pid_count;
	//! the pids array
	long *pids;
	//! names of processes
	char ** processes_names;
	//! used_gpu_mem w.r.t. to the total mem in percentage
	float *used_gpu_mem;
};

/**
 * The structure that will be sent from a scout to a trooper with monitoring
 * data
 *
 *  * the monitored data per gpu device; this correspnonds to what
 * we can get from the nvidia-smi
 *
 *
 * nvidia-smi -q -d MEMORY,UTILIZATION,TEMPERATURE,POWER,CLOCK,COMPUTE,PIDS,PERFORMANCE -i 0

==============NVSMI LOG==============

Timestamp                       : Thu Jul 12 12:12:42 2012

Driver Version                  : 285.05.33

Attached GPUs                   : 3

 GPU 0000:11:00.0
    Performance State           : P0
    Memory Usage
        Total                   : 5375 MB
        Used                    : 393 MB
        Free                    : 4982 MB
    Compute Mode                : Default
    Utilization
        Gpu                     : 84 %
        Memory                  : 10 %
    Temperature
        Gpu                     : N/A
    Power Readings
        Power Management        : Supported
        Power Draw              : 104.07 W
        Power Limit             : 225 W
    Clocks
        Graphics                : 650 MHz
        SM                      : 1301 MHz
        Memory                  : 1848 MHz
    Max Clocks
        Graphics                : 650 MHz
        SM                      : 1301 MHz
        Memory                  : 1848 MHz
    Compute Processes
        Process ID              : 26777
            Name                : /nics/d/home/smagg/opt/lammps/lmp_kid_mv18c41f321
            Used GPU Memory     : 188 MB
        Process ID              : 26791
            Name                : /nics/d/home/smagg/opt/lammps/lmp_kid_mv18c41f321
            Used GPU Memory     : 188 MB

 *
 */
struct nvid_mon {
	//! the timestamp of the measurement
	struct timeval ts;
	//! the id of the node where the measurement is going to be
	int id;
	//! the number of gpu devices attached to a node
	int gpu_count;

	//! stats arrays

	//! the performance state; the other forms of declaration like char *a[]
	//! cause the compilation error; it should work if you define a typedef
	//! with typedef char perf_state_t[5]; and here perf_state_t *performance_state;
	//! and then in the description of the field
	//! {"performance_state", "integer[gpu_count][5]", sizeof(char),
	//! FMOffset(struct nvid_mon *, performance_state)},
	char** performance_state;

	//! as reported by Memory Usage->Used; it is strange but the
	//! Used+Free < Total in Memory Usage
	int *mem_used_MB;
	//! Utilization->Gpu in percentage
	float *util_gpu;
	//! Utilization->Memory in percentage
	float *util_mem;
	//! Power Draw / Power Limit (in percentage)
	float *power_draw;
	//! Clock Graphics / Max Clock Graphics
	float *graphics_clock;
	float *sm_clock;
	float *mem_clock;

	//! the process statistics, currently ignored
	struct nvid_dev_process *processes;
};

// -----------------------------------
// utility structures
// -----------------------------------
/**
 * The global structure that should be used in the spy module to simplify
 * organization of the data.
 */
struct nvid_glob_data {
	//! data that will be passed to the secret service
	struct nvid_mon mon;
	//! the capabilities of the device; they are currently not passed to the
	//! secret service, since we pass only mon data
	struct nvid_cap caps;

	//! this will control the mmap_ctx; connection to the memory map
	struct bhb_mmap_ctx *p_bhb_ctx;
};

#endif /* NVID_SPY_TORSO_TYPES_H_ */

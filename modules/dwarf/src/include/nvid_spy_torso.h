/**
 * @file nvid_spy_torso.h
 *
 * @date Oct 29, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef NVID_SPY_TORSO_H_
#define NVID_SPY_TORSO_H_


// -----------------------------------------
// the variable for FFS usage
// ----------------------------------------
// the variables for FFS usage defined in spy.c
extern FMField timestamp_field_list[];
extern FMField identity_field_list[];
// they are defined in nvid_spy_torso.c
extern FMField nvid_cap_field_list[];
extern FMField nvid_dev_cap_field_list[];
extern FMStructDescRec nvid_cap_format_list[];

extern FMField nvid_dev_processes_field_list[];
extern FMField nvid_mon_field_list[];
extern FMStructDescRec nvid_mon_format_list[];

//! data handlers
extern void setup_nvid(void);
extern int read_nvid(void *data);
extern int process_nvid(void *data);
#endif /* NVID_SPY_TORSO_H_ */

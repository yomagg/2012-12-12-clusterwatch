/**
 * @file net_spy_torso.h
 *
 * @date Oct 4, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef NET_SPY_TORSO_H_
#define NET_SPY_TORSO_H_

// the variables for FFS usage defined in spy.c
extern FMField timestamp_field_list[];
extern FMField identity_field_list[];
// they are defined in cpy_spy_torso.c
extern FMField net_cap_field_list[];
extern FMStructDescRec net_cap_format_list[];
extern FMField net_mon_field_list[];
extern FMStructDescRec net_mon_format_list[];

//! data handlers
extern void setup_net(void);
extern int read_net(void *data);
extern int process_net(void *data);


#endif /* NET_SPY_TORSO_H_ */

/**
 * @file net_spy_torso_types.h
 *
 * @date Oct 4, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef NET_SPY_TORSO_TYPES_H_
#define NET_SPY_TORSO_TYPES_H_

#include <sys/time.h>
#include "mmap_manager.h"
#include "dwarf_types.h"

// the network mmap parameters

//! the number of entries in the index of mmap file for net
//! one node has one entry; on Keeneland we have max 120 that's why
//! I put this number here; on KFS we have 256 but operational is typically 239
#define NET_MMAP_BLOCK_COUNT 256
//! the size of the header for the header
#define NET_MMAP_HDR_HDR_SIZE_BYTES 200
//! the size of the entry from the cpu
#define NET_MMAP_HDR_ENTRY_SIZE_BYTES 200
//! the size of the body header
#define NET_MMAP_BODY_HDR_SIZE_BYTES 200
//! the size of the entry for the particular block
#define NET_MMAP_BLOCK_ENTRY_SIZE_BYTES 200
//! the number of elements in the block
#define NET_MMAP_BLOCK_ENTRY_COUNT 1


/**
 * The structure that is needed to send capability data
 * from a scout to a trooper
 */
struct net_cap {
	//! the timestamp when the measure took place
	struct timeval ts;
	//! the identity of the net scout
	struct identity id;
	//! the number of ib interfaces
	int ib_count;
	//! the arr of mbps
	float *ib_mbps_arr;
	//! the number of eth cards
	int eth_count;
	//! the number of mbps
	float *eth_mbps_arr;
};

/**
 * The structure that will be sent from a scout to a trooper with monitoring
 * data
 */
struct net_mon {
	//! the timestamp of the measurement
	struct timeval ts;
	//! the id of the node where the measurement is going to be
	int id;
	//! the number of IB interfaces
	int ib_count;
	//! this is the data transfer rate including bytes transmitted and recv
	//! [kB/s] kilo= 1000 kilobytes per second
	float *ib_usage_arr;
	//! this is the data transfer rate including bytes transmitted [kB/s]
	float *ib_transmitted_arr;
	//! this is the data transfer rate including bytes received [kB/s]
	float *ib_recv_arr;

	//! the number of eth interfaces
	int eth_count;
	//! this is the data transfer rate including bytes transmitted and recv
	//! [kB/s] kilo= 1000 kilobytes per second
	float *eth_usage_arr;
	//! this is the data transfer rate including bytes transmitted [kB/s]
	float *eth_transmitted_arr;
	//! this is the data transfer rate including bytes received [kB/s]
	float *eth_recv_arr;
};

// -------------------------------
// utility structures
// -------------------------------
/**
 * fields found in /proc/net/dev; we will use it later for the transmit
 * and receive part (they are almost the same)
 */
struct procfs_net_fields_common {
	unsigned long bytes;
	unsigned long packets;
	unsigned long errs;
	unsigned long drop;
	unsigned long fifo;
	unsigned long compressed;
};
/**
 * The readings from /proc/net/dev
 */
struct procfs_net {
	struct procfs_net_fields_common receive;
	struct procfs_net_fields_common transmit;
	//! the timestamp of the measurement
	struct timeval ts;
};

/**
 * to store the information for particular network interfaces
 */
struct net_usage {
	struct procfs_net curr;
	struct procfs_net prev;
};
/**
 * used for getting the monitoring data by the spy
 */
struct net_mon_cnt_data {
	//! stores the capabilities of the device
	struct net_cap caps;

	//! the handler to the open file /proc/net/dev
	FILE *p_proc_fs;

	//! data that will be passed to secret service
	struct net_mon data;

	//! the auxiliary arrays to get the data from the /proc/net/dev
	//! the arrays contain net_usage (procfs_net) records for eth and ib
	struct net_usage *eth_usage;
	struct net_usage *ib_usage;

	//! this will control the mmap_ctx; connection to the memory map
	struct bhb_mmap_ctx *p_bhb_ctx;
};

/**
 * used to indicate the variant of network usage
 */
enum net_data_transfer_rate_variant {
	NET_TOTAL,      //!< NET_TOTAL, the total number of bytes
	NET_TRANSMITTED,//!< NET_TRANSMITTED, the transmitted # of bytes
	NET_RECV        //!< NET_RECV, the received number of bytes
};

#endif /* NET_SPY_TORSO_TYPES_H_ */

/**
 * @file ini_utils.h
 *
 * @date Aug 8, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 *
 * This is the public interface to ini_util.c
 */

#ifndef INI_UTILS_H_
#define INI_UTILS_H_

// general
extern diag_t ini_get_integer(gchar *ini_name, const char *group, const char *key, int *pRes);


extern diag_t ini_get_bool(gchar *ini_name, const char *group, const char *key, gboolean *pRes);
extern diag_t ini_get_str(gchar *ini_name, const char *group, const char *key, gchar **res);
extern diag_t ini_print_ini(ini_t * pIni);

// trooper interface
extern diag_t ini_get_ranger_ini(ini_t * pIni);

extern diag_t ini_get_ranger_port(GKeyFile* pKeyFile, int *res);
extern diag_t ini_set_ranger_port(const ini_t* pIni, int port);
extern diag_t ini_set_ranger_host(const ini_t* pIni);
extern diag_t ini_get_ranger_host(GKeyFile* pKeyFile, char * pHostname);
extern diag_t ini_get_ranger_freq(GKeyFile* pKeyFile, double *res);
extern diag_t ini_get_ranger_output_dir(GKeyFile* pKeyFile, gchar **res);
extern diag_t ini_get_ranger_exec_time(GKeyFile* pKeyFile, double *res);

extern diag_t ini_free_ranger(ini_t* pIni);

// scout interface
extern diag_t ini_get_patrol_ini(ini_t* pIni);

extern diag_t ini_get_patrol_freq(GKeyFile* pKeyFile, double *res);
extern diag_t ini_get_patrol_exec_time(GKeyFile* pKeyFile, double *res);

extern diag_t ini_get_patrol_lynx_spy_q_name(GKeyFile* pKeyFile, char * pBuf);
extern diag_t ini_set_patrol_lynx_spy_q_max_msg_size(const ini_t* pIni, int msg_size);
extern diag_t ini_set_patrol_lynx_spy_q_name(const ini_t* pIni);
extern diag_t ini_get_patrol_lynx_spy_q_max_msg_count(GKeyFile* 	pKeyFile, int *res);
extern diag_t ini_get_patrol_lynx_spy_q_max_msg_size(GKeyFile* 	pKeyFile, int *res);


extern diag_t ini_free_patrol(ini_t* pIni);

// misc
extern GString* get_date_string(void);


#endif /* INI_UTILS_H_ */

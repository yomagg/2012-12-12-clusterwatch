/**
 * @file misc_utils.h
 *
 * @date Sep 24, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef MISC_UTILS_H_
#define MISC_UTILS_H_

#include <stdio.h>
#include "dwarf_types.h"
#include "mmap_manager_types.h"

// --------> BEGIN different misc util functions

#define NEGATIVE_VAL -1
#define POSITIVE_VAL 1
#define ZERO_VAL 0

extern struct bhb_mmap_ctx * bhb_create_mapped_file(diag_t *diag,
		struct bhb_mmap_param * p_params,
		struct dwarf_rt_ctx * p_dwarf_rt_ctx,
		struct spy_ctx * p_spy_ctx);
extern char *calc_ptr_buf_after_offset(char *buf, int *buf_len);

extern diag_t diag_ptr_null_ret(void *ptr, char *mesg);
extern diag_t diag_ptr_not_null_ret(void *ptr, char *mesg);
extern diag_t diag_ptr_warn_if_not_null(void *ptr, char *mesg);
extern diag_t diag_ptr_warn_if_null(void *ptr, char *mesg);
extern diag_t diag_mem_alloc_ret(void *ptr, char *mesg);
extern diag_t get_identity(struct identity *id);

extern long int timeval2usec(struct timeval *tv);

extern diag_t skip_line(FILE* p);

// ---------> END different misc util functions

#endif /* MISC_UTILS_H_ */

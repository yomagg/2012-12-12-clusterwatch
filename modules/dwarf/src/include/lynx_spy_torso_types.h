/**
 * lynx_spy_torso_types.h
 *
 *  Created on: Oct 21, 2013
 *  Author: Magda Slawinska aka Magic Magg magg dot gatech at gmail.com
 */

#ifndef LYNX_SPY_TORSO_TYPES_H_
#define LYNX_SPY_TORSO_TYPES_H_

#include <sys/time.h>
#include <mqueue.h>
#include "mmap_manager.h"
#include "kernel_profile.h"    // for the sizeof argument

// the lynx mmap parameters

//! the number of entries in the index of mmap file for lynx
//! one node has one entry; on Keeneland we have max 120 that's why
//! I put this number here; on KFS we have 256 but operational is typically 239
#define LYNX_MMAP_BLOCK_COUNT 256
//! the size of the header for the header
#define LYNX_MMAP_HDR_HDR_SIZE_BYTES 250
//! the size of the entry in the header
#define LYNX_MMAP_HDR_ENTRY_SIZE_BYTES 300
//! the size of the body header
#define LYNX_MMAP_BODY_HDR_SIZE_BYTES 200
//! the size of the entry for the particular block
#define LYNX_MMAP_BLOCK_ENTRY_SIZE_BYTES 200
//! the number of elements in the block
#define LYNX_MMAP_BLOCK_ENTRY_COUNT 1

// LYNX Message Queue
//! the maximum number of messages in the queue
//! for the maximum number of messages in the queue see
//! /proc/sys/fs/mqueue/msg_max
#define LYNX_MQ_MAX_MESSAGES 10
//! the maximum size of the message allowed in the queue
//! for the maximum allowed size of the message take a look at
//! /proc/sys/fs/mqueue/msgsize_max
#define LYNX_MQ_MAX_MSG_SIZE (sizeof(kernel_profile))
//! the creation parameters for the queue
#define LYNX_MQ_CREATE_OWNER_FLAGS		(O_RDWR | O_CREAT | O_EXCL | O_NONBLOCK)

// -----------------------------------------------
// structures that will be sent from a spy to an aggregator
// ------------------------------------------------

/**
 * It contains information what Lynx instrumentors are enabled.
 * Basically configure.lynx file
 */
struct lynx_cap {
	//! the timestamp when we recorded the data
	struct timeval ts;
	//! the identity of the node where the gpu devices are attached
	struct identity id;
	//! the name of the queue
	char qname[100];
};

/**
 * Masks for the mask field in kernel_prof; what was measured
 * i.e., what the record contains
 */

#define KERN_PROF_NONE 0x0
#define KERN_PROF_KERNEL_RT 0x1
#define KERN_PROF_MEM_EFF 0x2
#define KERN_PROF_INST_COUNT 0x4
#define KERN_PROF_EXEC_COUNT 0x8
#define KERN_PROF_BRANCH_DIVER 0x16
#define KERN_PROF_ACTIVITY_F 0x32

/**
 * This structure holds values from the instrumentors, and corresponds
 * to the logfile content from the configure.lynx file
 *
 * See also modules/lynx/lynx-1.0.0/lynx/instrumentation/interface/kernel_profile.h
 * This is making horizontal the union structure kernel_profile
 * Not all data are required to be sent
 */
struct kernel_prof {
	int pid;
	int device;
	char name[MAX_KERNEL_NAME_SIZE];

	//! indicates which field were measured
	unsigned long mask;

	double kernel_rt;
	struct memory_efficiency mem_eff;
	double instruction_count;
	struct branch_divergence branch_divergence;
	struct activity_factor activity_f;
	unsigned long barriers;
};

/**
 * This is a monitoring record
 */
struct lynx_mon {
	//! the timestamp of the measurement
	struct timeval ts;
	//! the id of the node where the measurement is going to be performed
	int id;

	//! the information about the kernel execution
	struct kernel_prof kernel_info;
};

// -----------------------------------
// utility structures
// -----------------------------------

/**
 * This structure is responsible for
 */
struct lynx_mq {
	//! the descriptor of the queue to get lynx data
	mqd_t q;
	//! the size of the queue and the size of the allocated memory for p_ker_prof
	long int q_max_msg_size;
	//! the maximum number of messages in the queue
	long int q_max_msg_count;
	//! the buffer reserved for the max message size that can be received from the
	//! mq
	kernel_profile * p_q_max_msg;
};

/**
 * The global structure that should be used in the spy module to simplify
 * organization of the data.
 */
struct lynx_glob_data {
	//! data that will be passed to the aggregator
	struct lynx_mon mon;
	//! the capabilities of the Lynx (what instrumentors are enabled)
	struct lynx_cap caps;

	//! this will control the mmap_ctx; connection to the memory map
	struct bhb_mmap_ctx *p_bhb_ctx;

	//! the message queue data related to the lynx queue (this is the
	//! link to the LYNX
	struct lynx_mq lynx_q;
};

#endif /* LYNX_SPY_TORSO_TYPES_H_ */

/**
 * @file dwarf_rt.h
 *
 * @date Dec 3, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef DWARF_RT_H_
#define DWARF_RT_H_

#include "dwarf_types.h"

// ----------------------
// global variables
// ----------------------
// defined in dwarf_utils.c
extern struct dwarf_rt_ctx rt_ctx;


// -------------------------
// ini related functions
// -------------------------
// general interface to the dwarf related functions
extern diag_t dwarf_cmd_parse(int argc, char** argv, gchar **ini_name);


// ---------------------------
// dwarf runtime functions defined in dwarf_utils.c
// ---------------------------
// core functions
extern diag_t dwarf_rt_init(struct dwarf_rt_ctx *p_ctx, gboolean is_patrol, gchar* ini_name);
extern diag_t dwarf_rt_reg_spy(struct dwarf_rt_ctx *p_ctx, char* spy_name, dwarf_init_spy_func_t p_init_spy_ctx_func);
extern diag_t dwarf_rt_enable_spy(struct dwarf_rt_ctx *p_ctx, char *spy_name);
// utility functions
extern diag_t dwarf_rt_deploy_spy_from_ini(struct dwarf_rt_ctx *p_ctx, char *spy_name, dwarf_init_spy_func_t p_init_spy_ctx_func);
extern diag_t dwarf_rt_prt_spy_status(struct dwarf_rt_ctx *p_ctx);



#endif /* DWARF_RT_H_ */

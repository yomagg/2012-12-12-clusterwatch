/**
 * @file mmap_manager_types.h
 *
 * @date Dec 12, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef MMAP_MANAGER_TYPES_H_
#define MMAP_MANAGER_TYPES_H_

#include "dwarf_types.h"

/**
 * This structure holds the parameters for the memory mapped region. In general,
 * the memory mapped region will consist of the header and the body,
 * the body will consist of "subbodies, " i.e. "blocks"
 * that correspond to each and one entry in the header.
 * bhb - body-header-block context
 */
struct bhb_mmap_param {
	//! the number of entries in the index of mmap file
	int block_count;
	//! the size of the header for the header section in bytes
	int hdr_header_size_bytes;
	//! the size [bytes] of the entry in the header section
	int hdr_entry_size_bytes;
	//! the size of the header for the body section
	int bdy_header_size_bytes;
	//! the size of the entry in the block in the body section
	int bdy_block_entry_size_bytes;
	//! the number of entries per block
	int bdy_block_entry_count;
};

/**
 * This structure keeps data necessary to manage the memory mapped things
 * bhb - body-header-block context
 */
struct bhb_mmap_ctx {
	//! the tab that holds the info what entities have appeared in our
	//! monitoring infrastructure; entities that reported that will provide
	//! a monitoring data e.g. a node (node granularity)
	GHashTable *mmap_idx_tab;
	//! the header; typically will have the capabilities of the entities
	//! that announced their presence
	struct mmap_ctx header;
	//! the body - it will hold the general information about blocks
	struct mmap_ctx body;
	//! the array of blocks that will contain the actual measurmenets for a
	//! particular node
	struct mmap_ctx *block_arr;
};

#endif /* MMAP_MANAGER_TYPES_H_ */

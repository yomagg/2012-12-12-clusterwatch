/**
 * lynx_spy_torso.h
 *
 *  Created on: Oct 21, 2013
 *  Author: Magda Slawinska aka Magic Magg magg dot gatech at gmail.com
 */

#ifndef LYNX_SPY_TORSO_H_
#define LYNX_SPY_TORSO_H_


// -----------------------------------------
// the variable for FFS usage
// ----------------------------------------
// the variables for FFS usage defined in spy.c
extern FMField timestamp_field_list[];
extern FMField identity_field_list[];
// they are defined in nvid_spy_torso.c
extern FMField lynx_cap_field_list[];
extern FMStructDescRec lynx_cap_format_list[];

extern FMField activity_factor_field_list[];
extern FMField memory_efficiency_field_list[];
extern FMField branch_divergence_field_list[];
extern FMField kernel_prof_field_list[];
extern FMField lynx_mon_field_list[];
extern FMStructDescRec lynx_mon_format_list[];

//! data handlers
//extern void setup_lynx(void);
extern int read_lynx(void *data);
extern int process_lynx(void *data);

#endif /* LYNX_SPY_TORSO_H_ */

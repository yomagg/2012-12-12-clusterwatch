/**
 * @file mem_spy_types.h
 *
 * @date Sep 14, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#ifndef MEM_SPY_TORSO_TYPES_H_
#define MEM_SPY_TORSO_TYPES_H_

#include <sys/time.h>
#include <stdio.h>
#include "mmap_manager.h"
#include "dwarf_types.h"

//! the number of entries in the index of mmap file for mem
//! one node has one entry; on Keeneland we have max 120 that's why
//! I put this number here, on KFS we have 256 but operational is typically 239
#define MEM_MMAP_BLOCK_COUNT 256
//! the size of the header for the header
#define MEM_MMAP_HDR_HDR_SIZE_BYTES 200
//! the size of the entry from the cpu
#define MEM_MMAP_HDR_ENTRY_SIZE_BYTES 200
//! the size of the body header
#define MEM_MMAP_BODY_HDR_SIZE_BYTES 200
//! the size of the entry for the particular block
#define MEM_MMAP_BLOCK_ENTRY_SIZE_BYTES 200
//! the number of elements in the block
#define MEM_MMAP_BLOCK_ENTRY_COUNT 1
// -------------------------------
// structures used by evpath
// ------------------------------
/**
 * the memory capabilities as what we can get from /proc/meminfo
 */
struct mem_cap {
	//! the timestamp indicating the start of measurument
	struct timeval ts;
	//! the identity of the the spy
	struct identity id;
	// the mem capabilities

	//! total amount of physical RAM in kilobytes
	long mem_total_kB;
	//! total amount of swap available in kilobytes
	long swap_total_kB;
};

/**
 * This is what will be reported by a spy to a secret service.
 *
 * I used /proc/meminfo for that so please refer to its documentation
 * e.g. http://www.centos.org/docs/5/html/5.2/Deployment_Guide/s2-proc-meminfo.html
 */
struct mem_mon {
	//! the timestamp of measurement occurrence
	struct timeval ts;

	//! the id of the sending entity
	int id;

	//! total amount of physical RAM in kilobytes - this is from the cap
	//! since now we do not send the first cap record; now we send
//	long mem_total_kB;
	//! total amount of swap available in kilobytes - this is from the cap
	//! we do not send the first cap record; now we send
//	long swap_total_kB;

	//! current mem utilization in percentage (mem_total_kB-mem_free)/mem_total_kB
	float mem_util_perc;

	//! The amount of physical RAM, used for file buffers.
	//! percentage of mem_total_kB
	float mem_buffers_util_perc;
	//! The amount of physical RAM, used as cache memory. Percentage of mem_total_kB
	float mem_cached_util_perc;
	//! The total amount of buffer or page cache memory, w.r.t mem_total_kB,
	//! that is in active use. This is memory that has been recently used and is
	//! usually not reclaimed for other purposes.
	float mem_active_util_perc;
	//! The total amount of buffer or page cache memory, w.r.t. mem_total_kB,
	//! that are free and available. This is memory that has not been recently
	//! used and can be reclaimed for other purposes.
	float mem_inactive_util_perc;

	//! The total amount of memory, w.r.t. mem_total_kB, used by the kernel to cache
	//! data structures for its own use.
	float slab_util_perc;
	//! The total amount of memory, in % w.r.t. mem_total_kB, which have been used to
	//! map devices, files, or libraries using the mmap command
	float mapped_perc;

	//! The amount of swap, in % w.r.t. swap_total_kB, used as cache memory.
	float swap_cached_perc;
	//! The total amount of swap used, in % w.r.t swap_total_kB.
	float swap_util_perc;
};

// -----------------------------------
// utility structures
// -----------------------------------
/**
 * The global structure that should be used in the spy module to simplify
 * organization of the data.
 */
struct mem_glob_data {
	//! the capabilities of the device; they are currently not passed to the
	//! secret service, since we pass only mon data
	struct mem_cap caps;

	//! this will control the mmap_ctx; connection to the memory map
	struct bhb_mmap_ctx *p_bhb_ctx;
};

#endif /* MEM_SPY_TORSO_TYPES_H_ */

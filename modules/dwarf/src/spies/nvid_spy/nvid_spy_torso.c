/**
 * @file gpu_spy_torso.c
 *
 * @date Oct 29, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <glib.h> // for g_snprintf

#include "clusterspy.h"
#include "evpath.h"
#include "devel.h"
#include "spy.h"
#include "nvid_spy_torso_types.h"


FMField nvid_dev_cap_field_list[] = {
		{"product_name", "integer[20]", sizeof(char), FMOffset(struct nvid_dev_cap *, product_name)},
		{"serial_number", "integer[20]", sizeof(char), FMOffset(struct nvid_dev_cap *, serial_number)},
		{"compute_mode", "integer[20]", sizeof(char), FMOffset(struct nvid_dev_cap*, compute_mode)},
		{"id", "integer", sizeof(int), FMOffset(struct nvid_dev_cap *, id)},
		{"memory_total", "integer", sizeof(int), FMOffset(struct nvid_dev_cap *, memory_total)},
		{"power_limit", "integer", sizeof(int), FMOffset(struct nvid_dev_cap *, power_limit)},
		{"max_graphics_clock", "integer", sizeof(int), FMOffset(struct nvid_dev_cap *, max_graphics_clock)},
		{"max_SM_clock", "integer", sizeof(int), FMOffset(struct nvid_dev_cap *, max_SM_clock)},
		{"max_memory_clock", "integer", sizeof(int), FMOffset(struct nvid_dev_cap *, max_memory_clock)},
		{NULL, NULL, 0, 0}
};

FMField nvid_cap_field_list[] = {
		{"ts", "struct timeval", sizeof(struct timeval), FMOffset(struct nvid_cap *, ts)},
		{"id", "struct identity", sizeof(struct identity), FMOffset(struct nvid_cap *, id)},
		{"driver_ver", "integer", sizeof(char), FMOffset(struct nvid_cap *, driver_ver)},
		{"gpu_count", "integer", sizeof(int), FMOffset(struct nvid_cap *, gpu_count)},
		{"devs", "struct nvid_dev_cap[gpu_count]", sizeof(struct nvid_dev_cap), FMOffset(struct nvid_cap *, devs)},
		{NULL, NULL, 0, 0}
};

//! start with the main structure, then describe the substructures
FMStructDescRec nvid_cap_format_list[] = {
		{ "struct nvid_cap", nvid_cap_field_list, sizeof(struct nvid_cap), NULL },
		{ "struct timeval", timestamp_field_list, sizeof(struct timeval), NULL },
		{ "struct identity", identity_field_list, sizeof(struct identity), NULL },
		{ "struct nvid_dev_cap", nvid_dev_cap_field_list, sizeof(struct nvid_dev_cap), NULL},
		{NULL, NULL, 0, NULL}
};

// -----------------------------------

FMField nvid_dev_processes_field_list[] = {
  {"pid_count", "integer", sizeof(int), FMOffset(struct nvid_dev_process *, pid_count)},
  {"pids", "integer[pid_count]", sizeof(long), FMOffset(struct nvid_dev_process *, pids)},
  {"processes_names", "string[pid_count]", sizeof(char*), FMOffset(struct nvid_dev_process *, processes_names)},
  {"used_gpu_mem", "float[pid_count]", sizeof(float), FMOffset(struct nvid_dev_process *, used_gpu_mem)},
  { NULL, NULL, 0, 0}
};

FMField nvid_mon_field_list[] = {
  {"ts", "struct timeval", sizeof(struct timeval), FMOffset(struct nvid_mon *, ts)},
  {"id", "integer", sizeof(int), FMOffset(struct nvid_mon *, id)},
  {"gpu_count", "integer", sizeof(int), FMOffset(struct nvid_mon *, gpu_count)},
  {"performance_state", "string[gpu_count]", sizeof(char*), FMOffset(struct nvid_mon *, performance_state)},
 // {"performance_state", "integer[gpu_count][5]", sizeof(char), FMOffset(struct nvid_mon *, performance_state)},
  {"mem_used_MB", "float[gpu_count]", sizeof(float), FMOffset(struct nvid_mon *, mem_used_MB)},
  {"util_gpu", "float[gpu_count]", sizeof(float), FMOffset(struct nvid_mon *, util_gpu)},
  {"util_mem", "float[gpu_count]", sizeof(float), FMOffset(struct nvid_mon *, util_mem)},
  {"power_draw", "float[gpu_count]", sizeof(float), FMOffset(struct nvid_mon *, power_draw)},
  {"graphics_clock", "float[gpu_count]", sizeof(float), FMOffset(struct nvid_mon *, graphics_clock)},
  {"sm_clock", "float[gpu_count]", sizeof(float), FMOffset(struct nvid_mon *, sm_clock)},
  {"mem_clock", "float[gpu_count]", sizeof(float), FMOffset(struct nvid_mon *, mem_clock)},
  {"processes", "struct nvid_dev_process[gpu_count]", sizeof(struct nvid_dev_process), FMOffset(struct nvid_mon *, processes)},
  {NULL, NULL, 0, 0}
};

//! start with the main structure, then describe the substructures
FMStructDescRec nvid_mon_format_list[] = {
  { "struct nvid_mon", nvid_mon_field_list, sizeof(struct nvid_mon), NULL },
  { "struct timeval", timestamp_field_list, sizeof(struct timeval), NULL },
  { "struct nvid_dev_process", nvid_dev_processes_field_list, sizeof(struct nvid_dev_process), NULL },
  {NULL, NULL, 0, NULL}
};

// other variables
static struct nvid_glob_data ctrl_data;

// forward declarations
static inline FILE * nvid_smi_open(char *cmd);
static diag_t init_gpu_caps(struct nvid_cap *p_cap);
static diag_t init_gpu_mon(struct nvid_mon *p_mon, struct nvid_cap *p_cap);
static diag_t fed_nvid_mon(struct nvid_mon *p_mon, struct nvid_glob_data *p_ctrl);

diag_t dwarf_init_nvid_ranger(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
diag_t dwarf_init_nvid_spy(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);

/**
 * Make use of the global variable ctrl_data that holds some helper variables.
 * I assume the identity has been already set in the init spy function.
 * @todo I am callocating data for devices; they should be freed somehow.
 *       right now they are lost.
 * @param data that will be sent over EVPATH, I guess memory for the data
 *        have been allocated accordingly
 * @return 0 - you were not able to read anything
 *         > 0 you have data to be sent; successful read
 */
int read_cap_nvid(void *data){

	int ret = POSITIVE_VAL;
	const int ERROR = 0;
	struct nvid_cap *p_rec = (struct nvid_cap*)data;

	// the ctrl_data.caps.id has been already set in net_spy_init
	p_rec->id = ctrl_data.caps.id;

	// get the timestamp
	gettimeofday(&ctrl_data.caps.ts, NULL);
	p_rec->ts = ctrl_data.caps.ts;

	p_rec->gpu_count = ctrl_data.caps.gpu_count;
	memcpy(p_rec->driver_ver, ctrl_data.caps.driver_ver, sizeof(ctrl_data.caps.driver_ver));

	// copy the characteristic of the GPU devices
	p_rec->devs = calloc(ctrl_data.caps.gpu_count, sizeof(struct nvid_dev_cap));
	if (diag_mem_alloc_ret(p_rec->devs, "For GPU devices\n") != DIAG_OK){
		return ERROR;
	}

	int i = 0;
	for(i = 0; i < ctrl_data.caps.gpu_count; i++){
		p_rec->devs[i] = ctrl_data.caps.devs[i];
	}

	return ret;
}

/**
 * @param data Data sent over EVPath
 * @return 0  success
 *         anything else some issues (should not happen)
 */
int process_cap_nvid(void *data ){
	diag_t diag = DIAG_OK;
	int ret = ZERO_VAL;

	struct nvid_cap *event = (struct nvid_cap*) data;
	struct nvid_glob_data *d = (struct nvid_glob_data*) &ctrl_data;

	// dump headers if this is the first time you are running it
	if (NULL == d->p_bhb_ctx->header.curr_entry){

		// write the layout
		const int B_SIZE = d->p_bhb_ctx->header.hdr_size;
		char b[B_SIZE];

		// the layout prefix
		GString *layout_prefix = g_string_new("");
		diag = bhb_mmap_layout_2_str(d->p_bhb_ctx, layout_prefix);
		g_snprintf(b, B_SIZE, "%s \nTIMESTAMP SPY_ID HOSTNAME PID DRIVER_VER GPU_COUNT\n"
					"GPU_ID COMPUTE_MODE SER_NO PROD_NAME MEM_TOT_MB POWER_LIMIT_W SM_CL_MHZ GRAPH_CL_MHZ MEM_CL_MHZ\n", layout_prefix->str);
		g_string_free(layout_prefix, TRUE);

		bhb_mmap_wrt_hdr_header(d->p_bhb_ctx, b);
		bhb_mmap_wrt_bdy_header(d->p_bhb_ctx, "TIMESTAMP SPY_ID UTIL_GPU_PERC UTIL_MEM_PERC MEM_USED_MB\n"
					"POWER_DRAW_PERC PERF_STATE\n"
					"CL_GR_PERC CL_SM_PERC CL_MEM_PERC\n"
					"GPU_ID PID_COUNT NAME_PID1 NAME_PID2 ....\n"
					"USED_GPU_MEM_PERC\n");
	}

	// now dump the values
	const int BUF_SIZE = d->p_bhb_ctx->header.elem_size;
	char buf[BUF_SIZE];
	memset(buf, 0, BUF_SIZE);

	// TODO this is strange but sometimes %d is required to print event->id.pid
	// and sometimes %ld formatter - see mem_scout.c
	g_snprintf(buf, BUF_SIZE, "%ld %d %s %d %s %d\n",
			timeval2usec(&event->ts),
			event->id.id,
			event->id.hostname,
			event->id.pid,
			event->driver_ver,
			event->gpu_count);
	int curr_len = 0;
	char *curr_buf = NULL;
	int i = 0;

	// iterate over the gpu devices
	for (i=0; i < event->gpu_count; i++){
		curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
		struct nvid_dev_cap *cap = &event->devs[i];
		g_snprintf(curr_buf, BUF_SIZE - curr_len, "%d %s %s %s %d %d %d %d %d\n", cap->id, cap->compute_mode, cap->serial_number,
				cap->product_name, cap->memory_total, cap->power_limit, cap->max_SM_clock, cap->max_graphics_clock,
				cap->max_memory_clock);
	}

	curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);

	if ( (diag = bhb_mmap_wrt_hdr_entry(d->p_bhb_ctx, buf, event->id.id)) != DIAG_OK){
		p_warn(DWARF_NAME ": Issues with writing memory mapped file\n");
	}

	return ret;
}



/**
 * Reads the nvidia values
 * @param data The structure nvid_mon
 * @return 0 - you were not able to read anything
 *         > 0 - you have data to be sent; indicates successful read
 */
int read_mon_nvid(void *data){
	diag_t diag = DIAG_OK;
	// the return value
	int ret = POSITIVE_VAL;
	struct nvid_mon *p_rec = (struct nvid_mon*) data;

	// connect the p_rec with the allocated buffers and copy the data
	// necessary data
	if ( (diag = fed_nvid_mon(p_rec, &ctrl_data)) != DIAG_OK ){
		p_error("Some problems with fed_nvid_mon()\n");
		return diag;
	}

	// get the timestamp
	gettimeofday(&p_rec->ts, NULL);

	int i = 0;
	// indicate that the clock data has been already read if set to true
	bool clocks_read = false;
	// There is a hack to skip the 'Clocks Throttle Reasons'
	// There are three occurrences of clocks, I don't need the
	// first one and the last one. I am interested only in the middle
	// one
	int first_clock = 0;

	for (i = 0; i < p_rec->gpu_count; i++){
		char buf[200];
		memset(buf, 0, sizeof(buf));
		clocks_read = false;
		first_clock = 0;

		g_snprintf(buf, sizeof(buf), "%s -i %d", NVID_SMI_CMD, i);

		FILE *fp=NULL;
		if ((fp = nvid_smi_open(buf)) == NULL){
			diag = DIAG_ERR;
			break;
		}

		while (1 == fscanf(fp, " %31s ", buf)){
			if (0 == strcmp(buf, "Performance")){
				if (1 == fscanf(fp, "State : %4s", buf)){
					strcpy(p_rec->performance_state[i], buf);
				}
			}
			if (0 == strcmp(buf, "Used")) {
				int mem_used = 0;
				if (1 == fscanf(fp, ": %d MB", &mem_used)) {
					p_rec->mem_used_MB[i] = mem_used;
				}
			}
			if (0 == strcmp(buf, "Utilization")){
				int tmp1 = 0;
				int tmp2 = 0;
				if (2 == fscanf(fp, "Gpu : %d %% Memory : %d %%", &tmp1, &tmp2)) {
					p_rec->util_gpu[i] = tmp1;
					p_rec->util_mem[i] = tmp2;
				}
			}
			if (0 == strcmp(buf, "Power")) {
				float tmp;
				if (1 == fscanf(fp, "Draw : %f W", &tmp)){
					p_rec->power_draw[i] = tmp / ctrl_data.caps.devs[i].power_limit * 100.0;
				}
			}
			// and the clocks; there is a section with max clocks so we need
			// to skip that max section
			if ( !clocks_read && (0 == strcmp(buf, "Clocks"))) {
				// there is a Clocks Trottle Reasons field; I need to skip it
				if (!first_clock){
					++first_clock;
					continue;
				}

				int tmp1 = 0;
				int tmp2 = 0;
				int tmp3 = 0;
				if (3 == fscanf(fp, "Graphics : %d MHz SM : %d MHz Memory : %d MHz",
						&tmp1, &tmp2, &tmp3)) {
					p_rec->graphics_clock[i] = tmp1 / ctrl_data.caps.devs[i].max_graphics_clock * 100.0;
					p_rec->sm_clock[i] = tmp1 / ctrl_data.caps.devs[i].max_SM_clock * 100.0;
					p_rec->mem_clock[i] = tmp1 / ctrl_data.caps.devs[i].max_memory_clock * 100.0;
					clocks_read = true;

					/*
					// >---------- TODO this should be removed
					// I ignore the p_rec->processes which are set to NULL
					// at this moment
					// this is the dummy record that should be in the future
					// filled with the correct values
					p_rec->processes[i].pid_count = 1;
					p_rec->processes[i].pids[0] = -1;
					strcpy(p_rec->processes[i].processes_names[0], "none");
					p_rec->processes[i].used_gpu_mem[0] = -1.0; */
					// end of the elements to removed ---------<
				}
				// so for now this is the last thing to do
				break;
			}
		}

		fclose(fp);
	}

	return ret;
}

/**
 * Process the data: currently display data on the screen
 * @param data
 * @return 0 success
 *         != 0 some issues (should not happen)
 */
int process_mon_nvid(void *data ){

	struct nvid_glob_data *d = (struct nvid_glob_data*) &ctrl_data;
	struct nvid_mon *p_rec = data;
	diag_t diag = DIAG_OK;
	const int ERROR = -1;

	const int BUF_SIZE = d->p_bhb_ctx->body.elem_size;
	char buf[BUF_SIZE];

	// clear the buf
	memset(buf, 0, BUF_SIZE);
	g_snprintf(buf, BUF_SIZE, "%ld %d", timeval2usec(&p_rec->ts), p_rec->id);

	int curr_len = strlen(buf);
	char *curr_buf = NULL;

	// ----------- first write utilization
	int i = 0;
	// gpu utilization
	for( i = 0; i < p_rec->gpu_count; i++){
		curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
		g_snprintf(curr_buf, BUF_SIZE - curr_len, " %.2f", p_rec->util_gpu[i]);
	}

	// memory utilization
	for (i=0; i < p_rec->gpu_count; i++){
		curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
		g_snprintf(curr_buf, BUF_SIZE - curr_len, " %.2f", p_rec->util_mem[i]);
	}

	// mem usage in MB
	for (i=0; i < p_rec->gpu_count; i++){
		curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
		g_snprintf(curr_buf, BUF_SIZE - curr_len, " %d", p_rec->mem_used_MB[i]);
	}

	curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
	g_snprintf(curr_buf, BUF_SIZE - curr_len, "\n");

	// power draw
	for (i=0; i < p_rec->gpu_count; i++){
		curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
		g_snprintf(curr_buf, BUF_SIZE - curr_len, " %.2f", p_rec->power_draw[i]);
	}

	// performance state
	for (i=0; i < p_rec->gpu_count; i++){
		curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
		g_snprintf(curr_buf, BUF_SIZE - curr_len, " %s", p_rec->performance_state[i]);
	}

	curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
	g_snprintf(curr_buf, BUF_SIZE - curr_len, "\n");

	// clocks
	for (i=0; i < p_rec->gpu_count; i++){
		curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
		g_snprintf(curr_buf, BUF_SIZE - curr_len, " %.2f", p_rec->graphics_clock[i]);
	}

	// sm clock
	for (i=0; i < p_rec->gpu_count; i++){
		curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
		g_snprintf(curr_buf, BUF_SIZE - curr_len, " %.2f", p_rec->sm_clock[i]);
		}

	for (i=0; i < p_rec->gpu_count; i++){
		curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
		g_snprintf(curr_buf, BUF_SIZE - curr_len, " %.2f", p_rec->mem_clock[i]);
	}

	curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
	g_snprintf(curr_buf, BUF_SIZE - curr_len, "\n");

	// gpu processes and memory usage
	/* >------ TODO right now it will be not supported
		int j = 0;
		for (i=0; i < event->gpu_count; i++){
			curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
			g_assert( event->processes );
			g_snprintf(curr_buf, BUF_SIZE - curr_len, " %d %d", i, event->processes->pid_count );

			for (j=0; j < event->processes->pid_count; j++){
				curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
				g_snprintf(curr_buf, BUF_SIZE - curr_len, " %s %ld %.2f",
						event->processes->processes_names[j],
						event->processes->pids[j],
						event->processes->used_gpu_mem[j]);
			}
			curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
			g_snprintf(curr_buf, BUF_SIZE - curr_len, "\n");
		}
		curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
		g_snprintf(curr_buf, BUF_SIZE - curr_len, "\n");
		// end not supporting -----------------------< */

	if ( (diag = bhb_mmap_wrt_bdy_entry(d->p_bhb_ctx, buf, p_rec->id)) != DIAG_OK){
		p_warn(DWARF_NAME ": some issues with writing to mmap.\n");
		return ERROR;
	}


#ifdef DEBUG

	printf("TIMESTAMP SPY_ID UTIL_GPU_PERC UTIL_MEM_PERC MEM_USED_MB\n"
					"POWER_DRAW_PERC PERF_STATE\n"
					"CL_GR_PERC CL_SM_PERC CL_MEM_PERC\n");
	printf("%ld %d", timeval2usec(&p_rec->ts), p_rec->id);
	int gpu_count = p_rec->gpu_count;
	for( i = 0; i < gpu_count; i++){
		printf(" %.2f", p_rec->util_gpu[i]);
	}
	printf(" |");
	for( i = 0; i < gpu_count; i++){
		printf(" %.2f", p_rec->util_mem[i]);
	}
	printf(" |");
	for( i = 0; i < gpu_count; i++){
		printf(" %d", p_rec->mem_used_MB[i]);
	}
	printf(" |");
	for( i = 0; i < gpu_count; i++){
		printf(" %.2f", p_rec->power_draw[i]);
	}
	printf(" |");
	for( i = 0; i < gpu_count; i++){
		printf(" %s", p_rec->performance_state[i]);
	}
	printf(" |");
	for( i = 0; i < gpu_count; i++){
		printf(" %.2f", p_rec->graphics_clock[i]);
	}
	printf(" |");
	for( i = 0; i < gpu_count; i++){
		printf(" %.2f", p_rec->sm_clock[i]);
	}
	printf(" |");
	for( i = 0; i < gpu_count; i++){
		printf(" %.2f", p_rec->mem_clock[i]);
	}
	printf("\n");
#endif

	return ZERO_VAL;
}

// =============================================================
// dwarf-related public functions
// =============================================================

diag_t dwarf_init_nvid_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx){
	diag_t diag = DIAG_OK;

	p_spy_ctx->init_ranger_func = &dwarf_init_nvid_ranger;
	p_spy_ctx->init_spy_func = &dwarf_init_nvid_spy;

	// initialize cap data
	p_spy_ctx->evpath_ctx_cap.format_list = nvid_cap_format_list;
	p_spy_ctx->evpath_ctx_cap.res_type = GPU;
	p_spy_ctx->evpath_ctx_cap.data_size = sizeof(struct nvid_cap);
	p_spy_ctx->evpath_ctx_cap.period_sec = 0;  // to indicate we want to call it only once

	// init transport functions on the collector (spy side)
	p_spy_ctx->evpath_ctx_cap.clctor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_cap.clctor_read_func = &read_cap_nvid;
	p_spy_ctx->evpath_ctx_cap.clctor_process_func = NULL;

	// right now we do not distinguish the functions on the aggregator side
	p_spy_ctx->evpath_ctx_cap.agrtor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_cap.agrtor_read_func = NULL;
	p_spy_ctx->evpath_ctx_cap.agrtor_process_func = &process_cap_nvid;


	// initialize monitoring data
	// init transport fields
	p_spy_ctx->evpath_ctx_mon.format_list = nvid_mon_format_list;
	p_spy_ctx->evpath_ctx_mon.res_type = GPU;
	p_spy_ctx->evpath_ctx_mon.data_size = sizeof(struct nvid_mon);
	p_spy_ctx->evpath_ctx_mon.period_sec = p_dwarf_rt_ctx->ini_file.patrol_freq;

	// init transport functions on the collector (spy side)
	p_spy_ctx->evpath_ctx_mon.clctor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_mon.clctor_read_func = &read_mon_nvid;
	p_spy_ctx->evpath_ctx_mon.clctor_process_func = NULL;

	// right now we do not distinguish the functions on the aggregator side
	p_spy_ctx->evpath_ctx_mon.agrtor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_mon.agrtor_read_func = NULL;
	p_spy_ctx->evpath_ctx_mon.agrtor_process_func = &process_mon_nvid;

	return diag;
}

/**
 * used for the initialization of the aggregator side; we need to initialize
 * the memory map regions
 * @param p_dwarf_rt_ctx
 * @param p_spy_ctx
 * @return
 */
diag_t dwarf_init_nvid_ranger(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx){
	diag_t diag = DIAG_OK;

	// initialize the bhb_mmap structure; the second part
	// of the initialization will take part when we know the
	// name of the file assigned to us. the second part of initialization
	// should be held in scut_trooper_run function; we create
	// the relevant pointers and structures for a header, body and blocks
	// in the body
	struct bhb_mmap_param params = {
				.block_count = NVID_MMAP_BLOCK_COUNT,
				.hdr_header_size_bytes = NVID_MMAP_HDR_HDR_SIZE_BYTES,
				.hdr_entry_size_bytes = NVID_MMAP_HDR_ENTRY_SIZE_BYTES,
				.bdy_header_size_bytes = NVID_MMAP_BODY_HDR_SIZE_BYTES,
				.bdy_block_entry_size_bytes = NVID_MMAP_BLOCK_ENTRY_SIZE_BYTES,
				.bdy_block_entry_count = NVID_MMAP_BLOCK_ENTRY_COUNT
	};

	ctrl_data.p_bhb_ctx = bhb_create_mapped_file(&diag, &params, p_dwarf_rt_ctx, p_spy_ctx);
	if (DIAG_OK != diag ){
		return diag;
	}

	if (!ctrl_data.p_bhb_ctx){
		diag = DIAG_ERR;
		return diag;
	}

	return diag;
}


/**
 * setups the gpu
 *
 * initializes the nvid_spy, i.e. the ctrl_data struct:
 * it gets the identity of the spy,
 * initializes the capability record and
 * allocates data for the monitoring record
 *
 * ctrl_data: performs necessary memory allocations and initializes caps and mon
 *            record
 *
 * @param p_dwarf_rt_ctx
 * @param p_spy_ctx
 * @return DIAG_OK if everything went ok, or
 *         != DIAG_OK if something went wrong
 */
diag_t dwarf_init_nvid_spy(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx){

	diag_t diag = DIAG_OK;

	// set identity
	if ( (diag = get_identity(&ctrl_data.caps.id)) != DIAG_OK ){
		return diag;
	}

	// get the number gpus
	if ( (diag = init_gpu_caps(&ctrl_data.caps)) != DIAG_OK ){
		return diag;
	}

	// now prepare the skeleton (allocate memory) for the monitoring data that will be
	// sent to the aggregator
	if ( (diag = init_gpu_mon(&ctrl_data.mon, &ctrl_data.caps)) != DIAG_OK ){
		return diag;
	}

	return diag;
}




// ==============================================================
// static functions
// ==============================================================


// --------------------------------------------
// private functions transport/evpath functions
// ---------------------------------------------

/**
 * Returns the pipe to the NVID_SMI_CMD (execution of the nvidia-smi
 * @param cmd The command to be executed
 * @return The handler to the open pipe
 *         NULL if some error occurred
 */
static inline FILE * nvid_smi_open(char *cmd){
	FILE *fp = popen(cmd, "r");
	if (fp == NULL){
		perror("nvidia-smi");
		p_error("Issues with nvidia-smi\n");
	}
	return fp;
}

/**
 * reads the gpu capabilities; this allocates memory if necessary
 * so ti should be release with the corresponding release struct nvid_cap
 *
 * @param p_cap (OUT) Here we will store the capabilities
 * @return DIAG_OK - everything went fine
 *         != DIAG_OK - there were some issues
 */
static diag_t init_gpu_caps(struct nvid_cap *p_cap){

	diag_t diag = DIAG_OK;
	char token[200];

	// handler for the pipe to the nvidia-smi command
	FILE *fp = NULL;

	if ((fp = nvid_smi_open(NVID_SMI_CMD)) == NULL ) {
		return DIAG_ERR;
	}

	bool driver_found = false;
	bool gpu_count_found = false;
	// now get the general data
	while (1 == fscanf(fp, " %31s ", token)) {
		if (driver_found && gpu_count_found)
			break;

		// first get the general data such as the driver version
		if (!driver_found && (0 == strcmp(token, "Version"))) {
			if (1 == fscanf(fp, " : %12s", token)) {
				strcpy(p_cap->driver_ver, token);
				driver_found = true;
			}
		}
		// and attached gpu count
		if (!gpu_count_found && (0 == strcmp(token, "GPUs"))) {
			if (1 == fscanf(fp, " : %2s", token)) {
				p_cap->gpu_count = atoi(token);
				gpu_count_found = true;
			}
		}
	}

	fclose(fp);

	if (p_cap->gpu_count <= 0) {
		p_error("The number of gpu_count is =< 0.\n");
		return DIAG_ERR;
	}
	p_cap->devs = calloc(p_cap->gpu_count, sizeof(struct nvid_dev_cap));

	// now read the device capabilities
	int i = 0;
	// a hack to skip the later power limits
	// indicates how many times we have read the power limit
	int power_limit = 0;
	for (i = 0; i < p_cap->gpu_count; i++) {
		char buf[200];
		memset(buf, 0, sizeof(buf));
		power_limit = 0;

		g_snprintf(buf, sizeof(buf), "nvidia-smi -q -i %d", i);

		// store the id
		p_cap->devs[i].id = i;

		if ((fp = nvid_smi_open(buf)) == NULL ) {
			diag = DIAG_ERR;
			break;
		}

		while (1 == fscanf(fp, " %31s ", token)) {
			// now read the dev cap values
			if (0 == strcmp(token, "Product")) {
				char buf1[10];
				if (2 == fscanf(fp, "Name : %20s %9s", token, buf1)) {
					strcpy(p_cap->devs[i].product_name, token);
					strcpy(
							p_cap->devs[i].product_name
									+ strlen(p_cap->devs[i].product_name),
							buf1);
				}
			}
			if (0 == strcmp(token, "Serial")) {
				if (1 == fscanf(fp, "Number : %20s", token)) {
					strcpy(p_cap->devs[i].serial_number, token);
				}
			}
			if (0 == strcmp(token, "Compute")) {
				if (1 == fscanf(fp, "Mode : %20s", token)) {
					strcpy(p_cap->devs[i].compute_mode, token);
				}
			}
			if (0 == strcmp(token, "Memory")) {
				if (1 == fscanf(fp, "Usage Total : %20s MB", token)) {
					p_cap->devs[i].memory_total = atoi(token);
				}
			}
			if (0 == strcmp(token, "Power")) {
				if (( !power_limit ) && (1 == fscanf(fp, "Limit : %20s W", token))) {
					p_cap->devs[i].power_limit = atoi(token);
					power_limit++;
					continue;
				}
			}
			if (0 == strcmp(token, "Max")) {
				char buf1[10];
				char buf2[10];
				if (3 == fscanf(fp, "Clocks Graphics : %20s MHz SM : %9s MHz Memory : %9s MHz", token, buf1, buf2)) {
					p_cap->devs[i].max_graphics_clock = atoi(token);
					p_cap->devs[i].max_SM_clock = atoi(buf1);
					p_cap->devs[i].max_memory_clock = atoi(buf2);
				}
			}
		}

		fclose(fp);
	}

	return diag;
}

/**
 * constructs the nvid_mon record, allocates the memory based on gpu number
 * @param p_mon (OUT) - the internal elements will be allocated
 * @param p_cap (IN) - the capabilities of the gpu system attached to the node,
 *                     currently only the gpu_count is used to allocate
 *                     appropriate amount of memory in the p_mon structure
 * @return DIAG_OK if everything went great
 *         != DIAG_OK if anything went wrong
 */
static diag_t init_gpu_mon(struct nvid_mon *p_mon, struct nvid_cap *p_cap){
	diag_t diag = DIAG_OK;
	int gpu_count = 0;

	if ( (diag = diag_ptr_null_ret(p_mon,
			"No memory allocated for the monitoring struct we want to construct\n")) != DIAG_OK ){
		return diag;
	}

	if ( (diag = diag_ptr_null_ret(p_cap, "The capability record pointer is NULL")) != DIAG_OK ){
		return diag;
	}

	// allocate the memory for gpu related stuff
	if ( (gpu_count = p_cap->gpu_count) > 0) {
		int i = 0;

		// allocate data for the performance state
		p_mon->performance_state = (char **) calloc(gpu_count, sizeof(char *));
		if ((diag = diag_mem_alloc_ret(p_mon->performance_state,"\n")) != DIAG_OK)
			return diag;

		for (i = 0; i < gpu_count; i++) {
			p_mon->performance_state[i] = (char *) calloc(PERF_STATE_STR_LEN, sizeof(char));
			if ((diag = diag_mem_alloc_ret(p_mon->performance_state[i],"\n")) != DIAG_OK)
				return diag;
		}

		p_mon->mem_used_MB = (int *) calloc(p_mon->gpu_count, sizeof(int));
		if ((diag = diag_mem_alloc_ret(p_mon->mem_used_MB,"\n")) != DIAG_OK)
			return diag;

		// helpful pointers for allocating memory
		float ** ptrs[] = { &p_mon->util_gpu,
				&p_mon->util_mem, &p_mon->power_draw,
				&p_mon->graphics_clock, &p_mon->sm_clock,
				&p_mon->mem_clock };

		for (i = 0; i < (int) (sizeof(ptrs) / sizeof(float*)); i++) {
			*ptrs[i] = (float *) calloc(gpu_count,sizeof(float));
			if ((diag = diag_mem_alloc_ret(ptrs[i], "\n")) != DIAG_OK)
				break;
		}

		if (DIAG_OK != diag)
			return diag;

/*		// TODO if necessary you need to allocate mem for processes
		// this piece of code might be useful when processes are taken
		// into account, since I am not using it here so just skip it
		// allocate memory for processes array:
		p_mon->processes = (struct nvid_dev_process *) calloc(gpu_count, sizeof(struct nvid_dev_process));
		if ((diag = diag_mem_alloc_ret(p_mon->processes,
				"Can't callocate mem\n")) != DIAG_OK)
			return diag;

		for (i = 0; i < gpu_count; i++) {
			p_mon->processes[i].pids = calloc(NVID_MAX_PIDS_COUNT,
					sizeof(long));
			if ((diag = diag_mem_alloc_ret(p_mon->processes[i].pids,
					"Can't callocate mem\n")) != DIAG_OK)
				return diag;

			p_mon->processes[i].used_gpu_mem = calloc(NVID_MAX_PIDS_COUNT, sizeof(float));
			if ((diag = diag_mem_alloc_ret(
					p_mon->processes[i].used_gpu_mem,
					"Can't callocate mem\n")) != DIAG_OK)
				return diag;

			// now for names
			p_mon->processes[i].processes_names = calloc(
					NVID_MAX_PIDS_COUNT, sizeof(char *));
			if ((diag = diag_mem_alloc_ret(
					p_mon->processes[i].processes_names,
					"Can't callocate mem\n")) != DIAG_OK)
				return diag;

			int j = 0;
			for (j = 0; j < NVID_MAX_PIDS_COUNT; j++) {
				p_mon->processes[i].processes_names[j] = calloc(
						NVID_MAX_PROC_NAME_LEN, sizeof(char));
				if ((diag = diag_mem_alloc_ret(
						p_mon->processes[i].processes_names[j],
						"Can't callocate mem\n")) != DIAG_OK)
					return diag;
			}
		}
// end of adding processes
*/
	}

	return diag;
}

/**
 * copy the data or pointers with data from the global control structure
 * (which has allocated memory) to the new monitoring structure (which
 * doesn't have the allocated memory)
 *
 * @param p_mon (OUT) where the data will be copied or clean
 * @param p_ctrl (IN)  from where data will be copied
 * @return
 */
static diag_t fed_nvid_mon(struct nvid_mon *p_mon, struct nvid_glob_data *p_ctrl){
	diag_t diag = DIAG_OK;

	// clean the data
	memset(p_mon, 0, sizeof(struct nvid_mon));

	// copy data
	p_mon->id = p_ctrl->caps.id.id;
	p_mon->gpu_count = p_ctrl->caps.gpu_count;

	// connect pointers
	p_mon->graphics_clock = p_ctrl->mon.graphics_clock;
	p_mon->mem_clock = p_ctrl->mon.mem_clock;
	p_mon->mem_used_MB = p_ctrl->mon.mem_used_MB;
	p_mon->performance_state = p_ctrl->mon.performance_state;
	p_mon->power_draw = p_ctrl->mon.power_draw;
	p_mon->sm_clock = p_ctrl->mon.sm_clock;
	p_mon->util_gpu = p_ctrl->mon.util_gpu;
	p_mon->util_mem = p_ctrl->mon.util_mem;

	// ignore p_mon->processes for now; it is set to NULL anyway
	assert(NULL == p_mon->processes);

	return diag;
}

/**
 * @file cpu_spy_torso.c
 *
 * @date Sep 25, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <glib/gprintf.h>
#include "clusterspy.h"
#include "evpath.h"
#include "devel.h"
#include "spy.h"
#include "cpu_spy_torso_types.h"
#include "ini_utils_types.h"


// EVpath related variables
// capabilities
FMField cpu_cap_field_list[] = {
		{ "ts", "struct timeval", sizeof(struct timeval), FMOffset(struct cpu_cap *, ts) },
		{"identity", "struct identity", sizeof(struct identity), FMOffset(struct cpu_cap *, id) },
		{ "core_count", "integer", sizeof(int), FMOffset(struct cpu_cap *, core_count) },
		{ NULL, NULL, 0, 0 }
};

FMStructDescRec cpu_cap_format_list[] = {
		{ "struct cpu_cap", cpu_cap_field_list, sizeof(struct cpu_cap), NULL },
		{ "struct timeval", timestamp_field_list, sizeof(struct timeval), NULL },
		{"struct identity", identity_field_list, sizeof(struct identity), NULL },
		{ NULL, NULL, 0, NULL }
};

// monitoring
FMField cpu_mon_field_list[] = {
		{"id", "integer", sizeof(int), FMOffset(struct cpu_mon *, id)},
		{"ts", "struct timeval", sizeof(struct timeval), FMOffset(struct cpu_mon *, ts)},
		{"core_plus_one_count", "integer", sizeof(int), FMOffset(struct cpu_mon *, core_plus_one_count)},
		{"cpu_and_core_usage", "float[core_plus_one_count]", sizeof(float), FMOffset(struct cpu_mon *, cpu_and_core_usage)},
		{NULL, NULL, 0, 0}
};

FMStructDescRec cpu_mon_format_list[] = {
		{"struct cpu_mon", cpu_mon_field_list, sizeof(struct cpu_mon), NULL},
		{"struct timeval", timestamp_field_list, sizeof(struct timeval), NULL },
		{NULL, NULL, 0, NULL}
};



// ------------------------
static struct cpu_mon_cnt_data ctrl_data;
static struct dwarf_cpu_ctrl cpuctrl;


diag_t dwarf_init_cpu_ranger(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
diag_t dwarf_init_cpu_spy(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);


/**
 * returns the total number of cores
 * @return the number of cores
 */
static int cpu_get_core_count(void) {
	return sysconf(_SC_NPROCESSORS_ONLN);
}


/**
 * Counts the cpu usage and fill the monitoring record with usage data
 *
 * @param p The pointer to the monitoring record; it should contain the valid
 *         number of cpus_plus_one
 * @param curr The array of current readings from /proc/stat
 * @param prev The array of previous readings from /proc/stat
 * @return DIAG_OK
 *         DIAG_NAN_ERR if NAN error appeared
 *
 * @todo check if overflow is possible
 * @bug not a number issue
 */
static  diag_t
cpu_count_usage(struct cpu_mon *p, struct procfs_cpu *curr, struct procfs_cpu *prev){

	diag_t diag = DIAG_OK;
	long delta_total_jiffs = 0UL;
	long delta_busy_jiffs = 0UL;
	int i;

	// count the busy jiffs between two readings
	for( i = 0; i < p->core_plus_one_count; i++ ){

		delta_busy_jiffs = curr[i].user - prev[i].user +
				curr[i].nice - prev[i].nice +
				curr[i].system - prev[i].system;

		delta_total_jiffs =
				delta_busy_jiffs +
				curr[i].idle - prev[i].idle +
				curr[i].iowait - prev[i].iowait +
				curr[i].irq - prev[i].irq +
				curr[i].softirq - prev[i].softirq +
				curr[i].steal - prev[i].steal;

		// count the usage as a percentage
		p->cpu_and_core_usage[i] = (((float) (delta_busy_jiffs)) / (delta_total_jiffs)) *100.0;
		if( isnan( p->cpu_and_core_usage[i]) != 0 ){
			float nan = IN_CASE_NAN;
			p_warn("A Not A Number identified: delta_busy_jiffs= %lu, delta_total_jiffs= %lu (total shouldn't be a zero). Setting default value %.2f\n",
				 delta_busy_jiffs, delta_total_jiffs, nan );
			p->cpu_and_core_usage[i] = nan;
			diag = DIAG_NAN_ERR;
		}
	}

	return diag;
}

/**
 * copy curr to prev
 * @param curr
 * @param prev
 * @return DIAG_OK
 */
static diag_t
cpu_copy_curr2prev(struct procfs_cpu * curr, struct procfs_cpu * prev, int len){
	int i;

	for( i = 0; i < len; i++ ){
		prev[i] = curr[i];
	}

	return DIAG_OK;
}

// -----------------------------------------
// evpath transport related functions
// -----------------------------------------
/**
 * Make use of the global variable ctrl_data that holds some helper variables
 *
 * @param data that will be sent over EVPATH, I guess memory for the data
 *        have been allocated accordingly
 * @return 0 - you were not able to read anything
 *         > 0 you have data to be sent; successful read
 */
int read_cap_cpu(void *data){
	int ret = POSITIVE_VAL;
	struct cpu_cap *p_rec = (struct cpu_cap*)data;

	*p_rec = ctrl_data.caps;
	// get the timestamp
	gettimeofday(&p_rec->ts, NULL);

	if (ctrl_data.caps.core_count <= 0)
		p_warn(DWARF_NAME ": The core count is 0. It should be >0. Ignoring\n");

	return ret;
}

/**
 * @param data Data sent over EVPath
 * @return 0  success
 *         anything else some issues (should not happen)
 */
int process_cap_cpu(void *data ){
	diag_t diag = DIAG_OK;
	int ret = ZERO_VAL;

	struct cpu_cap *event = data;
	struct dwarf_cpu_ctrl *d = &cpuctrl; // usage of the global variable

	if (NULL == d->p_bhb_ctx->header.curr_entry){
		const int B_SIZE = d->p_bhb_ctx->header.hdr_size;
		char b[B_SIZE];

		// the layout prefix
		GString *layout_prefix = g_string_new("");
		diag = bhb_mmap_layout_2_str(d->p_bhb_ctx, layout_prefix);
		g_snprintf(b, B_SIZE, "%s \nTIMESTAMP SCOUT_ID HOSTNAME PID CORE_COUNT\n", layout_prefix->str);
		g_string_free(layout_prefix, TRUE);

		bhb_mmap_wrt_hdr_header(d->p_bhb_ctx, b);
		bhb_mmap_wrt_bdy_header(d->p_bhb_ctx, "TIMESTAMP SCOUT_ID CPU0 CPU1 CPU2 CPU3 CPU4 CPU5 CPU6 CPU7 CPU8 CPU9 CPU10 CPU11 ...\n");
	}

	// now dump values
	const int BUF_SIZE = d->p_bhb_ctx->header.elem_size;
	char buf[BUF_SIZE];
	memset(buf, 0, BUF_SIZE);

	g_sprintf(buf, "%ld %d %s %d %d\n", timeval2usec(&event->ts),
			event->id.id, event->id.hostname,
			event->id.pid,
			event->core_count);

	if ( (diag = bhb_mmap_wrt_hdr_entry(d->p_bhb_ctx, buf, event->id.id))!=DIAG_OK){
		p_warn(DWARF_NAME ": issues with writing memory map.\n");
	}


	return ret;
}

/**
 * Make use of the global variable ctrl_data that holds some helper variables
 *
 * @param data that will be sent over EVPATH, I guess memory for the data
 *        have been allocated accordingly
 * @return 0 - you were not able to read anything
 *         > 0 you have data to be sent; successful read
 */
int read_mon_cpu(void *data){
	struct cpu_mon *p_rec = (struct cpu_mon*) data;
	diag_t diag = DIAG_OK;
	int ret = POSITIVE_VAL;

	char c[10]; // for the cpu
	int i = 0;
	char *line = NULL;
	size_t n = 0;


	// set values that we know for mon record; only the usage record will be
	// updated
	p_rec->id = ctrl_data.data.id;
	p_rec->core_plus_one_count = ctrl_data.data.core_plus_one_count;
	// get the timestamp
	gettimeofday(&p_rec->ts, NULL);
	// mem is not allocated in data for pointers; I guess all
	// the pointers are set to NULL (for sure this pointer)
	// so clear the buffer and point the rec value to the correct mem block
	memset(ctrl_data.data.cpu_and_core_usage, 0, sizeof(float) * ctrl_data.data.core_plus_one_count);
	p_rec->cpu_and_core_usage = ctrl_data.data.cpu_and_core_usage;


	struct procfs_cpu *curr = ctrl_data.cpu_and_core_usage_curr_arr;
	struct procfs_cpu *prev = ctrl_data.cpu_and_core_usage_prev_arr;

	// just in case
	memset(curr, 0, ctrl_data.data.core_plus_one_count * sizeof(struct procfs_cpu));


	// refresh the /proc/stat
	fseek(ctrl_data.p_proc_fs, 0, SEEK_SET);
	fflush(ctrl_data.p_proc_fs);

	// first line is the cpu as a whole, cpu
	// subsequent lines are cores: cpu0, cpu1, cpu2 ....

	for( i = 0; i < ctrl_data.data.core_plus_one_count; i++){
		if (getline(&line, &n, ctrl_data.p_proc_fs ) == -1)
			p_debug("Problem with reading the line from /proc/stat. Ignored ...\n");

		sscanf(line, "%s %ld %ld %ld %ld %ld %ld %ld %ld", c,
				&curr[i].user,
				&curr[i].nice,
				&curr[i].system,
				&curr[i].idle,
				&curr[i].iowait,
				&curr[i].irq,
				&curr[i].softirq,
				&curr[i].steal);
	}

	// count the usage for an entire cpu and for each core
	// the idle time is a combined time idle and iowait (maybe this assumption
	// is wrong but the cpu is waiting for io data during iowait, right?
	diag = cpu_count_usage(p_rec, curr, prev);

	cpu_copy_curr2prev(curr, prev, p_rec->core_plus_one_count);

	return ret;
}

/**
 * @param data Data sent over EVPath
 * @return 0  success
 *         anything else some issues (should not happen)
 */
int process_mon_cpu(void *data ){
	diag_t diag = DIAG_OK;
	int ret = ZERO_VAL;

	struct cpu_mon *p_rec = (struct cpu_mon *)data;

	const int BUF_SIZE = cpuctrl.p_bhb_ctx->body.elem_size;
	char buf[BUF_SIZE];

	memset(buf, 0, BUF_SIZE);

	g_sprintf(buf, "%ld %d", timeval2usec(&p_rec->ts), p_rec->id);

	int curr_len = strlen(buf);
	char *buf_curr = NULL;

	int i = 0;

	// now write the cpu utilization
	for( i = 0; i < p_rec->core_plus_one_count; i++){
		curr_len = strlen(buf);
		buf_curr = buf + curr_len;
		g_sprintf(buf_curr, " %.2f", p_rec->cpu_and_core_usage[i]);
	}

	// end it with a newline character
	buf_curr = calc_ptr_buf_after_offset(buf, &curr_len);
	g_snprintf(buf_curr, BUF_SIZE - curr_len, "\n");

	diag = bhb_mmap_wrt_bdy_entry(cpuctrl.p_bhb_ctx, buf, p_rec->id);

	if (DIAG_OK != diag)
		ret = -1;

	// this is a kind of temporary solution with printing data to the screen
#ifdef DEBUG
	// print it the data
	printf("TIMESTAMP SPY_ID CPU_AGG CPU0 CPU1 CPU2 ... CPUN\n");
	printf("%ld %d", timeval2usec(&p_rec->ts), p_rec->id);

	for( i = 0; i < p_rec->core_plus_one_count; i++){
		printf(" %.2f", p_rec->cpu_and_core_usage[i]);
	}
	printf("\n");
#endif
	return ret;
}


// ----------------------------------
// dwarf related functions
// ----------------------------------
/**
 * the initialization function that is invoked during
 * the enabling of the spy
 * @param p_dwarf_rt_ctx the context of the runtime
 * @param p_spy_ctx the context of the spy; it is allocated somewhere
 *        else; I do not allocate memory for this here
 * @return DIAG_OK if everything went ok, otherwise errors
 */
diag_t dwarf_init_cpu_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx){
	diag_t diag = DIAG_OK;

	p_spy_ctx->init_ranger_func = &dwarf_init_cpu_ranger;
	p_spy_ctx->init_spy_func = &dwarf_init_cpu_spy;

	// initialize cpu data
	p_spy_ctx->evpath_ctx_cap.format_list = cpu_cap_format_list;
	p_spy_ctx->evpath_ctx_cap.res_type = CPU;
	p_spy_ctx->evpath_ctx_cap.data_size = sizeof(struct cpu_cap);
	p_spy_ctx->evpath_ctx_cap.period_sec = 0;

	// init transport functions on the collector (spy side)
	p_spy_ctx->evpath_ctx_cap.clctor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_cap.clctor_read_func = &read_cap_cpu;
	p_spy_ctx->evpath_ctx_cap.clctor_process_func = NULL;

	// right now we do not distinguish the functions on the aggregator side
	p_spy_ctx->evpath_ctx_cap.agrtor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_cap.agrtor_read_func = NULL;
	p_spy_ctx->evpath_ctx_cap.agrtor_process_func = &process_cap_cpu;


	// initialize monitoring data
	// init transport fields
	p_spy_ctx->evpath_ctx_mon.format_list = cpu_mon_format_list;
	p_spy_ctx->evpath_ctx_mon.res_type = CPU;
	p_spy_ctx->evpath_ctx_mon.data_size = sizeof(struct cpu_mon);
	p_spy_ctx->evpath_ctx_mon.period_sec = p_dwarf_rt_ctx->ini_file.patrol_freq;

	// init transport functions on the collector (spy side)
	p_spy_ctx->evpath_ctx_mon.clctor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_mon.clctor_read_func = &read_mon_cpu;
	p_spy_ctx->evpath_ctx_mon.clctor_process_func = NULL;

	// right now we do not distinguish the functions on the aggregator side
	p_spy_ctx->evpath_ctx_mon.agrtor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_mon.agrtor_read_func = NULL;
	p_spy_ctx->evpath_ctx_mon.agrtor_process_func = &process_mon_cpu;

	return diag;
}

/**
 * used for the initialization of the aggregator side; we need to initialize
 * the memory map regions
 * @param p_dwarf_rt_ctx
 * @param p_spy_ctx
 * @return
 */
diag_t dwarf_init_cpu_ranger(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx){
	diag_t diag = DIAG_OK;

	// initialize the bhb_mmap structure; the second part
	// of the initialization will take part when we know the
	// name of the file assigned to us. the second part of initialization
	// should be held in scut_trooper_run function; we create
	// the relevant pointers and structures for a header, body and blocks
	// in the body
	struct bhb_mmap_param params = {
				.block_count = CPU_MMAP_BLOCK_COUNT,
				.hdr_header_size_bytes = CPU_MMAP_HDR_HDR_SIZE_BYTES,
				.hdr_entry_size_bytes = CPU_MMAP_HDR_ENTRY_SIZE_BYTES,
				.bdy_header_size_bytes = CPU_MMAP_BODY_HDR_SIZE_BYTES,
				.bdy_block_entry_size_bytes = CPU_MMAP_BLOCK_ENTRY_SIZE_BYTES,
				.bdy_block_entry_count = CPU_MMAP_BLOCK_ENTRY_COUNT
	};

	cpuctrl.p_bhb_ctx = bhb_create_mapped_file(&diag, &params, p_dwarf_rt_ctx, p_spy_ctx);
	if (DIAG_OK != diag ){
		return diag;
	}

	if (!cpuctrl.p_bhb_ctx){
		diag = DIAG_ERR;
		return diag;
	}

	return diag;
}

/**
 * I am using the ctrl_data global variable
 * @param p_dwarf_rt_ctx
 * @param p_spy_ctx
 * @return
 */
diag_t dwarf_init_cpu_spy(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx){
	diag_t diag = DIAG_OK;

	// first get caps
	// set identity
	if ( (diag = get_identity(&ctrl_data.caps.id)) != DIAG_OK ){
		return diag;
	}

	// TODO next line likely to be removed since the timestamp should be taken
	// somewhere else when the capabilities are taken in fact, i.e., in read_
	// read_cap_cpu(), and not here
	// gettimeofday(&ctrl_data.caps.ts, NULL);
	ctrl_data.caps.core_count = cpu_get_core_count();

	// now setup the structures for handling the mon data
	ctrl_data.data.core_plus_one_count = ctrl_data.caps.core_count + 1;

	// TODO not sure if this mem alloc is necessary
	ctrl_data.data.cpu_and_core_usage = calloc(ctrl_data.data.core_plus_one_count,
			sizeof(float));

	if ( (diag = diag_mem_alloc_ret(ctrl_data.data.cpu_and_core_usage, "Issues with calloc. Ignored ...\n")) != DIAG_OK ){
		return diag;
	}

	// set identity
	ctrl_data.data.id = ctrl_data.caps.id.id;

	ctrl_data.p_proc_fs = fopen("/proc/stat", "r");

	if ( (diag = diag_ptr_null_ret(ctrl_data.p_proc_fs, "Can't open /proc/stat\n")) != DIAG_OK) {
		p_error("Issues with opening /proc/stat. Ignored ...\n");
	}

	ctrl_data.cpu_and_core_usage_curr_arr = calloc(ctrl_data.data.core_plus_one_count, sizeof(struct procfs_cpu));

	if ( (diag = diag_mem_alloc_ret(ctrl_data.cpu_and_core_usage_curr_arr, "Issues with calloc. Ignored ... \n")) != DIAG_OK ){
		return diag;
	}

	ctrl_data.cpu_and_core_usage_prev_arr = calloc(ctrl_data.data.core_plus_one_count, sizeof(struct procfs_cpu));

	if ( (diag = diag_mem_alloc_ret(ctrl_data.cpu_and_core_usage_prev_arr, "Issues with calloc. Ignored ... \n")) != DIAG_OK ){
		return diag;
	}

	return diag;
}

/**
 * @file net_spy_torso.c
 *
 * @date Oct 4, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */


#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <glib.h>

#include "clusterspy.h"
#include "evpath.h"
#include "devel.h"
#include "spy.h"
#include "net_spy_torso_types.h"


// ------------------ FFS related data ---------------------
FMField net_cap_field_list[] = {
  {"ts", "struct timeval", sizeof(struct timeval), FMOffset( struct net_cap *, ts) },
  {"id", "struct identity", sizeof(struct identity), FMOffset( struct net_cap *, id)},
  {"ib_count", "integer", sizeof(int), FMOffset(struct net_cap *, ib_count)},
  {"ib_mbps_arr", "float[ib_count]", sizeof(float), FMOffset(struct net_cap *, ib_mbps_arr) },
  {"eth_count", "integer", sizeof(int), FMOffset(struct net_cap*, eth_count)},
  {"eth_mbps_arr", "float[eth_count]", sizeof(float), FMOffset(struct net_cap *, eth_mbps_arr)},
  {NULL, NULL, 0, 0}
};

//! the order of struct is important, first the main, then the elements that
//! are defined in the structure first; first introduce then explain
FMStructDescRec net_cap_format_list[] = {
		{ "struct net_cap", net_cap_field_list, sizeof(struct net_cap), NULL },
		{ "struct timeval", timestamp_field_list, sizeof(struct timeval), NULL },
		{ "struct identity", identity_field_list, sizeof(struct identity), NULL },
		{ NULL, NULL, 0, NULL}
};

FMField net_mon_field_list[] = {
  {"ts", "struct timeval", sizeof(struct timeval), FMOffset( struct net_mon *, ts) },
  {"id", "integer", sizeof(int), FMOffset( struct net_mon *, id)},
  {"ib_count", "integer", sizeof(int), FMOffset(struct net_mon *, ib_count)},
  {"ib_usage_arr", "float[ib_count]", sizeof(float), FMOffset(struct net_mon *, ib_usage_arr) },
  {"ib_transmitted_arr", "float[ib_count]",sizeof(float),FMOffset(struct net_mon *, ib_transmitted_arr) },
  {"ib_recv_arr", "float[ib_count]",sizeof(float),FMOffset(struct net_mon *, ib_recv_arr) },
  {"eth_count", "integer", sizeof(int), FMOffset(struct net_mon*, eth_count)},
  {"eth_usage_arr", "float[eth_count]", sizeof(float), FMOffset(struct net_mon *, eth_usage_arr)},
  {"eth_transmitted_arr", "float[eth_count]",sizeof(float),FMOffset(struct net_mon *, eth_transmitted_arr) },
  {"eth_recv_arr", "float[eth_count]",sizeof(float),FMOffset(struct net_mon *, eth_recv_arr) },

  {NULL, NULL, 0, 0}
};

FMStructDescRec net_mon_format_list[] = {
  { "struct net_mon", net_mon_field_list, sizeof(struct net_mon), NULL },
  { "struct timeval", timestamp_field_list, sizeof(struct timeval), NULL },
  { NULL, NULL, 0, NULL}
};

// other data
static struct net_mon_cnt_data ctrl_data;

// forward declarations
static diag_t get_iface_count(const char *iface_prefix,  int *count );
static diag_t net_count_specific_nic_usage(enum net_data_transfer_rate_variant variant,
		struct net_usage *usage_arr, int usage_arr_count, float *nic_arr, int nic_count);
static diag_t net_cpy_curr_to_prev(struct net_usage * a, int a_count);


diag_t dwarf_init_net_ranger(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
diag_t dwarf_init_net_spy(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);

/**
 * Make use of the global variable ctrl_data that holds some helper variables.
 * I assume the identity has been already set in the init spy function.
 * @todo I am ignoring the arrays of mbps, however I am callocing memory
 *       for the ctrl_data.caps.eth_mbps_arr, and ctrl_data.caps.ib_mbps_arr and
 *       they should be freed somewhere
 *
 * @param data that will be sent over EVPATH, I guess memory for the data
 *        have been allocated accordingly
 * @return 0 - you were not able to read anything
 *         > 0 you have data to be sent; successful read
 */
int read_cap_net(void *data){

	int ret = POSITIVE_VAL;
	const int ERROR = 0;
	struct net_cap *p_rec = (struct net_cap*)data;

	// the important caps have been already captured in the net_spy_init function
	// you just need to copy them to the caps structure
	ctrl_data.caps.eth_count = ctrl_data.data.eth_count;
	ctrl_data.caps.ib_count = ctrl_data.data.ib_count;

	// the ctrl_data.caps.id has been already set in net_spy_init

	// get the timestamp
	gettimeofday(&ctrl_data.caps.ts, NULL);

	// I just ignore the caps the arrays of mbps; but we are sending them
	ctrl_data.caps.eth_mbps_arr = calloc(ctrl_data.caps.eth_count, ctrl_data.caps.eth_count );
	if ( diag_mem_alloc_ret(ctrl_data.caps.eth_mbps_arr, "Can't callocate mem\n") != DIAG_OK){
		return ERROR;
	}
	// I just ignore the caps the arrays of mbps; but we are sending them
	ctrl_data.caps.ib_mbps_arr = calloc(ctrl_data.caps.ib_count, ctrl_data.caps.ib_count );
	if ( diag_mem_alloc_ret(ctrl_data.caps.ib_mbps_arr, "Can't callocate mem\n") != DIAG_OK){
		return ERROR;
	}

	*p_rec = ctrl_data.caps;

	return ret;
}

/**
 * @param data Data sent over EVPath
 * @return 0  success
 *         anything else some issues (should not happen)
 */
int process_cap_net(void *data ){
	diag_t diag = DIAG_OK;
	int ret = ZERO_VAL;
	struct net_cap *event = data;
	struct net_mon_cnt_data *d = &ctrl_data;

	// dump headers if this is the first time you are running it
	if (NULL == d->p_bhb_ctx->header.curr_entry){
		const int B_SIZE = d->p_bhb_ctx->header.hdr_size;
		char b[B_SIZE];

		// the layout prefix
		GString *layout_prefix = g_string_new("");
		diag = bhb_mmap_layout_2_str(d->p_bhb_ctx, layout_prefix);
		g_snprintf(b, B_SIZE, "%s \nTIMESTAMP SPY_ID HOSTNAME PID IB_COUNT ETH_COUNT IB0_MBPS IB1_MPBS ... | ETH0_MBPS ETH1_MBPS\n", layout_prefix->str);
		g_string_free(layout_prefix, TRUE);

		bhb_mmap_wrt_hdr_header(d->p_bhb_ctx, b);
		bhb_mmap_wrt_bdy_header(d->p_bhb_ctx, "TIMESTAMP SPY_ID IB0_TOT_KBPS IB0_TRANS_KBPS IB0_RECV_KBPS IB1_TOT_KBPS ... | ETH0_TOT_KBPS ETH0_TRANS_KBPS ETH0_RECV_KBPS ETH1_TOT_KBPS ...\n");
	}

	// now dump the values
	const int BUF_SIZE = d->p_bhb_ctx->header.elem_size;
	char buf[BUF_SIZE];
	memset(buf, 0, BUF_SIZE);

	g_snprintf(buf, BUF_SIZE, "%ld %d %s %d %d %d",
			timeval2usec(&event->ts),
			event->id.id,
			event->id.hostname,
			event->id.pid,
			event->ib_count,
			event->eth_count);
	int curr_len = 0;
	char *curr_buf = NULL;

	int i = 0;

	// first write the ib data
	for( i = 0; i < event->ib_count; i++){
		curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
		g_snprintf(curr_buf, BUF_SIZE - curr_len, " %.2f", event->ib_mbps_arr[i]);
	}

	// write the separator
	curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
	g_snprintf(curr_buf, BUF_SIZE - curr_len, " |");

	// now write the eth
	for( i = 0; i < event->eth_count; i++){
		curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
		g_snprintf(curr_buf, BUF_SIZE - curr_len, " %.2f", event->eth_mbps_arr[i]);
	}

	curr_buf = calc_ptr_buf_after_offset(buf, &curr_len);
	g_snprintf(curr_buf, BUF_SIZE - curr_len, "\n");

	diag = bhb_mmap_wrt_hdr_entry(d->p_bhb_ctx, buf, event->id.id);
	if( DIAG_OK != diag){
		p_warn("Issues with writing caps to memory mapped\n");
	}

	return ret;
}

/**
 * Make use of the global variable ctrl_data that holds some helper variables
 *
 * @param data that will be sent over EVPATH, I guess memory for the data
 *        have been allocated accordingly
 * @return 0 - you were not able to read anything
 *         > 0 you have data to be sent; successful read
 */
int read_mon_net(void *data){
	diag_t diag = DIAG_OK;
	// the return value
	int ret = POSITIVE_VAL;
	struct net_mon *p_rec = (struct net_mon*) data;

	// set values that we know for the mon record;
	// only the usage record will be updated
	p_rec->id = ctrl_data.data.id;
	p_rec->eth_count = ctrl_data.data.eth_count;
	p_rec->ib_count = ctrl_data.data.ib_count;
	// get the timestamp
	gettimeofday(&ctrl_data.data.ts, NULL);
	// a little bit of redundancy but for now it must work
	p_rec->ts = ctrl_data.data.ts;

	// we need to assign allocated data for data
	if (p_rec->eth_count>0){
		memset(ctrl_data.data.eth_usage_arr, 0, sizeof(float) * ctrl_data.data.eth_count );
		memset(ctrl_data.data.eth_transmitted_arr, 0, sizeof(float) * ctrl_data.data.eth_count );
		memset(ctrl_data.data.eth_recv_arr, 0, sizeof(float) * ctrl_data.data.eth_count );
	}
	if (p_rec->ib_count > 0 ){
		memset(ctrl_data.data.ib_usage_arr, 0, sizeof(float) * ctrl_data.data.ib_count );
		memset(ctrl_data.data.ib_transmitted_arr, 0, sizeof(float) * ctrl_data.data.ib_count );
		memset(ctrl_data.data.ib_recv_arr, 0, sizeof(float) * ctrl_data.data.ib_count );
	}

	p_rec->eth_usage_arr = ctrl_data.data.eth_usage_arr;
	p_rec->eth_transmitted_arr = ctrl_data.data.eth_transmitted_arr;
	p_rec->eth_recv_arr	= ctrl_data.data.eth_recv_arr;

	p_rec->ib_usage_arr = ctrl_data.data.ib_usage_arr;
	p_rec->ib_transmitted_arr = ctrl_data.data.ib_transmitted_arr;
	p_rec->ib_recv_arr = ctrl_data.data.ib_recv_arr;

	// now set the pointer to a beginning of the file
	fseek(ctrl_data.p_proc_fs, 0, SEEK_SET);
	fflush(ctrl_data.p_proc_fs);



	// skip first two lines since they are the header, assuming that
	// diagnostic info is fine so not checking it
	diag = skip_line(ctrl_data.p_proc_fs);
	diag = skip_line(ctrl_data.p_proc_fs);

	int i_eth = 0;
	int i_ib = 0;
	char *line = NULL;
	size_t n = 0;
	const int NAME_LEN = 30;
	char buf[NAME_LEN];
	gboolean is_ib = FALSE;
	gboolean is_eth = FALSE;

	while (getline(&line, &n, ctrl_data.p_proc_fs) != -1){
		// clear this before analyzing the line
		is_ib = FALSE;
		is_eth = FALSE;
		sscanf(line, "%s", buf);
		// just in case if you need to remove the leading whitespace
		// @todo it seems that this is unnecessary
		g_strchug(buf);
		// I assume that the order and number of interfaces don't change
		// during the execution of the program; so the order is preserved
		if (strncmp(buf, "e", 1) == 0) {
			is_eth = TRUE;
		}
		if (strncmp(buf, "i", 1) == 0) {
			is_ib = TRUE;
		}
		unsigned long trans_bytes = 0;
		unsigned long recv_bytes = 0;
		if (is_ib || is_eth) {
			// this is our interface we are monitoring, so get the data

			// this is for sscanf since %*lu produces warnings
			unsigned long r1, r2, r3, r4, r5, r6, r7, t1, t2, t3, t4, t5, t6,
					t7, t8;

			// the buf contains our first number we just need to get it
			// eth0:1341413412341; we need that number after eth0
			// so we need to have the line without the name of eth0:
			gchar ** only_numbers = g_strsplit(line, ":", 2);
			// the string name of the interface is in only_numbers[0];
			// the interesting part for us is in only_numbers[1]
			sscanf(only_numbers[1], "%lu %lu %lu %lu %lu %lu %lu %lu %lu "
					"%lu %lu %lu %lu %lu %lu %lu %lu", &recv_bytes, &r1, &r2,
					&r3, &r4, &r5, &r6, &r7, &trans_bytes, &t1, &t2, &t3, &t4,
					&t5, &t6, &t7, &t8);

			// '*' means that we are not interested in this value; you can use
			// sscanf(line, "%*s %lu %lu %lu %lu %lu %lu %lu %lu "
			//					"%lu %lu %lu %lu %lu %lu %lu %lu",
			//					&r1, &r2, &r3, &r4, &r5, &r6, &r7,
			//					&trans_bytes,
			//					&t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8);

			// now update the values in the array
			if (is_ib && i_ib < ctrl_data.data.ib_count) {
				ctrl_data.ib_usage[i_ib].curr.receive.bytes = recv_bytes;
				ctrl_data.ib_usage[i_ib].curr.transmit.bytes = trans_bytes;
				ctrl_data.ib_usage[i_ib].curr.ts = ctrl_data.data.ts;
				i_ib ++;
			}

			// now update the values in the array
			if (is_eth && i_eth < ctrl_data.data.eth_count) {
				ctrl_data.eth_usage[i_eth].curr.receive.bytes = recv_bytes;
				ctrl_data.eth_usage[i_eth].curr.transmit.bytes = trans_bytes;
				ctrl_data.eth_usage[i_eth].curr.ts = ctrl_data.data.ts;
				i_eth ++;
			}
		}
	}


	// now count usage; TODO think about now calling functions so many times
	// maybe some refactoring that will get this done in two or one functions
	if (ctrl_data.data.eth_count > 0 ){
		if ((diag = net_count_specific_nic_usage(NET_TOTAL, ctrl_data.eth_usage, ctrl_data.data.eth_count, ctrl_data.data.eth_usage_arr,
			ctrl_data.data.eth_count)) != DIAG_OK){
			return diag;
		}
		if ((diag = net_count_specific_nic_usage(NET_TRANSMITTED, ctrl_data.eth_usage, ctrl_data.data.eth_count, ctrl_data.data.eth_transmitted_arr,
				ctrl_data.data.eth_count)) != DIAG_OK){
			return diag;
		}
		if ((diag = net_count_specific_nic_usage(NET_RECV, ctrl_data.eth_usage, ctrl_data.data.eth_count, ctrl_data.data.eth_recv_arr,
				ctrl_data.data.eth_count)) != DIAG_OK){
			return diag;
		}
	}
	if ( ctrl_data.data.ib_count > 0 ){
		if ((diag = net_count_specific_nic_usage(NET_TOTAL, ctrl_data.ib_usage, ctrl_data.data.ib_count, ctrl_data.data.ib_usage_arr,
			ctrl_data.data.ib_count)) != DIAG_OK){
			return diag;
		}
		if ((diag = net_count_specific_nic_usage(NET_TRANSMITTED, ctrl_data.ib_usage, ctrl_data.data.ib_count, ctrl_data.data.ib_transmitted_arr,
			ctrl_data.data.ib_count)) != DIAG_OK){
			return diag;
		}
		if ((diag = net_count_specific_nic_usage(NET_RECV, ctrl_data.ib_usage, ctrl_data.data.ib_count, ctrl_data.data.ib_recv_arr,
			ctrl_data.data.ib_count)) != DIAG_OK){
			return diag;
		}
	}

	// now copy curr to prev
	if (ctrl_data.data.eth_count > 0)
		diag = net_cpy_curr_to_prev(ctrl_data.eth_usage, ctrl_data.data.eth_count);
	if (ctrl_data.data.ib_count > 0)
		diag = net_cpy_curr_to_prev(ctrl_data.ib_usage, ctrl_data.data.ib_count);

	return ret;
}

/**
 * @param data Data sent over EVPath
 * @return 0  success
 *         anything else some issues
 */
int process_mon_net(void *data ){
	struct net_mon *p_rec = (struct net_mon *)data;
	struct net_mon_cnt_data *d = &ctrl_data;
	diag_t diag = DIAG_OK;
	const int NOT_ZERO_VAL = -1;

	const int BUF_SIZE = d->p_bhb_ctx->body.elem_size;
	char buf[BUF_SIZE];

	memset(buf, 0, BUF_SIZE);

	g_snprintf(buf, BUF_SIZE, "%ld %d", timeval2usec(&p_rec->ts), p_rec->id);

	int curr_len = strlen(buf);
	char *buf_curr = NULL;

	int i = 0;

	// first write the ib data
	for( i = 0; i < p_rec->ib_count; i++){
		curr_len = strlen(buf);
		buf_curr = buf + curr_len;
		g_snprintf(buf_curr, BUF_SIZE - curr_len, " %.2f %.2f %.2f",
				p_rec->ib_usage_arr[i], p_rec->ib_transmitted_arr[i], p_rec->ib_recv_arr[i]);
	}

	// write the separator
	curr_len = strlen(buf);
	buf_curr = buf + curr_len;
	g_snprintf(buf_curr, BUF_SIZE - curr_len, " |");

	// now write the eth
	for( i = 0; i < p_rec->eth_count; i++){
		curr_len = strlen(buf);
		buf_curr = buf + curr_len;

		g_snprintf(buf_curr, BUF_SIZE - curr_len, " %.2f %.2f %.2f",
				p_rec->eth_usage_arr[i], p_rec->eth_transmitted_arr[i], p_rec->eth_recv_arr[i]);
	}

	buf_curr = calc_ptr_buf_after_offset(buf, &curr_len);
	g_snprintf(buf_curr, BUF_SIZE - curr_len, "\n");

	diag = bhb_mmap_wrt_bdy_entry(d->p_bhb_ctx, buf, p_rec->id);
	if ( DIAG_OK  != diag){
		p_warn("Issues with writing to the mmap.\n.");
		return NOT_ZERO_VAL;
	}

	// a temporary solution with printing data to the screen
#ifdef DEBUG
	printf("TIMESTAMP SPY_ID IB0(TSMITTED+RECV,TSMITTED,RECV) ... | ETH0(TSMITTED+RECV,TSMITTED,RECV) ...\n");
	printf("%ld %d", timeval2usec(&p_rec->ts), p_rec->id);

	for( i = 0; i < p_rec->ib_count; i++){
		printf(" %.2f %.2f %.2f", p_rec->ib_usage_arr[i], p_rec->ib_transmitted_arr[i],
				p_rec->ib_recv_arr[i]);
	}

	printf(" |");
	for( i = 0; i < p_rec->eth_count; i++){
		printf(" %.2f %.2f %.2f", p_rec->eth_usage_arr[i], p_rec->eth_transmitted_arr[i],
						p_rec->eth_recv_arr[i]);
	}
	printf("\n");
#endif

	return ZERO_VAL;
}



// =======================================================================
// dwarf related functions
// =======================================================================
diag_t dwarf_init_net_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx){
	diag_t diag = DIAG_OK;

	p_spy_ctx->init_ranger_func = &dwarf_init_net_ranger;
	p_spy_ctx->init_spy_func = &dwarf_init_net_spy;

	// initialize cap data
	p_spy_ctx->evpath_ctx_cap.format_list = net_cap_format_list;
	p_spy_ctx->evpath_ctx_cap.res_type = NETWORK;
	p_spy_ctx->evpath_ctx_cap.data_size = sizeof(struct net_cap);
	p_spy_ctx->evpath_ctx_cap.period_sec = 0;  // to indicate we want to call it only once

	// init transport functions on the collector (spy side)
	p_spy_ctx->evpath_ctx_cap.clctor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_cap.clctor_read_func = &read_cap_net;
	p_spy_ctx->evpath_ctx_cap.clctor_process_func = NULL;

	// right now we do not distinguish the functions on the aggregator side
	p_spy_ctx->evpath_ctx_cap.agrtor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_cap.agrtor_read_func = NULL;
	p_spy_ctx->evpath_ctx_cap.agrtor_process_func = &process_cap_net;


	// initialize monitoring data
	// init transport fields
	p_spy_ctx->evpath_ctx_mon.format_list = net_mon_format_list;
	p_spy_ctx->evpath_ctx_mon.res_type = NETWORK;
	p_spy_ctx->evpath_ctx_mon.data_size = sizeof(struct net_mon);
	p_spy_ctx->evpath_ctx_mon.period_sec = p_dwarf_rt_ctx->ini_file.patrol_freq;

	// init transport functions on the collector (spy side)
	p_spy_ctx->evpath_ctx_mon.clctor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_mon.clctor_read_func = &read_mon_net;
	p_spy_ctx->evpath_ctx_mon.clctor_process_func = NULL;

	// right now we do not distinguish the functions on the aggregator side
	p_spy_ctx->evpath_ctx_mon.agrtor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_mon.agrtor_read_func = NULL;
	p_spy_ctx->evpath_ctx_mon.agrtor_process_func = &process_mon_net;

	return diag;
}

/**
 * used for the initialization of the aggregator side; we need to initialize
 * the memory map regions
 * @param p_dwarf_rt_ctx
 * @param p_spy_ctx
 * @return
 */
diag_t dwarf_init_net_ranger(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx){
	diag_t diag = DIAG_OK;

	// initialize the bhb_mmap structure; the second part
	// of the initialization will take part when we know the
	// name of the file assigned to us. the second part of initialization
	// should be held in scut_trooper_run function; we create
	// the relevant pointers and structures for a header, body and blocks
	// in the body
	struct bhb_mmap_param params = {
				.block_count = NET_MMAP_BLOCK_COUNT,
				.hdr_header_size_bytes = NET_MMAP_HDR_HDR_SIZE_BYTES,
				.hdr_entry_size_bytes = NET_MMAP_HDR_ENTRY_SIZE_BYTES,
				.bdy_header_size_bytes = NET_MMAP_BODY_HDR_SIZE_BYTES,
				.bdy_block_entry_size_bytes = NET_MMAP_BLOCK_ENTRY_SIZE_BYTES,
				.bdy_block_entry_count = NET_MMAP_BLOCK_ENTRY_COUNT
	};

	ctrl_data.p_bhb_ctx = bhb_create_mapped_file(&diag, &params, p_dwarf_rt_ctx, p_spy_ctx);
	if (DIAG_OK != diag ){
		return diag;
	}

	if (!ctrl_data.p_bhb_ctx){
		diag = DIAG_ERR;
		return diag;
	}

	return diag;
}


/**
 * Initializes the net spy; This is invoked on the collector side.
 * mallocs the data for the net_mon structure for the actual data
 * sent to aggregator and for the utility structures to keep track
 * about changes in the /proc/ filesystem
 * I am using the ctrl_data global variable
 *
 * @param p_dwarf_rt_ctx
 * @param p_spy_ctx
 * @return
 */
diag_t dwarf_init_net_spy(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx){
	diag_t diag = DIAG_OK;

	// set identity
	if ( (diag = get_identity(&ctrl_data.caps.id)) != DIAG_OK ){
		return diag;
	}
	// set data identity to the id you got
	ctrl_data.data.id = ctrl_data.caps.id.id;

	// fill the iface data
	if ((diag = get_iface_count("eth", &ctrl_data.data.eth_count)) != DIAG_OK){
		return diag;
	}

	if ((diag = get_iface_count("ib", &ctrl_data.data.ib_count)) != DIAG_OK){
		return diag;
	}
	// @todo check if this works, since you might need to open/close
	// this file every time you want to send a task
	ctrl_data.p_proc_fs = fopen("/proc/net/dev", "r");
	if ( (diag = diag_ptr_null_ret(ctrl_data.p_proc_fs,"Can't open /proc/net/dev\n")) != DIAG_OK){
		return diag;
	}

	// init utility arrays for storing readings from /proc/net/dev
	ctrl_data.eth_usage = calloc(ctrl_data.data.eth_count, sizeof(struct net_usage));
	if ((diag= diag_mem_alloc_ret(ctrl_data.eth_usage, "Issues")) != DIAG_OK){
		return diag;
	}

	// init utility arrays for storing readings from /proc/net/dev
	ctrl_data.ib_usage = calloc(ctrl_data.data.ib_count, sizeof(struct net_usage));
	if ((diag= diag_mem_alloc_ret(ctrl_data.ib_usage, "Issues")) != DIAG_OK){
		return diag;
	}

	// allocate the memory for the usage on eth
	if( ctrl_data.data.eth_count > 0){

		ctrl_data.data.eth_usage_arr = (float *) calloc(ctrl_data.data.eth_count, sizeof(float));
		if ( (diag = diag_mem_alloc_ret(ctrl_data.data.eth_usage_arr, "Can't callocate mem\n")) != DIAG_OK){
			return diag;
		}
		ctrl_data.data.eth_transmitted_arr =  (float *) calloc(ctrl_data.data.eth_count, sizeof(float));
		if ( (diag = diag_mem_alloc_ret(ctrl_data.data.eth_transmitted_arr, "Can't callocate mem\n")) != DIAG_OK){
			return diag;
		}
		ctrl_data.data.eth_recv_arr = (float *) calloc(ctrl_data.data.eth_count, sizeof(float));
		if ( (diag = diag_mem_alloc_ret(ctrl_data.data.eth_recv_arr, "Can't callocate mem\n")) != DIAG_OK){
			return diag;
		}
	}

	// allocate the memory for the usage on ib
	if (ctrl_data.data.ib_count > 0){
		ctrl_data.data.ib_usage_arr = (float *) calloc(ctrl_data.data.ib_count, sizeof(float));
		if ( (diag = diag_mem_alloc_ret(ctrl_data.data.ib_usage_arr, "Can't callocate mem\n")) != DIAG_OK){
			return diag;
		}
		ctrl_data.data.ib_transmitted_arr = (float *) calloc(ctrl_data.data.ib_count, sizeof(float));
		if ( (diag = diag_mem_alloc_ret(ctrl_data.data.ib_transmitted_arr, "Can't callocate mem\n"))!= DIAG_OK){
			return diag;
		}
		ctrl_data.data.ib_recv_arr = (float *) calloc(ctrl_data.data.ib_count, sizeof(float));
		if ( (diag = diag_mem_alloc_ret(ctrl_data.data.ib_recv_arr, "Can't callocate mem\n")) != DIAG_OK){
			return diag;
		}
	}

	return diag;
}


// ================================================================
// static functions
// ================================================================

// ---------------------------------
// evpath transport  utility functions
// ---------------------------------


/**
 * counts the interfaces that start with a specified prefix in
 *  /proc/net/dev
 *
 *  The function skips the header line; read lines and tries to
 *  search for the prefix.
 * @param iface_prefix (IN) the prefix to be looked for
 * @param count (OUT) The number of prefix occurrences
 * @return SCOUT_OK if everything ok
 *         != SCOUT_OK if errors occurred
 */
static
diag_t get_iface_count(const char *iface_prefix,  int *count ){
	diag_t diag = DIAG_OK;
	FILE *f = fopen("/proc/net/dev", "r");
	// the buffer will be allocated
	char *line = NULL;
	size_t n = 0;
	ssize_t read;

	if ((diag =
			diag_ptr_null_ret((void *)iface_prefix, "The interface prefix is NULL")) != DIAG_OK){
		return diag;
	}

	*count ^= *count;   // ie. clear the counter *count = 0

	int i = 0;
	// skip two headers lines
	for( i = 0; i < 2; i++){
		if ((diag = skip_line(f)) != DIAG_OK){
			fclose(f);
			return diag;
		}
	}


	// the size of the line from /proc/net/dev
	const int BUF_SIZE=200;
	char buf[BUF_SIZE];

	// now read the lines containing interfaces
	while ( (read=getline(&line, &n, f)) != -1){
		char c;
		// you have to scanf character by character because in the variable
		// line you have randomly set '\0' NULL in that string
		memset(buf,0,BUF_SIZE);
		i = 0;
		g_assert(0 == i);
		char *line_org = line;
		while( sscanf(line_org, "%c", &c ) ){
			// check if we exceed the limit
			if( BUF_SIZE == i )
				p_error("Buffer size reached. Increase the buffer %d\n", BUF_SIZE);
			// check if should we leave the loop; since the ':'
			// is the delimiter of the interface name in /proc/net/dev
			if( ':' == c ){
				// don't need to put NULL character because buffer is
				// zeroed at the beginning of the loop
				break;
			} else {
				buf[i] = c;
				i++;
			}
			// get the next character
			line_org++;
		}
		// now remove leading spaces
		g_strchug(buf);
		if (g_str_has_prefix(buf, iface_prefix)){
			// ok we found it; increase the counter
			(*count)++;
		}
	}

	free(line);
	fclose(f);
	return diag;
}
/**
 * copy curr to prev of the array consisting of struct net_usage
 * @param a the array that we use
 * @param a_count the size of the array a
 * @return DIAG_OK
 */
static diag_t
net_cpy_curr_to_prev(struct net_usage * a, int a_count){
	diag_t diag = DIAG_OK;
	int i = 0;

	for( i = 0; i < a_count; i++ ){
		a[i].prev = a[i].curr;
	}

	return diag;
}
/**
 * Count the usage of network bandwidth
 * @param variant (IN) which variant of function we use to calculate data transfer rate
 * @param usage_arr (IN) The array that stores the previous and current readings
 * @param usage_arr_count (IN) The size of the usage_arr
 * @param nic_arr (OUT) The array that will have the data transfer rate since the last measurement
 *                      currently it is outputted in kilobytes per second (kilo meaning
 *                      1000)
 * @param nic_count
 * @return
 */
static diag_t
net_count_specific_nic_usage(enum net_data_transfer_rate_variant variant,
		struct net_usage *usage_arr, int usage_arr_count, float *nic_arr, int nic_count){
	diag_t diag = DIAG_OK;
	int i = 0;

	// we skip the verbose diagnostics for testing the NULL pointers or inequalities
	// in the number of nic_count and elements in usage_arr as they should
	// be equal

	g_assert(nic_arr);
	g_assert(usage_arr);
	g_assert(nic_count == usage_arr_count);

	if (!nic_count){
		p_warn("The number of NIC is 0. ...\n");
	}
	for (i=0; i < usage_arr_count; i++){
		// bytes transferred
		double  bytes_trsfr = 0.0;

		// @todo when run the very first time, I got the incorrect reading
		// like: NET 770927954 1319222932-899956 7197.17432
		//       NET 770927954 1319222933-899901 0.00179
		// right now, the just ignore the first reading in the log

		// select the relevant variant
		switch(variant){
		case NET_TOTAL:
			bytes_trsfr = usage_arr[i].curr.receive.bytes + usage_arr[i].curr.transmit.bytes -
			usage_arr[i].prev.receive.bytes - usage_arr[i].prev.transmit.bytes;
			break;
		case NET_TRANSMITTED:
			bytes_trsfr = usage_arr[i].curr.transmit.bytes - usage_arr[i].prev.transmit.bytes;
			break;
		case NET_RECV:
			bytes_trsfr = usage_arr[i].curr.receive.bytes - usage_arr[i].prev.receive.bytes;
			break;
		default:
			bytes_trsfr = 0.0;
			break;
		}

		long int elapsed_time = timeval2usec(&usage_arr[i].curr.ts) - timeval2usec(&usage_arr[i].prev.ts);

		if (elapsed_time < 0){
			nic_arr[i] = -1.0;
			p_error("Time curr is lower than the prev timestamp. Value set to %f\n", nic_arr[i]);
		} else {
			if (0 == elapsed_time)
				nic_arr[i] = 0.0;
			else{
				// x = bytes_per_sec / elapsed_time * 1000000 [bytes/s]
				// x = x / 1000 [kB/s]
				// so the final formula to get the [kB/s], you should
				// x = bytes_per_sec / (curr_usec - prev_usec) * 1000 [kB/s]
				nic_arr[i] = bytes_trsfr / elapsed_time *1000;
			}
		}
	}

	return diag;
}

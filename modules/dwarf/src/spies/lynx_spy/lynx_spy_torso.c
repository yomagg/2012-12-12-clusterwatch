/**
 * lynx_spy_torso.c
 *
 *  Created on: Oct 21, 2013
 *  Author: Magda Slawinska aka Magic Magg magg dot gatech at gmail.com
 */

#include "clusterspy.h"
#include "evpath.h"
#include "devel.h"
#include "spy.h"
#include "lynx_spy_torso_types.h"

#include <assert.h>
#include <string.h>

//#include "kernel_profile.h"
#include <errno.h>
#include <mqueue.h>
#include <fcntl.h> // for O_* constants
#include <sys/stat.h>  // for mode constants
#include <stdlib.h>

#include "debug.h"

diag_t dwarf_init_lynx_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
diag_t dwarf_init_lynx_ranger(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
diag_t dwarf_init_lynx_spy(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);


FMField lynx_cap_field_list[] = {
		{"ts", "struct timeval", sizeof(struct timeval), FMOffset(struct lynx_cap *, ts)},
		{"id", "struct identity", sizeof(struct identity), FMOffset(struct lynx_cap *, id)},
		{"qname", "integer", sizeof(char), FMOffset(struct lynx_cap *, qname)},
		{ NULL, NULL, 0, 0}
};

//! start with the main structure, then describe the substructures
FMStructDescRec lynx_cap_format_list[] = {
		{ "struct lynx_cap", lynx_cap_field_list, sizeof(struct lynx_cap), NULL },
		{ "struct timeval", timestamp_field_list, sizeof(struct timeval), NULL },
		{ "struct identity", identity_field_list, sizeof(struct identity), NULL },
		{NULL, NULL, 0, NULL}
};


// monitoring-related descriptions
FMField activity_factor_field_list[] = {
		{ "activeThreads", "integer", sizeof(unsigned long), FMOffset(struct activity_factor*, activeThreads)},
		{ "maxThreads", "integer", sizeof(unsigned long), FMOffset(struct activity_factor*, maxThreads)},
		{ "warps", "integer", sizeof(unsigned long), FMOffset(struct activity_factor*, warps)},
		{ NULL, NULL, 0, 0}
};

FMField memory_efficiency_field_list[] = {
		{ "me_metric", "float", sizeof(double), FMOffset(struct memory_efficiency*, me_metric)},
		{ "dynamicHalfWarps", "integer", sizeof(unsigned long), FMOffset(struct memory_efficiency*, dynamicHalfWarps)},
		{ "memTransactions", "integer", sizeof(unsigned long), FMOffset(struct memory_efficiency*, memTransactions)},
		{ NULL, NULL, 0, 0}
};

FMField branch_divergence_field_list[] = {
		{ "bd_metric", "float", sizeof(double), FMOffset(struct branch_divergence*, bd_metric)},
		{ "totalBranches", "integer", sizeof(unsigned long), FMOffset(struct branch_divergence*, totalBranches)},
		{ "divergentBranches", "integer", sizeof(unsigned long), FMOffset(struct branch_divergence*, divergentBranches)},
		{ NULL, NULL, 0, 0}
};

FMField kernel_prof_field_list[] = {
		{ "pid", "integer", sizeof(int), FMOffset(struct kernel_prof *, pid)},
		{ "device", "integer", sizeof(int), FMOffset(struct kernel_prof *, device)},
		// 256 is the value of the macro MAX_KERNEL_NAME_SIZE
		{ "name", "integer[256]", sizeof(char), FMOffset(struct kernel_prof*, name)},
		{ "mask", "integer", sizeof(unsigned long), FMOffset(struct kernel_prof*, mask)},
		{ "kernel_rt", "float", sizeof(double), FMOffset(struct kernel_prof*, kernel_rt)},
		{ "mem_eff", "struct memory_efficiency", sizeof(struct memory_efficiency), FMOffset(struct kernel_prof*, mem_eff)},
		{ "instruction_count", "float", sizeof(double), FMOffset(struct kernel_prof*, instruction_count)},
		{ "branch_divergence", "struct branch_divergence", sizeof(struct branch_divergence), FMOffset(struct kernel_prof*, branch_divergence)},
		{ "activity_f", "struct activity_factor", sizeof(struct activity_factor), FMOffset(struct kernel_prof*, activity_f)},
		{ "barriers", "integer", sizeof(unsigned long), FMOffset(struct kernel_prof*, barriers)},
		{ NULL, NULL, 0, 0}
};

FMField lynx_mon_field_list[] = {
		{"ts", "struct timeval", sizeof(struct timeval), FMOffset(struct lynx_mon *, ts)},
		{"id", "integer", sizeof(int), FMOffset(struct lynx_mon *, id)},
		{"kernel_info", "struct kernel_prof", sizeof(struct kernel_prof), FMOffset(struct lynx_mon *, kernel_info)},
		{NULL, NULL, 0, 0}
};

//! start with the main structure, then describe the substructures
FMStructDescRec lynx_mon_format_list[] = {
  { "struct lynx_mon", lynx_mon_field_list, sizeof(struct lynx_mon), NULL },
  { "struct timeval", timestamp_field_list, sizeof(struct timeval), NULL },
  { "struct kernel_prof", kernel_prof_field_list, sizeof(struct kernel_prof), NULL },
  { "struct activity_factor", activity_factor_field_list, sizeof(struct activity_factor), NULL},
  { "struct branch_divergence", branch_divergence_field_list, sizeof(struct branch_divergence), NULL },
  { "struct memory_efficiency", memory_efficiency_field_list, sizeof(struct memory_efficiency), NULL },
  {NULL, NULL, 0, NULL}
};


// other variables
static struct lynx_glob_data ctrl_data;

// forward declarations
static diag_t init_caps(struct lynx_cap *p_cap, ini_t *p_ini);
static diag_t init_mon(struct lynx_mon *p_mon, struct lynx_cap *p_cap);
static diag_t lynx_open_mq (struct lynx_cap *p_cap, struct lynx_glob_data * p_glob);
static diag_t prepare_mon_rec(struct lynx_mon *p_mon, struct lynx_glob_data *p_ctrl);

static diag_t get_lynx_data(struct kernel_prof *p_kernel_info, struct lynx_glob_data *p_ctrl_data);


/**
 * Make use of the global variable ctrl_data that holds some helper variables.
 * I assume the identity has been already set in the init spy function.
 * This is invoked on the patrol side (collector, local)
 * @param data that will be sent over EVPATH, I guess memory for the data
 *        have been allocated accordingly
 * @return 0 - you were not able to read anything
 *         > 0 you have data to be sent; successful read
 */
int read_cap_lynx(void *data){

	int ret = POSITIVE_VAL;
	struct lynx_cap *p_rec = (struct lynx_cap*) data;

	// the ctrl_data.caps.ts has been already set in lynx_spy_init
	*p_rec = ctrl_data.caps;

	// get the timestamp - and update the record with the timestamp
	gettimeofday(&ctrl_data.caps.ts, NULL);
	p_rec->ts = ctrl_data.caps.ts;

	return ret;
}

/**
 * This is called on the ranger (aggregator) side
 * @param data Data received over EVPath
 * @return 0 success
 *         anything else some issues (should not happen)
 */
int process_cap_lynx(void *data){
	diag_t diag = DIAG_OK;
	int ret = ZERO_VAL;

	struct lynx_cap *event = (struct lynx_cap*) data;
	struct lynx_glob_data *d = (struct lynx_glob_data*) &ctrl_data;

	if (NULL == d->p_bhb_ctx->header.curr_entry){
		// write the layout
		const int B_SIZE = d->p_bhb_ctx->header.hdr_size;
		char b[B_SIZE];

		// the layout prefix
		GString *layout_prefix = g_string_new("");
		diag = bhb_mmap_layout_2_str(d->p_bhb_ctx, layout_prefix);
		g_snprintf(b, B_SIZE, "%s \nTIMESTAMP SPY_ID HOSTNAME PID MQ_NAME\n", layout_prefix->str);
		g_string_free(layout_prefix, TRUE);

		bhb_mmap_wrt_hdr_header(d->p_bhb_ctx, b);
		bhb_mmap_wrt_bdy_header(d->p_bhb_ctx, "TIMESTAMP SPY_ID PID DEV_ID NAME "
				"TYPE KERNEL_RT MEM_EFF_METRIC ME_H_WARPS ME_MEM_TRANS "
				"INST_COUNT BRA_DIV_METRIC BD_TOTAL_BRA BD_DIV_BRA "
				"ACT_THREADS AF_MAX_THR AF_WARPS BARRIERS\n");
	}
	// now dump the values
	const int BUF_SIZE = d->p_bhb_ctx->header.elem_size;
	char buf[BUF_SIZE];
	memset(buf, 0, BUF_SIZE);

	g_snprintf(buf, BUF_SIZE, "%ld %d %s %d %s\n",
			timeval2usec(&event->ts),
			event->id.id,
			event->id.hostname,
			event->id.pid,
			event->qname);

	if ( (diag = bhb_mmap_wrt_hdr_entry(d->p_bhb_ctx, buf, event->id.id) ) != DIAG_OK){
		p_warn(DWARF_NAME ": Issues with writing memory mapped file\n");
	}

	return ret;
}


/**
 * Reads the lynx values.
 * @param data The structure lynx_mon
 * @return 0 - you were not able to read anything
 *         > 0 - you have data to be sent; indicates successful read
 */
int read_mon_lynx(void *data){
	diag_t diag = DIAG_OK;
	int ret = POSITIVE_VAL;
	struct lynx_mon *p_rec = (struct lynx_mon*) data;

	// copy data that are in ctrl_data to the monitoring record
	if ( (diag = prepare_mon_rec(p_rec, &ctrl_data)) != DIAG_OK){
		p_error("Some problems with preparing monitoring data\n");
		return ZERO_VAL;
	}

	if (-1 != ctrl_data.lynx_q.q ){
		// get the timestamp
		gettimeofday(&p_rec->ts, NULL);

		if ( (diag = get_lynx_data(&p_rec->kernel_info, &ctrl_data)) != DIAG_OK) {
			p_error("get_lynx_data issues\n");
			return ZERO_VAL;
		}
	} else {
		p_error("Issues with preparing monitoring data\n");
		return ZERO_VAL;
	}

	return ret;
}

/**
 * Process the data: process data on the aggregator side. Read is done on
 * the collector side.  Process is called on the aggregator side.
 *
 * @param data
 * @return 0 success
 *         != 0 some issues (should not happen)
 */
int process_mon_lynx(void *data ){

	struct lynx_mon *p_rec = data;

	diag_t diag = DIAG_OK;
	const int ERROR = -1;

	const int BUF_SIZE = ctrl_data.p_bhb_ctx->body.elem_size;
	char buf[BUF_SIZE];

	bhb_mmap_wrt_bdy_header(ctrl_data.p_bhb_ctx, "TIMESTAMP SPY_ID PID DEV_ID NAME "
					"TYPE KERNEL_RT MEM_EFF_METRIC ME_H_WARPS ME_MEM_TRANS "
					"INST_COUNT BRA_DIV_METRIC BD_TOTAL_BRA BD_DIV_BRA "
					"ACT_THREADS AF_MAX_THR AF_WARPS BARRIERS\n");

	// clear the buffer
	memset(buf, 0, BUF_SIZE);
	g_snprintf(buf, BUF_SIZE, "%ld %d %d %d %s %lu %.5f %.5f %lu %lu %.2f %.5f %lu %lu %lu %lu %lu %lu\n",
			timeval2usec(&p_rec->ts),
			p_rec->id,
			p_rec->kernel_info.pid,
			p_rec->kernel_info.device,
			p_rec->kernel_info.name,
			p_rec->kernel_info.mask,
			p_rec->kernel_info.kernel_rt,
			p_rec->kernel_info.mem_eff.me_metric,
			p_rec->kernel_info.mem_eff.dynamicHalfWarps,
			p_rec->kernel_info.mem_eff.memTransactions,
			p_rec->kernel_info.instruction_count,
			p_rec->kernel_info.branch_divergence.bd_metric,
			p_rec->kernel_info.branch_divergence.totalBranches,
			p_rec->kernel_info.branch_divergence.divergentBranches,
			p_rec->kernel_info.activity_f.activeThreads,
			p_rec->kernel_info.activity_f.maxThreads,
			p_rec->kernel_info.activity_f.warps,
			p_rec->kernel_info.barriers);

#ifdef DEBUG
	printf("%s", buf);
#endif
	if ( (diag = bhb_mmap_wrt_bdy_entry(ctrl_data.p_bhb_ctx, buf, p_rec->id)) != DIAG_OK){
		p_warn(DWARF_NAME ": some issues with writing to mmap.\n");
		return ERROR;
	}

	return 0;
}

// =============================================================
// dwarf-related public functions
// =============================================================

diag_t dwarf_init_lynx_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx){
	diag_t diag = DIAG_OK;

	p_spy_ctx->init_ranger_func = &dwarf_init_lynx_ranger;
	p_spy_ctx->init_spy_func = &dwarf_init_lynx_spy;

	// initialize cap data
	p_spy_ctx->evpath_ctx_cap.format_list = lynx_cap_format_list;
	p_spy_ctx->evpath_ctx_cap.res_type = GPU;
	p_spy_ctx->evpath_ctx_cap.data_size = sizeof(struct lynx_cap);
	p_spy_ctx->evpath_ctx_cap.period_sec = 0;  // to indicate we want to call it only once

	// init transport functions on the collector (spy side)
	p_spy_ctx->evpath_ctx_cap.clctor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_cap.clctor_read_func = &read_cap_lynx;
	p_spy_ctx->evpath_ctx_cap.clctor_process_func = NULL;

	// right now we do not distinguish the functions on the aggregator side
	p_spy_ctx->evpath_ctx_cap.agrtor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_cap.agrtor_read_func = NULL;
	p_spy_ctx->evpath_ctx_cap.agrtor_process_func = &process_cap_lynx;


	// initialize monitoring data
	// init transport fields
	p_spy_ctx->evpath_ctx_mon.format_list = lynx_mon_format_list;
	p_spy_ctx->evpath_ctx_mon.res_type = GPU;
	p_spy_ctx->evpath_ctx_mon.data_size = sizeof(struct lynx_mon);
	p_spy_ctx->evpath_ctx_mon.period_sec = p_dwarf_rt_ctx->ini_file.patrol_freq;

	// init transport functions on the collector (spy side)
	p_spy_ctx->evpath_ctx_mon.clctor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_mon.clctor_read_func = &read_mon_lynx;
	p_spy_ctx->evpath_ctx_mon.clctor_process_func = NULL;

	// right now we do not distinguish the functions on the aggregator side
	p_spy_ctx->evpath_ctx_mon.agrtor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_mon.agrtor_read_func = NULL;
	p_spy_ctx->evpath_ctx_mon.agrtor_process_func = &process_mon_lynx;

	return diag;
}

/**
 * used for the initialization of the aggregator side; we need to initialize
 * the memory map regions
 * @param p_dwarf_rt_ctx
 * @param p_spy_ctx
 * @return
 */
diag_t dwarf_init_lynx_ranger(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx){
	diag_t diag = DIAG_OK;

	// initialize the bhb_mmap structure; the second part
	// of the initialization will take part when we know the
	// name of the file assigned to us. the second part of initialization
	// should be held in scut_trooper_run function; we create
	// the relevant pointers and structures for a header, body and blocks
	// in the body
	struct bhb_mmap_param params = {
			.block_count = LYNX_MMAP_BLOCK_COUNT,
			.hdr_header_size_bytes = LYNX_MMAP_HDR_HDR_SIZE_BYTES,
			.hdr_entry_size_bytes = LYNX_MMAP_HDR_ENTRY_SIZE_BYTES,
			.bdy_header_size_bytes = LYNX_MMAP_BODY_HDR_SIZE_BYTES,
			.bdy_block_entry_size_bytes = LYNX_MMAP_BLOCK_ENTRY_SIZE_BYTES,
			.bdy_block_entry_count = LYNX_MMAP_BLOCK_ENTRY_COUNT
	};

	ctrl_data.p_bhb_ctx = bhb_create_mapped_file(&diag, &params, p_dwarf_rt_ctx, p_spy_ctx);
	if (DIAG_OK != diag ){
		return diag;
	}

	if (!ctrl_data.p_bhb_ctx){
		diag = DIAG_ERR;
		return diag;
	}

	return diag;
}

/**
 *
 * initializes the lynx_spy, i.e. the ctrl_data struct:
 * it gets the identity of the spy,
 * initializes the capability record and
 * allocates data for the monitoring record
 *
 *
 * @param p_dwarf_rt_ctx
 * @param p_spy_ctx
 * @return DIAG_OK if everything went ok, or
 *         != DIAG_OK if something went wrong
 */
diag_t dwarf_init_lynx_spy(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx){

	diag_t diag = DIAG_OK;

	// set identity of the spy
	if ( (diag = get_identity(&ctrl_data.caps.id)) != DIAG_OK ){
		return diag;
	}

	// we only need to setup the queue
	if ((diag = init_caps(&ctrl_data.caps, &p_dwarf_rt_ctx->ini_file)) != DIAG_OK) {
		return diag;
	}

	// now prepare the skeleton (allocate memory) for the monitoring data that will be
	// sent to the aggregator
	if ( (diag = init_mon(&ctrl_data.mon, &ctrl_data.caps)) != DIAG_OK ){
		return diag;
	}

	return diag;
}

// ==============================================================
// private functions
// ==============================================================

/**
 * reads the lynx capabilities - right now only the name of the
 * message queue.
 *
 * @param p_cap (OUT) Here the capabilities will be stored
 *
 * @return DIAG_OK - everything went fine
 *         != DIAG_OK - there were some issues
 */
static diag_t init_caps(struct lynx_cap *p_cap, ini_t * p_ini){
	diag_t diag = DIAG_OK;

	assert(NULL != p_ini);

	// get the name of the queue
	// just to be sure that the qname will be ended with '\0'
	memset(p_cap->qname, 0, sizeof(p_cap->qname));
	strncpy(p_cap->qname, p_ini->patrol_lynx_spy_q_name, sizeof(p_cap->qname) - 1);

	// TODO I know this is not consequent with the global data passed as argument
	// but maybe in the future this will be not global data
	if ( (diag = lynx_open_mq(p_cap, &ctrl_data)) != DIAG_OK ){
		return diag;
	}

	return diag;
}

/**
 * I
 * @param p_mon
 * @param p_cap
 * @return
 */
static diag_t init_mon(struct lynx_mon *p_mon, struct lynx_cap *p_cap){
	diag_t diag = DIAG_OK;

	assert(NULL != p_mon);
	assert(NULL != p_cap);

	memset(p_mon, 0, sizeof(*p_mon));


	return diag;
}

/**
 * Opens the message queue of lynx; it is assumed that the message
 * queue will be closed somehow and unlinked.
 * This is the opening interface for getting data from Lynx
 *
 * This function assume that the name of the queue to be open is determined and
 * set in p_cap->qname
 *
 * @param p_cap The pointer to the capabilities; I assume that the name of the queue
 *              is p_cap->qname to be open
 * @param p_glob In p_glob->lynx_q the descriptor of the message queue will be stored,
 *              and other information about the queue.
 *
 * @return DIAG_OK everything went fine
 *         != DIAG_OK some issues
 */
static diag_t lynx_open_mq (struct lynx_cap *p_cap, struct lynx_glob_data * p_glob){
	diag_t diag = DIAG_OK;
	struct mq_attr mq_attributes;
	memset(&p_glob->lynx_q, 0, sizeof((p_glob->lynx_q)));

	assert(NULL != p_cap->qname);

	mq_attributes.mq_maxmsg = LYNX_MQ_MAX_MESSAGES;
	mq_attributes.mq_msgsize = LYNX_MQ_MAX_MSG_SIZE;

	p_glob->lynx_q.q = mq_open(p_cap->qname, LYNX_MQ_CREATE_OWNER_FLAGS, 0666, &mq_attributes);

	if ( -1 == p_glob->lynx_q.q ){
		diag = DIAG_ERR;
		if ( EEXIST == errno ){
			p_error("Can't create mq_open: %s (%d) The queue exists. First unlink the queue."
					" Trouble queue: %s\n", strerror(errno), errno, p_cap->qname);
		} else {
			p_error("mq_open: %s (%d)\n", strerror(errno), errno);
		}
		return diag;
	} else {
		p_debug("mq opened: %s\n", p_cap->qname);
		p_glob->lynx_q.q_max_msg_count = mq_attributes.mq_maxmsg;
		p_glob->lynx_q.q_max_msg_size = mq_attributes.mq_msgsize;
		p_glob->lynx_q.p_q_max_msg = calloc(mq_attributes.mq_msgsize, sizeof(char));
	}

	return diag;
}

/**
 * prepare the monitoring record by copying data from the global control structure
 *
 * @param p_mon (OUT) where the data will be copied or clean
 * @param p_ctrl (IN)  from where data will be copied
 * @return
 */
static diag_t prepare_mon_rec(struct lynx_mon *p_mon, struct lynx_glob_data *p_ctrl){
	diag_t diag = DIAG_OK;

	memset( p_mon, 0, sizeof(struct lynx_mon));

	p_mon->id = p_ctrl->caps.id.id;

	return diag;
}
/**
 * Gets the Lynx data from the Lynx source.
 * The record will be filled with one event read from the queue.
 *
 * TODO this needs to be changed, because reading the entire
 * message queue will take long time, and I don't want to do this
 *
 * If the queue is empty, then nothing is set. It is assumed that the
 * *p_kernel_info is set to zero, so KERN_PROF_NONE is set.
 *
 * @param p_kernel_info (OUT) this structure will be filled out
 * @param p_ctrl_data (IN/OUT) The the buffer is used.
 *
 * @return DIAG_OK everything went find
 *         != DIAG_OK some issues
 */
static diag_t get_lynx_data(struct kernel_prof *p_kernel_info, struct lynx_glob_data *p_ctrl_data){
	diag_t diag = DIAG_OK;

	int ret = -1;  // assume nothing in the queue
	unsigned int msg_prio = 0;

	// the queue is open I assume
	p_ctrl_data->lynx_q.p_q_max_msg = memset(p_ctrl_data->lynx_q.p_q_max_msg, 0, p_ctrl_data->lynx_q.q_max_msg_size);

	ret = mq_receive(p_ctrl_data->lynx_q.q, (char*) p_ctrl_data->lynx_q.p_q_max_msg,
			p_ctrl_data->lynx_q.q_max_msg_size, &msg_prio);

	if (ret >= 0) {
		// just to save typing
		kernel_profile *p_data = p_ctrl_data->lynx_q.p_q_max_msg;

		// got a message, pack the message to the p_kernel_info
		p_kernel_info->pid = p_data->pid;
		strncpy(p_kernel_info->name,p_data->name, MAX_KERNEL_NAME_SIZE);
		p_kernel_info->device = p_data->device;

		// now convert the event from the message queue to the message that
		// will be sent over evpath

		// TODO in the LYNX code there is something like BARRIERS, it is in
		// Profiler.cpp, by I can't find where BARRIERS are set up. So
		// I am ignoring this at the moment.
		switch (p_data->type){
		case NONE:
			p_kernel_info->mask = KERN_PROF_NONE;
			break;
		case KERNEL_RUNTIME:
			p_kernel_info->mask = KERN_PROF_KERNEL_RT;
			p_kernel_info->kernel_rt = p_data->data.kernel_runtime;
			break;
		case MEM_EFFICIENCY:
			p_kernel_info->mask = KERN_PROF_MEM_EFF;
			p_kernel_info->mem_eff = p_data->data.mem_efficiency;
			break;
		case INST_COUNT:
			p_kernel_info->mask = KERN_PROF_INST_COUNT;
			p_kernel_info->instruction_count = p_data->data.instruction_count;
			break;
		case EXECUTION_COUNT:
			p_kernel_info->mask = KERN_PROF_EXEC_COUNT;
			p_kernel_info->instruction_count = p_data->data.instruction_count;
			break;
		case BRANCH_DIVERGENCE:
			p_kernel_info->mask = KERN_PROF_BRANCH_DIVER;
			p_kernel_info->branch_divergence = p_data->data.branch_divergence;
			break;
		case ACTIVITY_FACTOR:
			p_kernel_info->mask = KERN_PROF_ACTIVITY_F;
			p_kernel_info->activity_f = p_data->data.activity_f;
			break;

		default:
			p_warn("Unknown profile type.\n");
			break;
		}
	} else {
		if (EAGAIN == errno) {
			p_debug("Nothing in the mq.\n");
		} else {
			diag = DIAG_ERR;
		}
	}

	return diag;
}

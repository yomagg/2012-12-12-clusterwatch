/**
 * @file mem_spy_torso.c
 *
 * @date Sep 14, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#include "clusterspy.h"
#include "evpath.h"
#include "mem_spy_torso_types.h"
#include "spy.h"
#include "devel.h"


FMField mem_cap_field_list[] = {
  {"ts", "struct timeval", sizeof(struct timeval), FMOffset(struct mem_cap *, ts)},
  {"id", "struct identity", sizeof(struct identity), FMOffset(struct mem_cap *, id)},
  {"mem_total_kB", "integer", sizeof(long), FMOffset(struct mem_cap *, mem_total_kB)},
  {"swap_total_kB", "integer", sizeof(long), FMOffset(struct mem_cap *, swap_total_kB)},
  {NULL, NULL, 0, 0}
};

//! the order of struct is important, first the main, then the elements that
//! are defined in the structure first; first introduce then explain
FMStructDescRec mem_cap_format_list[] = {
		{ "struct mem_cap", mem_cap_field_list, sizeof(struct mem_cap), NULL },
		{ "struct timeval", timestamp_field_list, sizeof(struct timeval), NULL },
		{ "struct identity", identity_field_list, sizeof(struct identity), NULL },
		{ NULL, NULL, 0, NULL}
};


FMField mem_mon_field_list[] = {
  {"ts", "struct timeval", sizeof(struct timeval), FMOffset(struct mem_mon *, ts) },
  {"id", "integer", sizeof(int), FMOffset(struct mem_mon *, id)},
  //{"mem_total_kB", "integer", sizeof(long), FMOffset(struct mem_mon *, mem_total_kB)},
  //{"swap_total_kB", "integer", sizeof(long), FMOffset(struct mem_mon *, swap_total_kB)},
  {"mem_util_perc", "float", sizeof(float), FMOffset(struct mem_mon *, mem_util_perc)},
  {"mem_buffers_util_perc", "float", sizeof(float), FMOffset(struct mem_mon *, mem_buffers_util_perc)},
  {"mem_cached_util_perc", "float", sizeof(float), FMOffset(struct mem_mon *, mem_cached_util_perc)},
  {"mem_active_util_perc", "float", sizeof(float), FMOffset(struct mem_mon *, mem_active_util_perc)},
  {"mem_inactive_util_perc", "float", sizeof(float), FMOffset(struct mem_mon *, mem_inactive_util_perc)},
  {"slab_util_perc", "float", sizeof(float), FMOffset(struct mem_mon *, slab_util_perc)},
  {"mapped_perc", "float", sizeof(float), FMOffset(struct mem_mon *, mapped_perc)},
  {"swap_cached_perc", "float", sizeof(float), FMOffset(struct mem_mon *, swap_cached_perc)},
  {"swap_util_perc", "float", sizeof(float), FMOffset(struct mem_mon *, swap_util_perc)},
  { NULL, NULL, 0, 0}
};

FMStructDescRec mem_mon_format_list[] = {
		{"struct mem_mon", mem_mon_field_list, sizeof(struct mem_mon), NULL },
		{"struct timeval", timestamp_field_list, sizeof(struct timeval), NULL },
		{ NULL, NULL, 0, NULL}
};

// other variables
static struct mem_glob_data ctrl_data;

// forward declarations
static diag_t fed_mem_mon(struct mem_mon *p_mon, struct mem_glob_data *p_ctrl);

static diag_t get_caps(struct mem_cap *p_cap);

static inline float percentage(long total, float x);

diag_t dwarf_init_mem_ranger(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
diag_t dwarf_init_mem_spy(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);

// ------------------------------------
// evpath transport related functions
// ------------------------------------
/**
 * Make use of the global variable ctrl_data that holds some helper variables
 *
 * @param data that will be sent over EVPATH, I guess memory for the data
 *        have been allocated accordingly
 * @return 0 - you were not able to read anything
 *         > 0 you have data to be sent; successful read
 */
int read_cap_mem(void *data){
	int ret = POSITIVE_VAL;
	struct mem_cap *p_rec = (struct mem_cap*)data;

	// get the
	if ( get_caps(&ctrl_data.caps) != DIAG_OK){
		p_error(DWARF_NAME ": errors while reading capabilities.\n");
		return  ZERO_VAL;
	}

	*p_rec = ctrl_data.caps;

	return ret;
}

/**
 * @param data Data sent over EVPath
 * @return 0  success
 *         anything else some issues (should not happen)
 */
int process_cap_mem(void *data ){
	diag_t diag = DIAG_OK;
	int ret = ZERO_VAL;

	struct mem_cap *event = data;
	struct mem_glob_data *d = &ctrl_data;

	// dump header if this is the first time you are running it
	if (NULL == d->p_bhb_ctx->header.curr_entry){
		const int B_SIZE = d->p_bhb_ctx->header.hdr_size;
		char b[B_SIZE];

		// the layout prefix
		GString *layout_prefix = g_string_new("");
		diag = bhb_mmap_layout_2_str(d->p_bhb_ctx, layout_prefix);
		g_snprintf(b, B_SIZE, "%s \nTIMESTAMP SPY_ID HOSTNAME PID TOTAL_MEM_kB TOTAL_SWAP_kB\n", layout_prefix->str);
		g_string_free(layout_prefix, TRUE);

		bhb_mmap_wrt_hdr_header(d->p_bhb_ctx, b);
		bhb_mmap_wrt_bdy_header(d->p_bhb_ctx, "TIMESTAMP SPY_ID M_PERC MBUF_PERC MCACHED_PERC MACT_PERC MINACT_PERC SLAB_PERC MAPPED_PERC S_PERC SCACHED_PERC\n");
	}

	// now dump the values
	const int BUF_SIZE = d->p_bhb_ctx->header.elem_size;
	char buf[BUF_SIZE];

	memset(buf, 0, BUF_SIZE);

	g_snprintf(buf, BUF_SIZE, "%ld %d %s %d %ld %ld\n",
			timeval2usec(&event->ts),
			event->id.id, event->id.hostname,
			event->id.pid,
			event->mem_total_kB, event->swap_total_kB);

	if ( (diag = bhb_mmap_wrt_hdr_entry(d->p_bhb_ctx, buf, event->id.id)) != DIAG_OK){
		p_warn(DWARF_NAME ": issues with writing to memory map\n.");
	}

	return ret;
}


/**
 * We assume that
 *
 * ctrl_data.caps.mem_total_kB;
 * ctrl_data.caps.swap_total_kB;
 *
 * are set and can be used for processing
 *
 * @param data that will be sent over EVPATH
 * @return 0 - you were not able to read anything
 *         > 0 you have data to be sent; successful read
 */
int read_mon_mem(void *data){
	struct mem_mon * p_rec = (struct mem_mon*) data;
	FILE *f = NULL;
	diag_t diag = DIAG_OK;
	char *line = NULL;
	size_t n = 0;
	ssize_t read = 0;
	// this values should be set, otherwise we are in trouble
	long mem_total = ctrl_data.caps.mem_total_kB;
	long swap_total = ctrl_data.caps.swap_total_kB;

	// the size of the line from
	const int BUF_SIZE=100;
	char buf[BUF_SIZE];
	// the return value
	int ret = POSITIVE_VAL;


	// connect the p_rec with the allocated buffers and copy the data
	// necessary data
	if ( (diag = fed_mem_mon(p_rec, &ctrl_data)) != DIAG_OK ){
		p_error("Some problems with fed_nvid_mon()\n");
		return diag;
	}

	f = fopen("/proc/meminfo", "r");
	if ( (diag = diag_ptr_null_ret(f, "Can't open /proc/meminfo\n")) != DIAG_OK){
		ret = NEGATIVE_VAL;
		goto finish;
	}

	// get the timestamp
	gettimeofday(&p_rec->ts, NULL);

	while ((read = getline(&line, &n, f)) != -1) {
		long bytes = 0L;
		memset(buf, 0, BUF_SIZE);
		sscanf(line, "%s %ld kB", buf, &bytes);

		if (!strcmp("MemFree:", buf)) {
			p_rec->mem_util_perc = percentage(mem_total, (float) mem_total - bytes);
			continue;
		}

		if (!strcmp("Buffers:", buf)) {
			p_rec->mem_buffers_util_perc = percentage(mem_total, bytes);
			continue;
		}

		if (!strcmp("Cached:", buf)) {
			p_rec->mem_cached_util_perc = percentage(mem_total, bytes);
			continue;
		}

		if (!strcmp("SwapCached:", buf)) {
			p_rec->swap_cached_perc = percentage(swap_total, bytes);
			continue;
		}

		if (!strcmp("Active:", buf)) {
			p_rec->mem_active_util_perc =  percentage(mem_total, bytes);
			continue;
		}

		if (!strcmp("Inactive:", buf)) {
			p_rec->mem_inactive_util_perc = percentage(mem_total, bytes);
			continue;
		}

		if (!strcmp("SwapFree:", buf)) {
			p_rec->swap_util_perc = percentage(swap_total, swap_total - bytes);
			continue;
		}

		if (!strcmp("Mapped:", buf)) {
			p_rec->mapped_perc = percentage(mem_total, bytes);
			continue;
		}
		// this is the last thing we will be reading
		if (!strcmp("Slab:", buf)) {
			p_rec->slab_util_perc = percentage(mem_total, bytes);
			break;
		}
	}

finish:
	fclose(f);
	return ret;
}

/**
 * Counts the percentage
 * @param total The total value
 * @param x The value to be converted to the percentage w.r.t. the total
 * @return percentage x/total*100
 */
static inline float percentage(long total, float x){
	return (float) ( x / (float) total * 100.0);
}

/**
 * @param data Data sent over EVPath
 * @return 0  success
 *         anything else some issues (should not happen)
 */
int process_mon_mem(void *data){

	struct mem_glob_data *d = (struct mem_glob_data*) &ctrl_data;
	struct mem_mon *event = data;
	diag_t diag = DIAG_OK;

	const int BUF_SIZE = d->p_bhb_ctx->body.elem_size;
	char buf[BUF_SIZE];

	// clear the buf
	memset(buf, 0, BUF_SIZE);
	g_snprintf(buf, BUF_SIZE, "%ld %d %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f\n",
			timeval2usec(&event->ts),
			event->id,
			event->mem_util_perc, event->mem_buffers_util_perc, event->mem_cached_util_perc,
			event->mem_active_util_perc, event->mem_inactive_util_perc,
			event->slab_util_perc, event->mapped_perc, event->swap_util_perc, event->swap_cached_perc
	);

	diag = bhb_mmap_wrt_bdy_entry(d->p_bhb_ctx, buf, event->id);

#ifdef DEBUG
	printf("TIMESTAMP SPY_ID M_PERC MBUF_PERC MCACHED_PERC MACT_PERC MINACT_PERC SLAB_PERC MAPPED_PERC S_PERC SCACHED_PERC\n");
	printf("%ld %d %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f\n",
			timeval2usec(&event->ts),
			event->id,
			event->mem_util_perc, event->mem_buffers_util_perc, event->mem_cached_util_perc,
			event->mem_active_util_perc, event->mem_inactive_util_perc,
			event->slab_util_perc, event->mapped_perc, event->swap_util_perc, event->swap_cached_perc);
#endif
    return ZERO_VAL;
}

// ----------- private functions
/**
 * copy the data or pointers with data from the global control structure
 * (which has allocated memory) to the new monitoring structure (which
 * doesn't have the allocated memory)
 *
 * @param p_mon (OUT) where the data will be copied or clean
 * @param p_ctrl (IN)  from where data will be copied
 * @return
 */
static diag_t fed_mem_mon(struct mem_mon *p_mon, struct mem_glob_data *p_ctrl){
	diag_t diag = DIAG_OK;

	// copy data
	p_mon->id = p_ctrl->caps.id.id;

	return diag;
}

/**
 *
 * I want to get MemTotal and SwapTotal from this:
 *
 * [smagg@kidlogin2 ~]$ cat /proc/meminfo
MemTotal:     24676588 kB
MemFree:      14231836 kB
Buffers:        347832 kB
Cached:        6896608 kB
SwapCached:          0 kB
Active:        3027484 kB
Inactive:      5733040 kB
HighTotal:           0 kB
HighFree:            0 kB
LowTotal:     24676588 kB
LowFree:      14231836 kB
SwapTotal:     2031608 kB
SwapFree:      2031412 kB
Dirty:             616 kB
Writeback:           0 kB
AnonPages:     1515648 kB
Mapped:         116600 kB
Slab:          1322840 kB
PageTables:      69572 kB
NFS_Unstable:        0 kB
Bounce:              0 kB
CommitLimit:  14369900 kB
Committed_AS:  3866964 kB
VmallocTotal: 34359738367 kB
VmallocUsed:    336896 kB
VmallocChunk: 34359401183 kB
HugePages_Total:     0
HugePages_Free:      0
HugePages_Rsvd:      0
Hugepagesize:     2048 kB
 *
 *
 *
 * gets the capabilities but no identity. identity should be set in the
 * initialization function of the spy_ctx
 *
 * @param p_cap (OUT) fills the capabilities
 * @return DIAG_OK everything ok
 *         != DIAG_OK if some errors
 */
static diag_t get_caps(struct mem_cap *p_cap){
	diag_t diag = DIAG_OK;
	FILE *f = fopen("/proc/meminfo", "r");

	if ( (diag = diag_ptr_null_ret(f, "Can't open /proc/meminfo\n")) != DIAG_OK)
		return diag;

	char *line = NULL;
	size_t n = 0;
	ssize_t read = 0;

	if ( (diag=diag_ptr_null_ret(p_cap, "The pointer to capabilities is NULL\n")) != DIAG_OK)
		return diag;

	// the size of the line from
	const int BUF_SIZE=200;
	char buf[BUF_SIZE];

	// get the timestamp
	gettimeofday(&p_cap->ts, NULL);

	// now read the lines
	while ( (read=getline(&line, &n, f)) != -1){
		long bytes = 0L;
		memset(buf, 0, BUF_SIZE);
		sscanf(line, "%s %ld kB", buf, &bytes);
		// in case if there are leading whitespace
		//g_strchug(buf); // i think this is not necessary

		if ( !strcmp(buf, "MemTotal:" ) ){
			// this is what we are looking for
			p_cap->mem_total_kB = bytes;
			continue;
		}

		if ( !strcmp(buf, "SwapTotal:") ){
			// this is what we are looking for
			p_cap->swap_total_kB = bytes;
			break;
		}
	}

	fclose(f);

	return diag;
}

// ----------------------------------
// dwarf related functions
// ----------------------------------
/**
 * the initialization function that is invoked during
 * the enabling of the spy
 * @param p_dwarf_rt_ctx the context of the runtime
 * @param p_spy_ctx the context of the spy; it is allocated somewhere
 *        else; I do not allocate memory for this here
 * @return DIAG_OK if everything went ok, otherwise errors
 */
diag_t dwarf_init_mem_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx){
	diag_t diag = DIAG_OK;

	p_spy_ctx->init_ranger_func = &dwarf_init_mem_ranger;
	p_spy_ctx->init_spy_func = &dwarf_init_mem_spy;

	// initialize cap data
	p_spy_ctx->evpath_ctx_cap.format_list = mem_cap_format_list;
	p_spy_ctx->evpath_ctx_cap.res_type = MEMORY;
	p_spy_ctx->evpath_ctx_cap.data_size = sizeof(struct mem_cap);
	p_spy_ctx->evpath_ctx_cap.period_sec = 0;  // to indicate we want to call it only once

	// init transport functions on the collector (spy side)
	p_spy_ctx->evpath_ctx_cap.clctor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_cap.clctor_read_func = &read_cap_mem;
	p_spy_ctx->evpath_ctx_cap.clctor_process_func = NULL;

	// right now we do not distinguish the functions on the aggregator side
	p_spy_ctx->evpath_ctx_cap.agrtor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_cap.agrtor_read_func = NULL;
	p_spy_ctx->evpath_ctx_cap.agrtor_process_func = &process_cap_mem;


	// initialize monitoring data
	// init transport fields
	p_spy_ctx->evpath_ctx_mon.format_list = mem_mon_format_list;
	p_spy_ctx->evpath_ctx_mon.res_type = MEMORY;
	p_spy_ctx->evpath_ctx_mon.data_size = sizeof(struct mem_mon);
	p_spy_ctx->evpath_ctx_mon.period_sec = p_dwarf_rt_ctx->ini_file.patrol_freq;

	// init transport functions on the collector (spy side)
	p_spy_ctx->evpath_ctx_mon.clctor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_mon.clctor_read_func = &read_mon_mem;
	p_spy_ctx->evpath_ctx_mon.clctor_process_func = NULL;

	// right now we do not distinguish the functions on the aggregator side
	p_spy_ctx->evpath_ctx_mon.agrtor_setup_func = NULL;
	p_spy_ctx->evpath_ctx_mon.agrtor_read_func = NULL;
	p_spy_ctx->evpath_ctx_mon.agrtor_process_func = &process_mon_mem;

	return diag;
}

/**
 * used for the initialization of the aggregator side; we need to initialize
 * the memory map regions
 * @param p_dwarf_rt_ctx
 * @param p_spy_ctx
 * @return
 */
diag_t dwarf_init_mem_ranger(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx){
	diag_t diag = DIAG_OK;

	// initialize the bhb_mmap structure; the second part
	// of the initialization will take part when we know the
	// name of the file assigned to us. the second part of initialization
	// should be held in scut_trooper_run function; we create
	// the relevant pointers and structures for a header, body and blocks
	// in the body
	struct bhb_mmap_param params = {
				.block_count = MEM_MMAP_BLOCK_COUNT,
				.hdr_header_size_bytes = MEM_MMAP_HDR_HDR_SIZE_BYTES,
				.hdr_entry_size_bytes = MEM_MMAP_HDR_ENTRY_SIZE_BYTES,
				.bdy_header_size_bytes = MEM_MMAP_BODY_HDR_SIZE_BYTES,
				.bdy_block_entry_size_bytes = MEM_MMAP_BLOCK_ENTRY_SIZE_BYTES,
				.bdy_block_entry_count = MEM_MMAP_BLOCK_ENTRY_COUNT
	};

	ctrl_data.p_bhb_ctx = bhb_create_mapped_file(&diag, &params, p_dwarf_rt_ctx, p_spy_ctx);
	if (DIAG_OK != diag ){
		return diag;
	}

	if (!ctrl_data.p_bhb_ctx){
		diag = DIAG_ERR;
		return diag;
	}

	return diag;
}

/**
 * I am using the ctrl_data global variable
 * @param p_dwarf_rt_ctx
 * @param p_spy_ctx
 * @return
 */
diag_t dwarf_init_mem_spy(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx){
	diag_t diag = DIAG_OK;

	// set identity
	if ( (diag = get_identity(&ctrl_data.caps.id)) != DIAG_OK ){
		return diag;
	}

	return diag;
}


/**
 * @file dwarf_ranger.c
 *
 * @date Dec 2, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#include <glib.h>
#include <string.h>

#include "devel.h"
#include "dwarf_rt.h"
#include "dwarf_types.h"

#include "ini_utils_types.h"
#include "ini_utils.h"


extern diag_t dwarf_init_cpu_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
extern diag_t dwarf_init_mem_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
extern diag_t dwarf_init_net_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
extern diag_t dwarf_init_nvid_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
// normally I compile with NVML support
#ifndef NO_NVML
extern diag_t dwarf_init_nvml_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
extern diag_t dwarf_init_lynx_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
#endif


// clusterspy functions
extern int arrange_cluster(int exectime, int *p_port);
extern int run_watcher(char *mesg);

int
main(int argc, char **argv) {

	struct dwarf_rt_ctx ctx;
	diag_t diag = DIAG_OK;
	gchar *ini_name = NULL;

	// should be terminated with a record with NULL values
	struct dwarf_spy_ident default_spies []= {
			{INI_DWARF_STR_SPY_CPU, dwarf_init_cpu_spy_ctx},
			{INI_DWARF_STR_SPY_MEM, dwarf_init_mem_spy_ctx},
			{INI_DWARF_STR_SPY_NET, dwarf_init_net_spy_ctx},
			{INI_DWARF_STR_SPY_NVIDIA_SMI, dwarf_init_nvid_spy_ctx},
#ifndef NO_NVML
			{INI_DWARF_STR_SPY_NVML, dwarf_init_nvml_spy_ctx},
			{INI_DWARF_STR_SPY_LYNX, dwarf_init_lynx_spy_ctx},
#endif
			{NULL, NULL}
	};

	// just to be sure that we have no garbage
	memset(&ctx, 0, sizeof(ctx));
	ctx.is_initialized = FALSE;

	if ( (diag = dwarf_cmd_parse(argc, argv,&ini_name)) != DIAG_OK ){
		p_error(DWARF_NAME ": command line parsing error = %d. Abandon the ship\n", diag);
		return DIAG_ERR;
	}

	if ( (diag = dwarf_rt_init(&ctx, FALSE, ini_name)) != DIAG_OK){
		p_error(DWARF_NAME ": runtime initialization return with an error = %d. Abandon the ship\n", diag);
		return DIAG_ERR;
	}

	g_free(ini_name);

	int i = 0;
	while( default_spies[i].spy_name != NULL){
		diag = dwarf_rt_deploy_spy_from_ini(&ctx, default_spies[i].spy_name, default_spies[i].init_spy_ctx_func);
		if (diag != DIAG_OK){
			p_error(DWARF_NAME ": spy deployment error=%d. Abandon the ship\n", diag);
			break;
		}
		i++;
	}

	if (DIAG_OK != diag){
		p_error("Quitting ...\n");
		return diag;
	}

	p_debug(DWARF_NAME ": Arranging the cluster ...\n");
	// get the port
	if (arrange_cluster((int)ctx.ini_file.ranger_exectime, &ctx.ini_file.ranger_port) < 0){
		p_error(DWARF_NAME ": can't arrange the cluster. ATS\n");
		return DIAG_ERR;
	}
	// now we need to write down the port to the ini file
	if ((diag = ini_set_ranger_port(&ctx.ini_file, ctx.ini_file.ranger_port)) != DIAG_OK)
		return diag;

	p_info(DWARF_NAME ": Cluster ready to go ...\n");

	// now you can start watching the cluster
	run_watcher(DWARF_NAME ": Run watcher, run ...\n");

	return DIAG_OK;
}

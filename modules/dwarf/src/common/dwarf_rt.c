/**
 * @file dwarf_rt.c
 *
 * @date Dec 3, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#include <glib.h>
#include <string.h>    // memset
#include <time.h>

#include <stdio.h>	// getline()


#include "devel.h"
#include "dwarf_rt.h"
#include "ini_utils.h"
#include "ini_utils_types.h"

#include "clusterspy.h"

#include "kernel_profile.h"
// ---------------------
// global variables
// ---------------------

// --------------------
// local utils
// --------------------
// the mutex for accessing the runtime structures
static pthread_mutex_t rt_mtx = PTHREAD_MUTEX_INITIALIZER;
// the mutex for registering spy in htab in rt_ctx.patrol_htab
static pthread_mutex_t reg_spy_htab_mtx = PTHREAD_MUTEX_INITIALIZER;
// the mutex for enabling spies in patrol_htab
static pthread_mutex_t enab_spy_htab_mtx = PTHREAD_MUTEX_INITIALIZER;

// --------------------
// local functions
// --------------------
static diag_t deploy_agrtor(struct dwarf_rt_ctx *p_ctx);
static diag_t deploy_clctor(struct dwarf_rt_ctx *p_ctx);

void patrol_htab_key_destroy(gpointer data);
void patrol_htab_val_destroy(gpointer data);

static diag_t rt_enable_spy_agrtor(struct dwarf_rt_ctx* p_ctx, struct spy_ctx* p_spy_ctx);
static diag_t rt_enable_spy_clctor(struct dwarf_rt_ctx* p_ctx, struct spy_ctx* p_spy_ctx);


// --------------------
// ini utils
// --------------------

/**
 * A util function that parses the command line and extracts the
 * name of the ini file from the command line. It allocates memory
 * for the ini_name that should be deallocated if not needed with g_free(ini_name)
 *
 * @param argc the command line arguments count
 * @param argv the command line arguments
 * @param ini_name (OUT) the pointer that will hold the name of the
 * @return DIAG_OK if everything went smoothly
 *         DIAG_ERR if there were some issues
 */
diag_t
dwarf_cmd_parse(int argc, char** argv, gchar **ini_name){
	GError* error = NULL;
	GOptionContext* context;
	diag_t res = DIAG_OK;

	if (argc < 2){
		p_error("Error: no arguments given. To see the options run it with --help\n");
		return DIAG_ERR;
	}
	GOptionEntry entries[] = {
	  { "ini-file", 'f', 0, G_OPTION_ARG_FILENAME, ini_name,
			  "The name of or path to the configuration ini file", "file_name.ini" },
	  { NULL, '\0', 0, G_OPTION_ARG_NONE,  NULL, NULL, NULL}
	};

	context = g_option_context_new("- the monitoring system");
	g_option_context_set_summary(context,
			"The software is a part of the system software developed for the"
			" Keeneland project. This is the module Dwarf of the ClusterWatch and it allows "
			" to monitor usage of resources such as cpu, gpu, and network. "
	);
	g_option_context_add_main_entries( context, entries, NULL);

	g_option_context_set_help_enabled( context, TRUE);

	if( !g_option_context_parse(context, &argc, &argv, &error)){
		p_error("Option parsing failed: %s\n", error->message);
		res = DIAG_ERR;
		goto finish;
	}

	// check if the configuration file exists
	if (access(*ini_name, F_OK) == -1) {
		p_error("The ini file doesn't exist. Run with --help to see the options\n");
		res = DIAG_ERR;
	}

finish:
	// clean things if possible
	g_option_context_free(context);

	return res;
}

// ---------------------------
// dwarf runtime initialization
// ---------------------------
/**
 * Initialize the runtime library.
 *
 * @param p_ctx (OUT) The context that you will be used by the rest of callers;
 * @param is_collector (IN) if TRUE then it will initialize the collector part (server part)
 *                 if FALSE then it will initialize the aggregator part
 * @param ini_name (IN) The name of the ini file; the name is duplicated
 *                 to the context structure so you can do what you want
 * @return DIAG_OK if everything went smoothly
 */
diag_t
dwarf_rt_init(struct dwarf_rt_ctx *p_ctx, gboolean is_collector, gchar* ini_name){
	diag_t diag = DIAG_OK;
	pthread_mutex_lock( &rt_mtx );

	// check if we have already initialized the runtime context, go away if
	// initialized
	if (p_ctx->is_initialized){
		diag = DIAG_RT_CTX_ALREADY_INITIALIZED;
		goto finish;
	}

	// it is not initialized anyway, just in case
	memset(p_ctx,0,sizeof(*p_ctx));
	p_ctx->is_initialized = FALSE;

	p_ctx->ini_file.ini_name = g_strdup(ini_name);
	// fill the identity structure
	if ( (diag = get_identity(&p_ctx->identity)) != DIAG_OK)
		goto finish;

	if ( (diag = diag_ptr_warn_if_not_null(p_ctx->patrol_htab,
			"The pointer is not NULL. The patrols_htab has been already initialized.ATS\n")) != DIAG_OK)
		return diag;

	// need to initialize the patrol_htab
	p_ctx->patrol_htab = g_hash_table_new_full(g_str_hash, g_str_equal,
			patrol_htab_key_destroy, patrol_htab_val_destroy );

	if (TRUE == is_collector) {
		// this is the request on the collector side
		p_ctx->side = COLLECTOR_SIDE;

		// load the collector
		if ( (diag = deploy_clctor(p_ctx)) != DIAG_OK )
			goto finish;

		p_ctx->is_initialized = TRUE;
	} else {
		p_ctx->side = AGGREGATOR_SIDE;

		if ( (diag = deploy_agrtor(p_ctx)) != DIAG_OK )
			goto finish;

		p_ctx->is_initialized = TRUE;
	}

finish:
	// just release the lock
	pthread_mutex_unlock( &rt_mtx );

	return diag;
}

/**
 * The spy registration function. It makes visible to the runtime
 * the spy; it creates a space in the patrol_htab and remembers
 * the initialization function.
 * It DOES NOT call the initialization function.
 *
 * @param spy_name the name of the spy; we need that parameter to check if the
 *        spy is in the patrol_htab before inserting it again
 * @param p_init_spy_ctx_func A pointer to the init function
 * @return DIAG_OK it was a new spy and we inserted it
 *         DIAG_RT_CTX_SPY_REGISTERED if the spy has been already registered
 * It should be thread-safe.
 */
diag_t dwarf_rt_reg_spy(struct dwarf_rt_ctx *p_ctx, char* spy_name, dwarf_init_spy_func_t p_init_spy_ctx_func){
	diag_t diag = DIAG_OK;
	// this will be allocated and inserted into the patrol_htab if this is required
	struct spy_ctx * internal_ctx;

	// make sure the in parameters are valid
	if ( diag_ptr_null_ret(spy_name, "The spy name can't be NULL.\n") == DIAG_NOT_NULL_PTR_ERR
		 || diag_ptr_null_ret(p_init_spy_ctx_func, "The spy initialization function can't be NULL\n") == DIAG_NULL_PTR_ERR	)
		return DIAG_NOT_NULL_PTR_ERR;

	pthread_mutex_lock( &reg_spy_htab_mtx );
	// check if the name already exists in the patrol_htab
	// we assume that the patrol_htab should be already
	if ( NULL  != g_hash_table_lookup(p_ctx->patrol_htab, spy_name)){
		diag = DIAG_RT_CTX_SPY_REGISTERED;
		// it is already in the table, so go away
		goto finish;
	}

	// this is the new spy to be registered
	internal_ctx = calloc(sizeof(struct spy_ctx), 1);

	if ( (diag = diag_mem_alloc_ret(internal_ctx, "Can't allocate memory for spy ctx.")) != DIAG_OK )
		goto finish;

	internal_ctx->name = g_strdup(spy_name);
	// remember the pointer to the init_spy_ctx_function
	internal_ctx->init_spy_ctx_func = p_init_spy_ctx_func;
	// and be sure that the spy is set to be false
	internal_ctx->is_enabled = FALSE;

	// insert the structure to the patrol_htab
	g_hash_table_insert(p_ctx->patrol_htab, internal_ctx->name, internal_ctx);

	p_debug(DWARF_NAME ": The spy %s has been registered in the runtime.\n", internal_ctx->name);
finish:
	pthread_mutex_unlock( &reg_spy_htab_mtx);
	return diag;
}

/**
 * enables spy; in practice it only sets the is_enabled flag to TRUE
 * @param spy_name The name of the spy to be enabled (as you provided with the reg function)
 * @return DIAG_OK everything went fine
 *         DIAG_RT_CTX_SPY_NOT_FOUND The spy was not found in the patrol_htab
 */
diag_t dwarf_rt_enable_spy(struct dwarf_rt_ctx *p_ctx, char* spy_name){
	diag_t diag = DIAG_OK;
	gpointer p_val;
	struct spy_ctx * p_spy_ctx = NULL;
	// this is what you got from the initialization
	struct spy_ctx ctx;


	pthread_mutex_lock( &enab_spy_htab_mtx );
	// check if the name already exists in the patrol_htab
	// we assume that the patrol_htab should be already
	p_val = g_hash_table_lookup(p_ctx->patrol_htab, spy_name);
	if ( NULL  == p_val){
		diag = DIAG_RT_CTX_SPY_NOT_FOUND;
		p_error("Can't enable the spy %s since it is not present in patrol_htab\n", spy_name);
		// can't find the spy in the patrol_htab
		goto finish;
	}

	p_spy_ctx = (struct spy_ctx *) p_val;
	// now check if the spy has been already enabled
	if (p_spy_ctx->is_enabled){
		diag = DIAG_RT_CTX_SPY_ENABLED;
		p_warn("Can't enable the spy %s since it has been already enabled.\n", spy_name);
		goto finish;
	}

	p_debug("Enabling the spy %s ....\n", spy_name);
	// ok the spy has not been enabled, then enable it, i.e. call the init
	// function, the NULL pointer initialization function error should
	// be caught at the spy registration phase, we assume if the spy
	// can be found in the patrol_htab it means that it contains not a null
	// pointer
	memset(&ctx, 0, sizeof(ctx));
	diag = p_spy_ctx->init_spy_ctx_func(p_ctx, &ctx);
	p_spy_ctx->evpath_ctx_cap = ctx.evpath_ctx_cap;
	p_spy_ctx->evpath_ctx_mon = ctx.evpath_ctx_mon;
	p_spy_ctx->init_ranger_func = ctx.init_ranger_func;
	p_spy_ctx->init_spy_func = ctx.init_spy_func;


	if (diag != DIAG_OK){
		p_error(DWARF_NAME ": The spy %s initialization functions returned "
				"with errors. ATS.\n", spy_name);
		goto finish;
	}

	// there might be slightly different procedure for the collector
	// and for the aggregator side when adding a probe, that's why
	// we need to distinguish it here

	switch (p_ctx->side){
	case AGGREGATOR_SIDE:
		p_debug(DWARF_NAME ": Enabling aggregator side ...\n");
		diag = rt_enable_spy_agrtor(p_ctx, p_spy_ctx);
		break;
	case COLLECTOR_SIDE:
		p_debug(DWARF_NAME ": Enabling collector side ...\n");
		diag = rt_enable_spy_clctor(p_ctx, p_spy_ctx);
		break;
	default:
		diag = DIAG_RT_UNKNOWN_SIDE_ERR;
		p_error(DWARF_NAME ": the unknown side,neither aggregator nor collector. ATS\n");
		break;
	}

	if (DIAG_OK != diag)
		p_error(DWARF_NAME ": enabled unsuccessful. ATS\n");
	else {
		p_spy_ctx->is_enabled = TRUE;
		p_debug(DWARF_NAME ": %s enabled = %d\n", spy_name, p_spy_ctx->is_enabled);
	}

finish:
	pthread_mutex_unlock( &enab_spy_htab_mtx);
	return diag;
}

// --------------------------------------
// runtime utils
// --------------------------------------
/**
 * Deploys a spy by reading from the ini file. By deployment I mean registering
 * the spy and enabling it, previously checking if the spy has been asked
 * in the ini file. The function calls the reg function and enable_spy function.
 *
 * @param p_ctx
 * @param spy_name
 * @param p_init_spy_ctx_func
 * @return DIAG_OK everything went fine
 *         != DIAG_OK some errors
 */
diag_t
dwarf_rt_deploy_spy_from_ini(struct dwarf_rt_ctx *p_ctx, char *spy_name, dwarf_init_spy_func_t p_init_spy_ctx_func){

	diag_t diag = DIAG_OK;

	if ( (diag = diag_ptr_null_ret(p_ctx, "The pointer to the context is NULL.\n")) == DIAG_NULL_PTR_ERR
		|| (diag = diag_ptr_null_ret(spy_name, "The spy name can't be NULL.\n")) == DIAG_NULL_PTR_ERR) {
			p_error("Pointers errors. Abandoning the function.\n");
			return diag;
	}

	gboolean is_spy_present = FALSE;
	diag = ini_get_bool(p_ctx->ini_file.ini_name, INI_DWARF_STR_SPIES_GRP,spy_name, &is_spy_present);

	// we ignore the error and assume that it means that the spy is disabled
	if (DIAG_OK == diag && is_spy_present){
		// register a spy
		if ( (diag = dwarf_rt_reg_spy(p_ctx, spy_name, p_init_spy_ctx_func)) != DIAG_OK){
			p_warn(DWARF_NAME ": spy %s registration unsuccessful: error code: %d\n", spy_name,  diag);
			return diag;
		}

		// enable a spy
		if ( (diag = dwarf_rt_enable_spy(p_ctx, spy_name)) != DIAG_OK){
			p_warn(DWARF_NAME ": enabling a spy %s unsuccessful: error code: %d\n", spy_name, diag);
			return diag;
		}
	}

	return diag;
}

/**
 * prints status of the scout pCtx->scouts_htab
 * @param pCtx
 * @return DIAG_NULL_PTR_ERR if the contxt
 *         DIAG_OK if everything ok
 */
diag_t dwarf_rt_prt_spy_status(struct dwarf_rt_ctx *p_ctx) {
	diag_t diag = DIAG_OK;
	GHashTableIter iter;
	gpointer key, value;

	if ( (diag = diag_ptr_null_ret(p_ctx, "Context is a NULL pointer\n.")) != DIAG_OK)
		return diag;

	// show the enabled scouts
	g_hash_table_iter_init(&iter, p_ctx->patrol_htab);
	while (g_hash_table_iter_next(&iter, &key, &value)) {
		struct spy_ctx *info = value;
		if (info->is_enabled)
			p_info("Spy: %s ENABLED.\n", info->name);
		else
			p_info("Spy: %s DISABLED.\n", info->name);
	}

	return diag;
}


/**
 * destroys the key for the rt_ctx.patrol_htab
 *
 * @param data The data is the key, i.e. the string allocated for the key
 *             the name of the patrol
 *
 */
void patrol_htab_key_destroy(gpointer data){
	g_free(data);
}
/**
 * destroys the value for the rt_ctx.patrol_htab
 * @param data the value of the rt_ctx.patrol_htab
 */
void patrol_htab_val_destroy(gpointer data){
	struct spy_ctx *ctx = (struct spy_ctx *) data;

	if (ctx) {
		g_free(ctx->name);
		ctx->name = NULL;
	} else {
		p_warn("The entry to the rt_ctx.patrol_htab is NULL.\n");
	}

	// the value has been created in insert_scout_info()
	g_free(ctx);
}

/**
 * part of enabling spy on the aggregator side
 *
 * @param p_ctx
 * @param p_spy_ctx
 * @return DIAG_OK
 */
static diag_t rt_enable_spy_agrtor(struct dwarf_rt_ctx* p_ctx, struct spy_ctx* p_spy_ctx) {
	diag_t diag = DIAG_OK;

	p_debug(DWARF_NAME ": adding probes ...\n");

	add_probe(p_spy_ctx->evpath_ctx_cap.res_type,
			  p_spy_ctx->evpath_ctx_cap.format_list,
			  p_spy_ctx->evpath_ctx_cap.data_size,
			  p_spy_ctx->evpath_ctx_cap.period_sec,
			  p_spy_ctx->evpath_ctx_cap.agrtor_setup_func,
			  p_spy_ctx->evpath_ctx_cap.agrtor_read_func,
			  p_spy_ctx->evpath_ctx_cap.agrtor_process_func);

	p_debug(DWARF_NAME ": added a probe for CAP data for spy %s...\n", p_spy_ctx->name);

	add_probe(p_spy_ctx->evpath_ctx_mon.res_type,
			  p_spy_ctx->evpath_ctx_mon.format_list,
			  p_spy_ctx->evpath_ctx_mon.data_size,
			  p_spy_ctx->evpath_ctx_mon.period_sec,
			  p_spy_ctx->evpath_ctx_mon.agrtor_setup_func,
			  p_spy_ctx->evpath_ctx_mon.agrtor_read_func,
			  p_spy_ctx->evpath_ctx_mon.agrtor_process_func);

	p_debug(DWARF_NAME ": added a probe for MON data for spy %s...\n", p_spy_ctx->name);

	// now call all functions that need to be called for this spy
	if (p_spy_ctx->init_ranger_func){
		if( (diag = p_spy_ctx->init_ranger_func(p_ctx, p_spy_ctx)) != DIAG_OK ){
			p_error(DWARF_NAME ": Unsuccessful invocation of the init_ranger_func. ATS\n");
			return diag;
		}
	} else {
		p_debug(DWARF_NAME ": invocation of the init_ranger_func skipped (NULL). Continuing\n");
	}

	return diag;
}

/**
 * part of enabling spy on the collector side
 *
 * @param p_ctx
 * @param p_spy_ctx
 * @return DIAG_OK
 */
static diag_t rt_enable_spy_clctor(struct dwarf_rt_ctx* p_ctx, struct spy_ctx* p_spy_ctx) {
	diag_t diag = DIAG_OK;

	p_debug(DWARF_NAME ": adding probes ...\n");
	add_probe(p_spy_ctx->evpath_ctx_mon.res_type,
					p_spy_ctx->evpath_ctx_cap.format_list,
					p_spy_ctx->evpath_ctx_cap.data_size,
					p_spy_ctx->evpath_ctx_cap.period_sec,
					p_spy_ctx->evpath_ctx_cap.clctor_setup_func,
					p_spy_ctx->evpath_ctx_cap.clctor_read_func,
					p_spy_ctx->evpath_ctx_cap.clctor_process_func);
	p_debug(DWARF_NAME ": added a probe for CAP data for spy %s...\n", p_spy_ctx->name);

	add_probe(p_spy_ctx->evpath_ctx_mon.res_type,
			p_spy_ctx->evpath_ctx_mon.format_list,
			p_spy_ctx->evpath_ctx_mon.data_size,
			p_spy_ctx->evpath_ctx_mon.period_sec,
			p_spy_ctx->evpath_ctx_mon.clctor_setup_func,
			p_spy_ctx->evpath_ctx_mon.clctor_read_func,
			p_spy_ctx->evpath_ctx_mon.clctor_process_func);
	p_debug(DWARF_NAME ": added a probe for MON data for spy %s...\n", p_spy_ctx->name);

	// now call functions that need to be called
	// now call all functions that need to be called for this spy
	if (p_spy_ctx->init_spy_func){
		if( (diag = p_spy_ctx->init_spy_func(p_ctx, p_spy_ctx)) != DIAG_OK ){
			p_error(DWARF_NAME ": Unsuccessful invocation of the init_spy_func. ATS\n");
			return diag;
		}
	} else {
		p_debug(DWARF_NAME ": invocation of the init_spy_func skipped (NULL). Continuing\n");
	}

	return diag;
}

/**
 * deploys spies; should add probes
 * @param p_ctx
 * @return
 */
static
diag_t deploy_clctor(struct dwarf_rt_ctx *p_ctx){

	diag_t diag = DIAG_OK;

	// just check if we are the aggregator side
	if (COLLECTOR_SIDE != p_ctx->side){
		p_error("This is not COLLECTOR but: %d. Abandon the ship ...\n", p_ctx->side);
		diag = DIAG_ERR;
		return diag;
	}

	if ( (diag = diag_ptr_warn_if_null(p_ctx->patrol_htab,
			"The pointer is NULL. The patrols_htab should have been already initialized.\nATS")) != DIAG_OK)
		return diag;

	// write the message queue name for the lynx spy to the ini file
	if ((diag = ini_set_patrol_lynx_spy_q_name(&p_ctx->ini_file)) != DIAG_OK)
		return diag;

	// write the message queue
	if ( (diag = ini_set_patrol_lynx_spy_q_max_msg_size(&p_ctx->ini_file, sizeof(kernel_profile)))){
		return diag;
	}

	// get data from the ini file such as hostname, port, and others
	if ((diag = ini_get_patrol_ini(&p_ctx->ini_file)) != DIAG_OK)
		return diag;

	return diag;
}

/**
 * deploys an aggregator; allocates and initializes
 * all necessary structures
 * @param p_ctx
 * @return DIAG_OK everything ok
 *
 */
static diag_t deploy_agrtor(struct dwarf_rt_ctx *p_ctx){
	diag_t diag = DIAG_OK;

	// just check if we are the aggregator side
	if (AGGREGATOR_SIDE != p_ctx->side){
		p_error("This is not AGGREGATOR but: %d. Abandon the ship ...\n", p_ctx->side);
		diag = DIAG_ERR;
		return diag;
	}

	if ( (diag = diag_ptr_warn_if_null(p_ctx->patrol_htab, "The pointer is NULL."
			"The patrols_htab should have been already initialized. ATS\n")) != DIAG_OK){
		return diag;
	}

	// get the ini configuration

	// write the host to the ini file
	if ((diag = ini_set_ranger_host(&p_ctx->ini_file)) != DIAG_OK)
		return diag;


	// get data from the ini file such as hostname, port, and others
	if ((diag = ini_get_ranger_ini(&p_ctx->ini_file)) != DIAG_OK)
		return diag;


	return diag;
}

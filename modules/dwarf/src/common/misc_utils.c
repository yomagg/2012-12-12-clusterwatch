/**
 * @file misc_utils.c
 *
 * @date Sep 24, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#include <errno.h> //strerrno
#include <string.h> // strerrno
#include <sys/types.h>  // getpid
#include <unistd.h>     // getpid
#include <time.h>
#include <glib.h>


#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>


#include "misc_utils.h"
#include "misc_utils_types.h"
#include "debug.h"
#include "dwarf_types.h"
#include "mmap_manager.h"
#include "mmap_manager_types.h"
#include "ini_utils.h"


static diag_t
create_memmapped_log(struct dwarf_rt_ctx *p_ctx, const char *fix, struct spy_ctx *p_spy_ctx);

// ------------------------
// creating bhb maps
// -----------------------
/**
 * This function callocates memory the p_bhb_mmap_ctx, which should be
 * released with free(). If there were some errors the function
 * tries to free memory
 * @param diag the diagnostic output and feedback from the execution of the
 *             function
 * @param p_params the bhb params according to which the memory mapped file
 *               will be constructed
 * @param p_dwarf_rt_ctx
 * @param p_spy_ctx
 * @return NULL if some errors
 *         diag - the diagnostic, DIAG_OK, if everything ok, or != DIAG_OK if
 *         something wrong
 */
struct bhb_mmap_ctx * bhb_create_mapped_file(diag_t *diag,
		struct bhb_mmap_param * p_params,
		struct dwarf_rt_ctx * p_dwarf_rt_ctx,
		struct spy_ctx * p_spy_ctx){

	struct bhb_mmap_ctx * p_bhb_mmap_ctx = NULL;
	*diag = DIAG_OK;

	p_bhb_mmap_ctx = calloc(sizeof(struct bhb_mmap_ctx), 1);
	if ( (*diag = diag_mem_alloc_ret(p_bhb_mmap_ctx, "")) != DIAG_OK)
		return NULL;

	// initialize the bhb_mmap structure; the second part
	// of the initialization will take part when we know the
	// name of the file assigned to us. the second part of initialization:
	// we create the relevant pointers and structures for a header, body and
	// blocks in the body

	if ( (*diag = bhb_mmap_init_first(p_bhb_mmap_ctx, p_params )) != DIAG_OK){
		goto free_mem;
	}

	// after calling the initialization we are able to determine
	// the total size required for creation of the mmap
	if ( (*diag = bhb_mmap_get_total_size(p_bhb_mmap_ctx, &p_spy_ctx->mem_map.total_size)) != DIAG_OK){
		goto free_mem;
	}

	// here should be invoked the create_mmapped_log() since it requires
	// a size and the data it sets are required by mmap but I don't like the
	// idea of switching back an forth
	if ( (*diag = create_memmapped_log(p_dwarf_rt_ctx, p_spy_ctx->name, p_spy_ctx)) != DIAG_OK ){
		p_error("Issues with memory mapping. ATS \n");
		goto free_mem;
	}

	// finish initialization of the structure
	if ((*diag = bhb_mmap_init_pointers(p_bhb_mmap_ctx, p_spy_ctx)) != DIAG_OK) {
		p_error("Issues with creating mmap index for cpu\n");
		goto free_mem;
	}

	// don't release the memmap since everything went fine
	if (DIAG_OK == *diag)
		goto finish;

free_mem:
	p_info(DWARF_NAME ":There were errors during execution of this function. "
			"Releasing the mmap ...\n.");
	// clean some stuff
	free(p_bhb_mmap_ctx);
	p_bhb_mmap_ctx = NULL;

finish:
	return p_bhb_mmap_ctx;
}

// --------------------------------------
// mmap creation related functions
// --------------------------------------

/**
 * it returns the pointer ptr = buf + strlen(buf), and updates the buf_len
 * internal function used to move pointers
 *
 * @param buf (IN) The buffer
 * @param buf_len (OUT) updated and computed the length of the buf
 * @return buf + strlen(buf)
 */
char *calc_ptr_buf_after_offset(char *buf, int *buf_len){
	*buf_len = strlen(buf);
	return buf + *buf_len;
}


/**
 * It creates the file and maps it into memory. The p_spy structure
 * is updated with the memory mapped information
 * @param file_path (IN) The name of the file to be created and mapped
 * @param p_spy (IN/OUT) updated info about the mem mapped context
 * @return DIAG_OK if everything went fine
 *         DIAG_ERR if errors
 */
static diag_t
create_mem_map(GString *file_path, struct spy_ctx *p_spy){
	diag_t diag = DIAG_OK;
	int fd;
	if ((fd = open(file_path->str, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR))
			== -1) {
		p_error("Issues with open(). Returning ...\n");
		return DIAG_ERR;
	}
	if (lseek(fd, p_spy->mem_map.total_size + 1, SEEK_SET) < 0) {
		p_warn("Issues with lseek(). Returning ...\n");
		return DIAG_ERR;
	}
	if (write(fd, "", 1) <= 0) {
		p_error("Issues with write(). Returning ...\n");
		return DIAG_ERR;
	}
	if (lseek(fd, 0, SEEK_SET) < 0) {
		p_error("Issues with lseek(). Returning ...\n");
		return DIAG_ERR;
	}

	// create the memory mapping
	void *file_mem = NULL;

	if ( (file_mem = mmap(0, p_spy->mem_map.total_size, PROT_WRITE, MAP_SHARED, fd, 0 )) ==
			MAP_FAILED){
		p_error("mmap() failed. Returning ...: %s\n", strerror(errno));
		diag = DIAG_ERR;
		goto finish;
	}

	// update the p_spy
	p_spy->mem_map.filename_mapped = g_strdup(file_path->str);
	p_spy->mem_map.mmap_start = file_mem;

	p_debug("The file has been created and mem mapped: %s\n", file_path->str);

finish:
	close(fd);

	return diag;
}

/**
 * creates a log file, then maps it to the memory with mmap and
 * then closes the log file.
 *
 * The pointer to the memory is set in p_spy->mem_map.mmap_start
 * and the map should be released with
 * munmap (mmap_start, total_size);
 * or not. It is unmapped when the program finishes by Linux.
 *
 * @param p_ctx (IN) the output dir is taken from the ini file
 * @param fix (IN) to distinguish that this is gpu, cpu, or something else
 *            this will be included in the name
 * @param p_spy (IN/OUT) this context will be updated with the new info
 *        about created mem map region
 * @return DIAG_OK if everything ok
 *         otherwise != DIAG_OK
 */
static diag_t
create_memmapped_log(struct dwarf_rt_ctx *p_ctx, const char *fix, struct spy_ctx *p_spy_ctx){
	diag_t diag = DIAG_OK;

	if ( (diag = diag_ptr_null_ret(p_ctx, "The context is a NULL pointer. Can't continue ...\n")) != DIAG_OK)
		return diag;

	// check if directory exists and can be written
	if (access(p_ctx->ini_file.ranger_output_dir, W_OK) == -1) {
		p_warn("The output directory doesn't exist.\n");
		return DIAG_ERR;
	}

	GString* date = get_date_string();

	GString* file_root_name = g_string_new(p_ctx->identity.hostname);

	// append the prefix
	g_string_append_printf(file_root_name, "-mmap-%s", fix);

	// append the date
	g_string_append_printf(file_root_name, "-%s", date->str);

	// now check if the file exists or we need to create another file
	int i;
	GString* file_path = g_string_new("");
	// indicates if the file has been created
	gboolean file_created = FALSE;
	for (i = 0; i < FILE_CREATION_TRIES; i++) {
		g_string_append_printf(file_path, "%s%s-%04d.%s", p_ctx->ini_file.ranger_output_dir,
				file_root_name->str, i, LOG_FILE_EXT);

 		// check if the file exists
		if (access(file_path->str, F_OK) == -1) {
			// file doesn't exists
			if ( (diag = create_mem_map(file_path, p_spy_ctx)) != DIAG_OK){
				p_warn("Issues with create_mem_map. ....\n");
				goto finish;
			}
			file_created = TRUE;
			break;
		} else {
			p_debug("Try # %d: File exists %s\n", i, file_path->str);
		}
		// we have stored the output file handler
		g_string_erase(file_path, 0, -1);
	}

finish:
	g_string_free(file_path, TRUE);
	g_string_free(date, TRUE);
	g_string_free(file_root_name, TRUE);

	if ( (FALSE == file_created) || (DIAG_OK != diag) ){
		p_warn("The output file has not been created and memory mapping"
			   " has not been done. Tried %d times.\n.", i);
	}

	return diag;
}


// --------------------------
// pointers diagnostic
// --------------------------

/**
 * returns DIAG_NULL_PTR_ERR if the passed pointer is null
 * prints the mesg
 * @param ptr the pointer to be checked
 * @param mesg The message to be displayed
 * @return DIAG_OK if ptr is not null
 *         DIAG_NULL_PTR_ERR if ptr is null
 */
diag_t
diag_ptr_null_ret(void *ptr, char *mesg){
	if (!ptr) {
		p_warn(ptr, mesg);
		return DIAG_NULL_PTR_ERR;
	}
	return DIAG_OK;
}

/**
 * returns DIAG_NULL_PTR_ERR if the passed pointer is null
 * prints the mesg
 * @param ptr the pointer to be checked
 * @param mesg The message to be displayed
 * @return DIAG_OK if ptr is not null
 *         DIAG_NULL_PTR_ERR if ptr is null
 */
diag_t
diag_ptr_not_null_ret(void *ptr, char *mesg){
	if (ptr) {
		return DIAG_OK;
	} else {
		p_warn(ptr, mesg);
		return DIAG_NULL_PTR_ERR;
	}
}

/**
 * warn if the pointer is not null and return DIAG_NOT_NULL_PTR_ERR
 * @param ptr
 * @param mesg
 * @return DIAG_NOT_NULL_PTR_ERR if the pointer is not null
 *         DIAG_OK if the pointer is null
 */
diag_t
diag_ptr_warn_if_not_null(void *ptr, char *mesg){
	if (!ptr) {
		return DIAG_OK;
	} else {
		p_warn(ptr, mesg);
		return DIAG_NOT_NULL_PTR_ERR;
	}

}

/**
 * warn if the pointer is null and return NULL_PTR_ERR
 * @param ptr
 * @param mesg
 * @return DIAG_NULL_PTR_ERR if the pointer is null
 *         DIAG_OK if the pointer is not null
 */
diag_t
diag_ptr_warn_if_null(void *ptr, char *mesg){
	if (!ptr){
		p_warn(ptr, mesg);
		return DIAG_NULL_PTR_ERR;
	} else {
		return DIAG_OK;
	}
}
/**
 * returns DIAG_MEM_ALLOC_ERR if the passed pointer is null
 * prints the mesg
 * @param ptr the pointer to be checked
 * @param mesg The message to be displayed
 * @return DIAG_OK if ptr is not null
 *         DIAG_MEM_ALLOC_ERR if ptr is null
 */
diag_t
diag_mem_alloc_ret(void *ptr, char *mesg){
	if (!ptr) {
		p_warn(ptr, "Issues with memory allocations: %s. Extra mesg: %s", strerror(errno), mesg);
		return DIAG_MEM_ALLOC_ERR;
	}
	return DIAG_OK;
}

/**
 * Gets the identity structure filled with the data
 * @param id the id to be filled
 * @return DIAG_OK if everything ok
 *         DIAG_ERR if problems with getting the hostname
 */
diag_t
get_identity(struct identity *id){
	diag_t diag = DIAG_OK;
	// fill the identity structure
	time_t 		t;

	srand48(getpid() + time(&t));
	id->id = lrand48();
	id->pid = getpid();
	if( gethostname(id->hostname, sizeof(id->hostname)) != 0 ){
		p_error("Problems with getting the host name in the context\n");
		diag = DIAG_ERR;
	}

	return diag;
}

/**
 * converts struct timeval to microseconds
 * @param tv
 */
long int timeval2usec(struct timeval *tv){
	return (long long int)(tv->tv_sec * 1000000 + tv->tv_usec) ;
}

/**
 * skips the line in the provided handler
 * @param p a handler to the file you want to
 * @return SCOUT_OK
 *         SCOUT_ERR if can't read the file
 */
diag_t skip_line(FILE* p){
	diag_t diag = DIAG_OK;
	char* line = NULL;
	size_t n = 0;

	// just report if the pointer is null
	if (( diag = diag_ptr_null_ret(p, "") ) != DIAG_OK ){
		return diag;
	}

	if( getline(&line, &n, p) == -1){
		p_error("Problem with reading the line from file handler\n");
		diag = DIAG_ERR;
	}

	free(line);
	return diag;
}

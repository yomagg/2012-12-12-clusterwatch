/**
 * @file spy.c
 *
 * @date Sep 14, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#include <sys/time.h>
#include <sys/types.h>

#include "evpath.h"
#include "dwarf_types.h"


//! the timestamp format that can be used by other elements
FMField timestamp_field_list[] = {
		{ "tv_sec", "integer", sizeof(time_t), FMOffset(struct timeval*, tv_sec)},
		{ "tv_usec", "integer", sizeof(suseconds_t), FMOffset(struct timeval*, tv_usec)},
		{	NULL, NULL, 0, 0}
};

FMField identity_field_list[] = {
		{"id", "integer", sizeof(int), FMOffset(struct identity *, id)},
		{"hostname", "integer", sizeof(char), FMOffset(struct identity *, hostname)},
		{"pid", "integer", sizeof(pid_t), FMOffset(struct identity *, pid)},
		{ NULL, NULL, 0, 0}
};


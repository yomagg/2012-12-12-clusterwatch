/**
 * @file ini_utils.c
 *
 * @date Jun 8, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#include <unistd.h>
#include "ini_utils_types.h" // for ini structure and constants
#include <glib.h>
#include <string.h>
#include "devel.h"

#include <stdlib.h>
#include <stdio.h>

static diag_t
ini_get_int(GKeyFile* 	pKeyFile, const char* group, const char* key, int *res);

GString* get_date_string(void);
/**
 * it gets the ini file; The key file should be freed if not needed with
	g_key_file_free() somehow; if there is an error the **key_file is
	released within this function and set to NULL
 * @param ini_name (IN) : The name of the ini file
 * @param key_file (OUT) : the pointer with loaded key_file
 * @return DIAG_OK if everything is fine
 *         DIAG_xxx_ERR if errors appeared
 */
static diag_t
ini_get_ini(gchar *ini_name, GKeyFile **key_file) {
	GKeyFileFlags flags = G_KEY_FILE_NONE; // no flags, we just want to read
	GError* error = NULL;
	diag_t diag = DIAG_OK;

	if ( (diag = diag_ptr_null_ret(ini_name, "The name of the ini file is null. Provide the correct one")) != DIAG_OK)
		return diag;

	// get a new key_file
	*key_file = g_key_file_new();

	if (!g_key_file_load_from_file(*key_file, ini_name, flags, &error)) {
		p_warn(" Can't parse the key file: %s. Quitting ...\n", error->message);
		// release resources
		g_key_file_free(*key_file);
		*key_file = NULL;
		return DIAG_NULL_PTR_ERR;
	}

	return DIAG_OK;
}

/**
 * Returns the boolean value read from the ini file
 *
 * @param pKeyFile
 * @param group
 * @param key
 * @param res outcome of the procedure: true if the key is set to true, false if
 *            it is set to false
 * @return diagnostic info: DIAG_OK - everything went fine, otherwise some errors
 */
diag_t
ini_get_bool(gchar *ini_name, const char *group, const char *key, gboolean *pRes){

	GKeyFile* 	 key_file;
	GKeyFileFlags flags = G_KEY_FILE_NONE;	// no flags, we just want to read
	GError* error = NULL;
	diag_t res = DIAG_OK;

	if ( (res = diag_ptr_null_ret(ini_name, "The name of the ini file is null. Provide the correct one")) != DIAG_OK )
		return res;

	// get a new key_file
	key_file = g_key_file_new();

	if (!g_key_file_load_from_file (key_file, ini_name, flags, &error)){
		p_warn(" Can't parse the key file: %s. Quitting ...\n", error->message);
		return DIAG_NULL_PTR_ERR;
	}

	*pRes = g_key_file_get_boolean(key_file, group, key, &error);
	if(FALSE == *pRes && error != NULL && (G_KEY_FILE_ERROR_KEY_NOT_FOUND == error->code ||
			G_KEY_FILE_ERROR_INVALID_VALUE == error->code)){
		p_warn("Can't read the value from ini file: [%s:%s]\n", group, key);
		return DIAG_ERR;
	}

	// free our key_file
	g_key_file_free(key_file);

	return DIAG_OK;
}

/**
 * Returns the int value read from the ini file
 *
 * @param pKeyFile
 * @param group
 * @param key
 * @param res outcome of the procedure
 * @return the integer number
 */
diag_t
ini_get_integer(gchar *ini_name, const char *group, const char *key, int *pRes){

	GKeyFile* 	 key_file;
	GKeyFileFlags flags = G_KEY_FILE_NONE;	// no flags, we just want to read
	GError* error = NULL;
	diag_t diag = DIAG_OK;

	if ( (diag = diag_ptr_null_ret(ini_name, "The name of the ini file is null. Provide the correct one")) != DIAG_OK)
		return diag;

	// get a new key_file
	key_file = g_key_file_new();

	if (!g_key_file_load_from_file (key_file, ini_name, flags, &error)){
		p_warn(" Can't parse the key file: %s. Quitting ...\n", error->message);
		return DIAG_NULL_PTR_ERR;
	}

	diag = ini_get_int(key_file, group,  key, pRes);

	// free our key_file
	g_key_file_free(key_file);

	return diag;
}


/**
 * Get the string from the ini file, the string should be freed somehow
 * with g_free()
 *
 * @param pKeyFile The handler to the configuration file
 * @param pGroup The name of the group
 * @param pKey The name of the key
 * @return the string or returns the error code
 */
diag_t
ini_get_str(gchar *ini_name, const char *group, const char *key, gchar **res){
	GError* 		error = NULL;
	gchar* 			v;
	GKeyFile *p_key_file = NULL;
	diag_t diag = DIAG_OK;

	if ( (diag = ini_get_ini(ini_name, &p_key_file)) != DIAG_OK ){
		p_warn("Issues with ini_get_ini\n.");
		return diag;
	}

	v = g_key_file_get_string(p_key_file, group, key, &error);
	if (NULL == v && error != NULL && (G_KEY_FILE_ERROR_KEY_NOT_FOUND == error->code
			|| G_KEY_FILE_ERROR_GROUP_NOT_FOUND == error->code)){
		p_warn("Can't read the value from ini file: [%s:%s]\n", group, key);
		diag = DIAG_ERR;
		*res = NULL;
	} else {
		*res = v;
	}

	return diag;
}


/**
 * returns the int
 * @param pKeyFile the handler to the key file
 * @param group the group
 * @param key the key
 * @param res The result
 * @return DIAG_OK if everything is fine or != DIAG_OK if any errors
 */
static diag_t
ini_get_int(GKeyFile* 	pKeyFile, const char* group, const char* key, int *res){
	GError* error = NULL;
	diag_t diag = DIAG_OK;

	*res = g_key_file_get_integer(pKeyFile, group, key, &error);
	if(0 == *res && error != NULL && (G_KEY_FILE_ERROR_KEY_NOT_FOUND == error->code ||
			G_KEY_FILE_ERROR_INVALID_VALUE == error->code)){
		p_error("Can't read the value from ini file: [%s:%s]\n", group, key);
		diag = DIAG_ERR;
	}

	return diag;
}

/**
 * returns the double
 * @param pKeyFile the handler to the key file
 * @param group the group
 * @param key the key
 * @param res The result
 * @return the corresponding double
 */
static diag_t
ini_get_double(GKeyFile* pKeyFile, const char* group, const char* key, double *res){
	GError* error = NULL;
	diag_t diag = DIAG_OK;

	*res = g_key_file_get_double(pKeyFile, group, key, &error);
	if(0.0 == *res && error != NULL && (G_KEY_FILE_ERROR_KEY_NOT_FOUND == error->code ||
	   G_KEY_FILE_ERROR_INVALID_VALUE == error->code) ){
		p_error("Can't read the value from ini file: [%s:%s]\n", group, key);
		diag = DIAG_ERR;
	}

	return diag;
}

/**
 * Get the string from the ini file
 *
 * @param pKeyFile (in) the handler of the configuration file
 * @param pGroup The name of the group
 * @param pKey The name of the key
 * @param buffer where to copy the string; the buffer should have sufficient
 * 				 space to hold the value of the key
 * @return DIAG_OK if everything is fine
 *         != DIAG_OK if some errors
 */
static diag_t
ini_get_str_to_buffer(GKeyFile* pKeyFile, const char* pGroup, const char* pKey, char* buffer){

	GError* 		error = NULL;
	gchar* 			v;
	diag_t	diag = DIAG_OK;

	v = g_key_file_get_string(pKeyFile, pGroup, pKey, &error);
	if (NULL == v && error != NULL && (G_KEY_FILE_ERROR_KEY_NOT_FOUND == error->code
			|| G_KEY_FILE_ERROR_GROUP_NOT_FOUND == error->code)){
		p_error("Can't read the value from ini file: [%s:%s]\n", pGroup, pKey);
		diag =  DIAG_ERR;
	} else {
		strcpy(buffer, v);
	}

	g_free(v);

	return diag;
}

/**
 * Get the string from the ini file, the string should be freed somehow
 * with g_free()
 *
 * @param pKeyFile The handler to the configuration file
 * @param pGroup The name of the group
 * @param pKey The name of the key
 * @return the string or string equal NULL
 */
static diag_t
ini_get_string(GKeyFile* pKeyFile, const char* pGroup, const char* pKey, gchar **res){
	GError* 		error = NULL;
	gchar* 			v;
	diag_t   diag = DIAG_OK;

	v = g_key_file_get_string(pKeyFile, pGroup, pKey, &error);
	if (NULL == v && error != NULL && (G_KEY_FILE_ERROR_KEY_NOT_FOUND == error->code
			|| G_KEY_FILE_ERROR_GROUP_NOT_FOUND == error->code)){
		p_error("Can't read the value from ini file: [%s:%s]\n", pGroup, pKey);
		g_free(v);
		v = NULL;
		diag = DIAG_ERR;
	} else {
		*res = v;
	}

	return diag;
}


/**
 * Reads the the port of the Global Monitor it is listening on
 * @param res The result
 * @return DIAG_OK if everything went fine or error
 */
diag_t ini_get_ranger_port(GKeyFile* 	pKeyFile, int *res){
	return ini_get_int(pKeyFile, INI_DWARF_STR_RANGER_GRP, INI_DWARF_STR_RANGER_GRP_PORT, res);
}

/**
 * sets the hostname to the ini file; exits if there are some problems
 * @param pIni The pointer the ini structure, we want to use the name of the
 *             ini file
 * @return DIAG_OK if everything is fine
 *         != DIAG_OK if anything went wrong
 */
diag_t ini_set_ranger_host(const ini_t* pIni){
	GKeyFile*		key_file;

	// TODO G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS; this issues the
	// icc compiler warning that the enumerated type is mixed with
	// another type; I don't have time to investigate this issue
	// so the flag for keeping comments should be enough as I don't
	// use the translation anyway
	//GKeyFileFlags	flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
	GKeyFileFlags	flags = G_KEY_FILE_KEEP_COMMENTS;
	GError*			error = NULL;
	char 			hostname[256];
	diag_t	diag = DIAG_OK;

	if( gethostname(hostname, sizeof(hostname)) != 0 ){
		p_error("Problems with getting the host name\n");
		return DIAG_ERR;
	}

	key_file = g_key_file_new ();
	if( (diag = diag_ptr_null_ret(key_file, "Problems with opening the ini file\n")) != DIAG_OK)
		return diag;

	if( !g_key_file_load_from_file(key_file, pIni->ini_name, flags, &error)){
		p_error("Problems with loading the file: %s (code: %d)\n",
				error->message, error->code);
		return DIAG_ERR;
	}

	g_key_file_set_string(key_file, INI_DWARF_STR_RANGER_GRP, INI_DWARF_STR_RANGER_GRP_HOSTNAME, hostname);

	gsize length;
	gchar* ini_content;

	ini_content = g_key_file_to_data(key_file, &length, &error);
	FILE * f;
	f = fopen(pIni->ini_name, "w");
	fwrite(ini_content, 1, strlen(ini_content), f);

	fclose(f);
	g_key_file_free(key_file);

	p_debug("[" INI_DWARF_STR_RANGER_GRP ":" INI_DWARF_STR_RANGER_GRP_HOSTNAME "]"
			"set to: %s\n", hostname);

	return diag;
}



/**
 * get the global monitor host
 *
 * You should check if the return value is NULL.
 * @param pKeyFile (in) the handler to the configuration file
 * @param pHostname (out) The pointer to the char array that will hold the
 *        name of the host;
 * @return DIAG_OK if everything was ok
 *         != DIAG_OK if anything went ballistic
 */
diag_t
ini_get_ranger_host(GKeyFile* pKeyFile, char * pHostname){
	diag_t diag = DIAG_OK;

	diag = ini_get_str_to_buffer(pKeyFile, INI_DWARF_STR_RANGER_GRP,
			INI_DWARF_STR_RANGER_GRP_HOSTNAME, pHostname);

	return diag;
}

/**
 * sets the hostname to the ini file; exits if there are some problems
 * @param pIni The pointer the ini structure, we want to use the name of the
 *             ini file
 * @param port The port number you want to set
 * @return DIAG_OK if everything is fine
 *         != DIAG_OK if anything went wrong
 */
diag_t ini_set_ranger_port(const ini_t* pIni, int port){
	GKeyFile*		key_file;
	// TODO G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS; this issues the
	// icc compiler warning that the enumerated type is mixed with
	// another type; I don't have time to investigate this issue
	// so the flag for keeping comments should be enough as I don't
	// use the translation anyway
	//GKeyFileFlags	flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
	GKeyFileFlags	flags = G_KEY_FILE_KEEP_COMMENTS;
	GError*			error = NULL;
	diag_t	diag = DIAG_OK;

	key_file = g_key_file_new ();
	if( (diag = diag_ptr_null_ret(key_file, "Problems with opening the ini file\n")) != DIAG_OK)
		return diag;

	if( !g_key_file_load_from_file(key_file, pIni->ini_name, flags, &error)){
		p_error("Problems with loading the file: %s (code: %d)\n",
				error->message, error->code);
		return DIAG_ERR;
	}

	g_key_file_set_integer(key_file, INI_DWARF_STR_RANGER_GRP, INI_DWARF_STR_RANGER_GRP_PORT, port);

	gsize length;
	gchar* ini_content;

	ini_content = g_key_file_to_data(key_file, &length, &error);
	FILE * f;
	f = fopen(pIni->ini_name, "w");
	fwrite(ini_content, 1, strlen(ini_content), f);
	fclose(f);

	g_key_file_free(key_file);

	p_debug("[" INI_DWARF_STR_RANGER_GRP ":" INI_DWARF_STR_RANGER_GRP_PORT "]"
			" set to: %d\n", port);

	return diag;
}


/**
 * Gets the frequency of updates in the local thing
 *
 * @param pKeyFile the handler to the configuration file
 * @param pGroup
 * @param pKey
 * @param res (out) The result double number: the frequency in seconds, -1.0 if there are problems with
 *         getting the right number
 * @return DIAG_OK or != DIAG_OK
 */
static diag_t
ini_get_freq(GKeyFile* pKeyFile, const char* pGroup, const char* pKey, double *res) {

	diag_t diag = DIAG_OK;

	diag = ini_get_double(pKeyFile, pGroup, pKey, res);

	return diag;
}

/**
 * Get the frequency for the scout (local monitor)
 * @param pKeyFile the handler to the configuration file
 * @return DIAG_OK if everything is fine, DIAG_ERR otherwise
 */
diag_t ini_get_patrol_freq(GKeyFile* pKeyFile, double *res) {
	return ini_get_freq(pKeyFile, INI_DWARF_STR_PATROL_GRP,
			INI_DWARF_STR_PATROL_GRP_FREQ, res);
}

/**
 * Get the frequency for the trooper. Right now it the same field
 * as defined in scout:frequency field
 *
 * @param pKeyFile the handler to the configuration file
 * @param res (out) the frequency, -1 if some problems
 * @return DIAG_OK or != DIAG_OK
 */
diag_t ini_get_ranger_freq(GKeyFile* pKeyFile, double *res) {
	// this is the frequency (or rather a period) of the scout
	return ini_get_freq(pKeyFile, INI_DWARF_STR_PATROL_GRP, INI_DWARF_STR_PATROL_GRP_FREQ, res);
}


/**
 * gets the directory where the output file will be put
 *
 * @param pKeyFile (in) The handler to the configuration file
 * @param res (out) contains the pointer to the string; should
 *                  be released someday with g_free()
 * @return the string containing the directory
 */
diag_t
ini_get_ranger_output_dir(GKeyFile* pKeyFile, gchar **res){

	return ini_get_string(pKeyFile, INI_DWARF_STR_RANGER_GRP,
			INI_DWARF_STR_RANGER_GRP_OUTPUT_DIR, res);
}

/**
 * returns the yyyy-mm-dd string of the current date
 *
 * @return a string representing the date yyyy-mm-dd, should be freed with
 *         g_string_free
 */
GString*
get_date_string(void){
	GDateTime* date = g_date_time_new_now_local();

	GString*	str = g_string_new("");

	g_string_append_printf(str, "%04d-%02d-%02d",
			g_date_time_get_year(date),
			g_date_time_get_month(date),
			g_date_time_get_day_of_month(date)
			);

    g_date_time_unref(date);

    return str;
}
/**
 * Gets the exectime option from the simons file. Converts minutes
 * to seconds.
 *
 * @param pKeyFile the handler to the configuration file
 * @param pGroup
 * @param pKey
 * @param res the result
 * @return the number (the required execution time)
 */
static diag_t
ini_get_exec_time(GKeyFile* pKeyFile, const char* pGroup, const char* pKey, double * res){
	diag_t diag = DIAG_OK;

	if ((diag = ini_get_double(pKeyFile, pGroup, pKey, res)) == DIAG_OK)
		*res = *res * 60.0;

	return diag;
}
/**
 * Gets the exectime option from the simons file
 * @param pKeyFile the handler to the configuration file
 * @param res The exectime in seconds
 * @return DIAG_OK or != DIAG_OK
 */
diag_t
ini_get_patrol_exec_time(GKeyFile* pKeyFile, double *res){
	return ini_get_exec_time(pKeyFile, INI_DWARF_STR_PATROL_GRP, INI_DWARF_STR_PATROL_GRP_EXECTIME, res);
}
/**
 * gets the directory where the output file will be put
 *
 * @param pKeyFile (in) The handler to the configuration file
 * @param pBuf (out) contains the pointer to the string; should
 *                  be released someday with g_free()
 * @return the string containing the directory
 */
diag_t
ini_get_patrol_lynx_spy_q_name(GKeyFile* pKeyFile, char * pBuf){
	diag_t diag = DIAG_OK;

	diag = ini_get_str_to_buffer(pKeyFile, INI_DWARF_STR_LYNX_SPY_GRP,
			INI_DWARF_STR_LYNX_SPY_GRP_Q_NAME, pBuf);

	return diag;
}

/**
 * sets the hostname to the ini file; exits if there are some problems
 * @param pIni The pointer the ini structure, we want to use the name of the
 *             ini file
 * @return DIAG_OK if everything is fine
 *         != DIAG_OK if anything went wrong
 */
diag_t ini_set_patrol_lynx_spy_q_name(const ini_t* pIni){
	GKeyFile*		key_file;

	// TODO G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS; this issues the
	// icc compiler warning that the enumerated type is mixed with
	// another type; I don't have time to investigate this issue
	// so the flag for keeping comments should be enough as I don't
	// use the translation anyway
	//GKeyFileFlags	flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
	GKeyFileFlags	flags = G_KEY_FILE_KEEP_COMMENTS;
	GError*			error = NULL;
	char 			q_name[100];
	diag_t	diag = DIAG_OK;

	time_t 		t;
	srand48(getpid() + time(&t));
	// this number
	int random_number = 	lrand48();

	strcpy(q_name, "/lynx");
	//snprintf(q_name+strlen(q_name), 10,"_%d",random_number);

	key_file = g_key_file_new ();
	if( (diag = diag_ptr_null_ret(key_file, "Problems with opening the ini file\n")) != DIAG_OK)
		return diag;

	if( !g_key_file_load_from_file(key_file, pIni->ini_name, flags, &error)){
		p_error("Problems with loading the file: %s (code: %d)\n",
				error->message, error->code);
		return DIAG_ERR;
	}

	g_key_file_set_string(key_file, INI_DWARF_STR_LYNX_SPY_GRP, INI_DWARF_STR_LYNX_SPY_GRP_Q_NAME, q_name);

	gsize length;
	gchar* ini_content;

	ini_content = g_key_file_to_data(key_file, &length, &error);
	FILE * f;
	f = fopen(pIni->ini_name, "w");
	fwrite(ini_content, 1, strlen(ini_content), f);

	fclose(f);
	g_key_file_free(key_file);

	p_debug("[" INI_DWARF_STR_LYNX_SPY_GRP ":" INI_DWARF_STR_LYNX_SPY_GRP_Q_NAME "]"
			"set to: %s\n", q_name);

	return diag;
}

/**
 * sets the q_max_msg_size to the ini file; exits if there are some problems
 * @param pIni The pointer the ini structure, we want to use the name of the
 *             ini file
 * @param msg_size The max msg size for teh lynx spy queue
 * @return DIAG_OK if everything is fine
 *         != DIAG_OK if anything went wrong
 */
diag_t ini_set_patrol_lynx_spy_q_max_msg_size(const ini_t* pIni, int msg_size){
	GKeyFile*		key_file;
	// TODO G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS; this issues the
	// icc compiler warning that the enumerated type is mixed with
	// another type; I don't have time to investigate this issue
	// so the flag for keeping comments should be enough as I don't
	// use the translation anyway
	//GKeyFileFlags	flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
	GKeyFileFlags	flags = G_KEY_FILE_KEEP_COMMENTS;
	GError*			error = NULL;
	diag_t	diag = DIAG_OK;

	key_file = g_key_file_new ();
	if( (diag = diag_ptr_null_ret(key_file, "Problems with opening the ini file\n")) != DIAG_OK)
		return diag;

	if( !g_key_file_load_from_file(key_file, pIni->ini_name, flags, &error)){
		p_error("Problems with loading the file: %s (code: %d)\n",
				error->message, error->code);
		return DIAG_ERR;
	}

	g_key_file_set_integer(key_file, INI_DWARF_STR_LYNX_SPY_GRP, INI_DWARF_STR_LYNX_SPY_GRP_Q_MAX_MSG_SIZE_BYTES, msg_size);

	gsize length;
	gchar* ini_content;

	ini_content = g_key_file_to_data(key_file, &length, &error);
	FILE * f;
	f = fopen(pIni->ini_name, "w");
	fwrite(ini_content, 1, strlen(ini_content), f);
	fclose(f);

	g_key_file_free(key_file);

	p_debug("[" INI_DWARF_STR_LYNX_SPY_GRP ":" INI_DWARF_STR_LYNX_SPY_GRP_Q_MAX_MSG_SIZE_BYTES "]"
			" set to: %d\n", msg_size);

	return diag;
}

/**
 * Reads the the parameter for the q_max_msg_count
 * @param res The result
 * @return DIAG_OK if everything went fine or error
 */
diag_t ini_get_patrol_lynx_spy_q_max_msg_count(GKeyFile* 	pKeyFile, int *res){
	return ini_get_int(pKeyFile, INI_DWARF_STR_LYNX_SPY_GRP, INI_DWARF_STR_LYNX_SPY_GRP_Q_MAX_MSG_COUNT, res);
}

/**
 * Reads the the parameter for the q_max_msg_count; the msg size is in bytes
 * @param res The result
 * @return DIAG_OK if everything went fine or error
 */
diag_t ini_get_patrol_lynx_spy_q_max_msg_size(GKeyFile* 	pKeyFile, int *res){
	return ini_get_int(pKeyFile, INI_DWARF_STR_LYNX_SPY_GRP, INI_DWARF_STR_LYNX_SPY_GRP_Q_MAX_MSG_SIZE_BYTES, res);
}


/**
 * Gets the exectime option from the simons file
 * @param pKeyFile the handler to the configuration file
 *
 * @return the exectime in seconds
 */
diag_t
ini_get_ranger_exec_time(GKeyFile* pKeyFile, double *res){
	return  ini_get_exec_time(pKeyFile, INI_DWARF_STR_RANGER_GRP,
			INI_DWARF_STR_RANGER_GRP_EXECTIME, res);
}
/**
 * interprets if the time execution is forever
 * @param exectime
 * @return TRUE if the time is forever
 *         FALSE if the time has a finite value
 */
gboolean
ini_time_isForever(double exectime){
	return 0.0 == exectime ? TRUE : FALSE;
}
/**
 * prints the contents of the ini file pointed by the pIni pointer
 * @param pIni
 * @param is_trooper if TRUE then it will be used for displaying the
 *        trooper context otherwise the scout
 *
 * @return DIAG_OK
 */
diag_t
ini_print_ini(ini_t * pIni, gboolean is_trooper){
	diag_t res = DIAG_OK;

	if ( (res = diag_ptr_null_ret(pIni, "Can't print the null pointer\n")) != DIAG_OK)
		return res;

	printf("INI FILE SETTINGS FOR ");

	if (is_trooper){
		printf("RANGER: output_dir = %s, exectime = %.2f [sec]\n",
				pIni->ranger_output_dir, pIni->ranger_exectime);
	} else {
		printf("PATROL: freq = %.2f [sec], exectime = %.2f [sec], lynx_spy_queue_name = %s\n",
				pIni->patrol_freq, pIni->patrol_exectime, pIni->patrol_lynx_spy_q_name);
	}

	return DIAG_OK;
}

/**
 * gets the data related to local ini
 * @param pIni
 * @return DIAG_OK if everything went fine
 */
diag_t
ini_get_patrol_ini(ini_t* pIni){
	GKeyFile* 	 key_file = NULL;
	GKeyFileFlags flags = G_KEY_FILE_NONE;	// no flags, we just want to read
	GError* error = NULL;
	diag_t diag = DIAG_OK;

	// first some sanity checks
	if ((diag = diag_ptr_null_ret(pIni, "Pointer to ini file is NULL. Provide the correct pointer")) != DIAG_OK)
		return diag;

	if ((diag_ptr_null_ret(pIni->ini_name, "The name of the ini file is null. Provide the correct one")) != DIAG_OK)
		return diag;

	// get a new key_file
	key_file = g_key_file_new();

	if (!g_key_file_load_from_file (key_file, pIni->ini_name, flags, &error)){
		p_error(" Can't parse the key file: %s. Quitting ...\n", error->message);
		return DIAG_ERR;
	}

	if ( (diag = ini_get_ranger_host(key_file, pIni->ranger_hostname)) != DIAG_OK )
		return diag;

	if ( (diag = ini_get_ranger_port(key_file, &pIni->ranger_port)) != DIAG_OK)
		return diag;

	if ( (diag = ini_get_patrol_freq(key_file, &pIni->patrol_freq)) != DIAG_OK)
		return diag;

	if ( (diag = ini_get_patrol_exec_time(key_file, &pIni->patrol_exectime)) != DIAG_OK)
		return diag;

	if ( (diag = ini_get_patrol_lynx_spy_q_name(key_file, pIni->patrol_lynx_spy_q_name))){
		return diag;
	}

	// free our key_file
	g_key_file_free(key_file);

	if (DBG_LEVEL >= DBG_INFO)
			ini_print_ini(pIni, FALSE);

	return diag;
}

/**
 * gets the data from the ini file related to the aggregator
 * @param pIni we need the name of the ini file pIni->ini_name, so this
 *        should be set when invoking this function
 * @return 0
 */
diag_t
ini_get_ranger_ini(ini_t * pIni){

	GKeyFile* 	 key_file;
	GKeyFileFlags flags = G_KEY_FILE_NONE;	// no flags, we just want to read
	GError* error = NULL;
	diag_t diag = DIAG_OK;

	// first some sanity checks
	if ((diag = diag_ptr_null_ret(pIni, "Pointer to ini file is NULL. Provide the correct pointer")) != DIAG_OK)
			return diag;

	if ((diag = diag_ptr_null_ret(pIni->ini_name, "The name of the ini file is null. Provide the correct one")) != DIAG_OK)
			return diag;

	// get a new key_file
	key_file = g_key_file_new();

	if (!g_key_file_load_from_file (key_file, pIni->ini_name, flags, &error)){
		p_error(" Can't parse the key file: %s. Quitting ...\n", error->message);
		return DIAG_ERR;
	}

	if ( (diag = ini_get_ranger_exec_time(key_file, &pIni->ranger_exectime)) != DIAG_OK)
		return diag;

	// get the directory
	if ( (diag = ini_get_ranger_output_dir(key_file, &pIni->ranger_output_dir)) != DIAG_OK)
		return diag;

	// get the port
	if ( (diag = ini_get_ranger_port(key_file, &pIni->ranger_port)) != DIAG_OK)
		return diag;

	if ( (diag = ini_get_ranger_freq(key_file, &pIni->patrol_freq)) != DIAG_OK)
		return diag;
	// free our key_file
	g_key_file_free(key_file);

	// print the ini settings on the output
	if (DBG_LEVEL >= DBG_INFO)
		ini_print_ini(pIni, TRUE);

	return diag;
}


/**
 * frees resources associated with the ini_t structure
 *
 * Should be invoked when the ini data are not required any longer
 * @param pIni
 * @return 0
 */
diag_t
ini_free_ranger(ini_t* pIni){
	diag_t diag = DIAG_OK;
	g_free(pIni->ranger_output_dir);

	return diag;
}

/**
 * frees resources associated with the ini_t structure
 *
 * Should be invoked when the ini data are not required any longer
 * @param pIni
 * @return 0
 */
diag_t
ini_free_patrol(ini_t* pIni){
	diag_t diag = DIAG_OK;

	return diag;
}
// ----------------------------------------------------------
// ini file, end
// ----------------------------------------------------------

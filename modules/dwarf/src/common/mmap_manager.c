/**
 * @file mmap_manager.c
 *
 * @date Jul 8, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#include <string.h>

#include "mmap_manager.h"
#include "dwarf_types.h"
#include "debug.h"
#include "misc_utils.h"


static diag_t wrt_str(char *dest, size_t dest_size, char *str);
static diag_t check_params(struct bhb_mmap_param *p_params);

// ---------------------------------------------
// public interface
// --------------------------------------------
/**
 * This should be invoked in the init function of the scout
 *
 * it allocates the hash table which should be destroyed with
 * g_hash_table_destroy() and allocates memory for the block contexts
 * p_ctx->block_arr with malloc so should be destroyed with free()
 *
 * @param p_ctx (OUT) The values will be updated; before writing this is
 *        set to 0 values
 * @param p_params (IN) The parameters that will allow to initialize the p_ctx
 * @return DIAG_OK everything is fine; otherwise != DIAG_OK
 */
diag_t bhb_mmap_init_first(struct bhb_mmap_ctx *p_ctx, struct bhb_mmap_param
		*p_params){
	diag_t diag = DIAG_OK;

	if (( diag = diag_ptr_null_ret(p_ctx, "Context is NULL. Returning ...\n")) != DIAG_OK){
		return diag;
	}

	// check if parameters are ok
	if ((diag = check_params(p_params)) != DIAG_OK){
		return diag;
	}

	// now init the parameters

	// clean the bhb context
	memset(p_ctx, 0, sizeof(*p_ctx));


	// first fill the header context
	p_ctx->header.elem_size = p_params->hdr_entry_size_bytes;
	p_ctx->header.elem_count = p_params->block_count;
	p_ctx->header.hdr_size = p_params->hdr_header_size_bytes;
	p_ctx->header.total_size = p_ctx->header.hdr_size +
			p_ctx->header.elem_count * p_ctx->header.elem_size;

	// next fill the body context
	p_ctx->body.elem_size = p_params->bdy_block_entry_size_bytes *
			p_params->bdy_block_entry_count;
	p_ctx->body.elem_count = p_params->block_count;
	p_ctx->body.hdr_size = p_params->bdy_header_size_bytes;
	p_ctx->body.total_size = p_ctx->body.hdr_size +
			p_ctx->body.elem_count *  p_ctx->body.elem_size;

	// now reserve enough space for the body description for particular blocks
	p_ctx->block_arr = calloc( p_ctx->body.elem_count, sizeof(struct mmap_ctx));

	// init the particular blocks
	size_t i;
	for (i = 0; i < p_ctx->header.elem_count; i++){
		// we ignore the header size since there is none
		p_ctx->block_arr[i].elem_size = p_params->bdy_block_entry_size_bytes;
		p_ctx->block_arr[i].elem_count = p_params->bdy_block_entry_count;
		p_ctx->block_arr[i].total_size = p_ctx->block_arr[i].elem_size *
				p_ctx->block_arr[i].elem_count;
	}

	// for direct int you need to take g_direct_hash and g_direct_equal
	// and NOT g_int_hash and g_int_equal
	p_ctx->mmap_idx_tab = g_hash_table_new(g_direct_hash, g_direct_equal);

	return diag;
}

/**
 * This should be invoked when the name of the file associated with the
 * mapped memory is known; typically it will be in run_troopers function.
 *
 * It initializes pointers to mapped memory region.
 * The p_ctx->header.filename_mapped is allocated and should be freed with
 * free()
 *
 * @param p_ctx
 * @param p_spy_ctx
 * @return DIAG_OK if everything is fine, or != DIAG_OK otherwise
 *
 */
diag_t bhb_mmap_init_pointers(struct bhb_mmap_ctx *p_ctx, struct spy_ctx *p_spy_ctx){
	diag_t diag = DIAG_OK;

	if (( diag = diag_ptr_null_ret(p_ctx, "Context is NULL. Returning ...\n")) != DIAG_OK){
			return diag;
	}

	p_ctx->header.mmap_start = p_spy_ctx->mem_map.mmap_start;
	p_ctx->header.filename_mapped = strdup(p_spy_ctx->mem_map.filename_mapped);

	p_ctx->body.mmap_start = p_ctx->header.mmap_start + p_ctx->header.total_size;

	// where the first block starts (skip the header)
	char *p = p_ctx->body.mmap_start + p_ctx->body.hdr_size;
	size_t block_size = p_ctx->body.elem_size;

	// update the block fragments
	size_t i = 0;

	for (i=0; i < p_ctx->header.elem_count; i++){
		p_ctx->block_arr[i].mmap_start = p;
		g_assert( p_ctx->block_arr[i].total_size == block_size);
		p += block_size;
	}

	return diag;
}

/**
 * writes the header; if it is too long it is truncated; if null or empty
 * then nothing is done
 *
 * @param p_ctx
 * @param str The string to be written
 * @return DIAG_OK
 *         SCOUT_ENTRY_TRUNCATED_WARN the warning that the entry will be truncated
 */
diag_t bhb_mmap_wrt_hdr_header(struct bhb_mmap_ctx *p_ctx, char *str){
	diag_t diag=DIAG_OK;

	if( (diag = diag_ptr_null_ret(p_ctx, "The pointer to the context is NULL. Nothing done. Returning...\n")) != DIAG_OK){
		return diag;
	}

	diag = wrt_str(p_ctx->header.mmap_start, p_ctx->header.hdr_size, str);

	return diag;
}
/**
 * writes a header entry to a map, adds the new entry to the hash table
 *
 * @param p_ctx
 * @param str
 * @param id The id that will be the key in the hash table to be added
 * @return DIAG_OK everything as planned
 *         DIAG_DWARF_COUNT_OVERFLOW_ERR too many entries in the header; the limit has been exceeded
 *         DIAG_DWARF_ENTRY_EXISTS_WARN the entry exists and nothing has been written nowhere
 */
diag_t bhb_mmap_wrt_hdr_entry(struct bhb_mmap_ctx *p_ctx, char *str, int id){
	diag_t diag = DIAG_OK;

	if( (diag = diag_ptr_null_ret(p_ctx, "The pointer to the context is NULL. Nothing done. Returning...\n")) != DIAG_OK){
		return diag;
	}

	// check if the entry is already in the hash table
	if (g_hash_table_lookup(p_ctx->mmap_idx_tab, GINT_TO_POINTER(id))){
		p_warn("The key exists in the mmap idx table. Ignored writing the entry.\n");
		diag = DIAG_DWARF_ENTRY_EXISTS_WARN;
		return diag;
	}

	if (p_ctx->header.idx >= (long) p_ctx->header.elem_count){
		p_error("The number of entries in the header exceeded the permittable number of elements. Nothing done.\n");
		diag = DIAG_DWARF_COUNT_OVERFLOW_ERR;
		return diag;
	}

	if (NULL == p_ctx->header.curr_entry){
		// this is the first time
		p_ctx->header.curr_entry = p_ctx->header.mmap_start + p_ctx->header.hdr_size;
	}

	// ok, so now do the job: this is a new entry, the current index is set correctly
	// write the entry to the mapped file and update the hash table with a
	// new assignment of the block for this entry in the body

	// write the header to the mapped file
	diag = wrt_str(p_ctx->header.curr_entry, p_ctx->header.elem_size, str);
	// update the current entry index
	p_ctx->header.curr_entry += p_ctx->header.elem_size;
	p_ctx->header.idx++;

	// add the element to the hash table and assign a new block for this entry
	// in the body
	p_ctx->block_arr[p_ctx->body.idx].mmap_start =
			p_ctx->body.mmap_start + p_ctx->body.hdr_size +
			p_ctx->body.idx * p_ctx->body.elem_size;
	p_ctx->block_arr[p_ctx->body.idx].curr_entry = p_ctx->block_arr[p_ctx->body.idx].mmap_start;

	g_hash_table_insert(p_ctx->mmap_idx_tab, GINT_TO_POINTER(id),
						GINT_TO_POINTER(&p_ctx->block_arr[p_ctx->body.idx]));
	// update the index
	p_ctx->body.idx++;

	// we should have the same number of elements in the header and the body
	g_assert( p_ctx->header.idx == p_ctx->body.idx);

	return diag;
}

/**
 * writes the header; if it is too long it is truncated; if null or empty
 * then nothing is done
 *
 * @param p_ctx
 * @param str The string to be written
 * @return DIAG_OK
 *         SCOUT_ENTRY_TRUNCATED_WARN the warning that the entry will be truncated
 */
diag_t bhb_mmap_wrt_bdy_header(struct bhb_mmap_ctx *p_ctx, char *str){
	diag_t diag=DIAG_OK;

	if( (diag = diag_ptr_null_ret(p_ctx, "The pointer to the context is NULL. Nothing done. Returning...\n")) != DIAG_OK){
		return diag;
	}

	diag = wrt_str(p_ctx->body.mmap_start, p_ctx->body.hdr_size, str);

	return diag;
}
/**
 * it doesn't check if p_ctx is null, since this will be invoked very often
 *
 * @param p_ctx
 * @param str
 * @return DIAG_OK
 *         DIAG_DWARF_ENTRY_NOT_FOUND_WARN if the entry was not found; this might
 *                                    indicate an error
 */
diag_t bhb_mmap_wrt_bdy_entry(struct bhb_mmap_ctx *p_ctx, char *str, int id){
	diag_t diag = DIAG_OK;

	// the context associated with the block
	struct mmap_ctx *ctx = NULL;

	if ( (ctx = g_hash_table_lookup(p_ctx->mmap_idx_tab, GINT_TO_POINTER(id) )) == NULL ){
		p_warn("No entry with id %d has been found. It might indicate an error. No data written.\n.", id);
		diag = DIAG_DWARF_ENTRY_NOT_FOUND_WARN;
		return diag;
	}

	// first clean the memory; otherwise you might end up with
	// leftovers as g_sprintf copies only strlen(ctx->curr_entry)
	memset(ctx->curr_entry, 0, ctx->elem_size);
	diag = wrt_str(ctx->curr_entry, ctx->elem_size, str);
	// update the entry pointers for the next time
	ctx->idx++;
	if (ctx->elem_count == (size_t) ctx->idx){
		// we need to start from the beginning of our area in memory map
		// file next time
		ctx->idx = 0;
		ctx->curr_entry = ctx->mmap_start + ctx->hdr_size;
	} else {
		// just continue
		ctx->curr_entry += ctx->elem_size;
	}

	return diag;
}

/**
 * supposed to release resources allocated for the the bhb_mmap_ctx
 * @param p_ctx
 * @return
 */
diag_t bhb_mmap_dismiss(struct bhb_mmap_ctx *p_ctx){
	diag_t diag = DIAG_OK;

	g_hash_table_destroy(p_ctx->mmap_idx_tab);
	p_ctx->mmap_idx_tab = NULL;

	free(p_ctx->header.filename_mapped);
	p_ctx->header.filename_mapped = NULL;

	free(p_ctx->block_arr);
	p_ctx->block_arr = NULL;

	return diag;
}
/**
 * Returns the total size of the memory mapped region. The parameter *size
 * is always zeroed even if an error occurred
 * @param p_ctx (IN) The context to be checked
 * @param size (OUT) the actual size of the memory context
 * @return DIAG_OK everything went ok, otherwise some errors and then
 *                  you shouldn't interpret the *size
 */
diag_t bhb_mmap_get_total_size(struct bhb_mmap_ctx *p_ctx, size_t *size){
	diag_t diag = DIAG_OK;

	if (( diag = diag_ptr_null_ret(p_ctx, "Context is NULL. Returning ...\n")) != DIAG_OK){
		return diag;
	}

	*size = p_ctx->header.total_size + p_ctx->body.total_size;

	return diag;
}

/**
 * creates a string with the layout of the bhb_mmap; the string
 * should be somewhere allocated
 *
 * @param (in) p_ctx
 * @param (out) str The string with appended bhb_mmap_information
 * @return DIAG_OK everything went fine and str contains the correct string
 *         != DIAG_OK some errors
 */
diag_t
bhb_mmap_layout_2_str(struct bhb_mmap_ctx *p_ctx, GString *str){

	diag_t diag = DIAG_OK;

	// check parameters first
	if ( (diag = diag_ptr_null_ret(p_ctx, "Context NULL. Returning ... \n")) != DIAG_OK )
		return diag;
	if ( (diag = diag_ptr_null_ret(str, "String NULL. Returning ...\n")) != DIAG_OK)
		return diag;
	if ( (diag = diag_ptr_null_ret(p_ctx->block_arr, "Block array NULL. Returning ...\n")) != DIAG_OK)
		return diag;

	g_string_append_printf(str, "EL_COUNT H_H H_EL B_H B_BL_EL B_BL_EL_COUNT"
			" %ld %ld %ld %ld %ld %ld",
			p_ctx->header.elem_count,
			p_ctx->header.hdr_size,
			p_ctx->header.elem_size,
			p_ctx->body.hdr_size,
			p_ctx->block_arr[0].elem_size,
			p_ctx->block_arr[0].elem_count);

	return diag;
}
// --------------------------------------------------
// private functions
// --------------------------------------------------
/**
 * Checks if the parameters are >= 0
 * @param p_params (in) The parameters to be checked
 * @return DIAG_OK if everything went ok
 *         != DIAG_OK if something went wrong (e.g. parameters are negative)
 */
static
diag_t check_params(struct bhb_mmap_param *p_params){
	diag_t diag = DIAG_OK;

	if ( (diag = diag_ptr_null_ret(p_params, "The pointer to params shouldn't be zero. Returning error\n")) != DIAG_OK){
		return diag;
	}

	if ( (p_params->bdy_block_entry_count < 0) ||
		 (p_params->bdy_block_entry_size_bytes < 0) ||
		 (p_params->bdy_header_size_bytes < 0 ) ||
		 (p_params->hdr_entry_size_bytes < 0) ||
		 (p_params->hdr_header_size_bytes < 0) ||
		 (p_params->block_count < 0) ){
		p_error("At least one of the the parameters' value is negative. And all should be positive or zero\n.");
		diag = DIAG_ERR;
	}

	return diag;
}
/**
 * it writes a string to the destination
 *
 * @param dest where the string will be written
 * @param dest_size the maximum size of the destination buffer in bytes
 * @param str The string to be written; if the string is NULL or "" nothing
 *             will be done. If the length of the string is greater than
 *             a dest_size it will be truncated
 * @return DIAG_OK
 *         DIAG_DWARF_ENTRY_TRUNCATED_WARN warning that the entry will be truncated
 */
static
diag_t wrt_str(char *dest, size_t dest_size, char *str){
	diag_t diag = DIAG_OK;

	if ( (NULL == str) || strcmp(str, "") == 0){
		p_debug("The header string is NULL or is empty. Nothing done.\n");
		return diag;
	}

	// ok, clear the header space
	memset(dest, 0, dest_size);

	// check if the string is not too long
	if( strlen(str) + 1 > dest_size){
		p_warn("The string is too long. It will be truncated.\n");
		diag = DIAG_DWARF_ENTRY_TRUNCATED_WARN;
	}

	g_snprintf(dest, dest_size, "%s", str);

	return diag;
}

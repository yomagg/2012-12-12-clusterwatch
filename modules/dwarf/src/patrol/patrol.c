/**
 * @file dwarf_patrol.c
 *
 * @date Dec 3, 2012
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#include <glib.h>
#include <string.h>

#include "devel.h"
#include "dwarf_rt.h"
#include "dwarf_types.h"
#include "ini_utils_types.h"
#include "ini_utils.h"

extern diag_t dwarf_init_cpu_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
extern diag_t dwarf_init_mem_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
extern diag_t dwarf_init_net_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
extern diag_t dwarf_init_nvid_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
// this is only if the compilation is with NVML
#ifndef NO_NVML
extern diag_t dwarf_init_nvml_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
extern diag_t dwarf_init_lynx_spy_ctx(struct dwarf_rt_ctx * p_dwarf_rt_ctx, struct spy_ctx * p_spy_ctx);
#endif

// clusterspy functions
extern int arrange_node(char * (*f_get_host)(void), int (*f_get_port)(void), int exectime);
extern int run_watcher(char *mesg);

// I need this for the callback function
ini_t *p_ini = NULL;

/**
 * the callback function for reading the port by the node
 *
 * @return port The value read from the ini file
 *
 */
int get_port(void){
	diag_t diag = DIAG_OK;
	int port = -1;

	if ((diag = diag_ptr_warn_if_null(p_ini, "Context is NULL.\n"))!= DIAG_OK){
		return port;
	}

	if ( (diag = ini_get_integer(p_ini->ini_name,
			INI_DWARF_STR_RANGER_GRP, INI_DWARF_STR_RANGER_GRP_PORT, &port)) == DIAG_OK){
		p_ini->ranger_port = port;
		p_debug(DWARF_NAME ": got the port from ini file: %d\n", port);
	} else {
		p_error(DWARF_NAME ": errors when reading the port. Setting port to -1\n");
		port = -1;
	}

	return port;
}

/**
 * the callback function for reading the port by the node
 *
 * @return host The value read from the ini file; the duplicated string
 *         you need to free that string with free() (allocated with strdup())
 *		   NULL if the strdup doesn't work
 *		   a pointer to the new allocated string that contains a name of the host
 */
char* get_host(void){
	diag_t diag = DIAG_OK;

	if ((diag = diag_ptr_warn_if_null(p_ini, "Context is NULL.\n"))!= DIAG_OK){
		return NULL;
	}

	char *ptr = NULL;
	if ( (diag = ini_get_str(p_ini->ini_name, INI_DWARF_STR_RANGER_GRP,
			INI_DWARF_STR_RANGER_GRP_HOSTNAME, &ptr)) != DIAG_OK )
		p_error("Issues with getting the string\n");
	else {
		strcpy(p_ini->ranger_hostname, ptr);
		g_free(ptr);
		ptr=NULL;
	}

	return strdup(p_ini->ranger_hostname);
}



int
main(int argc, char **argv) {

	diag_t diag = DIAG_OK;
	struct dwarf_rt_ctx ctx;
	gchar* ini_name = NULL;

	// should be terminated with a record with NULL values
	struct dwarf_spy_ident default_spies []= {
			{INI_DWARF_STR_SPY_CPU, dwarf_init_cpu_spy_ctx},
			{INI_DWARF_STR_SPY_MEM, dwarf_init_mem_spy_ctx},
			{INI_DWARF_STR_SPY_NET, dwarf_init_net_spy_ctx},
			{INI_DWARF_STR_SPY_NVIDIA_SMI, dwarf_init_nvid_spy_ctx},
#ifndef NO_NVML
			{INI_DWARF_STR_SPY_NVML, dwarf_init_nvml_spy_ctx},
			{INI_DWARF_STR_SPY_LYNX, dwarf_init_lynx_spy_ctx},
#endif
			{NULL, NULL}
	};

	// just to be sure that we have no garbage
	memset(&ctx, 0, sizeof(ctx));
	ctx.is_initialized = FALSE;

	if ( (diag = dwarf_cmd_parse(argc, argv, &ini_name)) != DIAG_OK){
		p_error(DWARF_NAME ": command line parsing error = %d. Abandon the ship\n", diag);
		return diag;
	}

	if ((diag = dwarf_rt_init(&ctx, TRUE, ini_name)) != DIAG_OK){
		p_error(DWARF_NAME ": runtime initialization return with an error = %d. Abandon the ship\n", diag);
		return diag;
	}

	g_free(ini_name);

	p_ini = &ctx.ini_file;

	int i = 0;
	while( default_spies[i].spy_name != NULL){
		diag = dwarf_rt_deploy_spy_from_ini(&ctx, default_spies[i].spy_name, default_spies[i].init_spy_ctx_func);
		if (diag != DIAG_OK){
			p_error(DWARF_NAME ": spy deployment error=%d. Abandon the ship\n", diag);
			break;
		}
		i++;
	}

	if (DIAG_OK != diag){
			p_error("Quitting ...\n");
			return diag;
	}

	p_debug(DWARF_NAME ": Arranging the node ...\n");

	if( arrange_node(&get_host, &get_port, (int)ctx.ini_file.patrol_exectime) < 0){
		p_error(DWARF_NAME ": can't arrange the cluster. ATS\n");
		return DIAG_ERR;
	}

	p_info(DWARF_NAME ": Node ready to go ...\n");
	// now you can start watching the node
	run_watcher(DWARF_NAME ": Run watcher, run ....\n");

	return 0;
}

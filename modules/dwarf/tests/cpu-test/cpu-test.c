/**
 * @file cpu-test.c
 *
 * @date Apr 9, 2013
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

void usage(char *argv[]){
  printf("Usage: %s seconds\n", argv[0]);
  printf("\tseconds How long in seconds the workload should run\n");
}

int main(int argc, char *argv[]){
  
  if( 1 == argc || 2 != argc){
    usage(argv);
    return 0;
  }
  
  unsigned int seconds = atoi(argv[1]);
  
  unsigned int endwait = clock() + ( seconds * CLOCKS_PER_SEC );
  while( clock() < endwait ) {
  }
 
  
  return 0;
}

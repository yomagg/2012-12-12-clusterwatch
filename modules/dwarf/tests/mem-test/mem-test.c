/**
 * @file mem-test.c
 *
 * @date Apr 9, 2013
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

void usage(char *argv[]){
  printf("Usage: %s mem_in_bytes allocation_period allocations_count\n", argv[0]);
  printf("\tmem_in_bytes The size of memory allocated in bytes\n");
  printf("\tallocation_period time between subsequent allocationss\n");
  printf("\tallocations_count how many times the main loop will be passed\n");
}

int main(int argc, char *argv[]){

  
  if( 1 == argc || 4 != argc){
    usage(argv);
    return 0;
  }
  
  unsigned int mem_amount = atoi(argv[1]);
  unsigned int allocation_period = atoi (argv[2]);
  unsigned int passes_count = atoi(argv[3]);

  printf("Mem_amout=%d, allocation_period=%d, passes_count=%d\n", mem_amount, allocation_period, passes_count);
  char **ptr_arr; 

  ptr_arr = (char**) malloc(passes_count* sizeof(char*));
  memset(ptr_arr, 0,  passes_count*sizeof(char*));
  int i = 0;
  int diag = 0;
  for(i = 0; i < passes_count; i++){
    ptr_arr[i] = (char*) malloc(mem_amount*sizeof(char));
    // use the memory; otherwise you will see not increase in memory usage
    memset(ptr_arr[i],0,mem_amount);
    printf("==== Allocated %d bytes\n", mem_amount);
    if( !ptr_arr[i] ){
      printf("Malloc errors\n");
      diag = -1;
      break;
    }
    sleep(allocation_period);
  }

  for(i = 0; i < passes_count; i++){
    free(ptr_arr[i]);
  }
  free(ptr_arr);

  return 0;
}

#!/bin/bash

NUM=${NUM:-1}

function kill_all_jobs { jobs -p | xargs kill; }
trap kill_all_jobs SIGINT

MAX_CPUS=${MAX_CPUS:-$(grep processor /proc/cpuinfo | wc -l)}
NO_PKSM=${NO_PKSM:-}
OMP_NUM_THREADS=1
CPU=0
for ((i=0; i<NUM; i++))
do
    if [ $NO_PKSM ]
    then
        ((cpu_mask=(2**(MAX_CPUS)-1)))
    else
        ((cpu_mask=2**CPU))
    fi
    printf "CPU: %d (0x%x)\n" $CPU $cpu_mask
    taskset $(printf "%x" $cpu_mask) ./memset $@ &
    ((CPU=(CPU+1)%MAX_CPUS))
done

wait

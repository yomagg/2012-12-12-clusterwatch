/**
 * memset.c
 * Part of PKSM development, Georgia Tech Fall 2012
 * Kasimir Gabert, Juan Llanes
 */

/* Libraries: */

#include    <stdio.h>       /* printf, fprintf, getchar */
#include    <stdlib.h>      /* calloc, free, strtoul */
#include    <stdbool.h>     /* bool, true, false */
#include    <string.h>      /* memcpy */
#include    <assert.h>      /* assert */
#include    <inttypes.h>    /* uint64_t */
#include    <unistd.h>      /* sysconf, _SC_PAGESIZE */
#include    <time.h>        /* nanosleep, struct timespec */
#include    <sys/mman.h>    /* madvise, mmap, munmap */
#include    <sys/time.h>        /* timers */

/* Settings: */

// uncomment if you run the program on a machine that support KFS
//#define     NO_PROST

#define     MAX_MBS         UINT64_C(128*1024)

#define     FILL_BUCKETS    256
#define     PAGES_PER_ITEM  512
#define     FILL_CHAR       0x55

#define     START_SLEEP     0

/* Functions: */

int main(int argc, char **argv) {
    /** number of items to allocate */
    uint64_t num_items = 0;

    /** space is an array each with one 'item' allocated */
    char **space = NULL;

    /** if true, advise kernel to merge pages */
    bool do_madvise = false;

    /** value to increment for multiple buckets */
    uint64_t cur_bucket = 0;

    /** system pagesize */
    int pagesize = sysconf(_SC_PAGESIZE);

    /** item size based on page size */
    int itemsize = pagesize*PAGES_PER_ITEM;

    /** sleeping constructs */
    struct timespec sleep_time = { 0, 0 };

    /** counter for item loop */
    uint64_t ctr = 0;

    /** timers to time reading of space */
    struct timespec timer_start;
    struct timespec timer_end;
    double time_elapsed = 0.;

    /* parse the arguments */
    if (argc != 3 && argc != 4) {
        printf("Usage: %s (MiB to alloc) (ns sleep) [madvise]\n", argv[0]);
        return 1;
    }

    /* get the item count */
    num_items = (uint64_t)strtoll(argv[1], NULL, 10);
    assert(num_items < MAX_MBS);
    /* of course, we need to convert megabytes to items */
    num_items *= 1024*1024;
    num_items /= itemsize;
    fprintf(stderr,
        "[memset]\tnumber of items needed: %" PRIu64 "\n", num_items);

    /* get the sleep time */
    sleep_time.tv_nsec = (unsigned long)strtoll(argv[2], NULL, 10);

    /* check whether we should advise for KSM */
#ifdef NOT_PROST
// on prost this is not supported (older kernel)
    if (argc == 4) {
        do_madvise = true;
        fprintf(stderr, "[memset]\tadvising MADV_MERGEABLE\n");
    }
#endif

    /* now, allocate the space for all of the items */
    fprintf(stderr, "[memset]\tallocating total array... ");
    space = (char**)calloc(num_items, sizeof(char*));
    assert(space != NULL);
    fprintf(stderr, "done\n");

    fprintf(stderr, "[memset]\tfilling space... ");

    #pragma omp parallel for default(shared) private(ctr, cur_bucket)
    for (ctr = 0; ctr < num_items; ctr++) {
        assert(space[ctr] == NULL);
        /* Note: we cannot use a calloc here, as it may not map to a page
         * boundary */
        space[ctr] = (char*)mmap(NULL, itemsize*sizeof(char),
                PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS,
                0, 0);
        assert(space[ctr] != MAP_FAILED);

#ifdef NOT_PROST
// not supported on prost
        /* If we are advising the kernel on merging, do that now
         * Note: error isn't really important here */
        if (do_madvise) {
            int err = -1;
            err = madvise(space[ctr], itemsize*sizeof(char), MADV_MERGEABLE);
            if (err) {
                perror("[memset] error in madvise");
            }
        }
#endif

        /* Now, sleep a bit */
        nanosleep(&sleep_time, NULL);

        /* Then, finish filling up the space */
        for (uint64_t pages_ctr = 0; pages_ctr < PAGES_PER_ITEM;
                    pages_ctr++) {
            for (uint64_t char_ctr = 0; char_ctr < pagesize; char_ctr++) {
                space[ctr][pages_ctr*pagesize+char_ctr] = FILL_CHAR;
            }
            memcpy(&space[ctr][pages_ctr*pagesize], &cur_bucket,
                        sizeof(cur_bucket));
            cur_bucket = (cur_bucket+1) % FILL_BUCKETS;
        }
    }
    fprintf(stderr, "done\n");

    fprintf(stderr, "[memset]\tsleeping before reading space... ");
    sleep(START_SLEEP);
    fprintf(stderr, "done\n");

    /* Reading and timing to ensure correctness and find latency
     * of using dedup across numa nodes vs. not deduping */
    fprintf(stderr, "[memset]\treading space... ");

    /* start timing */
#ifdef NOT_PROST 
    clock_gettime(CLOCK_MONOTONIC_RAW, &timer_start);
#else 
    clock_gettime(CLOCK_MONOTONIC, &timer_start);
#endif

    cur_bucket = 0;
    #pragma omp parallel for default(shared) private(ctr, cur_bucket)
    for (ctr = 0; ctr < num_items; ctr++) {
        /* Read pages content for each item */
        for (uint64_t pages_ctr = 0; pages_ctr < PAGES_PER_ITEM;
                    pages_ctr++) {
            /* Check the bucket number */
            uint64_t temp_bucket = 0;
            memcpy(&temp_bucket, &space[ctr][pages_ctr*pagesize],
                        sizeof(temp_bucket));
            if (temp_bucket != cur_bucket){
                fprintf(stderr, "[memset]\tin item: %lu, page: %lu,"
                                " found: %lu, original: %lu\n",
                                ctr,
                                pages_ctr,
                                temp_bucket,
                                cur_bucket);
            }


            for (uint64_t char_ctr = sizeof(cur_bucket);
                        char_ctr < pagesize; char_ctr++) {
                if(space[ctr][pages_ctr*pagesize+char_ctr] != FILL_CHAR){
                    fprintf(stderr, "[memset]\tin item: %lu, page: %lu,"
                            " char:%lu, found: %x, original: %x\n",
                            ctr,
                            pages_ctr,
                            char_ctr,
                            space[ctr][pages_ctr*pagesize+char_ctr],
                            FILL_CHAR);
                }
            }
            cur_bucket = (cur_bucket+1) % FILL_BUCKETS;
        }
    }


    /* stop timing */
#ifdef NOT_PROST
    clock_gettime(CLOCK_MONOTONIC_RAW, &timer_end);
#else
    clock_gettime(CLOCK_MONOTONIC, &timer_end);
#endif

    time_elapsed = (double) timer_end.tv_sec +
                    1.0e-9 * (double) timer_end.tv_nsec -
                    (double) timer_start.tv_sec -
                    1.0e-9 * (double) timer_start.tv_nsec;


    fprintf(stderr, "done\n");

    fprintf(stderr, "[memset]\ttime_elapsed: %lf seconds\n", time_elapsed);

    fprintf(stderr, "[memset]\tpress enter to exit...");
    getchar();

    fprintf(stderr, "[memset]\tcleaning up... ");
    #pragma omp parallel for default(shared) private(ctr)
    for (ctr = 0; ctr < num_items; ctr++) {
        int err = -1;
        assert(space[ctr] != NULL);
        err = munmap(space[ctr], itemsize*sizeof(char));
        assert(!err);
    }
    free(space);
    fprintf(stderr, "done\n");

    return 0;
}

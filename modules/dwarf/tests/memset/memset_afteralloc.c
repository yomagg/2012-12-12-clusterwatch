/**
 * memset.c
 * Part of PKSM development, Georgia Tech Fall 2012
 * Kasimir Gabert, Juan Llanes
 */

/* Libraries: */

#include    <stdio.h>       /* printf, fprintf, getchar */
#include    <stdlib.h>      /* calloc, free, strtoul */
#include    <stdbool.h>     /* bool, true, false */
#include    <string.h>      /* memcpy */
#include    <assert.h>      /* assert */
#include    <inttypes.h>    /* uint64_t */
#include    <unistd.h>      /* sysconf, _SC_PAGESIZE */
#include    <time.h>        /* nanosleep, struct timespec */
#include    <sys/mman.h>    /* madvise, mmap, munmap */
#include    <sys/time.h>        /* timers */

/* Settings: */

// uncomment if you run the program on a machine that support KFS
//#define     NO_PROST


#define     MAX_MBS         UINT64_C(128*1024)

#define     FILL_BUCKETS    256
#define     PAGES_PER_ITEM  512
#define     FILL_CHAR       0x55

#define     START_SLEEP     5

/* Functions: */

int main(int argc, char **argv) {
    /** number of items to allocate */
    uint64_t num_items = 0;

    /** space is an array each with one 'item' allocated */
    char **space = NULL;

    /** if true, advise kernel to merge pages */
    bool do_madvise = false;

    /** value to increment for multiple buckets */
    uint64_t cur_bucket = 0;

    /** system pagesize */
    int pagesize = sysconf(_SC_PAGESIZE);

    /** item size based on page size */
    int itemsize = pagesize*PAGES_PER_ITEM;

    /** sleeping constructs */
    struct timespec sleep_time = { 0, 0 };

    /** counter for item loop */
    uint64_t ctr = 0;

    /* parse the arguments */
    if (argc != 3 && argc != 4) {
        printf("Usage: %s (MiB to alloc) (ns sleep) [madvise]\n", argv[0]);
        return 1;
    }

    /* get the item count */
    num_items = (uint64_t)strtoll(argv[1], NULL, 10);
    assert(num_items < MAX_MBS);
    /* of course, we need to convert megabytes to items */
    num_items *= 1024*1024;
    num_items /= itemsize;
    fprintf(stderr,
        "[memset]\tnumber of items needed: %" PRIu64 "\n", num_items);

    /* get the sleep time */
    sleep_time.tv_nsec = (unsigned long)strtoll(argv[2], NULL, 10);

    /* check whether we should advise for KSM */
    if (argc == 4) {
        do_madvise = true;
        fprintf(stderr, "[memset]\tadvising MADV_MERGEABLE\n");
    }

    /* now, allocate the space for all of the items */
    fprintf(stderr, "[memset]\tallocating total array... ");
    space = (char**)calloc(num_items, sizeof(char*));
    assert(space != NULL);
    fprintf(stderr, "done\n");

    fprintf(stderr, "[memset]\tfilling space... ");

    #pragma omp parallel for default(shared) private(ctr, cur_bucket)
    for (ctr = 0; ctr < num_items; ctr++) {
        assert(space[ctr] == NULL);
        /* Note: we cannot use a calloc here, as it may not map to a page
         * boundary */
        space[ctr] = (char*)mmap(NULL, itemsize*sizeof(char),
                PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS,
                0, 0);
        assert(space[ctr] != MAP_FAILED);

        /* Now, sleep a bit */
        nanosleep(&sleep_time, NULL);

        /* Then, finish filling up the space */
        for (uint64_t pages_ctr = 0; pages_ctr < PAGES_PER_ITEM;
                    pages_ctr++) {
            for (uint64_t char_ctr = 0; char_ctr < pagesize; char_ctr++) {
                space[ctr][pages_ctr*pagesize+char_ctr] = FILL_CHAR;
            }
            memcpy(&space[ctr][pages_ctr*pagesize], &cur_bucket,
                        sizeof(cur_bucket));
            cur_bucket = (cur_bucket+1) % FILL_BUCKETS;
        }
    }
    fprintf(stderr, "done\n");

    fprintf(stderr, "[memset]\tmarking advise...");
    #pragma omp parallel for default(shared) private(ctr, cur_bucket)
    for (ctr = 0; ctr < num_items; ctr++) {

        /* If we are advising the kernel on merging, do that now
         * Note: error isn't really important here */
#ifdef NO_PROST
        if (do_madvise) {
            int err = -1;
            err = madvise(space[ctr], itemsize*sizeof(char), MADV_MERGEABLE);
            if (err) {
                perror("[memset] error in madvise");
            }
        }
#endif

    }
    fprintf(stderr, "done\n");

    fprintf(stderr, "[memset]\twaiting for ksm...\n");
    while (1) {
        int64_t pages_sharing = 0;
        FILE *pages_sharing_fh = fopen("/sys/kernel/mm/ksm/pages_sharing", "r");

        fscanf(pages_sharing_fh, "%ld", &pages_sharing);
        fclose(pages_sharing_fh);

        fprintf(stderr, "[memset]\tgot: %ld\n", pages_sharing);
        if (pages_sharing > 510000) break;

        sleep(2);
    }
    sleep(5);
    fprintf(stderr, "[memset]\tdone\n");

    fprintf(stderr, "[memset]\tpress enter to exit...");
    getchar();

    fprintf(stderr, "[memset]\tcleaning up... ");
    #pragma omp parallel for default(shared) private(ctr)
    for (ctr = 0; ctr < num_items; ctr++) {
        int err = -1;
        assert(space[ctr] != NULL);
        err = munmap(space[ctr], itemsize*sizeof(char));
        assert(!err);
    }
    free(space);
    fprintf(stderr, "done\n");

    return 0;
}

/**
 * @file unlinkq.c
 *
 * @date Oct 9, 2013
 * @author Magdalena Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 */

#include <iostream>

#include <stdio.h>

#include <fcntl.h>           	// For O_* constants
#include <sys/stat.h>        	// For mode constants
#include <mqueue.h>
#include <string.h>   			// for strerror
#include <errno.h>

using namespace std;

/**
 * @param prog_name The name of the program
 *
 * @return always 0
 */
int usage(const char *prog_name){

	cout << "Usage: " << prog_name << " queue_name\n";
	cout << "Unlinks the queue.\n";
	cout << "Return:\n" ;
	cout << "  0 - SUCCESS\n";
	cout << " -1 - ERROR\n";
	cout << "E.g.: " << prog_name << " /lynx\n";

	return 0;
}

int main(int argc, char ** argv){
	int my_q = -1;

	if ( 2 != argc ){
		return usage(argv[0]);
	}

	my_q = mq_unlink(argv[1]);
	if (-1 != my_q){
		cerr << "mq_unlink: " << argv[1] << " : SUCCESS\n";
	} else {
		perror("mq_unlink()" );
		return -1;
	}

	return 0;
}

"""
	@file misc_utils.py
	@date Sep 17, 2012
	@author Magdalena Slawinska a.k.a. Magic Magg magg dot gatech @ gmail dot com
"""

import os
import time
import sys
import datetime
# on keeneland there is no argparse module so I need to use optparse
import optparse
import shutil
import logging

def exit_if_no_path(mypath, message):
    """
       exits if the specified path doesn't exist
       @param mypath: the location to the file to be checked
       @param message: the message to be displayed to the user  
    """
    if not os.path.exists(mypath):
        print(message, 'The specified path does not exist. Quitting ...', mypath)
        sys.exit()

def run_scout(file, nodes, options):
    file.write('\n\n# ------------ Enabling scout\n')
    file.write('echo "SCOUT_TROOPER=`hostname`"\n')
    file.write('echo -n "SCOUT_TROOPER_MPI="\n')
    file.write('mpiexec -np 1 hostname\n')
    file.write('SCOUT_BIN=')
    file.write(options.scout_bin)
    file.write('\n\n')
   
    # determine numa domain if specified - use the opposite
    numa_dom = '/usr/bin/numactl --cpunodebind='
		
    file.write('CMSelfFormats=1 mpiexec -np 1 ')
    scout_numadomain = None
    if options.scout_numadom != -1:
    	scout_numadomain = str(options.scout_numadom)
    	file.write(numa_dom + scout_numadomain + ' ')
    file.write('$SCOUT_BIN/ag -f kid_scout.ini &\n\n')
    file.write('CMSelfFormats=1 mpiexec -np ' + str(nodes) + ' -npernode 1 ')
    if options.scout_numadom != -1:
        file.write(numa_dom + str(1) + ' ')
    file.write('$SCOUT_BIN/scout -f kid_scout.ini &\n')
    
    file.write('sleep 20\n\n')
    file.write('mpiexec -np 1 ')
    if options.scout_numadom != -1:
        file.write(numa_dom + scout_numadomain + ' ')
    file.write('python $SCOUT_BIN/com_reader.py --cpu --mem --net --nvid --ini kid_scout.ini --logging-level info &\n')
    file.write('sleep 10\n')
    file.write('# ------------- Scout enabled ---------\n\n')
    
def kill_scout(file, nodes):
    file.write('# ------------- Slaughter Scout\n')
    file.write('sleep 30\n\n')
    file.write('mpiexec -np 1 killall python\n')
    file.write('mpiexec -np ' + str(nodes) + ' -npernode 1 killall scout\n')
    file.write('mpiexec -np 1 killall ag\n')
        

def mvapich2_modules(file, options):
    file.write('module use ~smagg/opt/modulefiles\n')
    file.write('module unload PE-intel\n')
    file.write('module load PE-gnu\n')
    file.write('module load fftw\n')
    file.write('module unload openmpi\n')
    file.write('module load mvapich2/1.8\n')
    file.write('module load mpiexec\n')
    file.write('module load python/2.7\n')
    file.write('module load mag-scout/stable\n')
    if 'cpu' == options.lammps1_type and 'cpu' == options.lammps2_type:
    	pass
    else:
    	# this is gpu or cuda, right now only support for cuda32
    	file.write('module unload cuda/4.1\n')
    	file.write('module load mag-cuda-3.2\n')
            
    file.write('module list\n\n')
    
    file.write('which mpiexec\n\n')
    
def run_lammps(file, pbs_file_name, options, nodes, lammps_dict):
    """
	  # of MPI processes is nodes*lammps['ppn']
	"""
    file.write('ls -l ' + lammps_dict['path'] + '\n\n')
    
    
    if options.microsec:
        file.write('/nics/d/home/smagg/proj/wksp-ecl-cpp/2012-04-30-lore-playground/monitor/microtime/microtime ')    
    
    file.write('mpiexec -np ')
    file.write(str(nodes * lammps_dict['ppn']) + ' -npernode ' + str(lammps_dict['ppn']) + ' ')
   
    if lammps_dict['numadom'] != -1:
        file.write('/usr/bin/numactl --cpunodebind=' + str(lammps_dict['numadom']) + ' ')
   
    file.write(lammps_dict['path'])
    
    file.write(' -l log.lammps.' + lammps_dict['in_file'] + '.$MY_JOBID') 
    
    if 'cuda32' == lammps_dict['type']:
        file.write(' -sf cuda ' )
    if 'gpu' == lammps_dict['type']:
        file.write(' -sf gpu ')

    file.write(' < ')
    file.write(lammps_dict['in_file'])    
    file.write(' > out_' + pbs_file_name + '.' + lammps_dict['in_file'] + '.$MY_JOBID.log')

def run_lammps_fill(file, pbs_file_name, options, nodes, lammps_dict, fill):
    """
	  you can steer how much the last node is filled
	"""
    file.write('ls -l ' + lammps_dict['path'] + '\n\n')
    
    
    if options.microsec:
        file.write('/nics/d/home/smagg/proj/wksp-ecl-cpp/2012-04-30-lore-playground/monitor/microtime/microtime ')    
    
    file.write('mpiexec -np ')
    file.write(str((nodes-1)*lammps_dict['ppn'] + fill) + ' -npernode ' + str(lammps_dict['ppn']) + ' ')
   
    if lammps_dict['numadom'] != -1:
        file.write('/usr/bin/numactl --cpunodebind=' + str(lammps_dict['numadom']) + ' ')
   
    file.write(lammps_dict['path'])
    
    file.write(' -l log.lammps.' + lammps_dict['in_file'] + '.$MY_JOBID') 
    
    if 'cuda32' == lammps_dict['type']:
        file.write(' -sf cuda ' )
    if 'gpu' == lammps_dict['type']:
        file.write(' -sf gpu ')

    file.write(' < ')
    file.write(lammps_dict['in_file'])    
    file.write(' > out_' + pbs_file_name + '.' + lammps_dict['in_file'] + '.$MY_JOBID.log')


    file.write(lammps_dict['in_file'])
    
    file.write(' > out_' + pbs_file_name + '.' + lammps_dict['in_file'] + '.$MY_JOBID.log')

def run_lammps_fill(file, pbs_file_name, options, nodes, lammps_dict, fill):
    """
	  you can steer how much the last node is filled
	"""
    file.write('ls -l ' + lammps_dict['path'] + '\n\n')
    
    
    if options.microsec:
        file.write('/nics/d/home/smagg/proj/wksp-ecl-cpp/2012-04-30-lore-playground/monitor/microtime/microtime ')    
    
    file.write('mpiexec -np ')
    file.write(str((nodes-1)*lammps_dict['ppn'] + fill) + ' -npernode ' + str(lammps_dict['ppn']) + ' ')
   
    if lammps_dict['numadom'] != -1:
        file.write('/usr/bin/numactl --cpunodebind=' + str(lammps_dict['numadom']) + ' ')
   
    file.write(lammps_dict['path'])
    
    file.write(' -l log.lammps.' + lammps_dict['in_file'] + '.$MY_JOBID') 
    
    if 'cuda32' == lammps_dict['type']:
        file.write(' -sf cuda ' )
    if 'gpu' == lammps_dict['type']:
        file.write(' -sf gpu ')

    file.write(' < ')
    file.write(lammps_dict['in_file'])    
    file.write(' > out_' + pbs_file_name + '.' + lammps_dict['in_file'] + '.$MY_JOBID.log')


def create_scout_ini(path):
    file = open(path + os.path.sep + 'kid_scout.ini', 'w')
    file.write('[scouts]\n')
    file.write('simple_scout=false\n')
    file.write('simple2_scout=false\n')
    file.write('periodic_scout=false\n')
    file.write('shm_scout=false\n')
    file.write('cpu_scout=true\n')
    file.write('net_scout=true\n')
    file.write('nvidia_smi_scout=true\n')
    file.write('mem_scout=true\n\n')

    file.write('[trooper]\n')
    file.write('hostname=kid047.nics.utk.edu\n')
    file.write('port=5353\n')
    file.write('output_dir=' + os.path.abspath(path) + os.path.sep + '\n')
    file.write('exectime=30.0\n\n')

    file.write('[scout]\n')
    file.write('exectime=25.0\n')
    file.write('frequency=1.0\n')
    file.close()

def gen_pbs_preamble(options, nodes):
	""" generate the preamble for the pbs file 
	@return file The file handler to the generated pbs file
	"""
	file_name = 'lammps_'
	# zero pad
	str_nodes=str('%(num)03d' % {'num' : nodes})
	file_name += 'nodes_' + str_nodes + '_ppn_' + str(options.pbs_ppn) + '.pbs'
	
	now = datetime.datetime.now()
	
	file = open(os.path.normpath(options.output_dir) + os.path.sep +  file_name, 'w')
	file.write('#!/bin/sh\n')
	file.write('# File generated by pbs_gen.py on ' + now.strftime("%Y-%m-%d %H:%M"))
	file.write('\n#PBS -N ' + file_name + '\n')
	file.write('#PBS -j oe\n')
	file.write('#PBS -q batch\n')
	file.write('#PBS -A UT-TENN0037\n')
	file.write('#PBS -l walltime=' + options.pbs_walltime + '\n')
	file.write('#PBS -l nodes=' + str(nodes))
	file.write(':ppn='+str(options.pbs_ppn))
	file.write(':gpus=3:shared\n\n')
	
	file.write('date\n')
	file.write('cd $PBS_O_WORKDIR\n\n')
	
	file.write('echo "nodefile="\n')
	file.write('cat $PBS_NODEFILE\n')
	file.write('echo "=end nodefile"\n\n')
	
	file.write('# extract the number of the job\n')
	file.write("MY_JOBID=`echo $PBS_JOBID | cut -d'.' -f1`\n\n")
	
	return (file, file_name)

def gen_pbs_epilog(file):
	file.write('\n\n# -------------- epilog ----------\n')
	file.write('\ndate\n\n')
	file.write('# eof\n')
	file.close()
	
def copy_files(options, nodes, pbs_file, input_file, fill=3):
	"""
	   copies the input file to the directory
	   @param root_path:  the root dir where to move the file
	   @param pbs_file: the name of the pbs file
	   @param nodes: The number of nodes
	   @param input_file: the input file to be moved
	"""
	rootpath = os.path.normpath(options.output_dir) + os.path.sep
	# zero pad
	str_nodes = str('%(num)03d' % {'num' : nodes})
	mypath = rootpath + 'nodes-' + str_nodes + '-' + str(fill)
	if not os.path.exists(mypath):
		os.mkdir(mypath)
	else:
		logging.debug("The directory exists. Ignoring ...")
	if os.path.exists(rootpath + pbs_file):
		shutil.move(rootpath + pbs_file,mypath)
	else:
		logging.debug("No PBS file found. Likely moved before ... Ignoring ...")
	shutil.copy(input_file, mypath)
	
	return mypath

'''
def gen_pbs_script(options, nodes):
    """
    generate a single pbs script
    @options the options got from the command line
    @nodes  on how many nodes this script needs to run
    """
    short_input_file_name = os.path.basename(options.input_file)
    short_input_file_name2 = os.path.basename(options.input_file2) if options.input_file2 else None
    
    file_name = 'lammps_' #+ short_input_file_name 
    # zero pad 
    str_nodes = str('%(num)03d' % {'num' : nodes})
    file_name +='nodes_' + str_nodes + '_ppn_' + str(options.ppn) + '.pbs'
    
    now = datetime.datetime.now()
            
    file = open(os.path.normpath(options.output_dir) + os.path.sep +  file_name, 'w')
    file.write('#!/bin/sh\n')
    file.write('# File generated by pbs_gen.py on ' + now.strftime("%Y-%m-%d %H:%M"))    
    file.write('\n#PBS -N ' + file_name + '\n')
    file.write('#PBS -j oe\n')
    file.write('#PBS -q batch\n')
    file.write('#PBS -A UT-TENN0037\n')
    file.write('#PBS -l walltime=' + options.walltime + '\n')
    file.write('#PBS -l nodes=' + str(nodes))
    file.write(':ppn=12')
    file.write(':gpus=3:shared\n\n')

    file.write('date\n')
    file.write('cd $PBS_O_WORKDIR\n\n')
    
    file.write('echo "nodefile="\n')
    file.write('cat $PBS_NODEFILE\n')
    file.write('echo "=end nodefile"\n\n')

    file.write('# extract the number of the job\n')
    file.write("MY_JOBID=`echo $PBS_JOBID | cut -d'.' -f1`\n\n")


    
    mvapich2_modules(file, options)
    
    file.write('which mpiexec\n\n')
    
    run_scout(file, nodes, options)
        
    run_lammps(file, nodes, options, file_name, short_input_file_name)
    if short_input_file_name2:
        # run the previous app in the background
        file.write(' &\n\n')
        run_lammps(file, nodes, options, file_name, short_input_file_name2)
    
    file.write('\n\n') 
    
    kill_scout(file, nodes)

    file.write('\ndate\n\n')
    file.write('# eof\n')

    file.close()
    
    rootpath = os.path.normpath(options.output_dir) + os.path.sep
    mypath = rootpath + 'nodes-' + str_nodes 
    os.mkdir(mypath)
    shutil.move(rootpath + file_name,mypath)
    shutil.copy(options.input_file, mypath)
    if options.input_file2:
        shutil.copy(options.input_file2, mypath)
    # create the ini file
    create_scout_ini(mypath)
'''
# copied from /c/synchrobox/proj/w-ecl/2012-08-15-lammps-utils/plotting/runme.sh
# and modified
EXPR=$1
RANGER=$2
EXPR_PATH="d:/synchrobox/proj/w-expr/cw-correctness-tmp/gpu/"$EXPR"/"$RANGER"-mmap"

if [ $# -eq 0 ]; then
	echo "Usage: `basename $0` part-of-$EXPR_PATH host"
	echo "Description: "
	echo "Ex. `basename $0` expr01/nodes-001 kid060.nics.utk.edu"
	exit -1
fi


PYT_PATH="/d/opt/Python27/python.exe"
MSUF="2013-06-03-0000.log.db"
NET=$EXPR_PATH-net_spy-$MSUF
NVML=$EXPR_PATH-nvml_spy-$MSUF
CPU=$EXPR_PATH-cpu_spy-$MSUF
MEM=$EXPR_PATH-mem_spy-$MSUF
SMI=$EXPR_PATH-nvidia_smi_spy-$MSUF

#$PYT_PATH plot_db.py --net_db $NET  --nvml_db $NVML --cpu_db $CPU --mem_db $MEM --ranger $RANGER
#$PYT_PATH plot_db.py --net_db $NET  --nvml_db $NVML --cpu_db $CPU  --ranger $RANGER
$PYT_PATH plot_db.py --nvml_db $NVML --nvi_db $SMI --ranger $RANGER
#$PYT_PATH plot_db.py --nvi_db $SMI --ranger $RANGER
#$PYT_PATH plot_db.py --cpu_db $CPU --mem_db $MEM --ranger $RANGER

#$PYT_PATH plot_db.py --nvi_db $SMI --ranger $RANGER
#$PYT_PATH plot_db.py --nvml_db $NVML --ranger $RANGER

#$PYT_PATH plot_db.py --net_db $NET --ranger prost.cc.gt.atl.ga.us

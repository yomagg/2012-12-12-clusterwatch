# copied from /c/synchrobox/proj/w-ecl/2012-08-15-lammps-utils/plotting/runme.sh
# and modified
EXPR=$1
EXPR_PATH="d:/synchrobox/proj/w-expr/2013-04-01-shoc-s3d-kids/"$1"/kid119.nics.utk.edu-mmap"

if [ $# -eq 0 ]; then
	echo "Usage: `basename $0` part-of-$EXPR_PATH "
	echo "Description: "
	echo "Ex. `basename $0` expr-s1-n100-np3"
	exit -1
fi


PYT_PATH="/d/opt/Python27/python.exe"
MSUF="2013-04-01-0000.log.db"
NET=$EXPR_PATH-net_spy-$MSUF
NVML=$EXPR_PATH-nvml_spy-$MSUF
CPU=$EXPR_PATH-cpu_spy-$MSUF
MEM=$EXPR_PATH-mem_spy-$MSUF

#$PYT_PATH /c/synchrobox/proj/w-ecl/2012-08-15-lammps-utils/plotting/plot_db.py --net_db $NET  --nvml_db $NVML --cpu_db $CPU --mem_db $MEM --ranger kid119.nics.utk.edu
$PYT_PATH /c/synchrobox/proj/w-ecl/2012-08-15-lammps-utils/plotting/plot_db.py --net_db $NET  --nvml_db $NVML --cpu_db $CPU --ranger kid119.nics.utk.edu

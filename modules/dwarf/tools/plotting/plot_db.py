"""
Created on Jan 9, 2013
@author: Magda Slawinska a.k.a. Magic Magg magg dot gatech @ gmail dot com

run: e.g.  /c/opt/python27/python.exe plot.py  --cpu_db '/d/tmp/tmp/cpu_spy.db'

This file strongly depends on the format of the sqlite3_reader from
the dwarf module of the clusterwatch

If you want to change the logging level: find the instruction in main and
change the level to the desired one
     logging.basicConfig(level=logging.DEBUG)
"""

import inspect

import os
import glob
import time
import sys
import datetime
import argparse
import logging

#sys.path.append('/nics/d/home/smagg/proj/wksp-ecl-cpp/2012-08-15-lammps-utils/gen-pbs/')
#sys.path.append('../gen-pbs/')
#sys.path.append('common')
from misc_utils import *

import sqlite3
import numpy as np
import matplotlib.pyplot as plt

# color names: http://www.w3schools.com/html/html_colornames.asp
PLOT_COLORS = ['DarkGreen', 'DarkSeaGreen', 'DarkOrange', 
               'DarkSalmon', 'Ivory', 'LightSkyBlue', 
               'Orange', 'OrangeRed', 'Yellow',
               'Brown', 'BurlyWood', 'DarkTurquoise', 
               'y', 'y', 'b', 'b', 'b', 'y', 'y']

FILES_NAME = {'cpu_total_util' : 'cpu_total_util.pdf',
              'cpu_core_stacked_util' : 'cpu_core_stacked_util.pdf',
              'cpu_core_sep_util' : 'cpu_core_sep_util.pdf',
              'mem_util' : 'mem_util.pdf',
              'net_load' : 'net_load.pdf',
              'gpu_util' : 'gpu_util.pdf'}

# the value that indicates the border of the new node reading
NODE_SEPARATOR=0

def get_spy_ids_sorted(cursor, sql_query):
    """
       gets the list of spy ids
       @param cursor: The cursor to the opened database
       @param sql_query: The SQL query that will ask about the distinct
       spy ids in the table
       @return sorted spy ids tuple  
    """
    cursor.execute(sql_query)
    tmp = cursor.fetchall()
    # a trick to flat the tuple
    spy_ids = sum(tmp, ())
    logging.debug('There are ' + str(len(spy_ids)) + ' spy_ids')
    logging.debug('Listing spies ids:')
    logging.debug(spy_ids)
    
    return sorted(spy_ids)

def get_spy_ids_str(cursor, sql_query, spy_ids, ranger): 
    """
    gets the list of spy ids converted to string with indication of the
    ranger if ranger is not none
    @param cursor: a cursor to the opened database      
    @param sql_query: The query to ask about the id of the ranger given
                     the host name in ranger parameter
    @param spy_ids: a list of spy ids
    @param ranger: The hostname of the ranger
    
    @return: spy_ids_str a list of spy ids converted to strings and with indication
                        to a ranger if the ranger is present
    """   
    spy_ids_str = []
    if ranger:
        cursor.execute(sql_query, (ranger,))
        tmp = cursor.fetchone()        
        # the trick to make it flat
        ranger_id = tmp[0]
        logging.debug('Ranger id: ')
         
        logging.debug(ranger_id)
        spy_ids_str = []
        for id in spy_ids:
            if (ranger_id == id):
                spy_ids_str.append(str(id) + '\nranger')
            else:
                spy_ids_str.append(str(id))
    else:
        spy_ids_str = [ str(id) for id in spy_ids ]            
    
    return spy_ids_str

def get_ticks_in_the_middle(ticks_index):
    """
       Counts ticks in the midle of the ticks index. Ticks_index provides the
       limits of the nodes. The functions returns the middle indexes of the ticks_index
       
       @param ticks_index: where are the limits of the particular nodes
               it should start with the 0 index
       @return: a list of ticks in the middle e.g. if ticks_index = [0, 2, 4],
       it should return something like [1, 3] 
    """
    ticks_in_the_middle = []
    for i in range(len(ticks_index)-1):
        # we logically want to do the below
        # delta = ticks_index[i] + (ticks_index[i+1] - ticks_index[i])/2 =
        # = (ticks_index[i] + ticks_index[i+1])/2
        delta = (ticks_index[i] + ticks_index[i+1])/2
        ticks_in_the_middle.append(delta)
        
    return ticks_in_the_middle
    
def add_to_plt_node_vline_separators(ticks_index):
    """
       Adds to the current plot the vertical lines that separate
       the borders of the node
       @param ticks_index: shows where the border should go 
       @return: True 
        
    """
    for terminator in ticks_index:
        # skip the first one
        if 0 == terminator:
            continue
        # plot the vertical line that limits the history of the node        
        plt.axvline(x=terminator, linestyle='--', color='b')
    
    return True

def save_fig(fig_dir, fig_name):
    """
       saves the plot to the file
       @param fig_dir: The path to the directory where the file will be saved
       @param fig_name: The name of the picture to be saved
       @return: True if everything went fine 
    """
    if fig_dir:
        f_dir = fig_dir + os.sep + fig_name
        F = plt.gcf()
        DPI = F.get_dpi()
        logging.debug('DPI: ' + str(DPI))
        def_size = F.get_size_inches()
        logging.debug('Default size in inches: ' + str(DPI*def_size[0]) + 'x' + str(DPI*def_size[1]))
        F.set_size_inches(def_size[0] * 2, def_size[1] * 2)
        plt.savefig(f_dir, dpi=300)    
        logging.debug('Fig saved to fig_dir= ' + f_dir)
        # reset the size
        F.set_size_inches(def_size)
        
    return True
# --------------------------------------------------
# plotting cpu
# --------------------------------------------------
def create_cpu_total_plot(cursor, spy_ids, ranger, fig_dir, plot_no):
    """
    @param cursor: The cursor to the opened database of the cpu measurements
    @param spy_ids: The tuple of spy ids retrieved from the cpu database
    @param ranger: The name of the ranger
    @param fig_dir: Where to save the figure 
    @param plot_no: which plot number it is
    @return: current plot_no
    
    Please read comment about the timestamps inside this function
    
    I will be cheating here and I will not preserve the 'distance' 
    between two timestamps but I will assign the subsequent numbers
    to the sequence of timestamps, I will treat them with the same 
    'distance' 0,1,2,3,4,5, ... with disrespect to the actual 'time distance'
    registered in the database e.g. timestamps 123455 134455 144444444 will
    get 0, 1, 2 indexes and not (134455-123455), (134455-123455), (14444444-123455) 
    this is on purpose to simplify plotting 
    """
    
    plt.figure(plot_no)
    # key: the spy_id, value: the list of cpu total utilization
    cpu_util = {}
    for id in spy_ids:        
        # retrieve the cpu utilization
        cursor.execute('SELECT cpu_total_util FROM cpu_mon_tab '
                       'WHERE spy_id=? ORDER BY ts', (id,))
        tmp = cursor.fetchall()
        # the trick to make it flat
        index = sum(tmp, ())
        cpu_util[id] = list(index)
    
    # the idea is to have the joint global index to plot one diagram
    # so I need to concatenate all timestamps and all corresponding results
    # in two tuples, one will be used to feed x coordinates, the utilization
    # will be the y value
    
    # create one x value tuple (see comments about the timestamps)
    # to have the same order of keys
    cpu_util_keys = sorted(cpu_util.keys())
    cpu_util_list = []
    ticks_index = [0]
    for id in cpu_util_keys:
        cpu_util_list = cpu_util_list + cpu_util[id]
        # indicate this is the end for this node
        cpu_util_list.append(NODE_SEPARATOR)
        # where the boundary of the next node starts (the index value
        # designating the tick separating nodes
        ticks_index.append(len(cpu_util_list)-1)
        
    
    index = range(len(cpu_util_list))
    #logging.debug('The index count: ' + str(index))
    
    # now generate the value, i.e. the cpu utilization
    plt.bar(index, cpu_util_list, color='r')
    
    # add node vline separators
    add_to_plt_node_vline_separators(ticks_index)
    
    # add descriptions
    plt.ylabel('CPU total utilization [%]')
    plt.xlabel('Node Id')
    plt.title('CPU utilization per node')
    ticks_in_the_middle = get_ticks_in_the_middle(ticks_index) 

    # check who is the ranger in town    
    cpu_util_keys_str = get_spy_ids_str( cursor, 
        'SELECT spy_id FROM cpu_cap_tab WHERE hostname=?', 
        cpu_util_keys, ranger)        
    
    plt.xticks(ticks_in_the_middle, cpu_util_keys_str)
    logging.debug(ticks_in_the_middle)
    logging.debug(ticks_index)

    # save figure if fig_dir says that  
    save_fig(fig_dir, FILES_NAME['cpu_total_util'])  
        
    return plot_no + 1
        
def create_cpu_core_plot(cursor, spy_ids, ranger, fig_dir, plot_no):
    """
    @param cursor: The cursor to the opened database of the cpu measurements
    @param spy_ids: The tuple of spy ids retrieved from the cpu database
    @param ranger: The name of the ranger
    @param plot_no: the current plot number
    @param fig_dir: the directory where figures will be saved if this is so
    @return: True if everything went well
    
    Please read the comment about timestamps in function:
    @see: create_cpu_total_plot 
    
    This method has been designed to take care of the varied number of cores
    on monitored nodes. It just should fill it with the 0 values. But it 
    was not tested for that.
    
    """
    
    
    # key is the spy id, the value is the number of cores
    spy_core_count = {}
    # get the core count
    for id in spy_ids:
        # id is a tuple
        cursor.execute('SELECT core_count FROM cpu_cap_tab WHERE spy_id=?', ( id, ))    
        core_count = cursor.fetchone()
        if core_count:
            spy_core_count[id] = core_count[0]
        else:
            logging.error('No core_count for ' + str(id[0]))
    
    logging.debug('Listing the spy ids and core_counts')
    logging.debug(spy_core_count)
    
    # now for each spy take a core utilization
    # the key is the spy id and the value is the 
    # dictionary with where a key is the core id and the value is the core util history
    spy_cores_util ={}
    
    for id in spy_ids:
        logging.info('Processing spy: ' + str(id))
        
        # the key is the core_id, the value: the history of the core utilization
        spy1 = {}
        # get per core utilization
        for core in range(spy_core_count[id]):
            cursor.execute('SELECT core_util FROM cpu_core_mon_tab '
                           'WHERE spy_id=? AND core_id=? ORDER BY ts', (id, core))
            tmp = cursor.fetchall()
            spy1[core] = list(sum(tmp, ()))
            
        spy_cores_util[id] = spy1        
    
    # now concatenate history for subsequent cores
    sorted_ids = spy_ids
    
    # the highest id for the core_id (the count starts from 0)
    max_core_id = max(spy_core_count.values()) - 1
    logging.debug('Max core_id value: ' + str(max_core_id))
    
    # our x values
    ticks_index = [0]
    # key: the core id, the value the history of the core utilization
    core_list = {}
    for id in sorted_ids:
        # the utilization of cores for this particular node id
        cores_util = spy_cores_util[id]
        for core_id in cores_util.keys():
            if None == core_list.get(core_id):
                core_list[core_id] = cores_util[core_id]
            else:
                core_list[core_id] = core_list[core_id] + cores_util[core_id]
            # just append the node designator terminator, that
            # the next time I will add the history for the new node    
            core_list[core_id].append(NODE_SEPARATOR)            
                
        # get the highest id for this node
        current_max_core_id = max(cores_util.keys())
        if current_max_core_id < max_core_id:
            assert(current_max_core_id > 0), 'The max core_id should be greater than 0. A node should have at least one core'
            # fill the rest core ids with 0
            # get the length of the history for this node
            ts_count = len(cores_util[0])
            zeros = ts_count * [ 0 ]
            for core_id in range(current_max_core_id + 1, max_core_id + 1):
                # this always should be bigger than core_id > 0
                if None == core_list.get(core_id):
                    core_list[core_id] = zeros + [0]
                else:
                    core_list[core_id] = core_list[core_id] + zeros + [0]
        # update the ticks_index with our current length of the history,
        # the core_id = 0
        ticks_index.append(len(core_list[0])-1)
    
    logging.debug('Printing the ticks_index: ')
    logging.debug(ticks_index)
    
    
    # generate the index (the x values) - the size of the history
    index = np.arange(len(core_list[0]))
    #logging.debug('x values count: ' + str(index))
    # plotting this figure is a kind of buggy (stacked bar)
    #plt.figure(num=plot_no, figsize=(8,6), dpi=80)
    
    #plots = []
    
    # add the core_id=0 plot, and then stack up the rest cores on it
    #plots.append(plt.bar(index, core_list[0], color=PLOT_COLORS[0]))
    logging.debug('Sorted core_list.keys() ' + str(sorted(core_list.keys())))
    #for core_id in sorted(core_list.keys()):
    #    if 0 == core_id:
    #        # already added to plots
    #        continue
    #    plots.append(plt.bar(index, core_list[core_id], color=PLOT_COLORS[core_id],
    #                         bottom=core_list[core_id-1]))
        # add the vline node separator
    #    add_to_plt_node_vline_separators(ticks_index)
        
    # add plot descriptions
    # add legend    
    cores_for_legend=tuple('core_' + str(i) for i in sorted(core_list.keys()))
    
    #t = []
    #for i in range(len(plots)):
    #    t.append(plots[i])
        
    #plt.legend( tuple(t), cores_for_legend )
    # add descriptions
    #plt.ylabel('CPU cores  utilization [%]')
    #plt.xlabel('Node Id')
    #plt.title('CPU core utilization per node ')
    ticks_in_the_middle = get_ticks_in_the_middle(ticks_index) 
    
    # check who is the ranger in town    
    spy_ids_str = get_spy_ids_str( cursor, 
        'SELECT spy_id FROM cpu_cap_tab WHERE hostname=?', spy_ids, ranger)        
    # add core_count
    for i in range(len(spy_ids)):
        spy_ids_str[i]  = spy_ids_str[i] + '\n#core ' + str(spy_core_count[spy_ids[i]])
    
    #plt.xticks(ticks_in_the_middle, spy_ids_str)
    #logging.debug(ticks_in_the_middle)
    #logging.debug(ticks_index)
    
    # save figure if fig_dir says that  
    #save_fig(fig_dir, FILES_NAME['cpu_core_stacked_util'])  
    #plot_no = plot_no + 1  
    
    # ----------------------------
  
    plt.figure(plot_no)
    plot_no = plot_no + 1
    
    # plot the individual cores histories
    max_core_count = max_core_id+1
    for plot_id in range(1, max_core_count + 1):
        plt.subplot(max_core_count, 1, plot_id)
        if 1 == plot_id:
            plt.title('CPU core utilization per node ')
        core_id = plot_id - 1
        plt.bar(index, core_list[core_id], color=PLOT_COLORS[core_id], label=cores_for_legend[core_id])
        plt.ylabel('CPU cores\util [%]')
        #plt.legend((t[core_id],), (cores_for_legend[core_id],), loc='upper right')
        plt.legend(loc='upper right')
        plt.yticks(np.arange(0, 110, 10))
        # add the vertical line separators to separate nodes' histories
        add_to_plt_node_vline_separators(ticks_index)
        
    #plt.legend( tuple(t), cores_for_legend )
    plt.xticks(ticks_in_the_middle, spy_ids_str)        
    plt.xlabel('Node Id')
    
    # save figure if fig_dir says that  
    save_fig(fig_dir, FILES_NAME['cpu_core_sep_util'])  
    
    return plot_no
    
def retrieve_cpu_data(database, ranger, fig_dir, plot_no):
    """
    @param database: the name of the database file to be opened
    @param ranger: a string, a hostname where the ranger where running
    @param fig_dir: a directory where save the figure
    @param plot_no: the number of the plot we can use
    @return: plot_no if we used this figure 
    """
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    
    # how many nodes we will have
    spy_ids = get_spy_ids_sorted(cursor, 'SELECT DISTINCT spy_id FROM cpu_core_mon_tab')
    
    logging.debug('CPU total plot no: ' + str(plot_no))
    plot_no = create_cpu_total_plot(cursor, spy_ids, ranger, fig_dir, plot_no)
    logging.debug('CPU core plot no: ' + str(plot_no))
    plot_no = create_cpu_core_plot(cursor, spy_ids, ranger, fig_dir, plot_no)
    
    # close the database
    conn.close()
    
    return plot_no
    
# --------------------------------------------------------------
# retrieves memory data 
# --------------------------------------------------------------
def create_mem_plot(cursor, spy_ids, ranger, fig_dir, plot_no):
    """
        @param cursor: The cursor to the opened database of the mem measurements
        @param spy_ids: The tuple of spy ids retrieved from the mem database
        @param ranger: The name of the ranger
        @param fig_dir: Where to save the figure 
        @param plot_no: which plot number it is
        @return: current plot_no
    """
    
    # these are our sorted ids
    sorted_ids = spy_ids
    # key: the spy_id, value: mem_util the dictionary where the key
    # are the metrics and the value is the history of values of the particular 
    # metric
    mem_util = {}
    # contains indexes of separators between nodes
    ticks_index = [0]

    metrics = [ 'mem_util', 'mem_buffered', 'mem_cached', 'mem_active', 'mem_inactive',
               'slab', 'mapped', 'swap_util', 'swap_cached' ]
    
    for id in sorted_ids:
        for metric in metrics:
            query = 'SELECT ' + metric + ' FROM mem_mon_tab WHERE spy_id=? ORDER BY ts'
            logging.debug('SQL query: ' + query)
            cursor.execute(query, (id, ))
            tmp = cursor.fetchall()
            tmp = list(sum(tmp, ()))
            
            # check if there are nan values, and if there are then substitute
            # this with 0
            logging.debug('tmp-->')
            logging.debug(tmp)
            for elem in range(len(tmp)):
                if tmp[elem] in ['-nan', 'nan']:
                    logging.warning('-nan or nan detected! Substituting with 0')
                    tmp[elem] = 0.0
                                        
            if None == mem_util.get(metric):
                mem_util[metric] = tmp
            else:
                mem_util[metric] = mem_util[metric] + tmp
            # add the node separator holder to indicate 
            # starting a history of a new node
            mem_util[metric].append(NODE_SEPARATOR)
        # remember our node separator holder; doesn't
        # matter which metric I ask, all should have the same length
        ticks_index.append(len(mem_util[metrics[0]])-1)
    
    logging.debug('mem: ticks_index: ')
    logging.debug(ticks_index)
    
    # now plot it
    plt.figure(num=plot_no)
    plot_no = plot_no + 1
    
    # my x values
    index = np.arange(len(mem_util[metrics[0]]))
    
    # how many plots I will have
    plots_count = len(metrics)
    
    for plot_id in range(1, plots_count + 1):
        # which metric we want to plot        
        metric_id = plot_id - 1
        metric_name = metrics[metric_id]

        plt.subplot(plots_count, 1, plot_id)
        if 1 == plot_id:
            plt.title('Mem utilization per node ')
        
        plt.bar(index, mem_util[metric_name], color=PLOT_COLORS[metric_id], label=metric_name)
        plt.ylabel('util [%]')
        plt.legend()
        plt.yticks(np.arange(0, 110, 10))
        # add the vertical line separators to separate nodes' histories
        add_to_plt_node_vline_separators(ticks_index)
        
    # get x description
    ticks_in_the_middle = get_ticks_in_the_middle(ticks_index)
    
    # check who is the ranger in town    
    sorted_ids_str = get_spy_ids_str( cursor, 
        'SELECT spy_id FROM mem_cap_tab WHERE hostname=?', sorted_ids, ranger)        
    # add max swap and mem in kb 
    for id in sorted_ids:
        cursor.execute('SELECT total_mem_kb, total_swap_kb FROM mem_cap_tab WHERE spy_id=?', (id,) )
        tmp = cursor.fetchone()
        i = sorted_ids.index(id)
        sorted_ids_str[i] = sorted_ids_str[i] + '\n' + str(tmp[0]) + ' mem KB'  + '\n' + str(tmp[1]) + ' swap KB'
    
    plt.xticks(ticks_in_the_middle, sorted_ids_str)        
    plt.xlabel('Node Id')
      
    # save figure if fig_dir says that  
    save_fig(fig_dir, FILES_NAME['mem_util'])  
    
    return plot_no


def retrieve_mem_data(args, fig_dir, plot_no):
    """
       @param args: The arguments from the command line, I need mainly
       args.mem_db, args.ranger
       @param fig_dir: The directory to the files that will be stored, if 
                       None nothing will be stored, it is assumed that the
                       name will be canonic
        @param plot_no: which plot we will be plotting, the value will be update
        @return: plot_no updated plot, which plot will be next to plot 
    """

    conn = sqlite3.connect(args.mem_db)
    cursor = conn.cursor()
    spy_ids = get_spy_ids_sorted(cursor, 'SELECT DISTINCT spy_id FROM mem_mon_tab')
    logging.debug('Mem plot no: ' + str(plot_no))
    plot_no = create_mem_plot(cursor, spy_ids, args.ranger, fig_dir, plot_no)

    conn.close()
        
    return plot_no

# ----------------------------------------------------------------
# plots net data
# ----------------------------------------------------------------
def get_nic_history(cursor, spy_ids, nic_type):
    """
       get the nic utilization across the whole database
       @param cursor: The cursor to the opened net database
       @param spy_ids: the identifiers of the spies
       @param nic_type: the type of the nic we are working on
                        can be 'eth' or 'ib'
       @return: nic_util - a dictionary, a history utilization
               key is the spy id, value is a dictionary where
               nic id is the key and value is the history of this
               nic utilization  
    """
    nic_util = {}
    # the name of the database field in net_cap_tab i.e. ib_count
    # or eth_count
    db_nic_count_name = nic_type + '_count'
    # the value of the field in net_mon_tab
    db_nic_type = nic_type
    
    for id in spy_ids:
        # first count nic interfaces of this type for this node
        cursor.execute('SELECT ' + db_nic_count_name + 
                       ' FROM net_cap_tab WHERE spy_id=?', (id, ))
        nic_count = cursor.fetchone()
        
        # key is the nic_id, value: history for that node
        tmp_d = {}
        # now get the history for this node
        for i in range(nic_count[0]):
            cursor.execute('SELECT nic_total, nic_trans, nic_recv '
                           'FROM net_mon_tab WHERE spy_id=? AND '
                           'nic_type=? AND nic_id=?', (id, db_nic_type, i))
            tmp=cursor.fetchall()
            
            nic_total = [ t[0] for t in tmp]
            nic_trans = [ t[1] for t in tmp]
            nic_recv = [ t[2] for t in tmp ]
            tmp_d[i] = { 'nic_total' : nic_total,
                        'nic_trans' : nic_trans,
                        'nic_recv' : nic_recv}
            logging.debug(tmp)
            logging.debug(tmp_d[i])
        # store the history
        logging.info('Stored history for: Spy id: ' + str(id) + 
                      ' Nic type: ' + db_nic_type +
                      ' Nic_count: ' + str(nic_count[0]))
        nic_util[id] = tmp_d
    
    return nic_util


def prepare_net_fig_data(spy_ids, net_load, nic_id):
    """
        @param spy_ids: The list of spy identifiers
        @param net_load: The big dictionary of the network load
        @param nic_id: The identifier of the NIC to be prepared
        
        @return: a dictionary with values prepare for plotting
        None if nothing to plot 
    """
    d = { 'index' : None,
         'nic_total' : [],
         'nic_trans' : [],
         'nic_recv' : [],
         'ticks_index' : [0],
         'ticks_in_the_middle' : None,
         'spy_ids' : [] }
    
    logging.debug('nic_id = ' + str(nic_id))
    for id in spy_ids:
        #logging.debug(net_load[id])
        # get nics per node
        logging.debug('Keys for id: ' + str(id))
        logging.debug(net_load[id].keys())
        nic = net_load[id].get(nic_id)
        logging.debug(nic)
        
        if not nic:
            continue
        
        # we got a new node with a history
        d['nic_total'] = d['nic_total'] + nic['nic_total'] + [NODE_SEPARATOR]
        d['nic_trans'] = d['nic_trans'] + nic['nic_trans'] + [NODE_SEPARATOR]
        d['nic_recv'] = d['nic_recv'] + nic['nic_recv'] + [NODE_SEPARATOR]
        d['spy_ids'].append(id)
        d['ticks_index'].append(len(d['nic_total']) - 1 )
    
    # check if anything has been written
    if len(d['nic_total']):
        d['index'] = np.arange(len(d['nic_total']))
        d['ticks_in_the_middle'] = get_ticks_in_the_middle(d['ticks_index'])
        return d
    else:
        # no history for this nic id
        return None

def create_nic_plot(cursor, ranger, fig_data, nic_type, nic_id):
    """
       plots the nic plot for
       @param cursor: The cursor to the open net database
       @param ranger: The host name where the ranger was running  
       @param fig_data: data to plot (as created by @see: prepare_net_fig_data 
       @param nic_type: 'ib' or 'eth'
       @param nic_id: The nic identifier 0,1,2, ..
       
       @return: True  
    """
        
    p = fig_data
    
    # check who is the ranger in town    
    sorted_ids_str = get_spy_ids_str( cursor, 
        'SELECT spy_id FROM net_cap_tab WHERE hostname=?', p['spy_ids'], ranger)        
        
    loads = ['nic_trans', 'nic_recv', 'nic_total']
    nic_id_str = nic_type + str(nic_id)
    for load in loads:
        logging.debug('Processing: [' + str(loads.index(load)) + '] = '+ load )
        plt.subplot(3, 1, loads.index(load)+1)
        plt.title(nic_id_str + ': ' + load + ' load per node ')
        plt.bar(p['index'], p[load], color=PLOT_COLORS[nic_id], label=nic_id_str)
        plt.ylabel('load [KB]')
        #plt.xticks(p['index'])
        #plt.legend()
        #plt.xticks(p['ticks_in_the_middle'], sorted_ids_str)        
        #plt.xlabel('Node Id')
        #plt.yticks(np.arange(0, 110, 10))
        # add the vertical line separators to separate nodes' histories
        add_to_plt_node_vline_separators(p['ticks_index'])
        
    #plt.legend()
    plt.xticks(p['ticks_in_the_middle'], sorted_ids_str)        
    plt.xlabel('Node Id')

    # add the vertical line separators to separate nodes' histories
    add_to_plt_node_vline_separators(p['ticks_index'])
            
    return True
    
def plot_nics(cursor, ranger, plot_no, sorted_ids, load, nic_type):
    """
        @param cursor: The cursor to the opened database
        @param ranger: The host name where the ranger run
        @param plot_no: The subsequent plot number
        @param sorted_ids: The sorted spies' ids
        @param load: the net load
        @param nic_type: for which type of interface 'ib' or 'eth'
        
        @return: plot_no The the next legal plot_no     
    """
    # we assume that the nics are numbered 0,1,2,3, ...
    nic_id = 0
    
    # first create the ib
    while True:
        logging.debug(inspect.stack()[0][3] + '(): nic_type ' + nic_type + ' nic_id =  ' + str(nic_id))
        p = prepare_net_fig_data(sorted_ids, load, nic_id)
        if not p:
            break
        
        plt.figure(num=plot_no)
        plot_no = plot_no + 1

        create_nic_plot(cursor, ranger, p, nic_type, nic_id)        

        # save figure if fig_dir says that  
        save_fig(fig_dir, nic_type + '_' + str(nic_id) + '_'+ FILES_NAME['net_load'])
    
        # get the new nic
        nic_id = nic_id + 1

    return plot_no
    
def create_net_plot(cursor, spy_ids, ranger, fig_dir, plot_no):
    """
        @param cursor: The cursor to the opened database of the net measurements
        @param spy_ids: The tuple of spy ids retrieved from the net database
        @param ranger: The name of the ranger
        @param fig_dir: Where to save the figure 
        @param plot_no: which plot number it is
        @return: current plot_no
    """
    
    # these are our sorted ids
    sorted_ids = spy_ids
    
    ib_load = get_nic_history(cursor, spy_ids, 'ib')
    logging.info('ib util spies #: ' + str(len(ib_load.keys())))
    eth_load = get_nic_history(cursor, spy_ids, 'eth')
    logging.info('eth util spies #: ' + str(len(eth_load.keys())))
    
    # now prepare for plotting, prepare x and y values
    # we assume that the nics are numbered 0,1,2,3, ...
    nic_id = 0
    
    # first create the ib
    plot_no = plot_nics(cursor, ranger, plot_no, sorted_ids, ib_load, 'ib')
    # next eth nics
    plot_no = plot_nics(cursor, ranger, plot_no, sorted_ids, eth_load, 'eth')
    
    return plot_no

def retrieve_net_data(args, fig_dir, plot_no):
    """
       @param args: The arguments from the command line, I need mainly
       args.mem_db, args.ranger
       @param fig_dir: The directory to the files that will be stored, if 
                       None nothing will be stored, it is assumed that the
                       name will be canonic
        @param plot_no: which plot we will be plotting, the value will be update
        @return: plot_no updated plot, which plot will be next to plot 
    """

    conn = sqlite3.connect(args.net_db)
    cursor = conn.cursor()
    
    # how many nodes we will have, this will be sorted
    spy_ids = get_spy_ids_sorted(cursor, 'SELECT DISTINCT spy_id FROM net_mon_tab')
    
    plot_no = create_net_plot(cursor, spy_ids, args.ranger, fig_dir, plot_no)

    conn.close()
        
    return plot_no

# --------------------------------------------------------
# get nvidia-smi
# --------------------------------------------------------
def get_nvi_history(cursor, spy_ids):
    """
       get the nic utilization across the whole database
       @param cursor: The cursor to the opened net database
       @param spy_ids: the identifiers of the spies
       @return: nvi_hist - a dictionary, a history utilization
               key is the spy id, value is a dictionary where
               gpu id is the key and value is the history of this
               gpu utilization  
    """
    nvi_hist = {}
    
    for id in spy_ids:
        # first count gpu devices for this id
        cursor.execute('SELECT gpu_count FROM nvid_cap_tab '
                       'WHERE spy_id=?', (id, ))
        gpu_count = cursor.fetchone()
                
        # key is the gpu_id, value: history for that node
        tmp_d = {}
        # now get the history for this node
        for i in range(gpu_count[0]):
            cursor.execute('SELECT gpu_util, mem_util, mem_used_MB, '
                           'power_draw, perf_state '
                           'FROM nvid_mon_tab WHERE spy_id=? AND '
                           'gpu_id=?', (id, i))
            tmp=cursor.fetchall()
            
            tmp_d[i] = { 'gpu_util' : [ t[0] for t in tmp],
                        'mem_util' : [ t[1] for t in tmp],
                        'mem_used_MB' : [ t[2] for t in tmp ],
                        'power_draw' : [ t[3] for t in tmp ],
                        'perf_state' : [ int(t[4][1:]) for t in tmp ]}
            logging.debug(tmp_d[i])
            logging.debug('perf_state')
            logging.debug(tmp_d[i]['perf_state'])
        # store the history
        logging.info('Stored history for: Spy id: ' + str(id) + 
                      ' GPU_count: ' + str(gpu_count[0]))
        nvi_hist[id] = tmp_d
    
    return nvi_hist

def prepare_nvi_fig_data(spy_ids, nvi_hist, gpu_id):
    """
        @param spy_ids: The list of spy identifiers
        @param nvi_hist: The big dictionary of the gpu util
        @param gpu_id: The identifier of the gpu device to be prepared
        
        @return: a dictionary with values prepare for plotting
        None if nothing to plot 
    """
    d = { 'index' : None,
         'gpu_util' : [],
         'mem_util' : [],
         'mem_used_MB' : [],
         'power_draw' : [],
         'perf_state' : [],
         'ticks_index' : [0],
         'ticks_in_the_middle' : None,
         'spy_ids' : [] }
    
    logging.debug('gpu_id = ' + str(gpu_id))
    for id in spy_ids:
        # get gpus per node
        logging.debug('Keys for id: ' + str(id))
        logging.debug(nvi_hist[id].keys())
        gpu = nvi_hist[id].get(gpu_id)
        logging.debug(gpu)
        
        if not gpu:
            continue
        
        # we got a new node with a history
        for i in ['gpu_util', 'mem_util', 'mem_used_MB', 'power_draw', 'perf_state']:
            d[i] = d[i] + gpu[i] + [NODE_SEPARATOR]
        
        d['spy_ids'].append(id)
        d['ticks_index'].append(len(d['gpu_util']) - 1 )
    
    # check if anything has been written
    if len(d['gpu_util']):
        d['index'] = np.arange(len(d['gpu_util']))
        d['ticks_in_the_middle'] = get_ticks_in_the_middle(d['ticks_index'])
        return d
    else:
        # no history for this nic id
        return None


def plot_gpu(cursor, ranger, fig_data,  gpu_id):
    """
       plots the nic plot for
       @param cursor: The cursor to the open net database
       @param ranger: The host name where the ranger was running  
       @param fig_data: data to plot (as created by @see: prepare_nvi_fig_data 
       @param gpu_id: The gpu device identifier 0,1,2, ..
       
       @return: True  
    """
        
    p = fig_data
    
    # check who is the ranger in town    
    sorted_ids_str = get_spy_ids_str( cursor, 
        'SELECT spy_id FROM nvid_cap_tab WHERE hostname=?', p['spy_ids'], ranger)        
    
    loads = ['gpu_util', 'mem_util', 'mem_used_MB', 'power_draw', 'perf_state']
    
    gpu_id_str = 'gpu_' + str(gpu_id)
    for load in loads:
        logging.debug('Processing: [' + str(loads.index(load)) + '] = '+ load )
        plt.subplot(len(loads), 1, loads.index(load)+1)
        plt.title(gpu_id_str + ': ' + load + ' per node ')
        plt.bar(p['index'], p[load], color=PLOT_COLORS[gpu_id], label=gpu_id_str)
        plt.ylabel('')
        #plt.xticks(p['index'])
        #plt.legend()
        #plt.xticks(p['ticks_in_the_middle'], sorted_ids_str)        
        #plt.xlabel('Node Id')
        #plt.yticks(np.arange(0, 110, 10))
        # add the vertical line separators to separate nodes' histories
        add_to_plt_node_vline_separators(p['ticks_index'])
        
    #plt.legend()
    plt.xticks(p['ticks_in_the_middle'], sorted_ids_str)        
    plt.xlabel('Node Id')

    # add the vertical line separators to separate nodes' histories
    add_to_plt_node_vline_separators(p['ticks_index'])
            
    return True


def create_nvi_plot(cursor, spy_ids, ranger, fig_dir, plot_no):
    """
        @param cursor: The cursor to the opened database of the net measurements
        @param spy_ids: The tuple of spy ids retrieved from the net database
        @param ranger: The name of the ranger
        @param fig_dir: Where to save the figure 
        @param plot_no: which plot number it is
        @return: current plot_no
    """
    
    # these are our sorted ids
    sorted_ids = spy_ids
    
    nvi_hist = get_nvi_history(cursor, spy_ids)

    # we assume that the nics are numbered 0,1,2,3, ...
    gpu_id = 0
    
    # first create the ib
    while True:
        logging.debug(inspect.stack()[0][3] + '(): gpu_id =  ' + str(gpu_id))
        p = prepare_nvi_fig_data(sorted_ids, nvi_hist, gpu_id)
        if not p:
            break
        
        plt.figure(num=plot_no)
        plot_no = plot_no + 1

        plot_gpu(cursor, ranger, p, gpu_id)        

        # save figure if fig_dir says that  
        save_fig(fig_dir, 'gpu_' + str(gpu_id) + '_'+ FILES_NAME['gpu_util'])
    
        # get the new nic
        gpu_id = gpu_id + 1
    
    return plot_no

def retrieve_nvi_data(args, fig_dir, plot_no):
    """
       @param args: The arguments from the command line, I need mainly
       args.mem_db, args.ranger
       @param fig_dir: The directory to the files that will be stored, if 
                       None nothing will be stored, it is assumed that the
                       name will be canonic
        @param plot_no: which plot we will be plotting, the value will be update
        @return: plot_no updated plot, which plot will be next to plot 
    """

    conn = sqlite3.connect(args.nvi_db)
    cursor = conn.cursor()
    
    # how many nodes we will have, this will be sorted
    spy_ids = get_spy_ids_sorted(cursor, 'SELECT DISTINCT spy_id FROM nvid_mon_tab')
    
    plot_no = create_nvi_plot(cursor, spy_ids, args.ranger, fig_dir, plot_no)

    conn.close()
        
    return plot_no

# --------------------------------------------------------
# get nvml
# --------------------------------------------------------
def get_nvml_history(cursor, spy_ids):
    """
       get the nic utilization across the whole database
       @param cursor: The cursor to the opened net database
       @param spy_ids: the identifiers of the spies
       @return: nvi_hist - a dictionary, a history utilization
               key is the spy id, value is a dictionary where
               gpu id is the key and value is the history of this
               gpu utilization  
    """
    nvml_hist = {}
    
    for id in spy_ids:
        # first count gpu devices for this id
        cursor.execute('SELECT gpu_count FROM nvml_cap_tab '
                       'WHERE spy_id=?', (id, ))
        gpu_count = cursor.fetchone()
                
        # key is the gpu_id, value: history for that node
        tmp_d = {}
        # now get the history for this node
        for i in range(gpu_count[0]):
            cursor.execute('SELECT gpu_util, mem_util, mem_used_MB, '
                           'power_draw, perf_state '
                           'FROM nvml_mon_tab WHERE spy_id=? AND '
                           'gpu_id=?', (id, i))
            tmp=cursor.fetchall()
            
            tmp_d[i] = { 'gpu_util' : [ t[0] for t in tmp],
                        'mem_util' : [ t[1] for t in tmp],
                        'mem_used_MB' : [ t[2] for t in tmp ],
                        'power_draw' : [ t[3] for t in tmp ],
                        'perf_state' : [ int(t[4][1:]) for t in tmp ]}
            logging.debug(tmp_d[i])
            logging.debug('perf_state')
            logging.debug(tmp_d[i]['perf_state'])
        # store the history
        logging.info('Stored history for: Spy id: ' + str(id) + 
                      ' GPU_count: ' + str(gpu_count[0]))
        nvml_hist[id] = tmp_d
    
    return nvml_hist

def prepare_nvml_fig_data(spy_ids, nvml_hist, gpu_id):
    """
        @param spy_ids: The list of spy identifiers
        @param nvi_hist: The big dictionary of the gpu util
        @param gpu_id: The identifier of the gpu device to be prepared
        
        @return: a dictionary with values prepare for plotting
        None if nothing to plot 
    """
    d = { 'index' : None,
         'gpu_util' : [],
         'mem_util' : [],
         'mem_used_MB' : [],
         'power_draw' : [],
         'perf_state' : [],
         'ticks_index' : [0],
         'ticks_in_the_middle' : None,
         'spy_ids' : [] }
    
    logging.debug('gpu_id = ' + str(gpu_id))
    for id in spy_ids:
        # get gpus per node
        logging.debug('Keys for id: ' + str(id))
        logging.debug(nvml_hist[id].keys())
        gpu = nvml_hist[id].get(gpu_id)
        logging.debug(gpu)
        
        if not gpu:
            continue
        
        # we got a new node with a history
        for i in ['gpu_util', 'mem_util', 'mem_used_MB', 'power_draw', 'perf_state']:
            d[i] = d[i] + gpu[i] + [NODE_SEPARATOR]
        
        d['spy_ids'].append(id)
        d['ticks_index'].append(len(d['gpu_util']) - 1 )
    
    # check if anything has been written
    if len(d['gpu_util']):
        d['index'] = np.arange(len(d['gpu_util']))
        d['ticks_in_the_middle'] = get_ticks_in_the_middle(d['ticks_index'])
        return d
    else:
        # no history for this nic id
        return None


def plot_nvml_gpu(cursor, ranger, fig_data,  gpu_id):
    """
       plots the nic plot for
       @param cursor: The cursor to the open net database
       @param ranger: The host name where the ranger was running  
       @param fig_data: data to plot (as created by @see: prepare_nvi_fig_data 
       @param gpu_id: The gpu device identifier 0,1,2, ..
       
       @return: True  
    """
        
    p = fig_data
    
    # check who is the ranger in town    
    sorted_ids_str = get_spy_ids_str( cursor, 
        'SELECT spy_id FROM nvml_cap_tab WHERE hostname=?', p['spy_ids'], ranger)        
    
    loads = ['gpu_util', 'mem_util', 'mem_used_MB', 'power_draw', 'perf_state']
    
    gpu_id_str = 'gpu_' + str(gpu_id)
    for load in loads:
        logging.debug('Processing: [' + str(loads.index(load)) + '] = '+ load )
        plt.subplot(len(loads), 1, loads.index(load)+1)
        plt.title(gpu_id_str + ': ' + load + ' per node ')
        plt.bar(p['index'], p[load], color=PLOT_COLORS[gpu_id], label=gpu_id_str)
        plt.ylabel('')
        #plt.xticks(p['index'])
        #plt.legend()
        #plt.xticks(p['ticks_in_the_middle'], sorted_ids_str)        
        #plt.xlabel('Node Id')
        #plt.yticks(np.arange(0, 110, 10))
        # add the vertical line separators to separate nodes' histories
        add_to_plt_node_vline_separators(p['ticks_index'])
        
    #plt.legend()
    plt.xticks(p['ticks_in_the_middle'], sorted_ids_str)        
    plt.xlabel('Node Id')

    # add the vertical line separators to separate nodes' histories
    add_to_plt_node_vline_separators(p['ticks_index'])
            
    return True


def create_nvml_plot(cursor, spy_ids, ranger, fig_dir, plot_no):
    """
        @param cursor: The cursor to the opened database of the net measurements
        @param spy_ids: The tuple of spy ids retrieved from the net database
        @param ranger: The name of the ranger
        @param fig_dir: Where to save the figure 
        @param plot_no: which plot number it is
        @return: current plot_no
    """
    
    # these are our sorted ids
    sorted_ids = spy_ids
    
    nvml_hist = get_nvml_history(cursor, spy_ids)

    # we assume that the nics are numbered 0,1,2,3, ...
    gpu_id = 0
    
    # first create the ib
    while True:
        logging.debug(inspect.stack()[0][3] + '(): gpu_id =  ' + str(gpu_id))
        p = prepare_nvml_fig_data(sorted_ids, nvml_hist, gpu_id)
        if not p:
            break
        
        plt.figure(num=plot_no)
        plot_no = plot_no + 1

        plot_nvml_gpu(cursor, ranger, p, gpu_id)        

        # save figure if fig_dir says that  
        save_fig(fig_dir, 'gpu_' + str(gpu_id) + '_'+ FILES_NAME['gpu_util'])
    
        # get the new nic
        gpu_id = gpu_id + 1
    
    return plot_no

def retrieve_nvml_data(args, fig_dir, plot_no):
    """
       @param args: The arguments from the command line, I need mainly
       args.mem_db, args.ranger
       @param fig_dir: The directory to the files that will be stored, if 
                       None nothing will be stored, it is assumed that the
                       name will be canonic
        @param plot_no: which plot we will be plotting, the value will be update
        @return: plot_no updated plot, which plot will be next to plot 
    """

    conn = sqlite3.connect(args.nvml_db)
    cursor = conn.cursor()
    
    # how many nodes we will have, this will be sorted
    spy_ids = get_spy_ids_sorted(cursor, 'SELECT DISTINCT spy_id FROM nvml_mon_tab')
    
    plot_no = create_nvml_plot(cursor, spy_ids, args.ranger, fig_dir, plot_no)

    conn.close()
        
    return plot_no



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generates plots from the databases '
                                     'produced by the Dwarf module from the ClusterWatch',
                                     epilog='eg. python --cpu_db cpu_spy.db')
    # argparse treats options as strings unless we state otherwise
    parser.add_argument('--cpu_db', help='path to the cpu database file')
    parser.add_argument('--net_db', help='path to the network database file')    
    parser.add_argument('--nvi_db', help='path to the nvidia smi database file')
    parser.add_argument('--nvml_db', help='path to the nvidia smi database file')
    parser.add_argument('--mem_db', help='path to the mem database file')
    parser.add_argument('--ranger', help='name of the hostname where the ranger was running')
    parser.add_argument('--fig_dir', help='if present this indicates where to store generated figures.'
                        ' End the dir with slash or backslash: --fig_dir d:\\\\tmp\\\\tmp\\\\')
    
    args = parser.parse_args()
    
    # the default level is logging.WARNING
    logging.basicConfig(level=logging.DEBUG)
    logging.debug('Checking if database files exist ...')
    # check if cpu database exists
    
    options = [args.cpu_db, args.net_db, args.nvi_db, args.nvml_db, args.mem_db]    
    
    for o in options:
        if o:
            exit_if_no_path(o, '')
            logging.debug('will be generated: ' + o)
    fig_dir = args.fig_dir
    if fig_dir:
        fig_dir = os.path.dirname(args.fig_dir)
        exit_if_no_path(fig_dir, 'The fig directory does not exist')
        logging.info('The figures will be saved into ' + fig_dir)
        
    # which plot can we plot
    plot_no = 1
    if args.cpu_db:
        plot_no = retrieve_cpu_data(args.cpu_db, args.ranger, fig_dir, plot_no)        
        logging.debug('Plot_no: ' + str(plot_no))
        
    if args.mem_db:
        plot_no = retrieve_mem_data(args, fig_dir, plot_no)
        logging.debug('Plot_no: ' + str(plot_no))
        
    if args.net_db:
        plot_no = retrieve_net_data(args, fig_dir, plot_no)
        logging.debug('Plot_no: ' + str(plot_no))
    
    if args.nvi_db:
        plot_no = retrieve_nvi_data(args, fig_dir, plot_no)
        logging.debug('Plot_no: ' + str(plot_no))

    if args.nvml_db:
        plot_no = retrieve_nvml_data(args, fig_dir, plot_no)
        logging.debug('Plot_no: ' + str(plot_no))
        
    # plot everything
    plt.show()    
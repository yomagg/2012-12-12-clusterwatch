# e.g. ./runme.sh melt-all/expr01

EXPR=$1
EXPR_PATH="d:/synchrobox/proj/w-expr/2013-03-25-cw-prost/src_data/"$1"/prost.cc.gt.atl.ga.us-mmap"

if [ $# -eq 0 ]; then
	echo "Usage: `basename $0` part-of-$EXPR_PATH "
	echo "Description: "
	echo "Ex. `basename $0` melt-all/expr01"
	exit -1
fi


PYT_PATH="/d/opt/Python27/python.exe"
MSUF="2013-03-26-0000.log.db"
NET=$EXPR_PATH-net_spy-$MSUF
NVML=$EXPR_PATH-nvml_spy-$MSUF
NVI=$EXPR_PATH-nvidia_smi_spy-$MSUF
CPU=$EXPR_PATH-cpu_spy-$MSUF
MEM=$EXPR_PATH-mem_spy-$MSUF

$PYT_PATH plot_db.py --net_db $NET  --nvml_db $NVML --nvi_db $NVI --cpu_db $CPU --mem_db $MEM --ranger prost.cc.gt.atl.ga.us
#/d/opt/Python27/python.exe plot_db.py --nvml_db d:/synchrobox/proj/w-expr/2013-03-25-cw-prost/src_data/melt-all/expr01/prost.cc.gt.atl.ga.us-mmap-nvml_spy-2013-03-26-0000.log.db  --ranger prost.cc.gt.atl.ga.us
#/d/opt/Python27/python.exe plot_db.py --nvi_db d:/synchrobox/proj/w-expr/2013-03-25-cw-prost/src_data/melt-all/expr01/prost.cc.gt.atl.ga.us-mmap-nvidia_smi_spy-2013-03-26-0000.log.db  --ranger prost.cc.gt.atl.ga.us
#/d/opt/Python27/python.exe plot_db.py --cpu_db d:/synchrobox/proj/w-expr/2013-03-25-cw-prost/src_data/melt-all/expr01/prost.cc.gt.atl.ga.us-mmap-cpu_spy-2013-03-26-0000.log.db  --ranger prost.cc.gt.atl.ga.us
#/d/opt/Python27/python.exe plot_db.py --mem_db d:/synchrobox/proj/w-expr/2013-03-25-cw-prost/src_data/melt-all/expr01/prost.cc.gt.atl.ga.us-mmap-mem_spy-2013-03-26-0000.log.db  --ranger prost.cc.gt.atl.ga.us

#/d/opt/Python27/python.exe plot_db.py --nvml_db d:/synchrobox/proj/w-expr/2013-02-17-cw/src_data/2013-02-17-melt-cw-nvml/expr01/nodes-002/kid085.nics.utk.edu-mmap-nvml_spy-2013-02-17-0000.log.db  --ranger kid085.nics.utk.edu

#/d/opt/Python27/python.exe plot_db.py --nvml_db d:/synchrobox/proj/w-expr/2013-02-17-cw/src_data/2013-02-17-melt-cw-nvml/expr01/nodes-004/kid082.nics.utk.edu-mmap-nvml_spy-2013-02-17-0000.log.db  --ranger kid082.nics.utk.edu

#/d/opt/Python27/python.exe plot_db.py --nvi_db d:/synchrobox/proj/w-expr/2012-12-28-dwarf-preliminary/src_data/2012-12-27-melt/expr01/nodes-002/kid096.nics.utk.edu-mmap-nvidia_smi_spy-2012-12-27-0000.log.db  --ranger kid096.nics.utk.edu

#/d/opt/Python27/python.exe plot_db.py --net_db d:/synchrobox/proj/w-expr/2012-12-28-dwarf-preliminary/src_data/2012-12-27-melt/expr01/nodes-004/kid021.nics.utk.edu-mmap-net_spy-2012-12-27-0000.log.db  --ranger kid021.nics.utk.edu

#/d/opt/Python27/python.exe plot_db.py --cpu_db d:/synchrobox/proj/w-expr/2012-12-28-dwarf-preliminary/src_data/2012-12-27-melt/expr01/nodes-002/kid096.nics.utk.edu-mmap-cpu_spy-2012-12-27-0000.log.db  --ranger kid096.nics.utk.edu

#/d/opt/Python27/python.exe plot_db.py --mem_db d:/synchrobox/proj/w-expr/2012-12-28-dwarf-preliminary/src_data/2012-12-27-melt/expr01/nodes-002/kid096.nics.utk.edu-mmap-mem_spy-2012-12-27-0000.log.db --cpu_db d:/synchrobox/proj/w-expr/2012-12-28-dwarf-preliminary/src_data/2012-12-27-melt/expr01/nodes-002/kid096.nics.utk.edu-mmap-cpu_spy-2012-12-27-0000.log.db --ranger kid096.nics.utk.edu


#/d/opt/Python27/python.exe plot_db.py --net_db d:/synchrobox/proj/w-expr/2012-12-28-dwarf-preliminary/src_data/2012-12-27-melt/expr01/nodes-002/kid096.nics.utk.edu-mmap-net_spy-2012-12-27-0000.log.db  --ranger kid096.nics.utk.edu

#/d/opt/Python27/python.exe plot_db.py --nvml_db d:/synchrobox/proj/w-expr/2013-02-17-cw/src_data/2013-02-17-melt-cw-nvml/expr01/nodes-002/kid085.nics.utk.edu-mmap-nvml_spy-2013-02-17-0000.log.db  --ranger kid085.nics.utk.edu

#/d/opt/Python27/python.exe plot_db.py --nvml_db d:/synchrobox/proj/w-expr/2013-02-17-cw/src_data/2013-02-17-melt-cw-nvml/expr01/nodes-004/kid082.nics.utk.edu-mmap-nvml_spy-2013-02-17-0000.log.db  --ranger kid082.nics.utk.edu

#/d/opt/Python27/python.exe plot_db.py --nvi_db d:/synchrobox/proj/w-expr/2012-12-28-dwarf-preliminary/src_data/2012-12-27-melt/expr01/nodes-002/kid096.nics.utk.edu-mmap-nvidia_smi_spy-2012-12-27-0000.log.db  --ranger kid096.nics.utk.edu

#/d/opt/Python27/python.exe plot_db.py --net_db d:/synchrobox/proj/w-expr/2012-12-28-dwarf-preliminary/src_data/2012-12-27-melt/expr01/nodes-004/kid021.nics.utk.edu-mmap-net_spy-2012-12-27-0000.log.db  --ranger kid021.nics.utk.edu

#/d/opt/Python27/python.exe plot_db.py --cpu_db d:/synchrobox/proj/w-expr/2012-12-28-dwarf-preliminary/src_data/2012-12-27-melt/expr01/nodes-002/kid096.nics.utk.edu-mmap-cpu_spy-2012-12-27-0000.log.db  --ranger kid096.nics.utk.edu

#/d/opt/Python27/python.exe plot_db.py --mem_db d:/synchrobox/proj/w-expr/2012-12-28-dwarf-preliminary/src_data/2012-12-27-melt/expr01/nodes-002/kid096.nics.utk.edu-mmap-mem_spy-2012-12-27-0000.log.db --cpu_db d:/synchrobox/proj/w-expr/2012-12-28-dwarf-preliminary/src_data/2012-12-27-melt/expr01/nodes-002/kid096.nics.utk.edu-mmap-cpu_spy-2012-12-27-0000.log.db --ranger kid096.nics.utk.edu

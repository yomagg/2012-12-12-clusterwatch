%
% @file pg.tex
%
% @date Sep 12, 2012
% @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
%
\documentclass[12pt]{article}


\title{ClusterWatch's Spy Programmer's Guide}

\author{Magdalena Slawinska\\
Georgia Institute of Technology\\
School of Computer Science\\
magg.gatech@gmail.com}

\date{Version: 2013-10-21\\
Compiled: \today}

\usepackage[T1]{fontenc}
\usepackage{pslatex}
\usepackage{hyperref}


\begin{document}
\maketitle
\section{Purpose}
A simple guide about how to write spies within the ClusterWatch infrastructure.
Currently, the ClusterWatch contains two modules. The Dwarf module contains the
implementation of the spies.
\section{Terminology}
\begin{itemize}
  \item spy -- a collector for a particular resource
  \item ranger -- an aggregrator process (one per monitor cluster)
  \item patrol -- a collector process with enabled spies (one per node on a cluster)
  \item reader -- the process that reads a memory mapped file
\end{itemize}

\section{Writing New Spies}
In order to write a new spy within the Dwarf module infrastructure a few
files need to be created or modified:
\begin{itemize}
  \item xxx\_spy\_torso.c -- the body of the spy; typically in dwarf/src/spies/xxx\_spy
  \item add the name of the spy to src/include/ini\_utils\_types.h, as it will
  appear in the ini file
  \item add the xxx\_spy to src/patrol/dwarf\_patrol.c
  \item add the xxx\_spy to src/ranger/dwarf\_ranger.c
  \item src/common/SConstruct -- add an xxx spy to the Dwarf build system
  \item src/readers/sqlite3\_reader.py -- add functions to read xxx\_spy data and insert them to sqlite3 b
\end{itemize}

\subsection{Writing a New Spy Torso}
This is the most tedious from the implementation point of view.

The current implementation of spies are presented in the src/spies directory.
\begin{itemize}
\item {\tt cpu\_spy} -- a spy that uses /proc to get data about cores' util
    \item {\tt net\_spy} -- a spy that uses /proc to get data about network load
\item {\tt mem\_spy} -- a spy that uses /proc to get data about memory util
\item {\tt nvid\_spy} -- a spy that uses nvidia-smi to get data about nvidia utilization
\item {\tt nvml\_spy} -- a spy that uses NVML library to get data about nvidia
utilization, it is a better alternative to nvid\_spy
\item {\tt lynx\_spy} -- a spy that uses GPU Lynx to get data about execution
of GPU applications at the kernel level.
\end{itemize}
The CPU, net, mem and nvid spies were ported from the predecessor of the 
ClusterWatch, and they are listed in the order of porting. The following spies
were developed for the ClusterWatch. 
So the cleanest way of the spy implementation is provided in nvid spy. 
The lynx spy allows to get the instrumentation provided by the GPU Lynx.


In general, a spy torso implementation requires:
\begin{itemize}
  \item data -- the structure that will be sent to an aggregator from a collector
  \item functions required by the transport/evpath such as setup\_(), read\_(), process\_()
  \item functions required by the Dwarf module, namely 
   dwarf\_init\_xxx\_spy\_ctx(),
   dwarf\_init\_xxx\_ranger(), dwarf\_init\_xxx\_spy()
\end{itemize}

\subsubsection{Data}
You need to provide a description of data struct as it is required by EVPath.
\subsubsection{Transport/EVPath}
\begin{itemize}
  \item setup() -- executed on a node by the collector
  \item read() -- actual collection of the data, i.e., how the data are collected
  and the data structure described in the EVPath format, this is executed
  by the collector
  \item process() -- aggregation of the data; this is invoked by the aggregator;
  this is executed by the aggregator
\end{itemize}
\begin{verbatim}
// typically it should allocate the memory for the structure that is used
// in read_blah; the read_blah at the beginning should have the invocation
// to function that connects the specific fields with preallocated buffers
// to the actual structure that is used for storing monitored data
void setup_blah(void);

// @return 0 - you were not able to read anything
//         > 0 you have data to be sent; successful read
int read_blah(void *data);

// @return 0  success
//        anything else some issues (should not happen)
int process_blah(void *data);
\end{verbatim}


\subsubsection{Dwarf Functions}
\begin{itemize}
  \item dwarf\_init\_xxx\_spy\_ctx() -- you need to fill up the dwarf spy context 
  structure; typically it requires setting pointers to the Transport/EVPath functions,
  as well as to the dwarf functions to inform the Dwarf runtime about the functions
  that need to be called by the collector or the aggregator side; it is like
  a constructor in C++
  \item  dwarf\_init\_xxx\_spy() -- the initialization of the spy on the collector side;
  typically you can open monitoring data sources; you will not need to have 
  Transport/EVPath setup() function if you implement this function
  \item dwarf\_init\_xxx\_ranger() -- initialization of the spy on the collector side;
  typically you can allocate structures for data aggregation and you 
  need to allocate structures for the memory mapped files 
\end{itemize}

\subsection{Adding Spies to Collectors and an Aggregator}
First add a new name of the spy to src/include/ini\_utils\_types.h that
defines names that the configuration file will recognize.

\begin{verbatim}
...
// src/include/ini\_utils\_types.h
//! names of the spies group
#define INI_DWARF_STR_SPIES_GRP                 "spies"
//! the name for the cpu spy
#define INI_DWARF_STR_SPY_CPU                   "cpu_spy"
//! the name for the net spy
#define INI_DWARF_STR_SPY_NET                   "net_spy"
//! the name for the nvidia_smi spy
#define INI_DWARF_STR_SPY_NVIDIA_SMI            "nvidia_smi_spy"
//! the name for the mem spy
#define INI_DWARF_STR_SPY_MEM                   "mem_spy"
//! the name for the xxx spy
#define INI_DWARF_STR_SPY_XXX                   "xxx_spy"
.....
\end{verbatim}

Next search for a {\tt default\_spies} array and add the new entry in
both {\tt ranger.c} and {\tt patrol.c} providing 
an extern declaration for the init context function if necessary.

\begin{verbatim}
struct dwarf_spy_ident default_spies []= {
            {INI_DWARF_STR_SPY_CPU, dwarf_init_cpu_spy_ctx},
            {INI_DWARF_STR_SPY_MEM, dwarf_init_mem_spy_ctx},
            {INI_DWARF_STR_SPY_NET, dwarf_init_net_spy_ctx},
            {INI_DWARF_STR_SPY_NVIDIA_SMI, dwarf_init_nvid_spy_ctx},
            {INI_DWARF_STR_SPY_XXX, dwarf_init_xxx_spy_ctx},
            {NULL, NULL}
    };
\end{verbatim}

\subsection{Modifying Build System}
Add the xxx spy torso to src/common/SConstruct:

\begin{verbatim}
env.SharedLibrary('libdwarf.so', ['ini_utils.c',  
                                  'misc_utils.c',
                                  'dwarf_rt.c',
                                  'mmap_manager.c',
                                  'spy.c',
                                  '../spies/cpu_spy/cpu_spy_torso.c',
                                  '../spies/mem_spy/mem_spy_torso.c',
                                  '../spies/net_spy/net_spy_torso.c',
                                  '../spies/nvid_spy/nvid_spy_torso.c',
                                  '../spies/xxx_spy/xxx_spy_torso.c])
\end{verbatim}

\subsection{Enabling Persistent Storage}
If the data have to be stored on a persistent storage, the implementation for 
it needs to be provided in src/readers/sqlite3\_reader.py.

\section{Additional Reading}
There is a technical report providing more insights into the ClusterWatch
architecture and some experimental evaluation.

\url{http://www.cercs.gatech.edu/tech-reports/tr2013/git-cercs-13-07.pdf}

\end{document}
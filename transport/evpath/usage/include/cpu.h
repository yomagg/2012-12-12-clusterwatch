/**
 * @file cpu.h
 * @author Naila Farooqui, naila@cc.gatech.edu
 * @date 02-12-2012
 * @brief CPU profile descriptions based on EVPath/FM structures
 *
 * ALL functions return 0 (zero) on success and less than 0 if not successful.
 * 
 * TODO LIST ASSUMPTIONS, LIMITATIONS, RESTRICTIONS HERE.
 */
 
#ifndef _CPU_H
#define _CPU_H

#include <evpath.h>

 /*  ------------------------ CPU PROFILE DEFINITION  ------------------------*/

typedef struct _cpu_profile_t {
    int profilesRead;
    unsigned long cpu_total;
    unsigned long cpu_idle;   
} cpu_profile_t;
 
/*  ------------------------ EVPATH RESOURCE PROFILE DESCRIPTORS ---------------*/
 
/* EVPath "description" of CPU profile */
static FMField CPUProfileFieldList[] = {
    {"profilesRead", "integer", sizeof(int), FMOffset(cpu_profile_t*, profilesRead)},
    {"cpu_total", "unsigned integer", sizeof(unsigned long), FMOffset(cpu_profile_t*, cpu_total)},
     {"cpu_idle", "unsigned integer", sizeof(unsigned long), FMOffset(cpu_profile_t*, cpu_idle)},
    {NULL, NULL, 0, 0}
};

/* FMStructDescRec is a way for us to tell EVPath what kinds of data structures
    it will expect to handle. It is not a means of describing the layout of one
    specific structure. */
static FMStructDescRec CPUProfileRecord[] = {
    {"cpu_profile_t", CPUProfileFieldList, sizeof(cpu_profile_t), NULL},
    {NULL, NULL, 0, NULL}
};

/*  -------------------------- CPU HANDLER FUNCTIONS -------------------------*/

int read_cpu(void *data);
int process_cpu(void *data);

#endif



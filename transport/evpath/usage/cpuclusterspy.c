#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "evpath.h"
#include "clusterspy.h"
#include "cpu.h"

int main(void)
{
    add_probe(CPU, CPUProfileRecord, sizeof(cpu_profile_t), 1, 
        NULL, &read_cpu, &process_cpu);

    watch_cluster(5353);
    
    return 0;
}

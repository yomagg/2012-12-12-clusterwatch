#include <stdio.h>
#include <cpu.h>
#include "clusterspy.h"

int main(int argc, char** argv)
{
    add_probe(CPU, CPUProfileRecord, sizeof(cpu_profile_t), 1, 
        NULL, &read_cpu, &process_cpu);

    if (argc == 1){
    	printf("Usage: %s hostname\n", argv[0]);
    	printf("\thostname the name of the host where cpuclusterspy runs\n");
    	printf("\tE.g.: %s kidlogin1\n", argv[0]);
    	return 0;
    }

    watch_node(argv[1], 5353);

    return 0;
}



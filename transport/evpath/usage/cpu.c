#include <stdio.h>
#include <unistd.h>
#include "cpu.h"

int read_cpu(void *data){

    FILE *proc_stat = NULL;
    char cpu[10];
    unsigned long user, system, nice, idle, iowait, irq, softirq;

    if((proc_stat = fopen("/proc/stat", "r")) != NULL)
    {
        fscanf(proc_stat, "%s\t%lu\t%lu\t%lu\t%lu\t%lu\t%lu\t%lu\n",
            cpu, &user, &nice, &system, &idle, &iowait, &irq, &softirq);

        fclose(proc_stat);
    }
    else {
        fprintf(stderr, "Unable to open /proc/stat!\n");
        return 0;
    }
    
    
    cpu_profile_t *cpuProfile = (cpu_profile_t *)data;
    cpuProfile->cpu_total = (user + system + nice + idle + iowait + irq + softirq);
    cpuProfile->cpu_idle = (idle + iowait);    

    return ++cpuProfile->profilesRead;    
}


int process_cpu(void *data){

    cpu_profile_t *cpuProfile = (cpu_profile_t *)data;
    printf("CPU Utilization: %.2f\n", 
        (((float)(cpuProfile->cpu_total - cpuProfile->cpu_idle))/
            (float)cpuProfile->cpu_total) * 100);

    
    return 0;
}






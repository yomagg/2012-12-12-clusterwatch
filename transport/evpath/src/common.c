/**
 * @file common.c
 * @author Naila Farooqui, naila@cc.gatech.edu
 * @author Magda Slawinska, magg.gatech@gmail.com
 * @date 02-11-2012
 * @brief Common function implementations for clustersink and clustersource
 *
 * ALL functions return 0 (zero) on success and less than 0 if not successful.
 * 
 * TODO LIST ASSUMPTIONS, LIMITATIONS, RESTRICTIONS HERE.
 */

#include <clusterspy.h>
#include <pthread.h>
#include <resources.h>
#include <profiles.h>

/* ------------------------------- INTERNAL STATE ---------------------------*/

//global resources list  
LIST_HEAD(resources);
pthread_mutex_t resourcesLock = PTHREAD_MUTEX_INITIALIZER;

CManager conn_mgr;
EVstone	 stone;

/* -------------------------- PRIVATE FUNCTIONS -----------------------------*/

static void
sigint_handler(int sig)
{
	exit(0);	// Force invocation of the atexit() routine.
}

static int
install_sigint_handler(void)
{
	int err;
	struct sigaction action;
	memset(&action, 0, sizeof(action));
	action.sa_handler = sigint_handler;
	sigemptyset(&action.sa_mask);
	err = sigaction(SIGINT, &action, NULL);
	if (err < 0)
		return -1;
	return 0;
}

void on_exit_handler(void) 
{
    // Register teardown routine to clean up state.
	int err = atexit(clusterspy_teardown);
	if (err < 0) {
		fprintf(stderr, "Could not install atexit routine\n");
	}
	// Enable translation of Ctrl+c to a call to exit() which invokes the
	// teardown, since CMrun_network never returns.
	err = install_sigint_handler();
	if (err < 0) {
		fprintf(stderr, "Could not install SIGINT handler\n");
	}
}

/**
 * The CM shutdown handler
 *
 * @author Magda Slawinska, magg.gatech@gmail.com
 * @param cm
 * @param client_data NULL (the value of this parameter is ignored). Set to NULL.
 * @return
 */
void
cm_shutdown_handler(CManager cm, void *client_data){
	printf("ClusterSpy: Time is up. Quitting ....\n");
	exit(0);
}
/**
 * Just enable the network, since everything is ready
 * This is the last function that should be called since it is blocking.
 *
 * @author Magda Slawinska, magg.gatech@gmail.com
 * @param mesg if NULL then the message will be not displayed
 *             otherwise the message will be displayed
 * @return 1 everything is ok (probably never happen because this is a blocking
 * call.
 */
int run_watcher(char *mesg){
	if( mesg )
		printf("%s", mesg);
    CMrun_network(conn_mgr);

    return 1;
}
/* -------------------------- PUBLIC FUNCTIONS -----------------------------*/

void clusterspy_teardown(void)
{

    ResourceNode *resourceNode, *tmpNode;

    pthread_mutex_lock(&resourcesLock);

    list_for_each_entry_safe(resourceNode, tmpNode, &resources, link) 
    {
        list_del(&resourceNode->link);
		free(resourceNode);
    }

    pthread_mutex_unlock(&resourcesLock);

   EVfree_stone(conn_mgr, stone);
   CManager_close(conn_mgr);
}

/**
* Adds monitoring/spying probe for a particular resource
*/
void add_probe(ResourceType type, FMStructDescList format, int size, int period, 
    void (*setup)(void), int (*read)(void *data), int (*process)(void *data))
{

    ResourceNode *newNode = malloc(sizeof(*newNode));
    if(newNode == NULL)
    {
	    fprintf(stderr, "Unable to malloc resource node!\n");
	    exit(-1);
	}
    
    Resource resource;

    resource.type = type;
    resource.format = format;
    resource.size = size;
    resource.period = period;
    resource.setup = setup;
    resource.read = read;
    resource.process = process;
  
    pthread_mutex_lock(&resourcesLock);

	newNode->resource = resource;
	list_add(&newNode->link, &resources);

    pthread_mutex_unlock(&resourcesLock);
}

/**
* Adds monitoring/spying probes (implemented by default) for multiple resources
*/
void add_probes(int types)
{
    if(types & GPU)
    {   
        add_probe(GPU, GPUProfileRecord, sizeof(gpu_profile_t), 1, 
            &setup_gpu, &read_gpu, &process_gpu);
    }
    if(types & CPU)
    {
        add_probe(CPU, CPUProfileRecord, sizeof(cpu_profile_t), 1, 
            NULL, &read_cpu, &process_cpu);
    }

}




/**
 * @file node.c
 * @author Naila Farooqui, naila@cc.gatech.edu
 * @author Magda Slawinska, magg.gatech@gmail.com
 * @date 02-11-2012
 * @brief Spying node implementation for cluster spy (collects monitoring 
        data on each node)
 *
 * ALL functions return 0 (zero) on success and less than 0 if not successful.
 * 
 * TODO LIST ASSUMPTIONS, LIMITATIONS, RESTRICTIONS HERE.
 */

#include <clusterspy.h>
#include <resources.h>
#include <profiles.h>

extern struct list_head resources;
extern pthread_mutex_t resourcesLock;

extern CManager conn_mgr;
extern EVstone  stone;

extern void on_exit_handler(void);
extern void cm_shutdown_handler(CManager cm, void *client_data);

/*-------------------------------------- PRIVATE FUNCTIONS -------------------------*/

/**
* EVPath specific polling function to send data periodically -- before sending via EVPath, 
* calls either client-specified collect function or default collect function to collect data 
* 
*/
void polling_handler(CManager conn_mgr, void *client_data)
{
    Resource *resource = (Resource *)client_data;

    resource->data = malloc(resource->size);
    if(resource->data == NULL)
    {
        fprintf(stderr, "Unable to malloc resource data!\n");
	    exit(-1);
    }

    if(resource->read && resource->read(resource->data) > 0)
    {
        EVsubmit(resource->source, resource->data, NULL);
    }

    free(resource->data);
}


/*-------------------------------------- PUBLIC FUNCTIONS -------------------------*/

/**
* Starts spying on each node -- collects monitoring data for all resources on this node
* 
*/
void watch_node(char *host, int port)
{
    attr_list contactList;

    conn_mgr = CManager_create();
    CMlisten(conn_mgr);

    stone = EValloc_stone(conn_mgr);

    contactList = create_attr_list();
    add_int_attr(contactList, attr_atom_from_string("IP_PORT"), port);
    add_string_attr(contactList, attr_atom_from_string("IP_HOST"), host);
	
    EVaction action;
	
    /* loop until connection established -- 
        assume only one stone exists at sink; hence, we pass in 0 for the stone ID of the
        remote address space
    */
    
    while ((action = EVassoc_bridge_action(conn_mgr, stone, contactList, 0)) != 0){
	fprintf(stdout, "Connecting with cluster spy, action = %d\n", action);
	sleep(1);
    }
    printf("Connected with cluster spy\n");	

    ResourceNode *resourceNode;

    pthread_mutex_lock(&resourcesLock);

    list_for_each_entry(resourceNode, &resources, link) 
    {
	    if(resourceNode->resource.setup != NULL)
	        resourceNode->resource.setup();

        // register data format with source
        resourceNode->resource.source = 
            EVcreate_submit_handle(conn_mgr, stone, resourceNode->resource.format);

        CMadd_periodic_task(conn_mgr, resourceNode->resource.period, 0, 
            polling_handler, &(resourceNode->resource));
    }

    pthread_mutex_unlock(&resourcesLock);

    on_exit_handler();

    printf("running node spy ...\n");
    CMrun_network(conn_mgr);
}

/**
*
*/
/**
 * Starts spying on each node -- collects monitoring data for all resources on this node
 * @author Magda Slawinska, magg.gatech@gmail.com
 *
 * @param f_get_host The callback to the function that re-reads the hostname
 * @param f_get_port the callback to the function that re-reads the port
 * @param exectime_sec execution time in sec (if <= 0 then forever will run)
 * @return 1 everything went great, you can proceed to something else
 *         -1 wrong port
 */
int arrange_node(char * (*f_get_host)(void), int (*f_get_port)(void), int exectime_sec){
	const int OK = 1;
	int diag = OK;
    attr_list contactList;
    int   port = -1;
    char *host = NULL;

    conn_mgr = CManager_create();
    CMlisten(conn_mgr);

    stone = EValloc_stone(conn_mgr);

    contactList = create_attr_list();
    port = f_get_port();
    if ( (host = f_get_host()) == NULL ){
    	fprintf(stdout, "ERROR: issues with getting port\n");
    }

    add_int_attr(contactList, attr_atom_from_string("IP_PORT"), port);
    add_string_attr(contactList, attr_atom_from_string("IP_HOST"), host);

    EVaction action = -1;

    /* loop until connection established --
        assume only one stone exists at sink; hence, we pass in 0 for the stone ID of the
        remote address space
    */

    while ((action = EVassoc_bridge_action(conn_mgr, stone, contactList, 0)) != 0){
    	fprintf(stdout, "EVassoc_bridge_action: UNSUCCESSFUL (action = %d)\n", action);
    	sleep(1);
    	// it might be because the changes in files didn't propagated yet
    	// so try to re-read the inifile
    	// get data from the ini file such as hostname, port, and others
    	port = f_get_port();

    	// prepare to get a new host (f_get_host() allocates memory with strdup)
    	free(host);
    	host = NULL;
    	if ( (host = f_get_host()) == NULL ){
    		fprintf(stdout, "ERROR: get_host()\n");
    	}

    	fprintf(stdout, "Trying with ip_host:ip_port=%s:%d ...", host, port);

    	// I only set the port attribute, I do not set IP_HOST e.g.
    	// add_string_attr(contact_list, attr_atom_from_string("IP_HOST"), pCtx->ini_file.trooper_hostname);
    	// or set_string_attr(blah, blah) because, the contact list has the address
    	// of the hostname buffer and I above I change any the value of the
    	// content of the buffer with strcpy(). I assume it is enough
    	// to have the updated IP_HOST contact_list attribute
    	set_int_attr(contactList, attr_atom_from_string("IP_PORT"), port);
    	set_string_attr(contactList, attr_atom_from_string("IP_HOST"), host);
    }
    fprintf(stdout, "EVassoc_bridge_action: SUCCESSFUL (action=%d). Connected with cluster spy\n", action);

    ResourceNode *resourceNode;

    pthread_mutex_lock(&resourcesLock);

    list_for_each_entry(resourceNode, &resources, link)
    {
	    if(resourceNode->resource.setup != NULL)
	        resourceNode->resource.setup();

        // register data format with source
        resourceNode->resource.source =
            EVcreate_submit_handle(conn_mgr, stone, resourceNode->resource.format);

        CMadd_periodic_task(conn_mgr, resourceNode->resource.period, 0,
            polling_handler, &(resourceNode->resource));
    }

    // install the shutdown handler
    if (exectime_sec > 0 ){
    	printf("Installing CM shutdown handler ...");
    	CMadd_periodic_task(conn_mgr, exectime_sec /* period */,
    			0 /*microsec*/,
    			cm_shutdown_handler, NULL);
    	printf(" ... INSTALLED.\n");
    }
    pthread_mutex_unlock(&resourcesLock);

    return diag;
}

/**
 * @file cluster.c
 * @author Naila Farooqui, naila@cc.gatech.edu
 * @author Magda Slawinska, magg.gatech@gmail.com
 * @date 02-11-2012
 * @brief Spying cluster implementation for clusterspy
 *
 * ALL functions return 0 (zero) on success and less than 0 if not successful.
 * 
 * TODO LIST ASSUMPTIONS, LIMITATIONS, RESTRICTIONS HERE.
 */


#include <clusterspy.h>
#include <resources.h>

extern struct list_head resources;
extern pthread_mutex_t resourcesLock;

extern CManager conn_mgr;
extern EVstone  stone;

extern void on_exit_handler(void);
extern void cm_shutdown_handler(CManager cm, void *client_data);

/*-------------------------------------- PRIVATE FUNCTIONS -------------------------*/

/**
* EVPath specific handler function for receiving resource data -- calls client-specified
* process handler function or default process handler function to process the data 
* 
 */
int receive_handler(CManager connectionMgr, void *message, 
        void *client_data, attr_list attrs)
{
    Resource *resource = (Resource *)client_data;
    int err = 0;
    
    if(resource->process != NULL)
       err = resource->process(message);
    
    return err;
}

/*-------------------------------------- PUBLIC FUNCTIONS -------------------------*/
/**
* Starts spying on cluster -- aggregates/processes data collected from 
    all spying nodes for specified resources
*/
void watch_cluster(int port)
{
    attr_list attributes = create_attr_list();
    conn_mgr = CManager_create();

    add_int_attr(attributes, attr_atom_from_string("IP_PORT"), port);
   
    CMlisten_specific(conn_mgr, attributes);
	
    stone = EValloc_stone(conn_mgr);
    
    ResourceNode *resourceNode;

    pthread_mutex_lock(&resourcesLock);

    list_for_each_entry(resourceNode, &resources, link) 
    {
        EVassoc_terminal_action(conn_mgr, stone, resourceNode->resource.format, 
            receive_handler, &(resourceNode->resource));
    }

    pthread_mutex_unlock(&resourcesLock);
    
    on_exit_handler();
    
    printf("running cluster spy...\n");
    CMrun_network(conn_mgr);
}



/**
 * Starts spying on cluster -- aggregates/processes data collected from
 * all spying nodes for specified resources.
 *
 * This is the modification of the watch_cluster with adding a subsequent
 * port.
 *
 * 1. the port is selected by EVPath automatically and reported to the out
 *    side world by setting the environment variable to the port value
 * 2. an automatic shutdown handler is added
 * @author Magda Slawinska
 * @param p_port (OUT) the port the ranger is listening on
 * @param exectime_sec (IN) how long the cluster watch is supposed to run in seconds
 *        if <= 0 then the CM shutdown will be not installed
 * @return 1 - execution fine
 *         -1 - errors in execution
 */
int arrange_cluster(int exectime_sec, int *p_port){
	const int OK = 1;
	const int NOT_OK = -1;
	int ret = OK;
	int transport_num = 0;  // diagnostic variable

    conn_mgr = CManager_create();

    // it is possible to use CMlisten_specific(*pCm, contact_list)
    // however, there might be an issue with the port, since
    // it is you who provides the port number; this might be an issue
    // since it may happen the subsequent invocation of the
    // CMlisten may result in an attempt to reuse the port that is
    // currently used, CMlisten selects the free port for you
    transport_num = CMlisten(conn_mgr);
    if (1 != transport_num ){
    	printf("CLUSTERSPY: ERROR: Transport number returned by CMlisten different than 1: %d\n",
    			transport_num);
    	return NOT_OK;
    }
    attr_list cl;
    cl = CMget_contact_list(conn_mgr);
    get_int_attr(cl, attr_atom_from_string("IP_PORT"), p_port );

    stone = EValloc_stone(conn_mgr);

    ResourceNode *resourceNode;

    pthread_mutex_lock(&resourcesLock);

    list_for_each_entry(resourceNode, &resources, link) {
    	EVassoc_terminal_action(conn_mgr, stone, resourceNode->resource.format,
    			receive_handler, &(resourceNode->resource));
    }

    // add the shutdown event
    if(  exectime_sec > 0  ){
    	printf("Installing CM shutdown handler ...");
    	CMadd_periodic_task(conn_mgr, exectime_sec /* period */,
    			0 /*microsec*/,
    			cm_shutdown_handler, NULL);
    	printf("INSTALLED\n");
    }

    pthread_mutex_unlock(&resourcesLock);

    return OK;
}

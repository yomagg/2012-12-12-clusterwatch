/**
 * @file resources.c
 * @author Naila Farooqui, naila@cc.gatech.edu
 * @date 02-14-2012
 * @brief Handler functions for node resources, such as GPU, CPU, etc
 *
 * ALL functions return 0 (zero) on success and less than 0 if not successful.
 * 
 * TODO LIST ASSUMPTIONS, LIMITATIONS, RESTRICTIONS HERE.
 */

#include <resources.h>
#include <profiles.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include <unistd.h> //gethostname

#define MAX_SIZE    100

#define BUF_LEN     256

/* ------------------------------- INTERNAL STATE ---------------------------*/

typedef struct _gpu_profile_node_t {
	struct list_head link;
	gpu_profile_t profile;
} gpu_profile_node_t;

typedef struct _kernel_attributes_t {
    int kernelsLaunched;
    double totalKernelRuntime;
} kernel_attributes_t;

static int gpuProfileCount = 0;
static int cpuProfileCount = 0;

typedef struct _cpu_profile_node_t {
	struct list_head link;
	cpu_profile_t profile;
} cpu_profile_node_t;

//global gpu and cpu profile lists  
static LIST_HEAD(gpuProfileList);
static pthread_mutex_t gpuProfileListLock = PTHREAD_MUTEX_INITIALIZER;

static LIST_HEAD(cpuProfileList);
static pthread_mutex_t cpuProfileListLock = PTHREAD_MUTEX_INITIALIZER;

/* ---------------------------- GPU HANDLER FUNCTIONS ---------------------------*/
void setup_gpu(void) 
{
    int err = 0;
    struct mq_attr qattr;
    qattr.mq_maxmsg = MQ_MAX_MESSAGES;
    qattr.mq_msgsize = MQ_MAX_MSG_SIZE;

    retry_open:
		lynxMsgQueue = mq_open(MSG_QUEUE, MQ_OPEN_OWNER_FLAGS, 0666, &qattr);
		if(lynxMsgQueue < 0) {
			if (errno == EEXIST) {
				err = mq_unlink(MSG_QUEUE);
				if (err < 0) {
					fprintf(stderr, "mq_unlink failed\n");
				}
				goto retry_open;
			} else {
				fprintf(stderr, "mq_open failed\n");
			}
		}

}

int read_gpu(void *data)
{
    gpu_profile_t *gpuProfile = (gpu_profile_t *)data;
    
    gethostname(gpuProfile->hostname, HOST_NAME_MAX - 1); 
    gpuProfile->hostname[HOST_NAME_MAX-1] = '\0';

    int err = 0;
	gpuProfile->profilesRead = 0;
	kernel_profile *kernelProfile;

	kernelProfile = &gpuProfile->profiles[0];
   
	while((err = mq_receive(lynxMsgQueue, (char *)kernelProfile, MQ_MAX_MSG_SIZE, NULL)) > 0)
	{
		gpuProfile->profilesRead++;
		kernelProfile++;

		if (gpuProfile->profilesRead >= MQ_MAX_MESSAGES)
			break; // can only read so many
	}    
	if (err < 0 && errno != EAGAIN) { // EAGAIN means empty, if MQ was created with O_NONBLOCK
		// die with msg
		fprintf(stderr, "mq_recv returned non-recoverable error: %s\n", strerror(errno));
        exit(-1);
	}

    return gpuProfile->profilesRead;
}

int process_gpu(void *data)
{
    gpu_profile_t *gpuProfile = (gpu_profile_t *)data;

    struct timeval timestamp;
    gettimeofday(&timestamp, NULL);
    gpuProfile->timestamp = timestamp.tv_sec + (timestamp.tv_usec/1000000.0);
    
    if(gpuProfile->profilesRead > 0)
	{
		gpu_profile_node_t *newNode = malloc(sizeof(*newNode));
		if(newNode == NULL) 
        {
			fprintf(stderr, "Unable to malloc gpuProfile node!\n");
			exit(-1);
		}

        pthread_mutex_lock(&gpuProfileListLock);

		if(gpuProfileCount >= MAX_SIZE) {
			gpu_profile_node_t *gpuProfileNode = 
				list_first_entry(&gpuProfileList, gpu_profile_node_t, link);
			list_del(&gpuProfileNode->link);
			free(gpuProfileNode);
			gpuProfileNode = NULL;

			gpuProfileCount--;
		}
		
		newNode->profile = *gpuProfile;
		list_add(&newNode->link, &gpuProfileList);

		gpuProfileCount++;

        pthread_mutex_unlock(&gpuProfileListLock);

		gpuProfile->profilesRead = 0;
	}

    return 0;
}


/* ---------------------------- CPU HANDLER FUNCTIONS ---------------------------*/

int read_cpu(void *data){

    FILE *proc_stat = NULL;
    char cpu[10];
    unsigned long user, system, nice, idle, iowait, irq, softirq;

    cpu_profile_t *cpuProfile = (cpu_profile_t *)data;
    
    gethostname(cpuProfile->hostname, HOST_NAME_MAX - 1); 
    cpuProfile->hostname[HOST_NAME_MAX-1] = '\0';

    if((proc_stat = fopen("/proc/stat", "r")) != NULL)
    {
        char buffer[BUF_LEN];
       
        //read the first line, which contains total CPU data
        if(fgets(buffer, BUF_LEN, proc_stat) != NULL)
        {    

            sscanf(buffer, "%s\t%lu\t%lu\t%lu\t%lu\t%lu\t%lu\t%lu\n",
                    cpu, &user, &nice, &system, &idle, &iowait, &irq, &softirq);

            cpuProfile->cpu_total = 
                    (user + system + nice + idle + iowait + irq + softirq);
                cpuProfile->cpu_idle = (idle + iowait);  
       
            //now continue reading data for the individual CPU cores
            int i = 0;
            do {
                if(fgets(buffer, BUF_LEN, proc_stat) == NULL)
                    break;

                sscanf(buffer, "%s\t%lu\t%lu\t%lu\t%lu\t%lu\t%lu\t%lu\n",
                    cpu, &user, &nice, &system, &idle, &iowait, &irq, &softirq);

                cpuProfile->cores[i].id = i;
                cpuProfile->cores[i].total = 
                    (user + system + nice + idle + iowait + irq + softirq);
                cpuProfile->cores[i].idle = (idle + iowait);  

                i++;

            } while(strstr(buffer, "cpu") != NULL);

            cpuProfile->num_cores = i;
        }//unable to read first line of /proc/stat

        fclose(proc_stat);
        cpuProfile->profilesRead++;

    } //unable to open /proc/stat
    else {
        fprintf(stderr, "Unable to open /proc/stat!\n");
        return 0;
    } 

    return cpuProfile->profilesRead;    
}


int process_cpu(void *data){

    cpu_profile_t *cpuProfile = (cpu_profile_t *)data;

    struct timeval timestamp;
    gettimeofday(&timestamp, NULL);
    cpuProfile->timestamp = timestamp.tv_sec + (timestamp.tv_usec/1000000.0);
    
    if(cpuProfile->profilesRead > 0)
	{
		cpu_profile_node_t *newNode = malloc(sizeof(*newNode));
		if(newNode == NULL) 
        {
			fprintf(stderr, "Unable to malloc cpuProfile node!\n");
			exit(-1);
		}

        pthread_mutex_lock(&cpuProfileListLock);

		if(gpuProfileCount >= MAX_SIZE) {
			cpu_profile_node_t *cpuProfileNode = 
				list_first_entry(&cpuProfileList, cpu_profile_node_t, link);
			list_del(&cpuProfileNode->link);
			free(cpuProfileNode);
			cpuProfileNode = NULL;

			cpuProfileCount--;
		}

		newNode->profile = *cpuProfile;
		list_add(&newNode->link, &cpuProfileList);

		cpuProfileCount++;

        pthread_mutex_unlock(&cpuProfileListLock);

		cpuProfile->profilesRead = 0;
	}

    return 0;
}

/* ---------------------------- GPU UTILITY FUNCTIONS ---------------------------*/
/**
* Utility function to aggregate GPU kernel data
*
**/
static kernel_attributes_t kernel_attributes(char *hostname, int device, int interval)
{
    kernel_attributes_t attr;
    struct timeval timestamp;
    
    gettimeofday(&timestamp, NULL);
    double intervalStart = (timestamp.tv_sec + (timestamp.tv_usec/1000000.0)) - interval;

    attr.kernelsLaunched = 0;
    attr.totalKernelRuntime = 0;

    gpu_profile_node_t *gpuProfileNode;

    pthread_mutex_lock(&gpuProfileListLock);

    list_for_each_entry(gpuProfileNode, &gpuProfileList, link) 
    {
	    if(gpuProfileNode->profile.timestamp >= intervalStart && 
            strcmp(gpuProfileNode->profile.hostname, hostname) == 0) 
        {
            int i = 0;		    
            for(i = 0; i < gpuProfileNode->profile.profilesRead; i++) 
            {
		
			    if(gpuProfileNode->profile.profiles[i].device == device) 
                {
				    attr.kernelsLaunched++;
				
				    if(gpuProfileNode->profile.profiles[i].type == KERNEL_RUNTIME)
					    attr.totalKernelRuntime += 
						    gpuProfileNode->profile.profiles[i].data.kernel_runtime;

			    }
            }
	    }
    }	

    pthread_mutex_unlock(&gpuProfileListLock);    

    return attr;
}

/* ---------------------------- GPU PUBLIC API -------------------------------*/

int gpu_kernel_launch_rate(char *hostname, int device, int interval) 
{
    return kernel_attributes(hostname, device, interval).kernelsLaunched;
}

double gpu_total_kernel_runtime(char *hostname, int device, int interval) 
{
    return kernel_attributes(hostname, device, interval).totalKernelRuntime;
}

double gpu_avg_kernel_runtime(char *hostname, int device, int interval) 
{
    kernel_attributes_t attr = kernel_attributes(hostname, device, interval);
	if(attr.kernelsLaunched == 0)
		return 0;

	return (attr.totalKernelRuntime/attr.kernelsLaunched);
}

double gpu_util(char *hostname, int device, int interval)
{
    if(interval <= 0)
        return 0.0;

    kernel_attributes_t attr = kernel_attributes(hostname, device, interval);
    
    //note totalKernelRuntime is in ms so we need to convert to seconds
    return ((attr.totalKernelRuntime/1000)/(double)interval) * 100;
}

/* ---------------------------- CPU PUBLIC API -------------------------------*/

/* if -1 is passed in for the core, the aggregate CPU utilization is obtained */
double cpu_util(char *hostname, int interval, int core)
{
    struct timeval timestamp;
    
    if(interval <= 0)
        return 0.0;

    gettimeofday(&timestamp, NULL);
    double intervalStart = (timestamp.tv_sec + (timestamp.tv_usec/1000000.0)) 
        - interval;

    cpu_profile_node_t *startNode = NULL, *endNode = NULL;

    pthread_mutex_lock(&cpuProfileListLock);

    //find the latest cpu profile record for the node identified by hostname
    list_for_each_entry(endNode, &cpuProfileList, link)
    {
        if(strcmp(endNode->profile.hostname, hostname) == 0)
            break;
    }

    pthread_mutex_unlock(&cpuProfileListLock);

    //if not found, that means we don't have info for this node
    if(!endNode){
        return 0.0;
    }

    pthread_mutex_lock(&cpuProfileListLock);

    //now find the earliest cpu profile record in this interval
    list_for_each_entry(startNode, &cpuProfileList, link) 
    {    
	    if(strcmp(startNode->profile.hostname, hostname) == 0 && 
            startNode->profile.timestamp < intervalStart) 
        {
            break;
	    }
    }	

    pthread_mutex_unlock(&cpuProfileListLock);    

    unsigned long total_diff = 0;
    unsigned long idle_diff = 0;

    /* get aggregate CPU utilization */
    if(core < 0){
        if(!startNode){
            total_diff = endNode->profile.cpu_total;
            idle_diff = endNode->profile.cpu_idle;
        }
        else {
            total_diff = endNode->profile.cpu_total - startNode->profile.cpu_total;
            idle_diff =  endNode->profile.cpu_idle - startNode->profile.cpu_idle;
        }
    }
    /* get per-core CPU utilization */
    else {
        /* check for invalid core ... */
        if((unsigned int)core >= endNode->profile.num_cores)
            return 0.0;

        if(!startNode){
            total_diff = endNode->profile.cores[core].total;
            idle_diff = endNode->profile.cores[core].idle;
        }
        else {
            total_diff = endNode->profile.cores[core].total - startNode->profile.cores[core].total;
            idle_diff =  endNode->profile.cores[core].idle - startNode->profile.cores[core].idle;
        }
    }

    if(total_diff == 0)
        return 0.0;

    return (((double)(total_diff - idle_diff))/((double)total_diff)) * 100;
}





#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "evpath.h"
#include "clusterspy.h"

struct workerThread {
	bool isAlive;
	pthread_t tid;
	int exitCode;
};

static struct workerThread *spyingThread = NULL;

static void *work(void *arg) {
    watch_cluster(5353);
    pthread_exit(NULL);
}

int main(void)
{
    int err;

    add_probes(GPU | CPU);

    spyingThread = calloc(1, sizeof(*spyingThread));
    if(!spyingThread) 
        return -1;    

    //spawn a thread that starts spying master
	err = pthread_create(&spyingThread->tid, NULL, work, spyingThread);
	if(err < 0)
		return -1;

    int i = 0;
    for(i = 0; i < 10; i++) {
        sleep(5);
        printf("CPU utilization: %.1f\n", cpu_util("naila-pc", 5, -1));
    }

    pthread_cancel(spyingThread->tid);
	pthread_join(spyingThread->tid, NULL);
	if (spyingThread)
		free(spyingThread);

}

/**
 * @file resources.h
 * @author Naila Farooqui, naila@cc.gatech.edu
 * @date 02-14-2012
 * @brief Handlers APIs -- functions for collecting/processing resource-specific data
 *
 * ALL functions return 0 (zero) on success and less than 0 if not successful.
 * 
 * TODO LIST ASSUMPTIONS, LIMITATIONS, RESTRICTIONS HERE.
 */

#ifndef _RESOURCES_H
#define _RESOURCES_H

#include <stdio.h>
#include <errno.h>
#include <mqueue.h>
#include <stdlib.h>
#include <string.h>
#include <list.h>

/* ------------------------- INTERNAL DEFINITIONS/STATE ---------------------*/
/**
 * Message queue file descriptor used to pull out CUDA PTX kernel
 * instrumentation data from Lynx.
 */
mqd_t lynxMsgQueue;

/* ------------------------------- INTERNAL FUNCTIONS ---------------------*/


void setup_gpu(void);

int read_gpu(void *data);
int process_gpu(void *data);

int read_cpu(void *data);
int process_cpu(void *data);

#endif

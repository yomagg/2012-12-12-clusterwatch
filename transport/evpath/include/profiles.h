/**
 * @file profiles.h
 * @author Naila Farooqui, naila@cc.gatech.edu
 * @date 02-12-2012
 * @brief Resource profile descriptions based on EVPath/FM structures -- used by EVPath to 
 *      marshal/unmarshal the data on both sides (sink and source)
 *
 * ALL functions return 0 (zero) on success and less than 0 if not successful.
 * 
 * TODO LIST ASSUMPTIONS, LIMITATIONS, RESTRICTIONS HERE.
 */

#ifndef _PROFILES_H
#define _PROFILES_H
 
#include <unistd.h> // gethostname()
#include <limits.h> // HOST_NAME_MAX
#include <evpath.h>
#include <kernel_profile.h>

#define MAX_CORES 32

 /*  ------------------------ RESOURCE PROFILE DEFINITIONS  -----------------------------------*/

typedef struct _gpu_profile_t {

    double timestamp;
    kernel_profile profiles[MQ_MAX_MESSAGES];
	int profilesRead;
	char hostname[HOST_NAME_MAX];

} gpu_profile_t;

typedef struct _cpu_core_t {

    unsigned long id;
    unsigned long total;
    unsigned long idle;   

} cpu_core_t;

typedef struct _cpu_profile_t {

    double timestamp;
    unsigned long cpu_total;
    unsigned long cpu_idle;
    unsigned long num_cores;
    cpu_core_t cores[MAX_CORES];
	int profilesRead;
	char hostname[HOST_NAME_MAX];

} cpu_profile_t;
 
/*  ------------------ EVPATH RESOURCE PROFILE DESCRIPTORS --------------------*/

/* -------------------------------- GPU descriptions --------------------------*/
 
/* EVPath "description" of Lynx's kernel_profile structure */
static FMField KernelProfileFieldList[] = {
	{ "pid",	"integer",	sizeof(int), FMOffset(kernel_profile*, pid)},
	{ "device",	"integer",	sizeof(int), FMOffset(kernel_profile*, device)},
	{ "type",	"unsigned integer",	sizeof(unsigned int), FMOffset(kernel_profile*, type)},
	{ "data",	"integer[2]",	sizeof(int), FMOffset(kernel_profile*, data)},
	{ "name",	"char[256]",	sizeof(char), FMOffset(kernel_profile*, name)},
    {NULL, NULL, 0, 0}
};	

/* EVPath "description" of GPU profile */
static FMField GPUProfileFieldList[] = {
    {"timestamp", "integer[2]", sizeof(int), FMOffset(gpu_profile_t*, timestamp)},
	{"profiles", "kernel_profile[8]", sizeof(kernel_profile), FMOffset(gpu_profile_t*, profiles)},
	{"profilesRead", "integer", sizeof(int), FMOffset(gpu_profile_t*, profilesRead)},
	{"hostname", "char[64]", sizeof(char), FMOffset(gpu_profile_t*, hostname)},
    {NULL, NULL, 0, 0}
};

/* FMStructDescRec is a way for us to tell EVPath what kinds of data structures
    it will expect to handle. It is not a means of describing the layout of one
    specific structure. */
static FMStructDescRec GPUProfileRecord[] = {
    {"gpu_profile_t", GPUProfileFieldList, sizeof(gpu_profile_t), NULL},
    {"kernel_profile", KernelProfileFieldList, sizeof(kernel_profile), NULL},
    {NULL, NULL, 0, NULL}
};

/* -------------------------------- CPU descriptions --------------------------*/
 
/* EVPath "description" CPU core structure */
static FMField CPUCoreFieldList[] = {
	{ "id",	    "unsigned integer",	sizeof(unsigned int), FMOffset(cpu_core_t*, id)},
	{ "total",	"unsigned integer",	sizeof(unsigned int), FMOffset(cpu_core_t*, total)},
	{ "idle",	"unsigned integer",	sizeof(unsigned int), FMOffset(cpu_core_t*, idle)},
    {NULL, NULL, 0, 0}
};	

/* EVPath "description" of CPU profile */
static FMField CPUProfileFieldList[] = {
    {"timestamp",   "integer[2]", sizeof(int), FMOffset(cpu_profile_t*, timestamp)},
    {"num_cores",   "unsigned integer",	sizeof(unsigned int), FMOffset(cpu_profile_t*, num_cores)},
	{"total",	    "unsigned integer",	sizeof(unsigned int), FMOffset(cpu_profile_t*, cpu_total)},
	{"idle",	    "unsigned integer",	sizeof(unsigned int), FMOffset(cpu_profile_t*, cpu_idle)},
	{"cores", "cpu_core_t[32]", sizeof(cpu_core_t), FMOffset(cpu_profile_t*, cores)},
	{"profilesRead", "integer", sizeof(int), FMOffset(cpu_profile_t*, profilesRead)},
	{"hostname", "char[64]", sizeof(char), FMOffset(cpu_profile_t*, hostname)},
    {NULL, NULL, 0, 0}
};

/* FMStructDescRec is a way for us to tell EVPath what kinds of data structures
    it will expect to handle. It is not a means of describing the layout of one
    specific structure. */
static FMStructDescRec CPUProfileRecord[] = {
    {"cpu_profile_t", CPUProfileFieldList, sizeof(cpu_profile_t), NULL},
    {"cpu_core_t", CPUCoreFieldList, sizeof(cpu_core_t), NULL},
    {NULL, NULL, 0, NULL}
};

#endif

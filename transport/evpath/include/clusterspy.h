/**
 * @file clusterspy.h
 * @author Naila Farooqui, naila@cc.gatech.edu
 * @date 02-12-2012
 * @brief Cluster spy interface -- APIs for both cluster sink and cluster source
 *
 * ALL functions return 0 (zero) on success and less than 0 if not successful.
 * 
 * TODO LIST ASSUMPTIONS, LIMITATIONS, RESTRICTIONS HERE.
 */

#ifndef _CLUSTERSPY_H
#define _CLUSTERSPY_H

#include <unistd.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <pthread.h>
#include <list.h>
#include "evpath.h"

/*-------------------------------------- DEFINITIONS --------------------------------*/

typedef enum _ResourceType { 
    GPU = 1, 
    CPU = 2, 
    MEMORY = 4, 
    NETWORK = 8,
} ResourceType;

typedef struct _Resource {

    ResourceType type;

    EVsource source;
    FMStructDescList format;
    int period; //! interval at which to poll system (in ms)
    
    void *data;
    size_t size;
    
    int (*read)(void *data);
    int (*process)(void *data);
    void (*setup)(void);
    
} Resource;

typedef struct _ResourceNode {
	struct list_head link;
	Resource resource;
} ResourceNode;

/*----------------------------- PUBLIC INTERFACE ----------------------------------*/

/**
* Adds monitoring/spying probe for a particular resource
*/
void add_probe(ResourceType type, FMStructDescList format, int size, int period, 
    void (*setup)(void), int (*read)(void *data), int (*process)(void *data));

/**
* Adds monitoring/spying probes (implemented by default) for multiple resources
*/
void add_probes(int types);

/**
* Starts spying cluster -- aggregates/processes data collected from all spying nodes for
*   all resources
*/
void watch_cluster(int port);

/**
* Starts spying on each node -- collects monitoring data for all resources on this node
* 
*/
void watch_node(char *host, int port);

/**
* Clean up all state
* 
*/
void clusterspy_teardown(void);

/*--------------------------- UTILITY FUNCTIONS ----------------------------------------*/

/**
 * Obtains the average kernel runtime (in ms) for the specified node (host) and device,
 * in the last <interval> seconds.
 *
 */
double gpu_avg_kernel_runtime(char *hostname, int device, int interval);

/**
 * Obtains the total kernel runtime (in ms) for the specified node (host) and device, in the last
 * <interval> seconds.
 *
 */
double gpu_total_kernel_runtime(char *hostname, int device, int interval);

/**
 * Obtains the total number of kernels launched for the specified node (host) and device,
 * in the last <interval> seconds.
 *
 */
int gpu_kernel_launch_rate(char *hostname, int device, int interval);

/**
 * Obtains the GPU utilization (as a %) for the specified node (host) and device,
 * in the last <interval> seconds.
 *
 */
double gpu_util(char *hostname, int device, int interval);

/**
 * Obtains the CPU utilization (as a %) for the specified node (host) and CPU core,
 * in the last <interval> seconds. If core is specified as -1, aggregate CPU utilization
 * is returned.
 *
 */
double cpu_util(char *hostname, int interval, int core);

#endif
